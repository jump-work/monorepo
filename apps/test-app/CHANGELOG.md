# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [0.3.0](https://gitlab.com/jump-work/monorepo/compare/@jump-work/test-app@0.2.0...@jump-work/test-app@0.3.0) (2021-10-17)


### Features

* resolve DEV-2682 testing 2 ([dd412cc](https://gitlab.com/jump-work/monorepo/commit/dd412cc9044c87ea2e00fcef08595fd2ce30ed3d))





# 0.2.0 (2021-09-28)


### Features

* resolve DEV-2682 testing ([0c9cb72](https://gitlab.com/jump-work/monorepo/commit/0c9cb72e34c4a7112e0532a654804dd69a5ac476))
