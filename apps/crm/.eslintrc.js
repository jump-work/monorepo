const isProd = process.env.CI === 'true';

module.exports = {
    extends: [
        'react-app',
        'eslint:recommended',
        'plugin:react/recommended',
        'plugin:import/recommended'
    ],
    plugins: [
        'react'
    ],
    parserOptions: {
        ecmaVersion: 6,
        sourceType: 'module',
        ecmaFeatures: {
            jsx: true
        }
    },
    rules: {
        'react/jsx-uses-react': 'error',
        'react/jsx-uses-vars': isProd ? 'error' : 'warn',
        'react/display-name': 'off',
        'react/no-unescaped-entities': 'off',
        'react/prop-types': 'warn',
        'no-empty': [
            'error',
            {
                'allowEmptyCatch': true
            }
        ],
        'quotes': [
            2,
            'single'
        ],
        'no-unused-vars': [
            isProd ? 'error' : 'warn',
            {
                'args': 'after-used',
                'argsIgnorePattern': '^_',
                'caughtErrors': 'none'
            }
        ],
        'sort-imports': [
            'warn',
            {
                'ignoreCase': true,
                'ignoreDeclarationSort': true,
                'ignoreMemberSort': true,
                'allowSeparatedGroups': true
            }
        ],
        'import/first': 2,
        'import/named': 2,
        'import/export': 2,
        'import/no-cycle': isProd ? 0 : 1,
        'import/namespace': 2,
        'import/extensions': ['error', 'never', {'svg': 'always', 'scss': 'always'}],
        'import/no-duplicates': 2,
        'import/no-self-import': 2,
        'import/no-absolute-path': 2,
        'import/no-dynamic-require': 2,
        'import/no-named-as-default': 0,
        'import/newline-after-import': ['error', { 'count': 1 }],
        'import/order': [
            isProd ? 'off' : 'warn',
            {
                'newlines-between': 'always',
                'warnOnUnassignedImports': true,
                'pathGroupsExcludedImportTypes': [ 'builtin', 'react' ],
                'groups': [ 'builtin', 'external', 'internal', 'parent', 'sibling', 'index', 'object' ],
                'pathGroups': [
                    {
                        'pattern': 'react',
                        'group': 'external',
                    },
                    {
                        'pattern': '@justlook/**',
                        'group': 'external',
                        'position': 'after',
                    },
                    {
                        'pattern': 'src/**',
                        'group': 'external',
                        'position': 'after',
                    },
                    {
                        'pattern': 'components/**',
                        'group': 'internal',
                    },
                    {
                        'pattern': 'const/**',
                        'group': 'internal',
                    },
                    {
                        'pattern': 'api/**',
                        'group': 'internal',
                    },
                    {
                        'pattern': 'utils/**',
                        'group': 'internal',
                    },
                    {
                        'pattern': 'hooks/**',
                        'group': 'internal',
                    },
                    {
                        'pattern': 'domains/**',
                        'group': 'internal',
                    },
                    {
                        'pattern': './**css',
                        'group': 'object',
                        'position': 'after',
                    }
                ],
                'alphabetize': {
                    'order': 'ignore',
                    'caseInsensitive': false
                }

            }
        ]
    },
    settings: {
        'import/resolver': {
            'node': {
                extensions: [ '.js', '.jsx', '.ts', '.tsx' ],
                moduleDirectory: [ 'node_modules', 'src/' ]
            }
        }
    },
}
