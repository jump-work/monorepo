window.__CONFIG__ = {
    brandName: 'Jump Taxi',
    protocol: 'https://',
    root_domain: 'jump.taxi',
    auth_page: 'my.jump.taxi',
    app_key: 'f5a1da62-ebe0-4b6b-b0f9-1483f7d62adf',
    home_page: 'https://driver.jump.taxi/drivers',
    crm_type: 'taxi',

    support: {
        email: 'support@jump.taxi',
        phone: '+7 (800) 550-38-68',
    },

    features: {
        enableRecoveryPassword: false
    },

    sentry:{
        dsn: '',
    },

    api_url: {
        lk: 'https://driver.jump.taxi/',
        v1: 'https://api.jump.taxi/',
        v2: 'https://api.jump.taxi/v2/',
    },
};
