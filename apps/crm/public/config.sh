#!/usr/bin/env bash

cp ./index.html ./index.php

sed -i -- `date +'s/config.js/config.js?v=%Y%m%d.%H%M%S/g'` ./index.php
sed -i -- "s/commit_short_sha/${CI_COMMIT_SHORT_SHA}/g" ./index.php