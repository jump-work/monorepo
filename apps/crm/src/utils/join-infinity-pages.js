/**
 * @param {array} array
 * @returns {*[]}
 */
export const joinInfinityPages = (array = []) => {
    const allData = [];

    array.map(page => page.items.map(item => allData.push(item)));

    return allData;
}
