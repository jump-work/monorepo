import React from 'react';

export const paymentInfo = ({ amount, commission, currency }) => {
    let message = `${amount} ${currency} сумма к получению`;

    if (commission) {
        message += `, ${commission} ${currency} комиссия за перевод`;
    } else {
        message += ', без комиссии';
    }

    return message + '.';
}

export const numberWithPercentLabel = (number, percent, unit) => {
    return [
        formatMoney(number),
        unit,
        percent ? `(${formatMoney(percent)}%)` : ''
    ].filter(Boolean).join(' ');
}

export const idFromString = (str) => {
    let id = 'id_';

    for (let i = 0, l = str.length; i < l; i++) {
        id += str.charCodeAt(i);
    }

    return id;
};

export const trimString = (s) => {
    return s.trim().replace(/\.+$/, '').trim();
}

/**
 * Заменяет в строке символ "\n" на тэг <br/>
 *
 * @param {String} text
 * @returns {(JSX.Element|T)[]}
 */
export function nl2br(text) {
    const regex = /(\n)/g;
    const lines = text.trim().split(regex) || [];

    return lines.map(function (line, index) {
        return line.match(regex) ? <br key={'key_' + index} /> : line;
    });
}

/**
 * Форматирует суммы в формат РФ
 *
 * @param {Number} amount
 * @param {Number} minimum
 * @returns {string}
 */
export function formatMoney(amount, min = 0, max = 2) {
    let preAmount = amount;

    if(min === 0 && max === 0) {
        preAmount = Math.trunc(preAmount);
    }

    return preAmount.toLocaleString('ru-RU', {
        minimumFractionDigits: min,
        maximumFractionDigits: max
    });
}

export function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

export const round = (amount, isShort) => {
    const min = isShort ? 0 : 2;
    const max = isShort ? 0 : 2;

    return formatMoney(amount, min, max);
}

export const validatePhone = (phone) => {
    const regex = /^(\+7|7|8)?[s-]?\(?[489][0-9]{2}\)?[s-]?[0-9]{3}[s-]?[0-9]{2}[s-]?[0-9]{2}$/g;

    return regex.test(phone);
}

/**
 * Проверяет заполнены ли переданные поля keys, в объекте obj
 *
 * @param {Object} obj
 * @param {Array} keys
 * @returns Boolean
 */
export const keysIsFill = (obj, keys) => {
    if(!obj) {
        return false;
    }

    for (let key of keys) {
        if (obj[key] === null || obj[key] === '') {
            return false
        }
    }

    return true;
}
