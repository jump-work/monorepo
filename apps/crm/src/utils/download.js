import api from 'api/client';

export const download = ({url, attacheFile}) => {
    return api.app.download(url)
            .then((data) => {
                const downloadUrl = window.URL.createObjectURL(new Blob([data]));
                const link = document.createElement('a');

                link.href = downloadUrl;
                link.setAttribute('download', attacheFile);
                document.body.appendChild(link);
                link.click();
                link.remove();
            });
};
