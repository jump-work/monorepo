import axios from 'axios';

/**
 * Генерируем ссылки
 * @param url {string|undefined}
 * @param config {Object}
 * @return {string}
 */
export const getUri = (url, config) => {
    return axios.getUri({ url, ...config })
}
