export const AccessCheck = (permissions = [], needPermissions = []) => {
    if(permissions.length === 0) return { access: false };

    for (let i = 0; i < needPermissions.length; i += 1) {
        if (permissions.includes(needPermissions[i])) {
            return { access: true };
        }

        if (!permissions.includes(needPermissions[i])) {
            return { access: false };
        }
    }
    return { access: false };
}
