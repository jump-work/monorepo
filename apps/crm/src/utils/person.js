import { keysIsFill } from 'utils/helpers';

export const getFio = (user) => {
    if(!user) return null;

    return [
        user.last_name,
        user.first_name,
        user.middle_name,
    ].filter(Boolean).join(' ');
}

export const hasFio = (user) => keysIsFill(user, ['first_name', 'last_name']);
