import { isToday, isYesterday, format, parseISO } from 'date-fns';
import ru from 'date-fns/locale/ru';

/**
 * @param date {string|Date}
 * @returns {string}
 */
export const formattingDate = (date) => {
    const verifiedDate = date ? (date instanceof Date ? date : parseISO(date)) : null;

    const prefix = (isToday(verifiedDate) && 'Сегодня') || (isYesterday(verifiedDate) && 'Вчера');

    if(prefix) return prefix

    return format(verifiedDate, 'dd MMM yyyy', { locale: ru });
}
