/**
 * @param {string} validator
 * @returns {Object}
 */
export function parseValidator (validator) {
    const formRules = {};
    const rules = validator.split('|');

    rules.forEach(rule => {
        if (rule === 'required') {
            formRules.required = 'Обязательно для ввода';
        }

        if (rule.startsWith('max:')) {
            formRules.maxLength = ruleMax(rule);
        }
    });

    return formRules;
}

function ruleMax (rule) {
    const maxRule = rule.split(':');

    return {
        value: maxRule[1],
        message: 'Максимальная длина ' + maxRule[1],
    };
}
