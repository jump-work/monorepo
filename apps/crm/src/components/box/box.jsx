import React from 'react';
import classes from 'classnames';
import PropTypes from 'prop-types';

import cx from './box.module.scss';

export const ContentBox = ({ children, size = 'default', className }) => {
    const cls = classes(cx[size], className)

    return (
        <div className={cls}>
            {children}
        </div>
    );
};
ContentBox.defaultProps = {
    size: 'default',
};

ContentBox.propTypes = {
    size: PropTypes.oneOf([ 'default', 'small', 'medium', 'large', 'none' ]),
    children: PropTypes.any,
    className: PropTypes.string,
};

const Box = ({ pl = 0, pr = 0, pb = 0, pt = 0, ml = 0, mr = 0, mb = 0, mt = 0, children, className, style }) => {
    const inlineStyle = {
        padding: `${pt}px ${pr}px ${pb}px ${pl}px`,
        margin: `${mt}px ${mr}px ${mb}px ${ml}px`,
        ...style,
    };

    return (
        <div style={inlineStyle} className={className}>
            {children}
        </div>
    );
};

Box.propTypes = {
    pt: PropTypes.number,
    pl: PropTypes.number,
    pb: PropTypes.number,
    pr: PropTypes.number,
    mt: PropTypes.number,
    ml: PropTypes.number,
    mb: PropTypes.number,
    mr: PropTypes.number,
    children: PropTypes.any,
    className: PropTypes.string,
    style: PropTypes.object,
};

export default Box;
