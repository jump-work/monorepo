import React from 'react';
import { Icons16 } from '@jump-work/uikit';

import cx from './default-marker.module.scss';

export const DefaultMarker = () => {
    return <Icons16.IconStar className={cx.base}/>
}
