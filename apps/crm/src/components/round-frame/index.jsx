import React from 'react';
import PropTypes from 'prop-types';

import cx from './round-frame.module.scss';

export const RoundFrame = ({ children, color, ...rest }) => {
    return (
        <div className={cx.base} style={{ backgroundColor: color }} {...rest}>
            { children }
        </div>
    )
}

RoundFrame.defaultProps = {
    color: '#D9FFE2',
}

RoundFrame.propTypes = {
    children: PropTypes.any.isRequired,
    color: PropTypes.string,
}
