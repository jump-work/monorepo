import React       from 'react';
import PropTypes   from 'prop-types';
import { NavLink } from 'react-router-dom';
import classes from 'classnames';

import cx from './list-link.module.scss';

const render = ({ children, to, muted, rest }) => {
    const classBuilder = ({isActive}) => {
        return classes({
            [cx.muted]: muted,
            [cx.active]: isActive
        });
    }

    return (
        <div className={cx.link}>
            <NavLink className={classBuilder} to={to} {...rest}>
                {children}
            </NavLink>
        </div>
    );
};

render.propTypes = {
    children: PropTypes.any,
    to: PropTypes.string.isRequired,
    rest: PropTypes.any,
    muted: PropTypes.bool,
};

export default render;
