import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import cx from './segment.module.scss';

export const Segment = ({ children, className, ...rest }) => {
    const cls = classes(cx.base, className);

    return (
        <div className={cls} {...rest}>
            { children }
        </div>
    )
}

Segment.propTypes = {
    children: PropTypes.any.isRequired,
    className: PropTypes.string,
}
