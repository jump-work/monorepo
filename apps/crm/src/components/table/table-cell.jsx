import React     from 'react';
import PropTypes from 'prop-types';
import classes   from 'classnames';

import cx from './table-cell.module.scss';

export const TableCell = ({ children, className, ...rest}) => {
    const cls = classes(cx.base, className)

    return (
        <div className={cls} {...rest}>
            {children}
        </div>
    )
}

TableCell.propTypes = {
    children: PropTypes.any.isRequired,
    className: PropTypes.string,
}
