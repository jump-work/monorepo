import React from 'react';
import PropTypes from 'prop-types';
import { Ellipsis } from '@jump-work/uikit';

import cx from './table.module.scss';

export const Table = ({ loading, children }) => {
    if(loading) {
        return (
            <div className={cx.loading}>
                <Ellipsis/>
            </div>
        )
    }

    return (
        <div className={cx.base}>
            {children}
        </div>
    )
}

Table.propTypes = {
    loading: PropTypes.bool,
    children: PropTypes.any.isRequired
}
