import React from 'react';
import PropTypes from 'prop-types';
import cx from './tree-shift.module.scss';

export const TreeShift = ({padding = 40, children}) => {

    return (
        <div className={cx[`shift-${padding}`]}>
            { children }
        </div>
    )
}

TreeShift.propTypes = {
    padding: PropTypes.number,
    children: PropTypes.any
}
