import { BAR, tickFormatter } from '../services/charts-service';

export function Tick(data) {
    this.xCoordinate = data.x ?? null;
    this.yCoordinate = data.y ?? null;
    this.height = data.height ?? null;
    this.width = data.width ?? null;
    this.payload = data.payload ?? null;
    this.index = data.index ?? null;
    this.orientation = data.orientation ?? null;
    this.visibleTicksCount = data.visibleTicksCount ?? null;
    this.chartType = data.chartType ?? null;
    this.textAnchor = data.textAnchor ?? null;
}

Tick.prototype.getIndex = function () {
    return this.index;
}

Tick.prototype.getHeight = function () {
    return this.height;
}

Tick.prototype.getWidth = function () {
    return this.width;
}

Tick.prototype.getXCoordinate = function () {
    return this.xCoordinate;
}

Tick.prototype.getYCoordinate = function () {
    return this.yCoordinate;
}

Tick.prototype.isXTick = function () {
    return this.orientation === 'bottom';
}

Tick.prototype.isYTick = function () {
    return this.orientation === 'left';
}

Tick.prototype.getPayloadValue = function () {
    return this.payload.value;
}

Tick.prototype.getPayloadCoordinate = function () {
    return this.payload.coordinate;
}

Tick.prototype.getLastIndexTick = function () {
    return this.visibleTicksCount - 1;
}

Tick.prototype.getTextAnchor = function() {
    if(this.isBarChart()) {
        return this.textAnchor
    }

    return (this.isXTick() && this.getPayloadCoordinate() > 380) ? 'end' : 'start';
}

Tick.prototype.getXSvgPath = function () {
    return this.isXTick() ? this.getPayloadCoordinate() : this.getXCoordinate() - 8;
}

Tick.prototype.getYSvgPath = function () {
    return this.isXTick() ? this.getYCoordinate() + 10 : this.getYCoordinate() + 12;
}

Tick.prototype.isFirstTick = function () {
    return this.isXTick() && this.getIndex() === 0;
}

Tick.prototype.isLastTick = function () {
    return this.isXTick() && this.getIndex() === this.getLastIndexTick();
}

Tick.prototype.isBarChart = function() {
    return this.chartType === BAR;
}

Tick.prototype.getFormattedTickBar = function () {
    if(this.isFirstTick() || this.isLastTick()) {
        return tickFormatter(this.getPayloadValue(), 'dd MMM');
    }

    return tickFormatter(this.getPayloadValue(), 'dd');
}

Tick.prototype.getFormattedTick = function () {
    if(this.isBarChart()) {
        return this.getFormattedTickBar()
    }

    return tickFormatter(this.getPayloadValue());
}

Tick.prototype.getTickValue = function () {
    if(this.isXTick()) {
        return this.getFormattedTick()
    }

    return this.getPayloadValue()
}
