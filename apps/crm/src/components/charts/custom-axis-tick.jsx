import React from 'react';
import classes from 'classnames';

import { Tick } from './models/tick';

import cx from './custom-axis-tick.module.scss';

export const CustomAxisTick = (props) => {
    const tick = new Tick(props);

    const cls = classes({
        [cx['custom-label-x']]: tick.isXTick(),
        [cx['custom-label-y']]: tick.isYTick(),
    })

    if (!tick.getPayloadValue()) return null;

    return (
        <text
            className={cls}
            x={tick.getXSvgPath()}
            y={tick.getYSvgPath()}
            width={tick.getWidth()}
            height={tick.getHeight()}
            textAnchor={tick.getTextAnchor()}
        >
            <tspan>
                {tick.getTickValue()}
            </tspan>
        </text>
    )
};
