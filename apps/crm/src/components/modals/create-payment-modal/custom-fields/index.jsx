import React from 'react';
import PropTypes from 'prop-types';
import {useFormContext } from 'react-hook-form';

import { FieldsByCondition } from './fields-by-condition';
import { Fields } from './fields';

export const CustomFields = ({ fields }) => {
    const { register } = useFormContext();
    const isSelect = fields.templates.length > 1;

    if (isSelect) {
        return (
            <FieldsByCondition fields={fields}/>
        )
    }

    return (
        <>
            <input ref={register({
                required: true
            })} type="hidden" name={fields.key} value={fields.templates[0].key}/>

            <Fields fields={fields.templates[0].placeholders}/>
        </>
    );
}

CustomFields.propTypes = {
    fields: PropTypes.object.isRequired,
}
