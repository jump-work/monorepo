import React     from 'react';
import PropTypes from 'prop-types';
import classes   from 'classnames';

import { Sidebar } from '../sidebar';
import { useMenu } from '../index';

import cx from './page.module.scss';

const render = ({ menu, userMenu, user, children }) => {
    // eslint-disable-next-line react-hooks/rules-of-hooks
    const { isCollapsedDesktop } = useMenu();

    const cls = classes(cx.layout, cx.with_contextual_sidebar, {
        [cx.with_icon_sidebar]: isCollapsedDesktop,
    });

    return (
        <div className={cls}>
            <Sidebar menu={menu} userMenu={userMenu} user={user}/>

            <div className={cx.content_wrapper}>
                <div className={cx.content}>
                    {children}
                </div>
            </div>
        </div>
    );
};

render.displayName = 'MenuPageLayout';

render.propTypes = {
    menu: PropTypes.any,
    userMenu: PropTypes.any,
    user: PropTypes.object,
    children: PropTypes.any,
};

export default render;
