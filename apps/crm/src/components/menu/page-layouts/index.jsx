import BlankPageLayout from './blank-layout';
import MenuPageLayout  from './menu-layout';

export {
    BlankPageLayout,
    MenuPageLayout,
};
