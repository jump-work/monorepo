class User {
    constructor () {
        this.id = 0;
        this.parent_id = 0;
        this.fullName = '';
        this.email = '';
        this.company = '';
    }

    getAvatarLetter () {
        return this.getFullName().charAt(0).toUpperCase();
    }

    setFullName (name) {
        this.fullName = name;

        return this;
    }

    getFullName () {
        return this.fullName;
    }

    setEmail (email) {
        this.email = email;

        return this;
    }

    getEmail () {
        return this.email;
    }

    setCompany (company) {
        this.company = company;

        return this;
    }

    getCompany () {
        return this.company;
    }

    setId (id) {
        this.id = parseInt(id);

        return this;
    }

    getId () {
        return this.id;
    }

    setParentId (id) {
        this.parent_id = parseInt(id);

        return this;
    }

    getParentId () {
        return this.parent_id;
    }
}

export { User };
