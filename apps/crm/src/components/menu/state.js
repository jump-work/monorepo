import { useReducer, createContext } from 'react';
import { isTablet, isDesktop }       from 'utils/browser';
import cx                            from './sidebar/sidebar.module.scss';

export const MenuContext = createContext(null);

function init (initialState) {
    return { view: initialState };
}

/**
 * Возвращает sidebar DOM-node
 * @returns {HTMLElement}
 */
function getNavBar () {
    return document.getElementById('nav-sidebar-root');
}

function reducer (state, action) {
    const cls = getNavBar().classList;

    switch (action.type) {
        case 'collapsedDesktop':
            cls.remove(cx.sidebar_expanded_mobile);
            cls.add(cx.sidebar_collapsed_desktop);
            localStorage.setItem('sidebar_view', 'collapsedDesktop');

            return { view: 'collapsedDesktop' };
        case 'expandedMobile':
            cls.remove(cx.sidebar_collapsed_desktop);
            cls.add(cx.sidebar_expanded_mobile);
            localStorage.setItem('sidebar_view', 'expandedMobile');

            return { view: 'expandedMobile' };
        case 'reset':
            cls.remove(cx.sidebar_collapsed_desktop);
            cls.remove(cx.sidebar_expanded_mobile);
            localStorage.setItem('sidebar_view', '');

            return { view: '' };
        default:
            return { view: localStorage.getItem('sidebar_view') || '' };
    }
}

export function useSideBarState () {
    const initialState = isDesktop()
        ? localStorage.getItem('sidebar_view') || ''
        : '';

    const [ state, dispatch ] = useReducer(reducer, initialState, init);

    const isCollapsedDesktop = state.view === 'collapsedDesktop';
    const isExpandedMobile = state.view === 'expandedMobile';

    const onCollapsedDesktop = () => dispatch({ type: 'collapsedDesktop' });
    const onExpandedMobile = () => dispatch({ type: 'expandedMobile' });
    const onReset = () => dispatch({ type: 'reset' });

    const onToggleSideBar = () => {
        if (isDesktop()) {
            isCollapsedDesktop ? onReset() : onCollapsedDesktop();
        }

        if (isTablet()) {
            isExpandedMobile ? onReset() : onExpandedMobile();
        }
    };

    return {
        view: state.view,
        onCollapsedDesktop,
        onExpandedMobile,
        onReset,
        isCollapsedDesktop,
        isExpandedMobile,
        onToggleSideBar,
    };
}
