import { useContext }  from 'react';
import { MenuContext } from './state';

export * from './provider';
export * from './page-layouts';
export * from './content-layouts';
export * from './user-model';

export const useMenu = () => {
    return useContext(MenuContext).menu;
};

export const useLogout = () => {
    return useContext(MenuContext).logoutAction || (() => {});
};

export const useBrandName = () => {
    return useContext(MenuContext).brandName || '';
};
