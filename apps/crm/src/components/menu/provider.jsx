import React                            from 'react';
import Props                            from 'prop-types';
import { useSideBarState, MenuContext } from './state';

export const MenuProvider = ({ children, logoutAction, brandName }) => {
    const menu = useSideBarState();

    return (
        <MenuContext.Provider value={{ menu, logoutAction, brandName }}>
            {children}
        </MenuContext.Provider>
    );
};

MenuProvider.propTypes = {
    children: Props.any,
    brandName: Props.string.isRequired,
    logoutAction: Props.func.isRequired,
};
