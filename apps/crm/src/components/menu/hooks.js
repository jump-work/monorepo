import { useEffect }   from 'react';
import { useLocation } from 'react-router-dom';

/**
 * Хук для проверки совпадения текущего пути с шаблоном
 * /home/ => home
 * @returns {function(...[*]=)}
 */
export function useMatchLocation () {
    const { pathname } = useLocation();
    const clearPathname = pathname.replace(/(^\/+)|(\/+$)/g, '').trim();

    return (to) => {
        const clearTo = to.replace(/(^\/+)|(\/+$)/g, '').trim();

        if (clearPathname === clearTo) {
            return true;
        }

        if (clearPathname === '' || clearTo === '') {
            return false;
        }

        return clearPathname.search(new RegExp(`^${clearTo}`)) !== -1;
    };
}

/**
 * Хук для проверки является ли группа активной
 * @returns {function(...[*]=)}
 */
export function useMatchRootLocation () {
    const isMatch = useMatchLocation();

    return (item) => {
        const { items = [], to } = item;

        if (isMatch(to)) {
            return true;
        }

        return items.filter(item => isMatch(item.to)).length > 0;
    };
}

export const useClickOutside = (ref, handler, callback) => {
    useEffect(() => {
        const closeHandler = (e) => {
            if (ref.current !== e.target && ref.current &&
                !ref.current.contains(e.target) && handler) {
                callback(false);
            }
        };

        if (handler) {
            document.addEventListener('mousedown', closeHandler);
            document.addEventListener('touchstart', closeHandler);
        } else {
            document.removeEventListener('mousedown', closeHandler);
            document.removeEventListener('touchstart', closeHandler);
        }

        return () => {
            document.removeEventListener('mousedown', closeHandler);
            document.removeEventListener('touchstart', closeHandler);
        };
    }, [ callback, handler, ref ]);
};
