import { OneColumnsLayout }  from './one-column-layout';
import { TwoColumnsLayout } from './two-columns-layout';

export {
    OneColumnsLayout,
    TwoColumnsLayout,
};
