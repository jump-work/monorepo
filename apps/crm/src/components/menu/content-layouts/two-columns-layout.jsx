import PropTypes from 'prop-types';
import React, { Fragment, useRef } from 'react';
import { VIEW_MODE_LIST, VIEW_MODE_FULLSCREEN, VIEW_MODE_DETAILS } from 'utils/browser';

import { useGetHeight } from 'hooks/use-get-height'

import cx from './two-columns-layout.module.scss';

const List = ({ header, search, children }) => {
    const ref = useRef();
    const { height } = useGetHeight(ref, 73);

    return (
        <Fragment>
            <div className={cx['header-wrapper']} ref={ref}>
                {header}
                {search}
            </div>

            <div
                className={cx['scroll-wrapper']}
                style={{ height: `calc(100vh - ${height}px - 73px)` }}
            >
                {children}
            </div>
        </Fragment>
    );
};

List.propTypes = {
    header: PropTypes.any.isRequired,
    search: PropTypes.any,
    children: PropTypes.any
};

const renderDesktop = (header, search, children, details) => {
    return (
        <div className={cx['page-wrapper']}>
            <div className={cx['list-wrapper']}>
                <List header={header} search={search}>
                    { children }
                </List>
            </div>

            <div className={cx['detail-wrapper']}>
                <div className={cx['scroll-wrapper']}>
                    {details}
                </div>
            </div>
        </div>
    );
};

const renderMobile = (header, search, children) => {
    return (
        <div className={cx['page-wrapper']}>
            <List header={header} search={search}>
                { children }
            </List>
        </div>
    );
};

export const TwoColumnsLayout = ({ viewMode, header, search, details, children }) => {
    const searchControl = viewMode !== VIEW_MODE_DETAILS && search;

    if (viewMode === VIEW_MODE_FULLSCREEN) {
        return renderDesktop(header, searchControl, children, details);
    }

    const content = viewMode === VIEW_MODE_LIST ? children : details;
    return renderMobile(header, searchControl, content);
};

TwoColumnsLayout.propTypes = {
    header: PropTypes.any.isRequired,
    search: PropTypes.any,
    details: PropTypes.any,
    children: PropTypes.any,
    viewMode: PropTypes.oneOf(
        [ VIEW_MODE_LIST, VIEW_MODE_FULLSCREEN, VIEW_MODE_DETAILS ]),
};
