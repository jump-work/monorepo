import React from 'react';
import PropTypes from 'prop-types';

import { Collapse } from './collapsible-table-collapse';
import { Cell } from './collapsible-table-cell';

export const Load = ({ label, onClick, style, ...rest }) => {

    const inlineStyle = {
        cursor: 'pointer',
        color: '#2691FF',
        ...style
    }

    return (
        <Collapse {...rest}>
            <Cell
                style={inlineStyle}
                onClick={onClick}
            >
                {label}
            </Cell>
        </Collapse>
    )
}

Load.propTypes = {
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
    style: PropTypes.object,
}
