import React from 'react';
import { Icons16 } from '@jump-work/uikit';

import { Collapse } from './collapsible-table-collapse';
import { Cell } from './collapsible-table-cell';

export const Loader = (props) => {
    return (
        <Collapse {...props}>
            <Cell>
                <Icons16.IconLoader/>
            </Cell>
        </Collapse>
    )
}
