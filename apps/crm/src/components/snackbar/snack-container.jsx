import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';

import { Snack } from 'components/snackbar/snack';

import cx from 'components/snackbar/snack-container.module.scss';

export const SnackContainer = ({ snacks, hiddenId }) => {
    const [allSnacks, setAllSnacks] = useState([]);

    useEffect(() => {
        if(snacks) setAllSnacks((prevSnack) => [...prevSnack, snacks])
    }, [snacks])

    const handleRemove = (id) => {
        setAllSnacks((prevSnack) => prevSnack.filter((snack) => snack.id !== id));
    };

    return (
        <div className={cx['snack-container']}>
            {allSnacks.map(snack => (
                <Snack
                    key={snack.id}
                    {...snack}
                    id={snack.id}
                    text={snack.text}
                    onClick={snack.onClick}
                    hideAfter={snack.hideAfter}
                    show={hiddenId !== snack.id}
                    onHide={handleRemove}
                />
            ))}
        </div>
    )
}

SnackContainer.defaultProps = {
    snacks: undefined,
    hiddenId: undefined,
};

SnackContainer.propTypes = {
    snacks: PropTypes.shape({}),
    hiddenId: PropTypes.number,
};
