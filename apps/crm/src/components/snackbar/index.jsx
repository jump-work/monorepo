import React from 'react';
import ReactDOM from 'react-dom';

import { SnackContainer } from 'components/snackbar/snack-container';
import { styleContainer } from 'components/snackbar/style-container';

function renderInlineStyleContainer(style = {}) {
    const arr = [];

    for (let key in style) arr.push({ property: key, value: style[key] });

    return arr;
}

let snackCount = 0;

export const snackBar = (text, options) => {
    let rootContainer = document.getElementById(options?.['container-id'] ||'snackbar');

    const style = renderInlineStyleContainer(options?.['style-container'] || styleContainer);

    if (!rootContainer) {
        rootContainer = document.createElement('div');
        rootContainer.id = options?.['container-id'] ||'snackbar';
        style.map(({property, value}) => rootContainer.style[property] = value)
        document.body.appendChild(rootContainer);
    }

    snackCount += 1;

    const hideTime = (options?.hideAfter === undefined ? 3 : options.hideAfter) * 1000;
    const snack = { id: snackCount, text, ...options };

    ReactDOM.render(<SnackContainer snacks={snack}/>, rootContainer);

    const hide = () => { ReactDOM.render(<SnackContainer hiddenId={snack.id} />, rootContainer) };

    const completePromise = new Promise((resolve) => {
        setTimeout(() => {
            resolve();
        }, hideTime);
    });

    completePromise.hide = hide;

    return completePromise;
}
