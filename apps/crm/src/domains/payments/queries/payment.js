import { useQuery, useQueryClient } from 'react-query';

import Api from 'api/client';
import { CACHE_PAYMENT_ITEM } from 'domains/common/cache-keys';

import { Payment } from '../models';

export const usePaymentItem = (id, initialData = undefined) => {
    const client = useQueryClient();

    const {data, isLoading, isFetching, isError, error} = useQuery(
        [CACHE_PAYMENT_ITEM, id],
        () => Api.payments.fetchPayment(id).then(resp => resp.item),
        { initialData }
    );

    const refreshPayment = () => client.refetchQueries([CACHE_PAYMENT_ITEM, id]);

    return {
        paymentData: new Payment(data),
        paymentHasError: isError,
        paymentError: error,
        isPaymentLoading: isLoading,
        isPaymentFetching: isFetching,
        refreshPayment
    }
}
