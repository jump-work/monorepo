import { useCallback } from 'react';
import { useQuery, useQueryClient } from 'react-query';

import Api from 'api/client';
import { CACHE_PAYMENT_LIST } from 'domains/common/cache-keys';

import { useFilter } from '../helpers/filter';
import { FILTER } from '../constants';

export const usePayments = () => {
    const queryClient = useQueryClient();

    const { filter, updateFilter } = useFilter();

    const { data,error, isLoading, isFetching, isError } = useQuery(
        [CACHE_PAYMENT_LIST, filter],
        () => Api.payments.fetchPaymentsList({...FILTER, ...filter})
    );

    const getPaymentById = (id) => {
        if ( ! id) {
            return null;
        }

        const data = queryClient.getQueryData([CACHE_PAYMENT_LIST, filter]) || {};

        return data?.items?.find(item => item.id === id) || null;
    }

    const refreshPayments = useCallback(() => {
        return queryClient.refetchQueries([CACHE_PAYMENT_LIST, filter])
    }, [filter, queryClient]);

    return {
        paymentsData: data?.items || [],
        paymentsMeta: data?.meta || {},
        paymentsHasError: isError,
        paymentsError: error,
        isPaymentsLoading: isLoading,
        isPaymentsFetching: isFetching,
        refreshPayments,
        filter,
        updateFilter,
        getPaymentById,
        bulkActions: data?.meta?.bulk_actions || [],
    }
}
