import { useLocation, useParams } from 'react-router-dom';
import { useMemo, useState } from 'react';

import { useUpdateQueryFilter } from 'hooks/use-update-search-filter';

import { FILTER } from '../constants';

export const DEFAULT_FILTER = {...FILTER};

export const useFilter = () => {
    const {id: contractor_id} = useParams();
    const {pathname} = useLocation();
    const isLocal = pathname !== '/payments';

    const query = useUpdateQueryFilter(DEFAULT_FILTER);

    const [localFilter, setLocalFilter] = useState({...DEFAULT_FILTER, contractor_id});
    const updateLocalFilter = (filter) => {
        setLocalFilter((old) => ({...old, ...filter}))
    }

    return useMemo(() => ({
        filter: isLocal ? localFilter : query.filter,
        updateFilter: isLocal ? updateLocalFilter : query.updateFilter,
    }), [isLocal, localFilter, query.filter, query.updateFilter]);
}
