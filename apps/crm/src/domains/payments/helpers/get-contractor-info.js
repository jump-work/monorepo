export const getContractorInfo = (contractor) => {
    const phone = contractor.needCreateContractor
        ? contractor?.search
        : contractor?.phone;

    return { ...contractor, phone };
}
