import { useHasPermission } from 'app/profile/store/selectors';

import { isWaitingConfirm } from './payment-status';

export const usePaymentHasConfirmStatus = () => {
    const canPay = useHasPermission('write-payment');

    return (payment) => canPay && isWaitingConfirm(payment?.status?.id)
}
