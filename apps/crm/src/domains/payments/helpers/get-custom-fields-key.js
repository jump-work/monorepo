export const getCustomFieldsKey = (fields) => {
    const keys = new Set();

    if (! fields) {
        return keys;
    }

    keys.add(fields.key);

    (fields?.templates || []).map(({ placeholders }) => {
        return placeholders.map(item => keys.add(item.key));
    });

    return keys;
}
