export const prepareStepFormRequestValues = (values) => {
    let prepareValues = {
        ...values,
        contractor_id: values?.id,
        amount: Number(values.amount),
        requisite: {
            type_id: 8,
            account_number: values?.requisite?.account_number?.replace(/\s/img, ''),
        }
    };

    if(values?.needCreateContractor) {
        prepareValues = {
            ...prepareValues,
            phone: values?.search,
        }
    }

    return prepareValues;
};
