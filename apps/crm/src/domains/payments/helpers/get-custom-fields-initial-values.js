import { parseJSON } from 'date-fns';

export const getCustomFieldsInitialValues = (fields = {}, values) => {
    const customFields = {};

    if (! fields) {
        return customFields;
    }

    customFields[fields.key] = values[fields.key];

    const valueKeyForFilter = values[fields.key]?.value || values[fields.key];

    (fields?.templates || [])
        .filter(template => template.key === valueKeyForFilter)
        .map(({ placeholders }) => (
            placeholders.map(({ key, validator }) => {
                if (validator.match('iso_date')) {
                    return customFields[key] = values[key] && parseJSON(values[key]);
                }

                return customFields[key] = values[key];
            })
        ));

    return customFields;
}
