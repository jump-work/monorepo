import { useEffect, useState } from 'react';
import { parse } from 'query-string';
import plural, { verb } from 'plural-ru';
import { useLocation } from 'react-router-dom';
import { useQueryClient } from 'react-query';

import Api from 'api/client';
import { useMoney } from 'hooks/use-money';
import { CACHE_PAYMENT_LIST } from 'domains/common/cache-keys';

import { useFilter } from '../helpers';

const defaultStatistic = {amount: 0, amount_to_pay: 0, count: 0, ids: ''}

const useStatisticMessage = () => {
    const {format, currency} = useMoney();

    return ({ count, amount, amount_to_pay }) => {
        const prefix = verb(count, 'Выбрана', 'Выбраны', 'Выбрано');
        const pays = plural(count, '%d выплата', '%d выплаты', '%d выплат');
        const applications = verb(count, 'заявки', 'заявок', 'заявок');

        const getFormatAmount = (amount) => format(amount) + currency;

        return `${prefix} ${pays}. Сумма ${applications} ${getFormatAmount(amount)}, сумма выплаты ${getFormatAmount(amount_to_pay)}.`;
    }
}

export const useSelectedPayments = () => {
    const queryClient = useQueryClient();
    const statisticToString = useStatisticMessage();

    const { search } = useLocation();
    const statusId = parse(search)?.status_id;

    const [ isAllSelected, setIsAllSelected ] = useState(false);
    const [ payments, setPayments ] = useState([]);
    const [ statistic, setStatistic ] = useState(defaultStatistic);
    const [ isLoadingInfo, setIsLoadingInfo ] = useState(false);

    const resetStatistic = () => {
        setIsAllSelected(false);
        setPayments([]);
        setStatistic(defaultStatistic);
    }

    const { filter } = useFilter();
    const paymentsCache = queryClient.getQueryData([CACHE_PAYMENT_LIST, filter]);

    const checkPartialSelection = (payments) => {
        const isAllAvailablePaymentsSelected = payments.length === paymentsCache?.meta?.total;
        const isLastPage = paymentsCache?.meta?.current_page === 1;

        if (isAllAvailablePaymentsSelected && isLastPage) {
            setIsAllSelected(true);
        }
    }

    useEffect(() => { resetStatistic() }, [statusId]);

    const onRefresh = (isAllSelected, payments) => {
        if (isAllSelected || payments.length) {
            setIsLoadingInfo(true)

            Api.payments.infoTotal({
                    payment_id: !isAllSelected ? payments.join(',') : null,
                    status_id: statusId,
                })
                .then(resp => {
                    setPayments((resp?.data?.ids || '').split(',').map(i => parseInt(i)))
                    setStatistic((prev) => ({ ...prev, ...(resp?.data || {}) }))
                })
                .catch(e => console.log(e))
                .finally(() => setIsLoadingInfo(false));
        } else {
            setStatistic(defaultStatistic);
        }
    };

    const onSelectAll = (value) => {
        setIsAllSelected(value)
        setPayments([]);
        onRefresh(value, []);
    }

    const onClearAll = () => {
        onSelectAll(false)
    }

    const onTogglePayment = (id, value) => {
        if (value) {
            setPayments((prev) =>  {
                const newState = [...prev, id]
                checkPartialSelection(newState);
                onRefresh(isAllSelected, newState);

                return newState;
            })
        } else {
            setPayments((prev) =>  {
                setIsAllSelected(false)

                const newState = prev.filter(idx => idx !== id)
                onRefresh(false, newState);

                return newState;
            })
        }
    }

    const isSelected = (id) => {
        if (isAllSelected) return true;

        return payments.includes(id);
    }

    return {
        statusId,
        hasPartialSelection: !isAllSelected && payments.length > 0,
        hasSelection: isAllSelected || payments.length > 0,
        statistic: statisticToString(statistic),
        isLoadingInfo,
        isSelected,
        onSelectAll,
        onTogglePayment,
        onClearAll,
        listPayments: payments
    }
}
