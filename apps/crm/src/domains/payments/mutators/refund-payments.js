import { useMutation } from 'react-query';

import Api from 'api/client';

export const useRefundPayments = (onSuccess) => {
    const { mutate, isLoading, isError, error } = useMutation(
        (data) => Api.payments.refundPayments(data), {
            onSuccess
        }
    );

    return {
        onRefund: mutate,
        mutateRefundHasError: isError,
        isRefundMutating: isLoading,
        mutateRefundError: error,
    }
}
