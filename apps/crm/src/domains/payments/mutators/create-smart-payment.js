import { useMutation } from 'react-query';

import Api from 'api/client';
import { addServerErrors } from 'utils/add-server-errors';

import { WITHDRAW_BALANCE_CONTRACTOR, WITHDRAW_BALANCE_COMPANY } from '../constants';

export const useCreateSmartPayment = (setError, onSuccess) => {
    const withdraw = async (data) => await Api.payments.withdraw(data)
    const withdrawSmart = async (data) => await Api.payments.withdrawSmart(data)

    const onCreate = (data) => {
        if (data?.payment_from?.value === WITHDRAW_BALANCE_CONTRACTOR) { return withdraw(data) }
        if (data?.payment_from?.value === WITHDRAW_BALANCE_COMPANY) { return withdrawSmart(data) }
    }

    const { mutate, isLoading, isError, error } = useMutation(onCreate, {
        onSuccess: (data) => onSuccess(data?.item),
        onError: (error) => addServerErrors(error?.errorData?.fields || [], setError)
    });

    return {
        onCreate: mutate,
        isLoading,
        isError,
        error,
    }
}
