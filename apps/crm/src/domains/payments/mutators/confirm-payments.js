import { useState } from 'react';
import { useMutation } from 'react-query';

import Api from 'api/client';

export const useConfirmListPayments = (onSuccess) => {
    const [ needConfirmOtp, setNeedConfirmOtp ] = useState(false);

    const { mutate, data, isLoading, isError, error, reset } = useMutation(
        (paymentIds) => Api.payments.confirmPayments(paymentIds), {
            onSuccess: (data) => {
                if (data?.hash) {
                    return setNeedConfirmOtp(true);
                }

                onSuccess(data);
            }
        }
    )

    return {
        reset,
        response: data,
        onConfirm: mutate,
        needConfirmOtp,
        isConfirming: isLoading,
        confirmError: error,
        confirmIsError: isError,
    }
}
