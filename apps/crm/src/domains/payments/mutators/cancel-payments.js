import { useMutation } from 'react-query';

import Api from 'api/client';

export const useCancelPayments = (onSuccess) => {
    const { mutate, isLoading, isError, error } = useMutation(
        (data) => Api.payments.cancelPayments(data), {
            onSuccess,
        }
    )

    return {
        onCancel: mutate,
        mutateCancelHasError: isError,
        isCancelMutating: isLoading,
        mutateCancelError: error,
    }
}
