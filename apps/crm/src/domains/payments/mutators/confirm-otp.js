import { useMutation } from 'react-query';

import Api from 'api/client';

export const useConfirmOtp = (onSuccess) => {
    const { mutate, data, isLoading, isError, error, reset } = useMutation(
        (data) => Api.payments.confirmOtp(data),
        { onSuccess }
    )

    return {
        resetOtp: reset,
        data,
        onConfirmOtp: mutate,
        isConfirmingOtp: isLoading,
        confirmOtpIsError: isError,
        confirmOtpError: error,
    }
}
