import { useState } from 'react';
import { useMutation } from 'react-query';

import Api from 'api/client';

export const useConfirmItemPayment = (onSuccessConfirm) => {
    const [ needConfirmOtp, setNeedConfirmOtp ] = useState(false);

    const { mutate, data, isLoading, isError, error } = useMutation(
        (paymentId) => Api.payments.confirm(paymentId), {
            onSuccess: (data) => {
                if (data?.hash) {
                    return setNeedConfirmOtp(true);
                }
                onSuccessConfirm(data?.item)
            }
        })

    return {
        onConfirmPayment: mutate,
        payment: data,
        needConfirmOtp,
        isLoading,
        isError,
        error
    }
}
