import React, { createContext, useContext } from 'react';
import PropTypes from 'prop-types';

import { useSelectedPayments } from '../services';

const SelectedPaymentsContext = createContext(null);

export const useSelectedPaymentsContext = () => {
    return useContext(SelectedPaymentsContext);
}

const SelectedPaymentsContextProvider = ({ children }) => {
    const contextValue = useSelectedPayments();

    return (
        <SelectedPaymentsContext.Provider value={contextValue}>
            { children }
        </SelectedPaymentsContext.Provider>
    )
}

SelectedPaymentsContextProvider.propTypes = {
    children: PropTypes.any
};

export default SelectedPaymentsContextProvider;
