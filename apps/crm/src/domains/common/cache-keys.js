export const CACHE_PAYMENT_LIST = 'payment_list'
export const CACHE_PAYMENT_ITEM = 'payment_item'
export const CACHE_PAYMENT_STATUSES = 'payment_status_badges'
export const CACHE_PAYMENT_ONLINE_PERMISSION = 'payment_online_permission'

export const CACHE_CONTRACTOR_SEARCH = 'contractor_search'
export const CACHE_CONTRACTOR_REQUISITES = 'contractor_requisites'
export const CACHE_CONTRACTOR_TAXES = 'contractor_taxes'
export const CACHE_CONTRACTOR_PERMISSIONS = 'contractor_permissions'
export const CACHE_CONTRACTOR_SELF_EMPLOYER_INFO = 'contractor_self_employer_info'

export const CACHE_CONDITIONS_RULE = 'conditions_rule'
export const CACHE_RULES_PAYMENTS = 'rules_payments'
export const CACHE_RULE_PAYMENT = 'rule_payment'

export const CACHE_INTEGRATIONS_LIST = 'integrations_list'
export const CACHE_INTEGRATION_ITEM = 'integration_item'

export const CACHE_TAXES_LIST = 'taxes_list'

export const CACHE_STATEMETS_LIST = 'statements_list'
export const CACHE_STATEMETS_ITEM = 'statements_item'

export const CACHE_AGENTS_MY = 'agents_my'
export const CACHE_AGENTS_LIST = 'agents_list'
export const CACHE_AGENT = 'agent'
export const CACHE_AGENT_BY_INN = 'agent_by_inn'

export const CACHE_DASHBOARD_BALANCE = 'dashboard_balance'
export const CACHE_BALANCE_SYNC_STATUS = 'balance_sync_status'
export const CACHE_DASHBOARD_CONTROL_ROOM = 'dashboard_control_room'
export const CACHE_DASHBOARD_SUMMARY = 'dashboard_summary'
export const CACHE_DASHBOARD_CHART = 'dashboard_chart'

export const CACHE_GROUPS = 'groups'

export const CACHE_REPORT_TRIPS_DRIVERS = 'report_trips_drivers'
export const CACHE_REPORT_TRIPS_INTEGRATIONS = 'report_trips_integrations'

export const CACHE_REPORT_DECREASES_DEBITS = 'report_decreases_debits'
export const CACHE_REPORT_DECREASES_DRIVERS = 'report_decreases_drivers'

export const CACHE_REPORT_PAYMENTS_INTEGRATIONS = 'report_payments_integrations'
export const CACHE_REPORT_PAYMENTS_BANKS = 'report_payments_banks'
export const CACHE_REPORT_PAYMENTS_DRIVERS = 'report_payments_drivers'
