import React, { createContext, useContext } from 'react';
import PropTypes from 'prop-types';

import { useStatement } from '../queries';

const StatementContext = createContext(null);

export const useStatementContext = () => {
    return useContext(StatementContext);
}

const StatementContextProvider = ({ id, options, children }) => {
    const contextValue = useStatement(id, options);
    return (
        <StatementContext.Provider value={contextValue}>
            { children }
        </StatementContext.Provider>
    )
}

StatementContextProvider.propTypes = {
    id: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    options: PropTypes.object,
    children: PropTypes.any
};

export default StatementContextProvider;
