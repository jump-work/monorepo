import { useMemo } from 'react';

import { FILTER } from 'domains/taxes/constants';
import { useUpdateQueryFilter } from 'hooks/use-update-search-filter';

export const DEFAULT_FILTER = { ...FILTER };

export const useFilter = () => {
    const query = useUpdateQueryFilter(DEFAULT_FILTER);

    return useMemo(() => ({
        filter: query.filter,
        filterFull: query.filterFull,
        updateFilter: query.updateFilter,
    }), [ query.filter, query.filterFull, query.updateFilter ]);
}
