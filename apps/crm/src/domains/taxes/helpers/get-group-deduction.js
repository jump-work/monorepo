import { STATEMENT_DEDUCTION_COMPLETED_STATUSES, STATEMENT_DEDUCTION_STATUSES } from '../constants';

export const getGroupDeduction = (deduction) => {
    const { paid, not_paid } = STATEMENT_DEDUCTION_COMPLETED_STATUSES;
    const { not_enough_money, cannot_pay, ready_pay } = STATEMENT_DEDUCTION_STATUSES;

    if (deduction?.status_when_completed) {
        if (deduction?.status_when_completed === paid)
            return ready_pay;
        else if (deduction?.status_when_completed === not_paid)
            return not_enough_money;
        else return cannot_pay;
    } else {
        if (deduction?.status_for_pay === ready_pay) return ready_pay;
        else if (deduction?.status_for_pay === not_enough_money) return not_enough_money;
        else return cannot_pay;
    }
}
