export const countSelected = (selectedDeductions) => {
    let count = 0;

    for (let key in selectedDeductions) {
        if (selectedDeductions[key]) {
            count += 1;
        }
    }

    return count;
}
