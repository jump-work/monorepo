import { STATEMENT_STATUSES, STATEMENT_DEDUCTION_COMPLETED_STATUSES } from '../constants';

export const isEditable = (item) => item?.status?.id === STATEMENT_STATUSES.wait_payment || item?.status?.id === STATEMENT_STATUSES.payment;
export const isBlocked = (item) => item?.status?.id !== STATEMENT_STATUSES.wait_payment;
export const isPaid = (item) => item?.status?.id === STATEMENT_STATUSES.paid;
export const isCompletedPay = (item) => item.status_when_completed === STATEMENT_DEDUCTION_COMPLETED_STATUSES.paid;

