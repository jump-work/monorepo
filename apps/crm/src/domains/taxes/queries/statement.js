import { useState, useCallback, useMemo, useEffect } from 'react';
import { useQueryClient, useQuery } from 'react-query';

import Api from 'api/client';
import { useHasPermission } from 'app/profile/store/selectors';
import { snackBar } from 'components/snackbar';
import { download } from 'utils/download';
import { CACHE_STATEMETS_ITEM } from 'domains/common/cache-keys';

import { STATEMENT_DEDUCTION_STATUSES, STATEMENT_STATUSES } from '../constants';
import { getGroupDeduction, isPaid, isBlocked, isEditable, isCompletedPay, countSelected } from '../helpers';

/**
 * @param id
 * @returns {Object}
 */
export const useStatement = (id) => {
    const client = useQueryClient();
    const [ selectedDeductions, setSelectionDeductions ] = useState({});
    const [ isSelectAllDeductions, setIsAllSelectionDeductions ] = useState(false);
    const [ deductionFilter, setDeductionFilter, ] = useState(STATEMENT_DEDUCTION_STATUSES.ready_pay);
    const [ isConfirmationLoading, setIsConfirmationLoading ] = useState(false);
    const [ isDownloading, setIsDownloading ] = useState(false);
    const [ needCompensationCompany, setNeedCompensationCompany ] = useState(false);
    const [ isOpenAboutPaymentTax, setIsOpenAboutPaymentTax ] = useState(false);

    const { data: statement, isLoading, isFetching, isError, error } = useQuery(
        [ CACHE_STATEMETS_ITEM, id ],
        () => Api.statements.fetchStatement(id)
            .then(resp => {
            if(resp.item?.refresh?.in_process) {
                client.setQueryDefaults(
                    [ CACHE_STATEMETS_ITEM, String(resp?.item?.id)],
                    {refetchInterval: 5000, enabled: true}
                );
            } else {
                client.setQueryDefaults(
                    [ CACHE_STATEMETS_ITEM, String(resp?.item?.id)],
                    {refetchInterval: false, enabled: true}
                )
            }

            return resp.item
        }), {
            cacheTime: 5000,
        }
    );

    const refreshStatementItemCache = useCallback((item) => {
        if (item) client.setQueryData([ CACHE_STATEMETS_ITEM, id ], () => item)
        return client.refetchQueries([ CACHE_STATEMETS_ITEM, id ])
    }, [ client, id ]);

    const deductionsGroupedByStatus = useMemo(() => {
        const all = statement?.deductions || [];
        const readyForPay = [];
        const notEnoughMoney = [];
        const rejected = [];

        all.forEach((deduction) => {
            if (STATEMENT_DEDUCTION_STATUSES.ready_pay === getGroupDeduction(deduction)) return readyForPay.push(deduction);
            if (STATEMENT_DEDUCTION_STATUSES.not_enough_money === getGroupDeduction(deduction)) return notEnoughMoney.push(deduction)
            else return rejected.push(deduction);
        });

        return { all, readyForPay, notEnoughMoney, rejected }
    }, [ statement ]);

    const availableSelectDeduction = useMemo(() => {
        if (!statement?.deductions) return [];

        return statement.deductions
            .filter((item) => item?.status_for_pay !== STATEMENT_DEDUCTION_STATUSES.cannot_pay)
            .map(item => item.id);
    }, [ statement ]);

    const isActiveDeductionsFilter = (id) => deductionFilter === id

    const onToggleDeduction = (id, checked) => {
        setSelectionDeductions((prev) => ({ ...prev, [id]: checked }));
    };

    const onToggleDeductions = (deductions, value) => {
        const toggle = {};
        deductions.forEach((deduction) => {
            toggle[deduction.id] = value;
        });

        setSelectionDeductions((prev) => ({ ...prev, ...toggle }));
    }

    useEffect(() => {
        const toggle = {};
        statement?.deductions && statement.deductions.forEach((deduction) => {
            toggle[deduction.id] = deduction?.need_pay;
        });

        setSelectionDeductions((prev) => ({ ...prev, ...toggle }))
    }, [ statement ]);


    useEffect(() => {
        const selectedCount = Object.keys(selectedDeductions)
            .filter(id => selectedDeductions[id]).length;

        setIsAllSelectionDeductions(availableSelectDeduction.length === selectedCount);
    }, [ availableSelectDeduction.length, selectedDeductions ]);

    const onSelectAllDeductions = (checked) => {
        if (!checked) {
            setSelectionDeductions({});
            return;
        }

        const selected = {};
        for (let i = 0; i < availableSelectDeduction.length; i++) {
            selected[availableSelectDeduction[i]] = true;
        }
        setSelectionDeductions(selected);
    };

    const downloadInvoice = useCallback(({ id, file_attach_name }) => {
        const statementId = id || statement.id;
        const attacheFile = file_attach_name || statement?.invoice?.file_attach_name;

        setIsDownloading(true);

        download({url: `taxes/statements/${statementId}/invoice`, attacheFile})
            .then(() => {
                setIsDownloading(false);
                setIsOpenAboutPaymentTax(true);
            })
            .catch((e) => {
                setIsDownloading(false)
                snackBar(e.message)
            })
    }, [ statement ])

    const confirmStatement = useCallback(() => {
        setIsConfirmationLoading(true);

        return Api.statements.confirmStatement(
            statement.id,
            Object.keys(selectedDeductions).filter((id) => selectedDeductions[id]),
            needCompensationCompany,
        ).then((result) => {
            refreshStatementItemCache().then(() => {
                setIsConfirmationLoading(false);
                setIsOpenAboutPaymentTax(true);
                downloadInvoice({
                    id: result?.item?.id,
                    file_attach_name: result.item?.invoice?.file_attach_name
                });
            });
            return result;
        });
    }, [ statement, refreshStatementItemCache, selectedDeductions, needCompensationCompany, downloadInvoice ]);


    const deleteConfirmStatement = useCallback(() => {
        setIsConfirmationLoading(true);
        return Api.statements.deleteConfirmStatement(statement.id).then((result) => {
            refreshStatementItemCache().then(() => setIsConfirmationLoading(false));
            return result;
        });
    }, [ statement, refreshStatementItemCache ]);

    return {
        statementData: statement,
        isEditable: isEditable(statement),
        isBlocked: isBlocked(statement),
        isPaid: isPaid(statement),
        isWriteTaxes: useHasPermission('write-taxes'),
        isCompletedPay,
        isStatementLoading: isLoading,
        isStatementFetching: isFetching,
        isStatementError: isError,
        isConfirmationLoading,
        availableSelectDeduction,
        error,
        deductionsGroupedByStatus,
        getGroupDeduction,
        refreshStatementItemCache,
        selectedDeductions,
        selectDeductionCount: countSelected(selectedDeductions),
        onToggleDeduction,
        onToggleDeductions,
        onSelectAllDeductions,
        onChangeDeductionsFilter: setDeductionFilter,
        needCompensationCompany,
        setNeedCompensationCompany,
        isActiveDeductionsFilter,
        isSelectAllDeductions,
        confirmStatement,
        deleteConfirmStatement,
        deductionFilter,
        downloadInvoice,
        isDownloading,
        STATUSES_IDS: STATEMENT_STATUSES,
        isOpenAboutPaymentTax,
        setIsOpenAboutPaymentTax
    }
}
