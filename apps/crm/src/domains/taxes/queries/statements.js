import { useQueryClient, useQuery } from 'react-query';

import Api from 'api/client';
import { CACHE_STATEMETS_LIST } from 'domains/common/cache-keys';

import { META } from '../constants';
import { useFilter } from '../helpers/filters';

/**
 * @returns {{
 *  isLoading: boolean,
 *  filter: ({per_page: number, page: number, tax_id, order: string}|*),
 *  isError: boolean,
 *  meta: (*|{}),
 *  getTaxById: ((function(*=): (null|*))|*),
 *  statements: (*|*[]),
 *  isFetching: boolean,
 *  updateFilter: ((function(*): void)),
 *  error: unknown
 *  }}
 */
export const useStatements = () => {
    const queryClient = useQueryClient();

    const { filter, filterFull, updateFilter } = useFilter();

    /**
     * Получить ведомость по id
     * @param id
     * @returns {null|*}
     */
    const getStatementById = (id) => {
        if (!id) {
            return null;
        }

        const data = queryClient.getQueryData([ CACHE_STATEMETS_LIST, filter ]) || {};

        return data?.items?.find(item => item.id === id) || null;
    }

    const { data, error, isLoading, isFetching, isError } = useQuery(
        [ CACHE_STATEMETS_LIST, filter ],
        () => Api.statements.fetchStatementsList({ ...filter })
    );

    return {
        statements: data?.items || [],
        meta: data?.meta || META,
        isLoading, isFetching, isError, error,
        filter,
        filterFull,
        updateFilter,
        getStatementById
    }
}
