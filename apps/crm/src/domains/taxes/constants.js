export const STATEMENT_STATUSES = {
    formed: 1,
    wait_payment: 2,
    payment: 3,
    paid: 4
}

export const STATEMENT_STATUSES_STYLE = {
    1: 'formed',
    2: 'warning',
    3: 'info',
    4: 'success',
}
 export const STATEMENT_DEDUCTION_STATUSES = {
     cannot_pay: 'cannot_pay',
     ready_pay: 'ready_pay',
     not_enough_money: 'not_enough_money'
 }
export const STATEMENT_DEDUCTION_COMPLETED_STATUSES = {
    paid: 'paid',
    not_paid: 'not_paid',
    declined: 'declined'
}

export const FILTER = {
    order: '-period',
    page: 1,
    per_page: 30
};

export const META = {
    page: 1,
    current_page: 1,
    per_page: 30
}
