export function Total (data = {}) {
    this.preAmount = { title: 'Удержано', value: data.pre_amount };
    this.taxAmount = { title: 'Начислен налог', value: data.tax_amount };
    this.paidAmount = { title: 'Уплачен налог', value: data.paid_amount };
}

Total.prototype.getTotals = function() {
    const preTotals = [];

    for (let total in this) {
        if (typeof this[total].value === 'number') {
            preTotals.push(this[total])
        }
    }

    return preTotals;
}
