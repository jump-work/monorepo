import { useMutation, useQueryClient } from 'react-query';

import { CACHE_STATEMETS_ITEM } from 'domains/common/cache-keys';
import Api from 'api/client';

export const useRefreshStatement = (onSuccess, onError) => {
    const queryClient = useQueryClient();

    const { mutate, isLoading, isError, error } = useMutation(
        ({id}) => Api.statements.refreshStatement(id), {
            onError,
            onSuccess: ({ item }) => {
                queryClient.setQueryData([ CACHE_STATEMETS_ITEM, String(item?.id) ], item);
                queryClient.setQueryDefaults([ CACHE_STATEMETS_ITEM, String(item?.id)], {refetchInterval: 5000, enabled: true});

                onSuccess();
            },
        }
    )

    return {
        onRefreshStatement: mutate,
        mutateRefreshStatementHasError: isError,
        isRefreshingStatementMutating: isLoading,
        mutateRefreshStatementError: error,
    }
}
