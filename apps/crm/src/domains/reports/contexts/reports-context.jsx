import React, {createContext, useContext} from 'react';
import PropTypes from 'prop-types';

import { useInitialReports } from '../services';

const NewPaymentContext = createContext(null);

export const useReportsContext = () => {
    return useContext(NewPaymentContext);
}

const ReportsContextProvider = ({ children }) => {
    const contextValues = useInitialReports();

    return (
        <NewPaymentContext.Provider value={contextValues}>
            { children }
        </NewPaymentContext.Provider>
    )
}

ReportsContextProvider.propTypes = {
    children: PropTypes.any
}

export default ReportsContextProvider;
