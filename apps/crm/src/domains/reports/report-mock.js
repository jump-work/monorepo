export const useAgents = (filter) => {
    return {
        filter,
        agents: [
            {
                'id': 109,
                'drivers_count': 129,
                'count': 15051,
                'count_non_cash': 13151,
                'sum_non_cash': 5242725,
                'sum': 7164554,
                'commission': 277758.74,
                'name': 'ООО Ситимобил апи'
            },
            {
                'id': 106,
                'drivers_count': 129,
                'count': 15049,
                'count_non_cash': 13149,
                'sum_non_cash': 5242428,
                'sum': 7163834,
                'commission': 277734.64,
                'name': 'ООО Ситимобил Данные от ОМ-Такси'
            }
        ],
        isLoading: false
    }
}
