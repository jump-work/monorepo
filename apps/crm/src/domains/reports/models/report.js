import { useMemo } from 'react';

import { chartsByReport, isValid, reportName, oldLinkReports } from '../services';

export const EMPTY_ID = -1;

export const useReport = (type) => {
    return useMemo(() => {
        return new Report(type);
    }, [ type ]);
}

class Report {
    reportType = '';

    constructor (report) {
        this.reportType = report;
    }

    type () {
        return this.reportType;
    }

    isValid () {
        return isValid(this.type());
    }

    title () {
        return reportName(this.type());
    }

    charts () {
        return chartsByReport(this.type());
    }

    isLoadingCharts (loader) {
        return this.charts().reduce((acc, chart) => {
            return acc + ((loader[chart] ?? false) ? 1 : 0)
        }, 0) > 0;
    }

    oldLinkReport () {
        return oldLinkReports(this.type());
    }
}

export default Report;
