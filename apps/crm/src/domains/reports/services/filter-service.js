import { useMemo } from 'react';
import { useLocation } from 'react-router-dom';
import { parse } from 'query-string';

import { getPeriodOfPath } from 'utils/get-period-of-path';

const parseFilter = (hash) => {
    const path = parse(hash);

    return {...path, ...getPeriodOfPath(path?.period || path)};
}

export const useReportFilter = () => {
    const { hash } = useLocation();

    return useMemo(() => {
        return parseFilter(hash);
    }, [ hash ]);
}
