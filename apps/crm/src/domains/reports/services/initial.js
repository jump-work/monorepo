import { useState } from 'react';

export const useInitialReports = () => {
    const [ isLoadingDetails, setIsLoadingDetails ] = useState(false);
    const [ reportDownloadLinks, setReportDownloadLinks ] = useState([]);

    return {
        isLoadingDetails,
        setIsLoadingDetails,
        reportDownloadLinks,
        setReportDownloadLinks
    }
}
