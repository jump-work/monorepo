import { useQuery } from 'react-query';

import Api from 'api/client';
import { CACHE_DASHBOARD_CHART } from 'domains/common/cache-keys';

const DASHBOARD_TAXI = 'taxi';
const DASHBOARD_WORK = 'work';

const DASHBOARD_TAXI_CHARTS = [
    'aggregators_payments',
    'aggregators_trips',
    'aggregators_commissions',
    'drivers_payments',
    'drivers_commissions',
    'drivers_registrations',
    'drivers_activities',
];

const DASHBOARD_WORK_CHARTS = [
    'delivery_bids',
    'drivers_payments',
]

const charts = {
    [DASHBOARD_TAXI]: DASHBOARD_TAXI_CHARTS,
    [DASHBOARD_WORK]: DASHBOARD_WORK_CHARTS,
}

const chartsByDashboard = (chart) => {
    return charts[chart] ?? [];
}

export const isLoadingCharts = (type, loader) => {
    return chartsByDashboard(type).reduce((acc, chart) => {
        return acc + ((loader[chart] ?? false) ? 1 : 0)
    }, 0) > 0;
}

export const useChartDashboard = (chart, filter) => {
    const cacheKey = CACHE_DASHBOARD_CHART + '_' + chart + '_root';

    const { data, isLoading, isFetching, isError, error } = useQuery(
        [cacheKey, filter],
        () => Api.dashboard.fetchChart(chart, filter),
        {
            refetchOnWindowFocus: false,
            staleTime: 300000,
        }
    );

    return {
        data,
        isLoading,
        isFetching,
        isError,
        error,
    }
}
