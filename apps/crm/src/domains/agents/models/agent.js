export function Agent (data = {}) {
    this.original = data;

    this.id = data?.id ?? '';
    this.name = data?.name ?? '';
    this.inn = data?.inn ?? '';
    this.ogrn = data?.ogrn ?? '';
    this.address = data?.address ?? '';
}

Agent.prototype.getId = function() {
    return this.id;
}

Agent.prototype.getName = function() {
    return this.name;
}

Agent.prototype.getInn = function() {
    return this.inn;
}

Agent.prototype.getOgrn = function() {
    return this.ogrn;
}

Agent.prototype.getAddress = function() {
    return this.address;
}

Agent.prototype.getMobileDescription = function(prefix = '') {
    const description = [
        this.getInn(),
        this.getOgrn(),
        this.getAddress(),
    ];

    return description.filter(Boolean).join(prefix);
}
