import { useMutation, useQueryClient } from 'react-query';

import Api from 'api/client';

import { addServerErrors } from 'utils/add-server-errors';

import { CACHE_AGENTS_LIST, CACHE_AGENT } from 'domains/common/cache-keys';

import { useFilter } from 'domains/agents';

export const useCreate = (onSuccess, setError) => {
    const queryClient = useQueryClient();

    const refetchAgentsList = () => queryClient.refetchQueries([CACHE_AGENTS_LIST]);

    const { mutate: onCreate, isLoading: isCreating, isError, error } = useMutation(
        (data) => Api.agents.create(data), {
            onSuccess: ({ item }) => {
                refetchAgentsList();
                onSuccess(item);
            },

            onError: (error) => {
                addServerErrors(error?.errorData?.fields || [], setError);
            }
        }
    )

    return {
        onCreate,
        isCreating,
        isError,
        error,
    }
}

export const useUpdate = (id, onSuccess, setError) => {
    const queryClient = useQueryClient();

    const { filter } = useFilter();

    const { isLoading: isUpdating, mutate: onUpdate, error, isError } = useMutation(
        (data) => Api.agents.update(id, data), {
            onSuccess: async () => {
                await queryClient.refetchQueries([CACHE_AGENTS_LIST, filter])
                await queryClient.refetchQueries([CACHE_AGENT, id]);
                await onSuccess();
            },

            onError: (error) => {
                addServerErrors(error?.errorData?.fields || [], setError);
            }
        }
    )

    return {
        onUpdate,
        isUpdating,
        isError,
        error,
    }
}

export const useDelete = (id, onSuccess) => {
    const queryClient = useQueryClient();

    const { filter } = useFilter();

    const { isLoading: isDeleting, mutate: onDelete, isError, error } = useMutation(
        () => Api.agents.delete(id), {
            onSuccess: async () => {
                await queryClient.refetchQueries([CACHE_AGENTS_LIST, filter]);
                await onSuccess();
            }
        }
    )

    return {
        onDelete,
        isDeleting,
        isError,
        error,
    }
}
