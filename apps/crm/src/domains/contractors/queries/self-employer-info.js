import { useState } from 'react';
import { useQuery, useQueryClient } from 'react-query';
import { CancelToken } from 'axios';

import Api from 'api/client';
import { CACHE_CONTRACTOR_SELF_EMPLOYER_INFO } from 'domains/common/cache-keys';

export const useSelfEmployerInfo = (contractorId, isSelfEmployed) => {
    const queryClient = useQueryClient();

    const REFETCH_INTERVAL_SECONDS = 2000;
    const [ refetchInterval, setRefetchInterval ] = useState(false);

    const { data, isLoading, isFetching } = useQuery([ CACHE_CONTRACTOR_SELF_EMPLOYER_INFO, contractorId ], () => {
        const source = CancelToken.source()

        const promise = Api.contractors.fetchSelfEmployerInfo(contractorId, source.token);

        promise.cancel = () => source.cancel();

        return promise;
    }, {
        onSuccess: ({ item }) => {
            if(item?.sync?.in_process) {
                setRefetchInterval(REFETCH_INTERVAL_SECONDS);
            } else {
                setRefetchInterval(false);
            }
        },
        onError: () => {
            setRefetchInterval(false);
        },
        enabled: Boolean(contractorId) && Boolean(isSelfEmployed),
        refetchOnWindowFocus: false,
        refetchInterval,
    })

    const getSelfEmployerInfo = () => queryClient.refetchQueries([CACHE_CONTRACTOR_SELF_EMPLOYER_INFO, contractorId]);

    const clearSelfEmployerInfo = () => queryClient.removeQueries([CACHE_CONTRACTOR_SELF_EMPLOYER_INFO, contractorId]);

    return {
        getSelfEmployerInfo,
        clearSelfEmployerInfo,
        selfEmployerData: data?.item || {},
        isSelfEmployerInfoLoading: isLoading,
        isSelfEmployerInfoFetching: isFetching,
        cancelSelfEmployerInfo: () => queryClient.cancelQueries([CACHE_CONTRACTOR_SELF_EMPLOYER_INFO, contractorId]),
    }
}
