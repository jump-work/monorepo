import { useEffect } from 'react';

import { useTestMode } from 'hooks/use-testing';

import { useSelfEmployerInfo } from '../queries';
import { useSelfEmployerSync } from '../mutators';

export const useSelfEmployer = (contractorId, isSelfEmployed) => {
    const { isTestMode } = useTestMode();

    const {
        selfEmployerData,
        getSelfEmployerInfo,
        clearSelfEmployerInfo,
        isSelfEmployerInfoLoading,
        isSelfEmployerInfoFetching,
        cancelSelfEmployerInfo
    } = useSelfEmployerInfo(contractorId, (isSelfEmployed && isTestMode));

    const { sync, abilities, messages, is_can_pay_taxes, is_verified } = selfEmployerData;

    const syncInProcess = sync?.in_process;
    const canCheck = sync?.in_process === false && abilities?.can_check;

    const { onSelfEmployerSync, isSelfEmployerSyncMutating } = useSelfEmployerSync(getSelfEmployerInfo);

    useEffect(() => {
        if(!isSelfEmployed) {
            clearSelfEmployerInfo();
        }
    }, [clearSelfEmployerInfo, isSelfEmployed]);

    useEffect(() => {
        return cancelSelfEmployerInfo;
    }, []); // eslint-disable-line

    return {
        onSelfEmployerSync,
        clearSelfEmployerInfo,
        isSelfEmployerSyncMutating,
        isSelfEmployerInfoLoading,
        isSelfEmployerInfoFetching,
        isConfirmed: is_can_pay_taxes && is_verified && !syncInProcess,
        syncInProcess,
        canCheck,
        messages,
    }
}
