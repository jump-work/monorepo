import { useState } from 'react';
import { useQuery } from 'react-query';

import Api from 'api/client';
import {
    CACHE_CONTRACTOR_REQUISITES,
    CACHE_CONTRACTOR_SEARCH,
    CACHE_CONTRACTOR_TAXES,
    CACHE_CONTRACTOR_PERMISSIONS,
} from 'domains/common/cache-keys';
import { validatePhone } from 'utils/helpers';

export const useSearchContractor = () => {
    const [ value, setValue ] = useState(null);
    const [ isPhone, setIsPhone ] = useState(false);
    const [ needCreate, setNeedCreate ] = useState(false);

    const onFetch = async (value) => {
        return await Api.contractors.searchContractorForPayments({ search: value })
    }

    const onClearState = () => {
        setIsPhone(false)
        setNeedCreate(false)
        setValue(null);
    }

    const { data, isLoading, isFetching, isError, error } = useQuery([CACHE_CONTRACTOR_SEARCH, value], () => onFetch(value), {
        onSuccess: ({ items }) => {
            if(validatePhone(value)) {
                setIsPhone(true)
            }

            if(items.length === 0) {
                setNeedCreate(true)
            }
        },
        enabled:  Boolean(value),
        refetchOnWindowFocus: false,
    })

    return {
        result: data?.items || [],
        onSearch: setValue,
        onClearState,
        isPhone,
        needCreate,
        isLoading,
        isFetching,
        isError,
        error,
    }
}

export const useRequisites = (id) => {
    const onFetch = async (id) => {
        const resp = await Api.contractorRequisites.fetchRequisitesList(id, {per_page: 30, type_id: 8})

        return resp?.items || [];
    }

    const { data, isLoading, isFetching } = useQuery([CACHE_CONTRACTOR_REQUISITES, id], () => onFetch(id), {
        enabled: Boolean(id),
        refetchOnWindowFocus: false
    })

    return {
        requisites: data || [],
        isLoading,
        isFetching,
    }
}

export const useContractorTaxes = (id) => {
    const onFetch = async () => {
        const resp = await Api.contractors.fetchTaxes(id);

        return resp?.data || {};
    }

    const { data, isLoading, isFetching } = useQuery([CACHE_CONTRACTOR_TAXES, id], () => onFetch(), {
        enabled: Boolean(id),
    })

    return {
        data,
        isLoading,
        isFetching,
    }
}

export const useContractorPermissions = (contractorId, permissionId) => {
    const onFetch = async () => {
        const resp = await Api.contractorPermissions.fetch(contractorId, permissionId);

        return resp?.item || {}
    }

    const { data, isLoading, isFetching, isError, error } = useQuery(
        [CACHE_CONTRACTOR_PERMISSIONS, contractorId],
        () => onFetch(), {
            enabled: Boolean(contractorId),
        }
    )

    return {
        isEnable: data?.value || false,
        data,
        isLoading,
        isFetching,
        isError,
        error,
    }
}
