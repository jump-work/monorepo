import { useState } from 'react';
import { useSelector } from 'react-redux';

import { getFeatures } from 'app/init/store/selectors';
import { useHasPermission } from 'app/profile/store/selectors';

export const usePermissions = () => {
    const canWriteUsers = useHasPermission('write-contractor');
    const canReadAllPayments = useHasPermission('read-payment');
    const canReadSelfPayments = useHasPermission('read-self-payment');
    const canReadPayments = canReadAllPayments || canReadSelfPayments;
    const canReadOrders = useHasPermission('read-order');
    const canReadTaxes = useHasPermission('read-taxes');

    return {
        canWriteUsers,
        canReadPayments,
        canReadOrders,
        canReadTaxes,
    }
}

export const usePayTaxes = () => {
    const features = useSelector(getFeatures);
    const [ showCompanyAgreesPayTaxes, updateShowCompanyAgreesPayTaxes ] = useState(false);

    const setShowCompanyAgreesPayTaxes = (option) => {
        updateShowCompanyAgreesPayTaxes(option && option?.value === 2);
    }

    return {
        showCompanyAgreesPayTaxes,
        setShowCompanyAgreesPayTaxes,
        isPaymentOfTaxes: features?.payment_of_taxes || false,
        paymentOfTaxesDefault: features?.payment_of_taxes_default || false
    }
}
