import { useMutation } from 'react-query';

import Api from 'api/client';

export const useSelfEmployerSync = (onSuccess) => {
    const { mutate, isLoading, isError, error } = useMutation(
        (id) => Api.contractors.selfEmployerSync(id), {
            onSuccess
        }
    )

    return {
        onSelfEmployerSync: mutate,
        isSelfEmployerSyncMutating: isLoading,
        mutateSelfEmployerSyncHasError: isError,
        mutateSelfEmployerSyncError: error,
    }
}
