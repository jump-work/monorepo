import { useState } from 'react';
import { useQuery } from 'react-query';

import Api from 'api/client';

import { CACHE_GROUPS } from 'domains/common/cache-keys';

export const useGroups = () => {
    const [ defaultGroup, setDefaultGroup ] = useState({});

    const onFetch = async () => {
        const resp = await Api.groups.fetchList();
        const items = resp?.items || [];

        if(items.length > 0) { setDefaultGroup(items[0]) }

        return items;
    }

    const { data, isLoading, isFetching } =  useQuery([CACHE_GROUPS], onFetch, { refetchOnWindowFocus: false });

    return {
        groups: data || [],
        defaultGroup,
        isLoading,
        isFetching,
    }
}
