import { useState } from 'react';
import { useMutation, useQueryClient } from 'react-query';

import Api from 'api/client';
import { addServerErrors } from 'utils/add-server-errors';
import { CACHE_INTEGRATIONS_LIST, CACHE_INTEGRATION_ITEM } from 'domains/common/cache-keys';

import { useFilter, servicesIdNeedConfirmation } from './integrations-service';

export const useCreateMutator = (onSuccess, setPayloadSubmit, setError) => {
    const [ needConfirmation, setNeedConfirmation ] = useState(false);

    const queryClient = useQueryClient();

    const refetchIntegrationsList = () => queryClient.refetchQueries([CACHE_INTEGRATIONS_LIST]);

    const { isLoading: isCreating, mutate: onCreate, isError, error } = useMutation(
        (data) => Api.integrations.create(data), {
            onSuccess: ({ item }) => {
                if(servicesIdNeedConfirmation.some(id => item?.aggregator?.id === id)) {
                    setNeedConfirmation(true);
                    setPayloadSubmit(item);

                    return refetchIntegrationsList();
                }

                refetchIntegrationsList();
                onSuccess(item);
            },

            onError: (error) => {
                addServerErrors(error?.errorData?.fields || [], setError);
            },

            onSettled: () => {
                setNeedConfirmation(false);
            }
        }
    )

    return {
        needConfirmationAfterCreation: needConfirmation,
        onCreate,
        isCreating,
        isError,
        error,
    }
}

export const useUpdateMutator = (id, onSuccess, setPayloadSubmit, setError) => {
    const [ needConfirmation, setNeedConfirmation ] = useState(false);

    const queryClient = useQueryClient();

    const { filter } = useFilter();

    const refetchIntegrationList = () => queryClient.refetchQueries([CACHE_INTEGRATIONS_LIST, filter]);
    const refetchIntegrationItem = () => queryClient.refetchQueries([CACHE_INTEGRATION_ITEM, id]);

    const { isLoading: isUpdating, mutate: onUpdate, error, isError } = useMutation(
        data => Api.integrations.update(id, data),
        {
            onSuccess: ({ item }) => {
                if(item.is_connected === false) {
                    setNeedConfirmation(true);
                    setPayloadSubmit(item);

                    refetchIntegrationList();
                    refetchIntegrationItem();

                    return;
                }

                refetchIntegrationList();
                refetchIntegrationItem();
                onSuccess();
            },

            onError: (error) => {
                addServerErrors(error?.errorData?.fields || [], setError);
            },

            onSettled: () => {
                setNeedConfirmation(false);
            }
        });

    return {
        needConfirmationAfterUpdate: needConfirmation,
        isUpdating,
        onUpdate,
        error,
        isError,
    }
}

export const useDeleteMutator = (id, onSuccess) => {
    const queryClient = useQueryClient();

    const { filter } = useFilter();

    const { isLoading: isDeleting, mutate: onDelete, isSuccess, isError, error } = useMutation(
        () => Api.integrations.delete(id),
        {
            onSuccess: () => {
                const mutationData = ({ items, ...rest }) => ({
                    items: items.filter(item => item.id !== id),
                    ...rest,
                });

                queryClient.setQueryData([CACHE_INTEGRATIONS_LIST, filter], mutationData);

                onSuccess();
            }
        }
    )

    return { isDeleting, onDelete, isSuccess, isError, error }
}

export const useChangeStatus = (id) => {
    const queryClient = useQueryClient();

    const { filter } = useFilter();

    const { mutate, isLoading, isError, error } = useMutation(
        (value) => Api.integrations.changeStatus(id, value), {
            onSuccess: ({ item: updatedItem }) => {
                queryClient.setQueryData([CACHE_INTEGRATIONS_LIST, filter], ({ items, ...rest }) => ({
                    items: items.map(item => item.id === updatedItem.id ? updatedItem : item),
                    ...rest,
                }));
            }
        }
    )

    return {
        mutate,
        isLoading,
        isError,
        error,
    }
}

export const useRequestOtp = () => {
    return useMutation((id) => Api.integrations.requestOtp(id))
}

export const useConfirmOtp = (id, onSuccess) => {
    const queryClient = useQueryClient();

    const { filter } = useFilter();

    const { mutate, isLoading, isError, error } = useMutation(
        ({ id, code }) => Api.integrations.confirmOtp(id, code), {
            onSuccess: async () => {
                await queryClient.refetchQueries([CACHE_INTEGRATIONS_LIST, filter]);
                await queryClient.refetchQueries([CACHE_INTEGRATION_ITEM, id]);
                await onSuccess();
            },
        }
    )

    return {
        onConfirmOtp: mutate,
        isConfirming: isLoading,
        isError,
        error,
    }
}

