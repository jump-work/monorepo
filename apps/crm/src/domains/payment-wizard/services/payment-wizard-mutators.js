import { useMutation, useQueryClient } from 'react-query';

import { addServerErrors } from 'utils/add-server-errors';

import { CACHE_RULES_PAYMENTS, CACHE_RULE_PAYMENT } from 'domains/common/cache-keys';

import Api from 'api/client';

export const useCreateRulePayment = (onSuccess, setError) => {
    const queryClient = useQueryClient();

    const { mutate, isLoading, isError, error } = useMutation(
        (data) => Api.paymentWizard.create(data), {
        onSuccess: async ({ item }) => {
            await queryClient.setQueryData([CACHE_RULES_PAYMENTS], (oldData) => ([ item, ...oldData ]));
            await queryClient.refetchQueries([CACHE_RULES_PAYMENTS]);

            await onSuccess();
        },
        onError: (error) => {
            addServerErrors(error?.errorData?.fields || [], setError);
        },
    })

    return {
        onCreate: mutate,
        isCreating: isLoading,
        isError,
        error,
    }
}

export const useUpdateRulePayment = (onSuccess, setError) => {
    const queryClient = useQueryClient();

    const { mutate, isLoading, isError, error } = useMutation(
        ({ id, data }) => Api.paymentWizard.update(id, data), {
            onSuccess: async ({ item: updatedItem }) => {
                await queryClient.refetchQueries([CACHE_RULES_PAYMENTS]);
                await queryClient.refetchQueries([CACHE_RULE_PAYMENT, updatedItem.id]);

                await onSuccess();
            },
            onError: (error) => {
                addServerErrors(error?.errorData?.fields || [], setError);
            },
        }
    )

    return {
        onUpdate: mutate,
        isUpdating: isLoading,
        isError,
        error,
    }
}

export const useDeleteRulePayment = (onSuccess) => {
    const queryClient = useQueryClient();

    const { mutate, isLoading, isError, error } = useMutation((id) => Api.paymentWizard.delete(id), {
            onSuccess: async () => {
                await queryClient.refetchQueries([CACHE_RULES_PAYMENTS]);
                await onSuccess();
            }
        })

    return {
        onDelete: mutate,
        isLoading,
        isError,
        error,
    }
}

export const useUpdateRulePriorities = () => {
    const queryClient = useQueryClient();

    const { mutate, isLoading, isError, error } = useMutation(
        (ids) => Api.paymentWizard.updatePriorities(ids), {
            onSuccess: () => queryClient.refetchQueries([CACHE_RULES_PAYMENTS])
        }
    )

    return {
        onUpdatePriorities: mutate,
        isLoading,
        isError,
        error,
    }
}
