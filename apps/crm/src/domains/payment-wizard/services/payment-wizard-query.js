import { useState } from 'react';
import { useQuery, useQueryClient } from 'react-query';

import { CACHE_RULES_PAYMENTS, CACHE_CONDITIONS_RULE, CACHE_RULE_PAYMENT } from 'domains/common/cache-keys';

import Api from 'api/client';

export const useConditions = () => {
    const { data, isLoading, isFetching, isError, error } = useQuery(
        [CACHE_CONDITIONS_RULE], () => Api.paymentWizard.fetchConditionsRule()
    )

    return {
        conditions: data?.conditions || [],
        results: data?.results || [],
        isLoading,
        isFetching,
        isError,
        error,
    }
}

export const useRulesPayments = () => {
    const queryClient = useQueryClient();

    const [ rules, setRulesPriority ] = useState(queryClient.getQueryData([CACHE_RULES_PAYMENTS]) || []);

    const { isLoading, isError, isFetching, error } = useQuery(
        [CACHE_RULES_PAYMENTS], async () => {
            const result = await Api.paymentWizard.fetchRules();

            setRulesPriority(result?.items || [])

            return result?.items || [];
        }
    );

    return {
        rules,
        setRulesPriority,
        isLoading,
        isFetching,
        isError,
        error,
    }
}

export const useRulePayment = (ruleId) => {
    const { data, isLoading, isFetching, isError, error } = useQuery(
        [CACHE_RULE_PAYMENT, ruleId], () => Api.paymentWizard.fetchRule(ruleId), {
            enabled: Boolean(ruleId),
        }
    )

    return {
        rule: data?.item,
        isLoading,
        isFetching,
        isError,
        error,
    }
}
