import { Bank } from 'app/general-settings/components/payment-wizard/result-forms/bank';
import { Commission } from 'app/general-settings/components/payment-wizard/result-forms/commission';

export const ResultForms = {
    bank: Bank,
    commission: Commission,
}
