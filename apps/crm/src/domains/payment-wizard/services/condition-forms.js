import { Amount } from 'app/general-settings/components/payment-wizard/condition-forms/amount';

export const ConditionForms = {
    amount_max: Amount,
    amount_min: Amount,
    bank_tinkoff: null,
}
