import { useState, useEffect } from 'react';

export const useGetHeight = (ref, initialHeight) => {
    const [ height, setHeight ] = useState(initialHeight);

    useEffect(() => setHeight(ref.current.clientHeight)); // eslint-disable-line

    return { height }
};
