import { useCallback, useState } from 'react';

export const usePhoneField = (value) => {
    const [ isPhoneEntered, setIsPhoneEntered ] = useState(false);
    const [ maskPhone, setMaskPhone ] = useState(null);

    const phonePointers = ['+', '7', '8', '9'];

    const onChangeField = (value) => {
        for (let item of phonePointers) {
            if(value.startsWith(item)) {
                setIsPhoneEntered(true);
                getMaskPhone(item, value);
                break;
            } else {
                setIsPhoneEntered(false);
            }
        }
    }

    const getNeededMask = useCallback((value) => value && (value.match(/_/g) || []).length !== 10, [])

    const getMaskPhone = useCallback((pointer, value) => {
        const masks = {
            '+': '+79999999999',
            '7': '+79999999999',
            '8': '89999999999',
            '9': '+79999999999',
        };

        setMaskPhone(getNeededMask(value) ? masks[pointer] : '')
    }, [ getNeededMask ])

    const prepareValue = (value) => {
        if(!getNeededMask(value)) { return '' }

        return value;
    }

    const onReset = () => {
        setIsPhoneEntered(false);
        setMaskPhone(null);
    }

    return {
        isPhoneEntered,
        onReset,
        onChangeField,
        maskPhone,
        valueField: prepareValue(value),
    }
}
