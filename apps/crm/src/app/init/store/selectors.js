const formatSymbol = (symbol) => String.fromCharCode(symbol.replace(/[^\d]/ig, ''));

export const isLoading = (state) => state?.app?.isLoading ?? true;
export const isAuthorized = (state) => state?.app?.isAuthorized ?? false;
export const getError = (state) => state?.app?.fatalError ?? null;
export const intercomConfig = (state) => state?.app?.config?.intercom ?? {};
export const getMenu = (state) => state?.app?.config?.menu ?? [];
export const getFeatures = (state) => state?.profile?.data?.features ?? {};
export const getCurrency = (state) => formatSymbol(state?.app?.config?.currency?.symbol ?? '');
export const getDictionaries = (state) => state?.app?.dictionaries?.items ?? {};
export const getDynamicDictionaries = (state) => state?.app?.dynamicDictionaries ?? {};
export const getProfile = (state) => state?.profile?.data || {}
