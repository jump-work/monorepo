import { useRequisites } from 'domains/contractors';
import { useAgentsMy } from 'domains/agents';

export const useInitialBanksFields = (id) => {
    const { requisites, ...otherRequisites } = useRequisites(id);
    const { agents, defaultAgent, ...otherAgents } = useAgentsMy();

    const isLoadingRequisites = otherRequisites.isLoading || otherRequisites.isFetching;
    const isLoadingAgents = otherAgents.isLoading || otherAgents.isFetching;

    return {
        requisites,
        agents,
        defaultAgent,
        isLoading: isLoadingRequisites || isLoadingAgents,
    }
}
