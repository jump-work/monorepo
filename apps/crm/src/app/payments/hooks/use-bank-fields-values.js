import { useCallback } from 'react';
import { useFormContext } from 'react-hook-form';

const REQUIRED_BANKS_FIELDS = [
    'agent_id',
    'requisite',
    'amount',
]

export const useBankFieldsValues = () => {
    const { watch } = useFormContext();

    return useCallback(() => REQUIRED_BANKS_FIELDS.every(field => watch()[field]), [watch]);
}
