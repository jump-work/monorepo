import React, { Fragment, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { STEP_OTP, STEP_SUCCESS } from 'domains/integrations';

import { useInitialForm } from './editor/editor-service';
import { Success, Otp } from './services-forms/wheely/steps';
import { IntegrationForm } from './integration-form';

const steps = {
    [STEP_OTP]: Otp,
    [STEP_SUCCESS]: Success
}

export const IntegrationModal = ({ onDismiss, id }) => {
    const [ step, setStep ] = useState(null);
    const [ payload, setPayload ] = useState();

    const Step = steps[step];

    const {
        needConfirmation, responseAtMutation, onResetCacheDefaultValues, ...rest
    } = useInitialForm(id || payload?.id, onDismiss);

    useEffect(() => { if(needConfirmation) setStep(STEP_OTP) }, [ needConfirmation ]);

    const onGoStep = (step, payload) => {
        setStep(step);
        setPayload(payload);
        onResetCacheDefaultValues(payload);
    }

    return (
        <Fragment>
            {
                !Step &&
                <IntegrationForm
                    onDismiss={onDismiss}
                    {...rest}
                />
            }

            {
                Step &&
                <Step
                    payload={responseAtMutation || payload}
                    onGoStep={onGoStep}
                    onDismiss={onDismiss}
                />
            }
        </Fragment>
    )
}

IntegrationModal.propTypes = {
    onDismiss: PropTypes.func,
    id: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
    ]),
}
