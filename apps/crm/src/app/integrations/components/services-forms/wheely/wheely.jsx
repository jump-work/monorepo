import React from 'react';
import PropTypes from 'prop-types';

import { InputController } from 'components/index'

import cx from '../../integration-form.module.scss';

export const Wheely = ({ control, errors }) => {
    return (
        <InputController
            name='phone'
            label='Номер телефона'
            control={control}
            className={cx.middle}
            mask='+79999999999'
            error={errors?.phone?.message}
            isRequired
        />
    )
}

Wheely.propTypes = {
    control: PropTypes.object.isRequired,
    errors: PropTypes.object,
    editForm: PropTypes.bool,
}
