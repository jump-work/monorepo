import React from 'react';
import { Icons24, MobileHeader } from '@jump-work/uikit';
import { useIfMediaScreen } from '@jump-work/uikit/lib/hooks';

import { useMenu } from 'components/menu';

const HeaderStatementsPage = () => {
    const isMobile = useIfMediaScreen();
    const { onExpandedMobile } = useMenu();

    return (
        <MobileHeader header='Налоги самозанятых'>
            { isMobile && <MobileHeader.Button icon={<Icons24.IconMenu/>} onClick={onExpandedMobile}/> }
        </MobileHeader>
    )
}

export default HeaderStatementsPage;
