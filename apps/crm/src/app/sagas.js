import { fork } from 'redux-saga/effects';

import authWatcher from 'app/auth/store/sagas';
import contractorRequisitesWatcher from 'app/contractors/pages/requisites/store/sagas';
import contractorOrdersWatcher from 'app/contractors/pages/orders/store/sagas';
import contractorsWatcher from 'app/contractors/store/sagas';
import documentWatcher from 'app/documents/store/sagas';
import initWatcher from 'app/init/store/sagas';
import ordersWatcher from 'app/orders/store/sagas';
import profileWatcher from 'app/profile/store/sagas';
import settingsWatcher from 'app/general-settings/store/sagas';
import userWatcher from 'app/users/store/sagas';

export default function * root () {
    yield fork(authWatcher);
    yield fork(contractorRequisitesWatcher);
    yield fork(contractorOrdersWatcher);
    yield fork(contractorsWatcher);
    yield fork(documentWatcher);
    yield fork(initWatcher);
    yield fork(ordersWatcher);
    yield fork(profileWatcher);
    yield fork(settingsWatcher);
    yield fork(userWatcher);
}
