export const GENERAL_SETTINGS = '@@general-settings/GENERAL_SETTINGS';

export const SETTINGS_LEGAL_FORM = '@@general-settings/SETTINGS_LEGAL_FORM';
export const SETTINGS_PAYMENTS = '@@general-settings/SETTINGS_PAYMENTS';
export const SETTINGS_TAX = '@@general-settings/SETTINGS_TAX';

export const COMMISSIONS = '@@general-settings/COMMISSIONS';
export const LIMITS = '@@general-settings/LIMITS';

export const SUCCESS_SUBMIT_FORM = '@@general-settings/SUCCESS_SUBMIT_FORM';
