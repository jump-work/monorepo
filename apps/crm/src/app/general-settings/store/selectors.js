const getGeneralSettings = (state) => state?.settings?.items?.data ?? [];
const getGeneralSettingsIsRequest = (state) => state?.settings?.items?.isRequest ?? false;

const getLegalFormSettings = (state) => state?.settings?.legal_form?.data ?? [];
const getLegalFormSettingsIsRequest = (state) => state?.settings?.legal_form?.isRequest ?? false;

const getPaymentsSettings = (state) => state?.settings?.payments?.data ?? [];
const getPaymentsInvalidFields = (state) => state?.settings?.payments?.error?.errorData?.fields ?? [];
const getPaymentsCommission = (state) => state?.settings?.payments?.data?.commission ?? null;
const getPaymentsLimit = (state) => state?.settings?.payments?.data?.limit ?? null;
const getPaymentsSettingsTax = (state) => state?.settings?.payments?.data?.receipt_default_service ?? '';
const getPaymentsSettingsInterface = (state) => state?.settings?.payments?.data?.interfaces ?? []
const getPaymentsSettingsIsRequest = (state) => state?.settings?.payments?.isRequest ?? false;

const getTaxSettingsIsRequest = (state) => state?.settings?.tax?.isRequest ?? false;

const getFormState = state => {
    return {
        isSubmitting: state?.settings?.formState?.isSubmitting ?? false,
        isSubmitted: state?.settings?.formState?.isSubmitted ?? false,
    }
};


export const SettingsSelectors = {
    getGeneralSettings,
    getGeneralSettingsIsRequest,

    getLegalFormSettings,
    getLegalFormSettingsIsRequest,

    getPaymentsSettings,
    getPaymentsInvalidFields,
    getPaymentsCommission,
    getPaymentsLimit,
    getPaymentsSettingsTax,
    getPaymentsSettingsInterface,
    getPaymentsSettingsIsRequest,
    getTaxSettingsIsRequest,

    getFormState,
}
