import React from 'react';
import PropTypes from 'prop-types';

import { useMoney } from 'hooks/use-money';

import { InputController } from 'components/index';

import cx from '../common.module.scss';

export const Amount = ({ control, errors, data }) => {
    const { currency } = useMoney();

    return (
        <InputController
            type='number'
            name='condition.data.value'
            control={control}
            defaultValue={data?.value}
            placeholder={`0 ${currency}`}
            className={cx.small}
            error={errors?.condition?.data?.value?.message || ''}
            isRequired
        />
    )
}

Amount.propTypes = {
    control: PropTypes.object,
    data: PropTypes.object,
    errors: PropTypes.object,
}
