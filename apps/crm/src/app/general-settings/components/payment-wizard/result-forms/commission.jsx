import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { useMoney } from 'hooks/use-money';

import { SelectController, InputController } from 'components';

import cx from '../common.module.scss';

const Percent = ({ control, errors, data, placeholder, currency, children }) => {
    return (
        <div className={cx.commission}>
            <InputController
                type='number'
                name='result.data.percent_value'
                control={control}
                defaultValue={data?.percent_value || ''}
                className={cx.small}
                placeholder={placeholder}
                error={errors?.result?.data?.percent_value?.message || ''}
                isRequired
            />

            { children }

            <InputController
                type='number'
                label={`Мин. ${currency}`}
                name='result.data.fix_value'
                control={control}
                defaultValue={data?.fix_value}
                className={`${cx.min} ${cx.currency}`}
                placeholder={`0 ${currency}`}
                error={errors?.result?.data?.fix_value?.message || ''}
                isRequired
            />
        </div>
    )
}

const Fixed = ({ control, errors, data, placeholder, children }) => {
    return (
        <div className={cx.commission}>
            <InputController
                name='result.data.fix_value'
                control={control}
                defaultValue={data?.fix_value}
                className={cx.small}
                placeholder={placeholder}
                error={errors?.result?.data?.fix_value?.message || ''}
            />
            {children}
        </div>
    )
}

const forms = {
    percent: Percent,
    fixed: Fixed,
}

export const Commission = ({ control, errors, data }) => {
    const [ type, setType ] = useState(data?.type || 'percent');

    const { currency } = useMoney();

    const placeholder = `0 ${type === 'percent' ? '%' : currency}`;

    const commissionTypeDefaultValue = type === 'percent'
        ? { value: 'percent', label: '%' }
        : { value: 'fixed', label: currency }

    const options = [
        { value: 'percent', label: '%' },
        { value: 'fixed', label: currency },
    ]

    const buildForm = (type) => {
        if(!type) return null;

        const Component = forms[type];

        const props = {
            control,
            errors,
            currency,
            placeholder,
            data
        }

        return (
            <Component {...props}>
                <SelectController
                    name='result.data.type'
                    control={control}
                    options={options}
                    defaultValue={commissionTypeDefaultValue}
                    onSelect={setType}
                    error={errors?.result?.data?.type?.message || ''}
                    isRequired
                />
            </Component>
        )
    }

    return buildForm(type)
}

Percent.propTypes = {
    control: PropTypes.object.isRequired,
    errors: PropTypes.object,
    data: PropTypes.object,
    placeholder: PropTypes.string,
    currency: PropTypes.string,
    children: PropTypes.node,
}

Fixed.propTypes = {
    control: PropTypes.object.isRequired,
    errors: PropTypes.object,
    data: PropTypes.object,
    placeholder: PropTypes.string,
    children: PropTypes.node,
}

Commission.propTypes = {
    control: PropTypes.object.isRequired,
    errors: PropTypes.object,
    data: PropTypes.object,
}
