import GeneralSettingsPage from './pages/general-settings-page';
import LegalFormPage  from './pages/legal-form-page';
import PaymentsPage  from './pages/payments-page';
import PaymentWizard from './pages/payment-wizard';

export {
    GeneralSettingsPage,
    LegalFormPage,
    PaymentsPage,
    PaymentWizard,
}
