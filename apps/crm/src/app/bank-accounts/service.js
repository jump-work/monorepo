import { useEffect, useState } from 'react';
import { useQuery, useQueryClient } from 'react-query';

import { BANK_ACCOUNTS_FILTER } from './filters';
import { BankForms } from './components/bank-forms';

import Api from 'api/client';

const forms = {
    '2:4': BankForms.TinkoffOpenApi,
    '2:5': BankForms.TinkoffMoment,
    '2:6': BankForms.TinkoffSBP,
    '15:3': BankForms.AlfaBankApi,
    '16:5': BankForms.QiwiTransit,
    '16:13': BankForms.QiwiTransit,
    '17:5': BankForms.SberMoment,
    '18:5': BankForms.Kassa24,
    '20:5': BankForms.KzMoment,
}

export function selectForm(bankId, paymentTypeId) {
    const key = bankId + ':' + paymentTypeId;

    return forms[key] || null;
}

export const preparingDataForSubmit = (values) => {
    const isDefaultAgent = values?.is_default === undefined;

    return {
        ...values,
        bank_id: values?.bank_id?.value,
        agent_id: values?.agent_id?.value,
        payment_system_type_id: values?.payment_system_type_id?.value,
        is_default: isDefaultAgent ? true : values?.is_default,
    }
}

export const useModalState = (id) => {
    return useQuery(['bank_account', id], () => {
        return Promise.all([
            id ? Api.bankAccounts.fetchItem(id) : {},
            Api.agents.my({per_page: 100}),
            Api.bankAccounts.fetchBanksDictionary(),
        ]).then(data => {
            return {
                item: data[0].item,
                agents: data[1].items,
                banks: data[2].items
            }
        });
    }, {
        retry: false,
        refetchOnWindowFocus: false
    });
}

export const defaultFields = {
    name: '', login: '', password: '', terminal_id: '', is_default: false,
    short_name:'', inn: '', kpp: '', bik: '', rs:'', ks:'', bank_name: '',
    payment_text: '', last_id: '', corp_binding_id: '',
    payment_system_type_id: '', bank_id: '', agent_id: '', api_key: '',
    refresh_token: '', client_secret: '', client_id: '',
};

export const makeListModels = (items) => {
    return items.map(item => {
        const has_default_account = items.some(({agent, is_default}) => {
            return agent?.id === item?.agent?.id && is_default;
        });

        return {
            ...item,
            has_default_account
        }
    })
}

export const useFetchList = () => {
    const { data, isLoading, isError } = useQuery(['bank_accounts'], async () => {
        return await Api.bankAccounts.fetchList(BANK_ACCOUNTS_FILTER);
    });

    return {
        data: data?.items || [],
        meta: data?.meta || {},
        hasArchived: data?.meta?.has_archived || false,
        isLoading,
        isError
    }
}

export const useHasIsDefaultAccountForAgent = (agentId, paymentSystemTypeId) => {
    const [ hasIsDefaultAccount, setHasIsDefaultAccount ] = useState(false);

    const client = useQueryClient();
    const { items: bankAccounts } = client.getQueryData(['bank_accounts']);

    useEffect(() => {
        if(agentId && paymentSystemTypeId) {
            const isDefault = bankAccounts.some(({ agent, payment_system_type, is_default }) => {
                return agent?.id === agentId
                    && payment_system_type.id === paymentSystemTypeId
                    && is_default;
            });

            setHasIsDefaultAccount(isDefault);
        }
    }, [agentId, bankAccounts, paymentSystemTypeId])

    return hasIsDefaultAccount;
}
