export const BANK_ACCOUNTS_FILTER = {
    order: '-is_active,payment_system_id,-id',
    is_deprecated: 0,
    page: 1,
    per_page: 200,
};
