import { useMutation, useQueryClient } from 'react-query';

import Api from 'api/client';

import { addServerErrors } from 'utils/add-server-errors';

const useUpdateMutator = (id, setError) => {
    const queryClient = useQueryClient();

    const { isLoading: isUpdating, mutate: onUpdate, isSuccess, error, isError } = useMutation(
        data => Api.bankAccounts.update(id, data),
        {
            onSuccess: ({ item: updatedItem }) => {
                queryClient.setQueryData(['bank_accounts'], ({ items, ...rest }) => ({
                    items: items.map(item => item.id === updatedItem.id ? updatedItem : item),
                    rest,
                }));

                if(updatedItem.is_default) {
                    return queryClient.refetchQueries(['bank_accounts']);
                }
            },

            onError: (error) => {
                addServerErrors(error?.errorData?.fields || [], setError);
            }
        });

    return { isUpdating, onUpdate, isSuccess, error, isError }
}

const useCreateMutator = (setError) => {
    const queryClient = useQueryClient();

    const { isLoading: isCreating, mutate: onCreate, isSuccess, isError, error } = useMutation(
        data => Api.bankAccounts.create(data),
        {
            onSuccess: ({ item }) => {
                queryClient.setQueryData('bank_accounts', ({ items, ...rest }) => ({
                    items: [ item, ...items ],
                    rest,
                }));

                if(item.is_default) {
                    return queryClient.refetchQueries(['bank_accounts']);
                }
            },

            onError: (error) => {
                addServerErrors(error?.errorData?.fields || [], setError);
            }
        })

    return { isCreating, onCreate, isSuccess, isError, error }
}

export const useDeleteMutator = (initialId) => {
    const queryClient = useQueryClient();

    const { isLoading: isDeleting, mutate: onDelete, isSuccess, isError, error } = useMutation(
        (id) => Api.bankAccounts.delete(initialId || id),
        {
            onSuccess: () => {
                if(initialId) {
                    const mutationData = ({ items, ...rest }) => ({
                        items: items.filter(item => item.id !== initialId),
                        rest,
                    });

                    return queryClient.setQueryData('bank_accounts', mutationData);
                }

                return queryClient.refetchQueries(['bank_accounts']);
            }
        }
    )

    return { isDeleting, onDelete, isSuccess, isError, error }
}

export const useStatusMutator = (id) => {
    const queryClient = useQueryClient();

    return useMutation(
        value => Api.bankAccounts.changeStatus(id, value),
        {
            onSuccess: ({ item: updatedItem }) => {
                queryClient.setQueryData(['bank_accounts'], ({ items, ...rest }) => ({
                    items: items.map(item => item.id === updatedItem.id ? updatedItem : item),
                    rest,
                }));
            }
        }
    );
}

export const useEditMutator = (id, setError) => {
    const {
        onUpdate, isUpdating, isSuccess: isSuccessUpdate,
        isError: isUpdateError, error: updateError,
    } = useUpdateMutator(id, setError);
    const {
        onCreate, isCreating, isSuccess: isSuccessCreate,
        isError: isCreateError, error: createError,
    } = useCreateMutator(setError);

    return {
        onUpdate,
        onCreate,
        isProcessing: isUpdating || isCreating,
        isSuccess: isSuccessUpdate || isSuccessCreate,
        isError: isUpdateError || isCreateError,
        error: updateError || createError,
    }
}
