import React from 'react';
import { Helmet } from 'react-helmet';
import { Button, Form, Paragraph } from '@jump-work/uikit';
import { Config, Hooks } from '@jump-work/core';
import { Logo } from 'components/logo';
import { loginScreen } from '../store/actions';

const FormRecoverySuccess = () => {
    const onLoginScreen = Hooks.useActionToDispatch(loginScreen);

    return (
        <div>
            <Helmet>
                <title>{Config.brandName} - Восстановление пароля</title>
            </Helmet>

            <Logo/><br/><br/>

            <Form.Title>
                {`Восстановление пароля от ${Config.brandName}`}
            </Form.Title>

            <Form.Field>
                <Paragraph>
                    На указанную вами почту отправлено письмо с дальнейшими
                    инструкциями.
                </Paragraph>
            </Form.Field>

            <Button styling='hollow' onClick={onLoginScreen}>
                <span style={{ color: '#2691FF' }}>Войти</span>
            </Button>
        </div>
    );
}

export default FormRecoverySuccess;
