import React from 'react';
import PropTypes from 'prop-types';

import { CollapsibleTable } from 'components';

export const Header = ({ isShow = true, children }) => {
    if (isShow) {
        return (
            <CollapsibleTable headerCell={[
                { label: 'Агрегатор и банк' },
                { label: 'Транзакции' },
                { label: 'Сумма' },
                { label: 'Комиссия' },
            ]}>
                { children }
            </CollapsibleTable>
        )
    }

    return children;
}

Header.propTypes = {
    isShow: PropTypes.bool,
    children: PropTypes.any
}
