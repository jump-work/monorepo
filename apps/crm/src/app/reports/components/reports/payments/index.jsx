import React from 'react';
import PropTypes from 'prop-types';

import { EMPTY_ID } from 'domains/reports/models';

import { Agents } from './agents';
import { Banks } from './banks';
import { Integrations } from './intergations';

const ReportPayments = ({ filter }) => {
    const groupType = filter?.group_by ?? 'integration';
    const groupId = filter[groupType + '_id'] ?? EMPTY_ID;

    let Details = null;

    if (groupType === 'integration' && groupId === EMPTY_ID) {
        Details = Integrations;
    } else if (groupType === 'integration' && groupId !== EMPTY_ID) {
        Details = Banks;
    } else if (groupType === 'agent' && groupId === EMPTY_ID) {
        Details = Integrations;
    } else if (groupType === 'agent' && groupId !== EMPTY_ID) {
        Details = Agents;
    }

    return <Details isShowHeader filter={filter}/>
}

ReportPayments.propTypes = {
    filter: PropTypes.object.isRequired,
}

export default ReportPayments;
