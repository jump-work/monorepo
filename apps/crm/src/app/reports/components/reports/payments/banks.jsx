import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { useMoney } from 'hooks/use-money';
import { CollapsibleTable, EmptyContent, TreeShift } from 'components';
import { useBanksPayments } from 'domains/reports/queries';
import { EMPTY_ID } from 'domains/reports/models';

import { LoaderReports } from '../loader-reports';
import { CellWithCounts } from '../cell-with-counts';

import { Header } from './header';
import { Drivers } from './drivers';

export const Banks = ({ filter, isShowHeader = false }) => {
    const [ activeId, setActiveId ] = useState(EMPTY_ID);
    const { format } = useMoney();

    const { banks, isLoading } = useBanksPayments(filter);

    const isEmptyContent = isShowHeader && banks.length === 0 && !isLoading;

    if(isShowHeader && isLoading) {
        return <LoaderReports/>
    }

    return (
        <Header isShow={isShowHeader}>
            {
                isEmptyContent &&
                <EmptyContent message='Нет данных соответсвующих условиям.' isMute/>
            }

            {
                banks.map(bank => {
                    return (
                        <CollapsibleTable.Row
                            key={bank.id}
                            cells={[
                                <CellWithCounts key={bank.id} label={bank.bank_account_name} counts={format(bank.drivers_count)}/>,
                                format(bank.count),
                                format(bank.sum),
                                format(bank.commission),
                            ]}
                            active={activeId === bank.id}
                            onClick={() => setActiveId(bank.id === activeId ? EMPTY_ID : bank.id)}
                            nested
                        >
                            <TreeShift>
                                { activeId === bank.id && <Drivers filter={{...filter, bank_id: bank.id}} /> }
                            </TreeShift>
                        </CollapsibleTable.Row>
                    )
                })
            }
            { isLoading && <CollapsibleTable.Loader/> }
        </Header>
    )
}

Banks.propTypes = {
    isShowHeader: PropTypes.bool,
    filter: PropTypes.object.isRequired
}
