import React from 'react';
import PropTypes from 'prop-types';
import ContentLoader from 'react-content-loader';

export const LoaderReports = ({ props }) => {
    return (
        <ContentLoader
            speed={1}
            width={980}
            height={238}
            viewBox='0 0 980 238'
            backgroundColor='#E7EAEE'
            foregroundColor='#DBDDE6'
            style={{ display: 'block' }}
            {...props}
        >
            <rect x='0' y='0' rx='4' ry='4' width='980' height='40' />
            <rect x='0' y='42' rx='4' ry='4' width='980' height='64' />
            <rect x='0' y='108' rx='4' ry='4' width='980' height='64' />
            <rect x='0' y='174' rx='4' ry='4' width='980' height='64' />
        </ContentLoader>
    )
}

LoaderReports.propTypes = {
    props: PropTypes.any
}
