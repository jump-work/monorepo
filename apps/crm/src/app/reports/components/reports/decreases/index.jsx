import React from 'react';
import PropTypes from 'prop-types';

import { EMPTY_ID } from 'domains/reports/models';

import { Agents } from './agents';
import { Debit } from './debit';

const ReportDecreases = ({ filter }) => {
    const groupType = filter?.group_by ?? 'integration';
    const groupId = filter[groupType + '_id'] ?? EMPTY_ID;

    let Details = null;

    if (groupType === 'integration' && groupId === EMPTY_ID) {
        Details = Debit;
    } else if (groupType === 'integration' && groupId !== EMPTY_ID) {
        Details = Debit;
    } else if (groupType === 'agent' && groupId === EMPTY_ID) {
        Details = Agents;
    } else if (groupType === 'agent' && groupId !== EMPTY_ID) {
        Details = Debit;
    }

    return <Details isShowHeader filter={filter}/>
}

ReportDecreases.propTypes = {
    filter: PropTypes.object.isRequired,
}

export default ReportDecreases;
