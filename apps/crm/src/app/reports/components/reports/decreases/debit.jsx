import React, {useEffect, useState} from 'react'
import PropTypes from 'prop-types';

import { useMoney } from 'hooks/use-money';
import { CollapsibleTable, EmptyContent, TreeShift } from 'components';
import { useDebitsDecreases } from 'domains/reports/queries';
import { EMPTY_ID } from 'domains/reports/models';
import {useReportsContext} from 'domains/reports/contexts';

import { LoaderReports } from '../loader-reports';
import { CellWithCounts } from '../cell-with-counts';

import { Header } from './header';
import { Drivers } from './drivers';

export const Debit = ({ filter, isShowHeader = false }) => {
    const [ activeId, setActiveId ] = useState(EMPTY_ID);
    const { format } = useMoney();

    const { setIsLoadingDetails, setReportDownloadLinks } = useReportsContext();

    const { debits, isLoading, isFetching, attachments } = useDebitsDecreases(filter);

    useEffect(() => {
        setIsLoadingDetails(isLoading || isFetching);
        setReportDownloadLinks(attachments);
    }, [isLoading, isFetching, attachments, setIsLoadingDetails, setReportDownloadLinks]);

    const isEmptyContent = isShowHeader && debits.length === 0 && !isLoading;

    if(isShowHeader && isLoading) {
        return <LoaderReports/>
    }

    return (
        <Header isShow={isShowHeader}>
            {
                isEmptyContent &&
                <EmptyContent message='Нет данных соответсвующих условиям.' isMute/>
            }

            {
                debits.map(item => {
                    return (
                        <CollapsibleTable.Row
                            key={item.id}
                            cells={[
                                <CellWithCounts key={item.id} label={item.name} counts={item.decreases_count}/>,
                                '',
                                format(item.sum),
                            ]}
                            active={activeId === item.id}
                            onClick={() => setActiveId(item.id === activeId ? EMPTY_ID : item.id)}
                        >
                            <TreeShift>
                                {activeId === item.id && <Drivers filter={{...filter, debit_id: item.id}} />}
                            </TreeShift>
                        </CollapsibleTable.Row>
                    )
                })
            }

            { isLoading && <CollapsibleTable.Loader/> }
        </Header>
    )
}

Debit.propTypes = {
    isShowHeader: PropTypes.bool,
    filter: PropTypes.object.isRequired
}
