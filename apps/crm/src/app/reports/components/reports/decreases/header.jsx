import React from 'react';
import PropTypes from 'prop-types';

import { CollapsibleTable } from 'components';

export const Header = ({ isShow = true, children }) => {
    if (isShow) {
        return (
            <CollapsibleTable headerCell={[
                { label: 'Тип списания и водители' },
                { label: 'Дата' },
                { label: 'Сумма'  },
            ]}>
                { children }
            </CollapsibleTable>
        )
    }

    return children;
}

Header.propTypes = {
    isShow: PropTypes.bool,
    children: PropTypes.any.isRequired,
}
