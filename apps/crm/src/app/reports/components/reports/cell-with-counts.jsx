import React from 'react';
import PropTypes from 'prop-types';

export const CellWithCounts = ({ label, counts, ...rest }) => {
    return (
        <span {...rest}>
            {label}
            &nbsp;
            <span style={{ color: '#ABADBA' }}>
                ({ counts })
            </span>
        </span>
    )
}

CellWithCounts.propTypes = {
    label: PropTypes.string.isRequired,
    counts: PropTypes.oneOfType([
        PropTypes.string.isRequired,
        PropTypes.number.isRequired,
    ])
}
