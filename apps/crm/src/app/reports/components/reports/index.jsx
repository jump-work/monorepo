import React from 'react';
import PropTypes from 'prop-types';

import { REPORT_TRIPS,REPORT_PAYMENTS,REPORT_DECREASES } from 'domains/reports/services';

import ReportTrips from './trips';
import ReportPayments from './payments';
import ReportDecreases  from './decreases';

const map = {
    [REPORT_TRIPS]: ReportTrips,
    [REPORT_PAYMENTS]: ReportPayments,
    [REPORT_DECREASES]: ReportDecreases,
}

const ReportDetails = ({ filter, reportType }) => {
    const Report = map[reportType];

    return <Report filter={filter} />
}

ReportDetails.propTypes = {
    filter: PropTypes.object.isRequired,
    reportType: PropTypes.oneOf([
        REPORT_TRIPS, REPORT_PAYMENTS, REPORT_DECREASES
    ]).isRequired,
}

export default ReportDetails;
