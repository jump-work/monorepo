import React from 'react';

import { CollapsibleTable } from 'components';
import PropTypes from 'prop-types';

export const Header = ({ isShow = true, children }) => {
    if (isShow) {
        return (
            <CollapsibleTable headerCell={[
                { label: 'Агрегатор и водители' },
                { label: 'Заказы' },
                { label: 'Безнал' },
                { label: 'Сумма безнал' },
                { label: 'Общая сумма' },
                { label: 'Доход' },
            ]}>
                { children }
            </CollapsibleTable>
        )
    }

    return children;
}

Header.propTypes = {
    isShow: PropTypes.bool,
    children: PropTypes.any
}
