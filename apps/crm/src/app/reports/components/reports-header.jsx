import React from 'react';
import PropTypes from 'prop-types';

import { Icons24, MobileHeader } from '@jump-work/uikit';
import { useIfMediaScreen } from '@jump-work/uikit/lib/hooks';

import { useMenu } from 'components/menu';

export const ReportsHeader = ({ title }) => {
    const { onExpandedMobile } = useMenu();
    const isMobile = useIfMediaScreen();

    return (
        <MobileHeader header={title}>
            { isMobile && <MobileHeader.Button icon={<Icons24.IconMenu/>} onClick={onExpandedMobile}/> }
        </MobileHeader>
    )
}

ReportsHeader.propTypes = {
    title: PropTypes.string,
}
