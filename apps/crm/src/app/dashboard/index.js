import WorkDashboardPage from './pages/work-dashboard-page';
import TaxiDashboardPage from './pages/taxi-dashboard-page';
import DashboardPage from './pages/dashboard-page';

export {
    WorkDashboardPage,
    TaxiDashboardPage,
    DashboardPage
};
