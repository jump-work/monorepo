import React from 'react';
import PropTypes from 'prop-types';
import ContentLoader from 'react-content-loader';

export const LoaderControlRoom = ({ props }) => {
    return (
        <ContentLoader
            speed={1}
            width={227}
            height={40}
            viewBox='0 0 227 40'
            backgroundColor='#E7EAEE'
            foregroundColor='#DBDDE6'
            style={{ display: 'block' }}
            {...props}
        >
            <rect x='0' y='0' rx='2' ry='2' width='227' height='40' />
        </ContentLoader>
    )
}

LoaderControlRoom.propTypes = {
    props: PropTypes.any,
}
