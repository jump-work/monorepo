import ContentLoader from 'react-content-loader';
import PropTypes from 'prop-types';
import React from 'react';

export const LoaderEvents = ({props}) => {
    return (
        <ContentLoader
            speed={1}
            width={980}
            height={74}
            viewBox='0 0 980 74'
            backgroundColor='#E7EAEE'
            foregroundColor='#DBDDE6'
            style={{ display: 'block' }}
            {...props}
        >
            <rect x='0' y='0' rx='4' ry='4' width='980' height='74' />
        </ContentLoader>
    )
}

LoaderEvents.propTypes = {
    props: PropTypes.any,
}
