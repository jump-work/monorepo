import React from 'react';
import PropTypes from 'prop-types';
import ContentLoader from 'react-content-loader';

export const LoaderBank = ({ props }) => {
    return (
        <ContentLoader
            speed={1}
            width={208}
            height={76}
            viewBox='0 0 208 76'
            backgroundColor='#E7EAEE'
            foregroundColor='#DBDDE6'
            style={{ display: 'block' }}
            {...props}
        >
            <rect x='0' y='0' rx='2' ry='2' width='80' height='14' />
            <rect x='0' y='18' rx='4' ry='4' width='120' height='20' />
            <rect x='0' y='46' rx='4' ry='4' width='208' height='12' />
            <rect x='0' y='60' rx='4' ry='4' width='208' height='12' />
        </ContentLoader>
    )
}

LoaderBank.propTypes = {
    props: PropTypes.any
}
