import React from 'react';

import { Icons24, MobileHeader } from '@jump-work/uikit';
import { useIfMediaScreen } from '@jump-work/uikit/lib/hooks';

import { useMenu } from 'components/menu';

export const DashboardHeader = () => {
    const { onExpandedMobile } = useMenu();
    const isMobile = useIfMediaScreen();

    return (
        <MobileHeader header='Сводка'>
            { isMobile && <MobileHeader.Button icon={<Icons24.IconMenu/>} onClick={onExpandedMobile}/> }
        </MobileHeader>
    )
};
