import DriversPage from './pages/drivers-page';
import DriverDetailPage from './pages/drivers-details-page';

export {
    DriversPage,
    DriverDetailPage,
};
