import SecurityPage         from './pages/security-page';
import SecuritySessionsPage from './pages/security-sessions-page';
import SecurityTwoAuthPage  from './pages/security-two-auth-page';

export {
    SecurityPage,
    SecuritySessionsPage,
    SecurityTwoAuthPage,
};
