import { useSelector } from 'react-redux';

export const getProfile = (state) => state?.profile?.data ?? null;
export const getPermissions = (state) => state?.profile?.data?.access?.permissions ?? [];
export const useHasPermission = (permission) => useSelector(state => getPermissions(state).includes(permission));
