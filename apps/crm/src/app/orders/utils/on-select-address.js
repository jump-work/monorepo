export const onSelectAddress = (address, setState, type = '', setValue, field) => {
    const fullAddress = (data) => (
        `${data?.region_with_type || ''} ${data?.city || ''} ${data?.street || ''} ${data?.house || ''}`
    );

    setValue(field, fullAddress(address));

    setState(draft => {
        draft[`${type}_city`] = address.city;
        draft[`${type}_street`] = address.street;
        draft[`${type}_house`] = address.house;

        draft[`${type}_geo_lat`] = address.geo_lat;
        draft[`${type}_geo_lon`] = address.geo_lon;
    })
};
