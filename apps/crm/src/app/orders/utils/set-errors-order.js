export const setErrorsOrder = (errors = [], setError = () => {}) => {

    const prepareErrors = (errors = []) => {
        const allErrors = [];

        errors.map(item => {
            if(item.field === 'address.city') { allErrors.push({ type: 'from-address', ...item }) }
            if(item.field === 'address.street') { allErrors.push({ type: 'from-address', ...item }) }
            if(item.field === 'address.house') { allErrors.push({ type: 'from-address', ...item }) }
            if(item.field === 'address.coordinates.lat') { allErrors.push({ type: 'from-address', ...item }) }
            if(item.field === 'phone') { allErrors.push({ type: 'from-phone', ...item }) }

            if(item.field === 'offers.0.address.city') { allErrors.push({ type: 'to-address', ...item }) }
            if(item.field === 'offers.0.address.street') { allErrors.push({ type: 'to-address', ...item }) }
            if(item.field === 'offers.0.address.house') { allErrors.push({ type: 'to-address', ...item }) }
            if(item.field === 'offers.0.address.coordinates.lat') { allErrors.push({ type: 'to-address', ...item }) }
            if(item.field === 'offers.0.client_phone') { allErrors.push({ type: 'to-phone', ...item }) }

            return null
        })

        return allErrors;
    }

    const makeErrorMessage = (errors = []) => {
        let fromMessage = [];
        let toMessage = [];
        const message = [];
        const getErrors = (item, type) => {
            const errorList = {
                city: 'Введите город',
                street: 'Введите улицу',
                house: 'Введите номер здания',
                coordinates: 'Адрес введен некорректно',
            };

            const errors = [];

            for(let key in errorList) {
                if (item.messages[0].includes(key) && item.type === type) {
                    errors.push(errorList[key]);
                }
            }

            return errors;
        }

        errors.map(item => {
            fromMessage = fromMessage.concat(getErrors(item, 'from-address'));
            toMessage = toMessage.concat(getErrors(item, 'to-address'));

            if(item.type === 'from-phone') { message.push({ ...item, message: item.messages[0] }) }
            if(item.type === 'to-phone') { message.push({ ...item, message: item.messages[0] }) }

            return null
        })

        return [
            ...message,
            {type: 'from-address', message: fromMessage.join(', ')},
            {type: 'to-address', message: toMessage.join(', ')}
        ]
    }

    return makeErrorMessage(prepareErrors(errors)).map(item => setError(item.type, { message: item.message }))
}
