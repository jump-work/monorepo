const getOrdersList = state => state?.orders?.items?.data ?? [];
const getIsRequest = state => state?.orders?.items?.isRequest ?? false;
const getOrdersMeta = state => state?.orders?.items?.meta ?? {};
const getOrdersErrors = state => state?.orders?.items?.error ?? {};
const getOrderInvalidFields = state => state?.orders?.items?.error?.errorData?.fields ?? [];

const getFormState = state => ({
    isSubmitting: state?.orders?.formState?.isSubmitting ?? false,
    isSubmitted: state?.orders?.formState?.isSubmitted ?? false,
});

export const OrdersSelectors = {
    getOrdersList,
    getIsRequest,
    getOrdersMeta,
    getOrdersErrors,

    getFormState,
    getOrderInvalidFields,
}
