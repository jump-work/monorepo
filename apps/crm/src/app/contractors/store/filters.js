export const CONTRACTORS_FILTER = {
    order: '-id',
    page: 1,
    per_page: 15,
    search: '',
};
