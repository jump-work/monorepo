const getContractors = (state) => state?.contractors?.items?.data ?? [];
const getContractorsMeta = (state) => state?.contractors?.items?.meta ?? {};
const getContractorsError = (state) => state?.contractors?.items?.error?.errorData ?? null;

const getContractorInvalidFields = (state) => state?.contractors?.items?.error?.errorData?.fields ?? [];
const getContractorsIsRequest = (state) => state?.contractors?.items?.isRequest ?? false;

const getContractorsDict = (state) => state?.app?.dictionaries?.items?.contractor?.dictionaries[0] ?? {};

const getContractor = (state) => state?.contractors?.item?.data ?? {};
const getContractorIsRequest = (state) => state?.contractors?.item?.isRequest ?? false;
const getContractorAbilities = (state) => state?.contractors?.item?.abilities ?? null;

const getLegalFormId = (state) => state?.contractors?.item?.data?.legal_form?.id;
const getLegalFormData = (state) => ({
    isVerified: state?.contractors?.item?.data?.legal_form?.status?.is_verified,
    isSelfEmployed: state?.contractors?.item?.data?.legal_form?.is_self_employed,
})

const getContractorBalance = (state) => state?.contractors?.item?.data?.balance?.value ?? 0;
const getIsFetchingBalance = (state) => state?.contractors?.item?.isFetchingBalance ?? false;

const getFormState = (state) => ({
    isSubmitting: state?.contractors?.formState?.isSubmitting ?? false,
    isSubmitted: state?.contractors?.formState?.isSubmitted ?? false,
})

export const ContractorSelectors = {
    getContractors,
    getContractorsMeta,
    getContractorsError,
    getContractorInvalidFields,
    getContractorsIsRequest,
    getContractorsDict,
    getContractor,
    getContractorIsRequest,
    getContractorAbilities,
    getFormState,
    getLegalFormId,
    getLegalFormData,
    getContractorBalance,
    getIsFetchingBalance,
}
