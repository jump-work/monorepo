import Bind from './bind';
import Confirm from './confirm';
import Failed from './failed';
import Success from './success';

export const BIND = 'bind';
export const CONFIRM = 'confirm';
export const SUCCESS = 'success';
export const FAILED = 'failed';

export {
    Bind,
    Confirm,
    Failed,
    Success
}
