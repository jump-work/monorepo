import React from 'react';
import PropTypes from 'prop-types';
import ContentLoader from 'react-content-loader';

export const LoaderWithdrawalAccess = ({ props }) => {
    return (
        <ContentLoader
            speed={1}
            width={225}
            height={32}
            viewBox='0 0 225 32'
            backgroundColor='#E7EAEE'
            foregroundColor='#DBDDE6'
            style={{ display: 'block' }}
            {...props}
        >
            <rect x='0' y='8' rx='4' ry='4' width='225' height='24' />
        </ContentLoader>
    )
}

LoaderWithdrawalAccess.propTypes = {
    props: PropTypes.any
}
