import { combineReducers } from 'redux';

import { auth } from 'app/auth/store/reducer';
import { app } from 'app/init/store/reducer';
import { contractorRequisites } from 'app/contractors/pages/requisites/store/reducer';
import { contractorOrders } from 'app/contractors/pages/orders/store/reducer';
import { contractors } from 'app/contractors/store/reducer';
import { documents } from 'app/documents/store/reducer';
import { orders } from 'app/orders/store/reducer';
import { profile } from 'app/profile/store/reducer';
import { settings } from 'app/general-settings/store/reducer';
import { users } from 'app/users/store/reducer';

export const rootReducer = (/*history*/) => combineReducers({
    app,
    auth,
    contractorOrders,
    contractorRequisites,
    contractors,
    documents,
    orders,
    profile,
    settings,
    users,
})
