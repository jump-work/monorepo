const getDocuments = state => state?.documents?.items?.data ?? [];
const getDocumentsMeta = state => state?.documents?.items?.meta ?? {};
const getDocumentsIsRequest = state => state?.documents?.items?.isRequest ?? false;
const getDocumentsError = state => state?.documents?.items?.error ?? {};

const getDocument = state => state?.documents?.item?.data ?? [];
const getDocumentIsRequest = state => state?.documents?.item?.isRequest ?? false;
const getDocumentInvalidFields = state => state?.documents?.items?.error?.errorData?.fields ?? [];

const getFormState = state => {
    return {
        isSubmitting: state?.documents?.formState?.isSubmitting ?? false,
        isSubmitted: state?.documents?.formState?.isSubmitted ?? false,
    }
};

export const DocumentsSelectors = {
    getDocuments,
    getDocumentsMeta,
    getDocumentsIsRequest,
    getDocumentsError,
    getDocumentInvalidFields,

    getDocument,
    getDocumentIsRequest,
    getFormState,
}
