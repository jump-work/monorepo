import { createStore, applyMiddleware, compose } from 'redux';
import { Provider } from 'react-redux';
import createSagaMiddleware from 'redux-saga';
import { createBrowserHistory } from 'history';

import { rootReducer } from 'app/reducer';
import rootSaga from 'app/sagas';
import initialState from 'app/init/store/state';

const devTools = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
const developMode = process.env.NODE_ENV !== 'production';
const enableDevTools = typeof window === 'object' && devTools && developMode;

const history = createBrowserHistory();
const composeEnhancers = enableDevTools ? devTools : compose;
const sagaMiddleware = createSagaMiddleware();

const store = createStore(
    rootReducer(history),
    initialState,
    composeEnhancers(applyMiddleware(sagaMiddleware)),
);

if (module.hot) {
    module.hot.accept('./reducer', () => {
        store.replaceReducer(rootReducer(history));
    });
}

sagaMiddleware.run(rootSaga);

export { store, history, Provider };
