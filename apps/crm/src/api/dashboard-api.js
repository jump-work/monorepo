export const Dashboard = {
    fetchBalance(id) {
        return this.get(`dashboard/balances/${id || ''}`)
    },

    fetchGasBalance() {
        return this.get('dashboard/gas_balance')
    },

    fetchSummary() {
        return this.get('dashboard/summary');
    },

    fetchChart(chart, filter) {
        return this.get(`dashboard/charts/${chart}`, filter)
    },

    fetchIntegrations() {
        return this.get('integrations/dictionary');
    },

    syncBalances() {
        return this.post('banks_accounts/sync_balances');
    },

    syncBalancesStatus(id, cancelToken) {
        return this.getStatus(`banks_accounts/sync_balances_status/${id}`, null, null, cancelToken);
    }
}
