export const Orders = {
    fetchOrdersList(filter) {
        return this.get('orders', filter);
    },

    fetchOrder(id) {
        return this.get(`orders/${id}`);
    },

    createDelivery(order) {
        return this.post('orders', order);
    }
}
