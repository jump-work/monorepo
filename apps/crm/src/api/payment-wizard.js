export const PaymentWizard = {
    fetchConditionsRule() {
        return this.get('payment_wizard/dictionaries');
    },

    updatePriorities(ids) {
        return this.patch('payment_wizard/priorities', { rule_ids: ids });
    },

    fetchRules(filter) {
        return this.get('payment_wizard', filter);
    },

    fetchRule(id) {
        return this.get(`payment_wizard/${id}`);
    },

    create(data) {
        return this.post('payment_wizard', data);
    },

    update(id, data) {
        return this.put(`payment_wizard/${id}`, data);
    },

    delete(id) {
        return this.deleteRequest(`payment_wizard/${id}`);
    }
}
