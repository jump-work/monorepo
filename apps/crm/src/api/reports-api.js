export const Reports = {
    fetch(path, filter){
        return this.get(`reports/${path}`, filter);
    },

    fetchAggregators(url, filter) {
        return this.get(`reports/${url}`, filter);
    },

    fetchDrivers(url, id,  filter) {
        return this.get(`reports/${url}/${id}`, filter);
    },

    fetchBankAccounts(aggregatorId, bankId, filter) {
        return this.get(`reports/payments/${aggregatorId}/${bankId}`, filter);
    },

    fetchReportsChart(reportName, chartName, filter) {
        return this.get(`reports/${reportName}/charts/${chartName}`, filter);
    }
}
