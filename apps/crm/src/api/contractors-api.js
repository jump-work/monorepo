export const Contractor =  {
    fetchContractorsList(filter) {
        return this.get('contractors', filter);
    },

    fetchContractor(id, filter) {
        return this.get(`contractors/${id}`, filter);
    },

    searchContractor(filter) {
        return this.get('contractors/find', filter);
    },

    searchContractorForPayments(filter) {
        return this.get('contractors/find_smart', filter);
    },

    create(data) {
        return this.post('contractors', data);
    },

    attach(id, data) {
        return this.post(`contractors/${id}/attach`, data);
    },

    update(id, data) {
        return this.put(`contractors/${id}`, data);
    },

    delete(id) {
        return this.deleteRequest(`contractors/${id}`);
    },

    fetchTaxes(id) {
        return this.get(`contractors/${id}/taxes`)
    },

    fetchTaxBind(id) {
        return this.post(`contractors/${id}/tax_bind`);
    },

    fetchTaxCheck(id) {
        return this.post(`contractors/${id}/tax_check`);
    },

    fetchTaxRequest(id, bindId) {
        return this.get(`contractors/${id}/tax_requests/${bindId}`);
    },

    fetchSelfEmployerInfo(id, cancelToken) {
        return this.get(`contractors/${id}/selfemployer`, null, null, cancelToken);
    },

    selfEmployerSync(id) {
        return this.post(`contractors/${id}/selfemployer/sync`);
    }
};

export const Requisites = {
    fetchRequisitesList(id, filter) {
        return this.get(`contractors/${id}/requisites`, { order: '-id', per_page: 10, ...filter });
    },

    fetchRequisite(id, req_id) {
        return this.get(`contractors/${id}/requisites/${req_id}`);
    },

    create(id, data) {
        return this.post(`contractors/${id}/requisites`, data);
    },

    update(id, req_id, data) {
        return this.put(`contractors/${id}/requisites/${req_id}`, data);
    },

    delete(id, req_id) {
        return this.deleteRequest(`contractors/${id}/requisites/${req_id}`);
    }
}

export const Permissions = {
    fetch(contractorId, permissionId) {
        return this.get(`contractors/${contractorId}/permissions/${permissionId}`)
    },

    update(contractorId, permissionId, value) {
        return this.put(`contractors/${contractorId}/permissions/${permissionId}`, { value })
    }
}
