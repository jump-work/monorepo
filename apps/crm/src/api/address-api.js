import axios from 'axios';

const URL = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/suggest/address';
const API_KEY = '8188b2ffaf8171348db48121989e702b83bd3a3b';

export const Address = {
    fetchAddress(data) {
        return axios.post(URL, { query: data }, {
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': `Token ${API_KEY}`,
            },
            withCredentials: false,
        })
    }
}
