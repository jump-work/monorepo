export const Payments = {
    checkPermissions: function () {
        return this.get('payments/permissions');
    },

    fetchPaymentsList: function (filter) {
        return this.get('payments', filter);
    },

    fetchPayment: function (id, filter) {
        return this.get(`payments/${id}`, { include: 'agent, creator, requisite, history, abilities', ...filter });
    },

    counts: function () {
        return this.get('payments/info/counts');
    },

    infoTotal: function(data) {
        return this.get('payments/info/total', data);
    },

    errorable: function(data) {
        return this.get('payments/info/errorable', data);
    },

    withdraw: function (data) {
        return this.post('payments/withdraw?include=agent,history,creator,requisite', data);
    },

    withdrawPreview: function (data) {
        return this.post('payments/withdraw/preview', data);
    },

    withdrawSmart: function (data) {
        return this.post('payments/withdraw_smart?include=agent,history,creator,requisite', data);
    },

    withdrawSmartPreview: function (data) {
        return this.post('payments/withdraw_smart/preview', data);
    },

    confirm: function (id) {
        return this.post(`/payments/${id}/confirm`, { include: 'agent,history,creator,requisite' });
    },

    confirmPayments: function (ids) {
        return this.post('payments/confirm', { payment_id: ids });
    },

    repeatPayments: function(ids) {
        return this.post('payments/repeat', { payment_id: ids });
    },

    refundPayments: function(ids) {
        return this.post('payments/refund', { payment_id: ids });
    },

    cancelPayments: function(ids) {
        return this.deleteRequest('payments', { payment_id: ids });
    },

    confirmOtp: function (data) {
        return this.post('payments/confirm/otp', data);
    },

    cancelReceipt: function (id, reason) {
        return this.deleteRequest(`/payments/${id}/receipt?reason=${reason}&include=agent,history,creator,requisite`)
    },
}
