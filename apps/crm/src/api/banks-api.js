export const Banks = {
    fetchBanksList(filter) {
        return this.get('banks', filter);
    },

    fetchBank(id) {
        return this.get(`banks/${id}`)
    }
}
