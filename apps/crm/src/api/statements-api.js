export const Statements = {
    /**
     * Получение списка ведомостей
     * @param filter {{}}
     * @returns {Promise}
     */
    fetchStatementsList(filter = {}) {
        return this.get('taxes/statements', filter);
    },

    /**
     * Получение ведомости по id
     * @param id
     * @returns {*}
     */
    fetchStatement(id, filter) {
        return this.get(`taxes/statements/${id}`, { include: 'agent,creator,requisite,history', ...filter });
    },

    /**
     * @param id {string|number}
     * @param deductionIds {Array<string|number>|string}
     * @param compensation_from_company {Boolean}
     * @returns {*}
     */
    confirmStatement(id, deductionIds, compensation_from_company = false) {
        return this.post(`taxes/statements/${id}/confirm`, {
            tax_deduction_id: Array.isArray(deductionIds) ? deductionIds.join(',') : deductionIds,
            compensation_from_company,
        })
    },

    refreshStatement(id) {
        return this.post(`taxes/statements/${id}/refresh`, {})
    },

    /**
     * @param id {string|number}
     * @returns {*}
     */
    deleteConfirmStatement(id) {
        return this.deleteRequest(`taxes/statements/${id}/confirm`)
    }
}
