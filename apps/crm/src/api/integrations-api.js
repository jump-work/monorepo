export const Integrations = {
    fetchList: function(filter) {
        return this.get('integrations', filter);
    },

    fetchItem: function(id, filter) {
        return this.get(`integrations/${id}`, filter);
    },

    create: function(data) {
        return this.post('integrations', data);
    },

    update: function(id, data) {
        return this.put(`integrations/${id}`, data);
    },

    delete: function(id) {
        return this.deleteRequest(`integrations/${id}`);
    },

    changeStatus: function(id, value) {
        return this.put(`integrations/${id}/status`, { value });
    },

    requestOtp: function(id) {
        return this.post(`integrations/${id}/wheely/auth/otp`);
    },

    confirmOtp: function(id, code) {
        return this.put(`integrations/${id}/wheely/auth/otp`, { code });
    }
}
