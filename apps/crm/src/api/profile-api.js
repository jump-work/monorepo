const Profile = {
    fetchProfile: function () {
        return this.get('profile');
    },
};

export default Profile;
