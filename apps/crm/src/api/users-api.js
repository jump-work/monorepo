const Users = {
    fetchUserList: function (filter) {
        return this.get('customers', filter);
    },

    create: function (user) {
        return this.post('customers', user);
    },

    update: function (id, user) {
        return this.put(`customers/${id}`, user);
    },

    delete: function (id) {
        return this.deleteRequest(`customers/${id}`);
    },

    recovery: function(id) {
        return this.patch(`customers/${id}/restore`)
    },

    fetchPermissions: function (userId) {
        return this.get(`customers/${userId}/permissions`);
    },

    updatePermissions: function (userId, data) {
        return this.patch(`customers/${userId}/permissions`, data);
    },

    updateAgentPermissions: function (userId, data) {
        return this.patch(`customers/${userId}/permissions/agents`, data);
    }
};

export default Users;
