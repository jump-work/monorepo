const Agents = {
    fetchAgentsList: function (filter) {
        return this.get('agents', filter)
    },

    my: function () {
        return this.get('agents/my')
    },

    fetchAgent: function (id) {
        return this.get(`agents/${id}`)
    },

    create: function(agent) {
        return this.post('agents', agent)
    },

    update: function(id, agent) {
        return this.put(`agents/${id}`, agent)
    },

    delete: function(id) {
        return this.deleteRequest(`agents/${id}`)
    },

    fetchAgentByInn: function(inn) {
        return this.get('dadata/suggestions', {
            url: 'findById/party',
            query: inn,
            type: 'LEGAL'
        })
    }
};

export default Agents;
