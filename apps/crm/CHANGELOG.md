# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

## [0.6.1](https://gitlab.com/jump-work/monorepo/compare/@jump-work/crm@0.6.0...@jump-work/crm@0.6.1) (2021-10-17)

**Note:** Version bump only for package @jump-work/crm





# [0.6.0](https://gitlab.com/jump-work/monorepo/compare/@jump-work/crm@0.5.0...@jump-work/crm@0.6.0) (2021-09-28)


### Features

* resolve DEV-2682 testing ([98f587e](https://gitlab.com/jump-work/monorepo/commit/98f587eb9fa04eef0180c53cb503b942272e3d2e))





# [0.5.0](https://gitlab.com/jump-work/monorepo/compare/@jump-work/crm@0.4.0...@jump-work/crm@0.5.0) (2021-09-28)


### Features

* resolve DEV-2682 testing ([909cc0a](https://gitlab.com/jump-work/monorepo/commit/909cc0ad9068bec5b3e3bfac109c21f93bf02460))





# 0.4.0 (2021-09-28)


### Features

* resolve DEV-2682 ci ([dc6fe3c](https://gitlab.com/jump-work/monorepo/commit/dc6fe3ca9b60a82302d30f31d79ad33e7ddca143))
* resolve DEV-2682 ci ([6aea7cd](https://gitlab.com/jump-work/monorepo/commit/6aea7cd59d72d024543bb27c43cb431d5f4b38f6))
* resolve DEV-2682 testing ([2c0a049](https://gitlab.com/jump-work/monorepo/commit/2c0a04973b85e6dacac8e6858c15da032c142a06))
* resolve DEV-2682 testing ([0c9cb72](https://gitlab.com/jump-work/monorepo/commit/0c9cb72e34c4a7112e0532a654804dd69a5ac476))
* resolve DEV-2682 ui-kit ([1ac8475](https://gitlab.com/jump-work/monorepo/commit/1ac8475fe931561cde97baa270aa71f4e92f5aca))





## [0.3.1](https://gitlab.com/bogdan.panazdyr/jump-monorepo/compare/crm@0.3.0...crm@0.3.1) (2021-09-19)

**Note:** Version bump only for package crm





# [0.3.0](https://gitlab.com/bogdan.panazdyr/jump-monorepo/compare/crm@0.1.3...crm@0.3.0) (2021-09-19)


### Features

* resolve DEV-2682 ci ([dc6fe3c](https://gitlab.com/bogdan.panazdyr/jump-monorepo/commit/dc6fe3ca9b60a82302d30f31d79ad33e7ddca143))
* resolve DEV-2682 ci ([6aea7cd](https://gitlab.com/bogdan.panazdyr/jump-monorepo/commit/6aea7cd59d72d024543bb27c43cb431d5f4b38f6))





# [0.2.0](https://gitlab.com/bogdan.panazdyr/jump-monorepo/compare/crm@0.1.3...crm@0.2.0) (2021-09-19)


### Features

* resolve DEV-2682 ci ([6aea7cd](https://gitlab.com/bogdan.panazdyr/jump-monorepo/commit/6aea7cd59d72d024543bb27c43cb431d5f4b38f6))





## [0.1.4](https://gitlab.com/bogdan.panazdyr/jump-monorepo/compare/crm@0.1.3...crm@0.1.4) (2021-09-19)

**Note:** Version bump only for package crm
