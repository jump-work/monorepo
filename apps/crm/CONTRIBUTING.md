# Правила именования коммитов/Мердж реквестов
- Номер задачи из JIRA (DEV-xxx)
- Пробел
- Комментарий с ответом на вопрос "что делает?", с маленькой буквы

Пример: **DEV-0001 добавляет правила код стайла**

# Правила форматирования кода

## Структура доменных сущностей

Вся бизнес-логика складывается в папку **domains**.

Структура сущности:

```shell
 ---domain_name
    |---models
    |    └index.js
    |---mutators
    |    └index.js
    |---queries
    |    └index.js
    |---helpers
    |    └index.js
    |---services
    |    └index.js
    |---contexts
    |    └index.js
    |--constants.js
    |--index.js
```

## Интерфейсы для mutators
**Именование файлов: {action}-{entity}.js**

Пример: create-user.js, restore-user-permission.js

> Параметры мутации передаем в саму мутацию, а не входным параметром хука

Интерфейс:
```ts
    interface MutateObject {
        onXxx: ({params}?: any) => {},
        mutateXxxHasError: boolean,
        isXxxMutating: boolean,
        mutateXxxError: ApiError|Error
    }
```

## Интерфейсы для queries
**Именование файлов: {entity|entities}.js**

Пример: users.js, user.js, user-permissions.js 

Интерфейс:
```ts
    interface QueryObject {
        xxxData?: any,
        xxxMeta?: any,
        refreshXxx: () => Promise,
        xxxHasError: boolean,
        isXxxLoading: boolean,
        isXxxFetching?: boolean,
        xxxError: ApiError|Error,
        onXxxYyy?: (args?: any) => void,
    }
```

##Контексты
**Именование файлов: {context-name}.jsx**

> Note: В конце имени файлов не ставим "\***-context.\***"

Интерфейс:
```ts
import * as React from 'react';

interface Context {
    useXXXContext: React.Context,
    xxxContextProvider: React.ContextProvider
}
```

##Helpers и Services
Helpers - Хуки и Функции без бизнес логики
Services - Сервисы содержат бизнес логику 

##Constants
***constants.js*** - контанты, в том числе FILTERS и META
