# Develop

- создаем ветку от `master`
- вносим изменения в код
- увеличить версию в `package.json`
- запустить `yarn lint`
- `yarn build`
- `git commit`
- `git push`
- создать в GitLab merge request
- review
- слить merge request в GitLab
- создать новый тег от ветки `master`
