import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { useTabListener } from '../hooks';
import { Paragraph } from '../paragraph';
import { Icons24 } from '../icons';

import cx from './button.module.scss';

const Button = ({element: Btn, icon, children, className, wide, styling, loading, ...attrs}) => {

    const { focusedByTab } = useTabListener();

    const cl = classNames(className, cx.base, {
        [cx.primary]: styling === 'primary',
        [cx.hollow]: styling === 'hollow',
        [cx['hollow-border']]: styling === 'hollow-border',
        [cx.icon]: icon,
        [cx.text]: icon && children,
        [cx.disabled]: attrs.disabled,
        [cx.wide]: wide,
        [cx.loading]: loading,
        [cx.focus]: focusedByTab,
    });

    const renderChildren = () => {
        return (
            <Fragment>
                { icon }
                {loading
                    ? <Icons24.IconLoader />
                    : <Paragraph> { children } </Paragraph>
                }
            </Fragment>
        )
    }

    const renderChildrenWithIcon = () => {
        return (
            <Fragment>
                <Icons24.IconLoader />
                <Paragraph>{ children }</Paragraph>
            </Fragment>
        )
    }

    return (
        <Btn
            className={cl}
            {...attrs}
        >
            {loading && icon
                ? renderChildrenWithIcon()
                : renderChildren()
            }
        </Btn>
    );
};

Button.defaultProps ={
    type: 'button',
    styling: 'primary',
    element: 'button',
};

Button.propTypes = {
    className: PropTypes.string,
    children: PropTypes.any,
    element: PropTypes.string,
    type: PropTypes.string,
    icon: PropTypes.node,
    styling: PropTypes.oneOf(['primary', 'hollow', 'hollow-border']),
    loading: PropTypes.bool,
    wide: PropTypes.bool,
};

export default Button;
