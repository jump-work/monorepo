import React from 'react';
import { storiesOf } from '@storybook/react';
import { text, select, boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import Button from './button';
import { Icons24 } from '../icons';
import readme from './readme.md';

storiesOf('Компоненты/Кнопки', module)
    .add(
        'Базовая',
        () => {
            return (
                <Button
                    element={text('Элемент', 'button')}
                    type={select('Type', {
                        Submit: 'submit',
                        Button: 'button',
                        Reset: 'reset',
                    },'button')}

                    styling={select('Styling', {
                        Primary: 'primary',
                        Hollow: 'hollow',
                        'Hollow with border': 'hollow-border'
                    }, 'primary')}

                    loading={boolean('Loading', false)}
                    disabled={boolean('Disabled', false)}
                    onClick={action('clicked')}
                >
                    Войти
                </Button>
            )
        },{notes: readme}
    ).add(
        'С иконкой',
        () => {
            return (
                <Button
                    icon={<Icons24.IconTrash/>}
                    element={text('Элемент', 'button')}
                    type={select('Type', {
                        Submit: 'submit',
                        Button: 'button',
                        Reset: 'reset',
                    },'button')}

                    styling={select('Styling', {
                        Primary: 'primary',
                        Hollow: 'hollow',
                        'Hollow with border': 'hollow-border'
                    },'primary')}
                    loading={boolean('Loading', false)}
                    disabled={boolean('Disabled', false)}
                    onClick={action('clicked')}
                >
                    {text('Children', 'Удалить')}
                </Button>
            )
        },{notes: readme}
    );
