# Кнопки

## Импорт
```javascript
import { Button } from '@justlook/uikit';
```

## Использование

```javascript
const el = () => {
    return <Button>Удалить</Button>
}
```

## Использование разных тегов

```javascript
const el = () => {
    return <Button element='a'>Удалить</Button>
}
```

## Стили

- **primary**: кнопка с заливкой
- **hollow**: кнопка призрак
- **hollow-border**: кнопка призрак с рамкой

##Props
- **element**: определяет тег элемента
- **icon**: иконка на элементе
- **className**: класс элемента
- **wide**: элемент на ширину в 100%
- **styling**: стили кнопки (см. выше)
- **loading**: состояние элемента в процессе загрузки

В качестве дополнительных `props` можно указывать атрибуты, которые поддерживает элемент, 
переданный в `element`. По умолчанию `element='button'`.
