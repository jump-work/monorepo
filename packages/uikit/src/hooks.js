import { useEffect, useLayoutEffect, useState, useCallback } from 'react';

export function useIfMediaScreen() {

    function checkIfMediaScreen() {
        return window.matchMedia('(max-width: 768px)').matches;
    }

    const [isMediaScreen, setIsMediaScreen] = useState(checkIfMediaScreen);

    useEffect(() => {
        let time;

        function handleResize() {
            if (time) {
                clearTimeout(time);
            }

            time = setTimeout(() => {
                setIsMediaScreen(checkIfMediaScreen())
            }, 200);
        }

        window.addEventListener('resize', handleResize);
        return () => window.removeEventListener('resize', handleResize);
    }, []);

    return isMediaScreen;
}

export function useOnClickOutside(ref, handler) {
    useEffect(
        () => {
            const listener = event => {
                if (!ref.current || ref.current.contains(event.target)) {
                    return;
                }

                handler(event);
            };

            document.addEventListener('mousedown', listener);
            document.addEventListener('touchstart', listener);

            return () => {
                document.removeEventListener('mousedown', listener);
                document.removeEventListener('touchstart', listener);
            };
        },
        [ref, handler]
    );
}

export function useDebounce(value, delay) {
    const [debouncedValue, setDebouncedValue] = useState(value);
    const [cancel, setCancel] = useState(null);

    useEffect(
        () => {
            const handler = setTimeout(() => {
                setDebouncedValue(value);
            }, delay);
            setCancel(handler);

            return () => {
                clearTimeout(handler);
            };
        },
        [value, delay]
    );

    return [debouncedValue, cancel];
}

export function useModalState (state = false) {
    const hookData = useState(state);
    const isShow = hookData[0];
    const onShow = hookData[1];

    const onOpenForm = () => onShow(true);
    const onCloseForm = () => onShow(false);

    return { isShow, onOpenForm, onCloseForm };
}

export function useTabListener (focus = false) {
    const [ focusedByTab, setFocusedByTab ] = useState(focus);

    useEffect(() => {

        const listener = (event) => {
            setFocusedByTab(() => {
                return 9 === event.keyCode || event.keyCode === 48;
            })
        }

        document.body.addEventListener('mousedown', listener);
        document.body.addEventListener('keydown', listener);

        return () => {
            document.body.removeEventListener('mousedown', listener);
            document.body.removeEventListener('keydown', listener);
        }
    })

    return { focusedByTab };
}

const WindowSizeObserver = (function(){
    let listeners = [];

    const getSize = () => ({
        width: document.documentElement?.clientWidth || window.innerWidth,
        height: document.documentElement?.clientHeight || window.innerHeight,
    })

    const resizeListener = () => {
        const windowSize = getSize();
        listeners.map(listener => listener(windowSize))
    }

    return {
        addListener (listener) {
            if (listeners.length === 0) {
                window.addEventListener('resize', resizeListener);
            }

            listeners.push(listener)
            listener(getSize())
        },
        removeListener (listener) {
            listeners = listeners.filter(item => item !== listener)

            if (listeners.length === 0) {
                window.removeEventListener('resize', resizeListener);
            }
        }
    }
}());

export const useWindowSize = () => {
    const [ size, setSize ] = useState({ width: 0, height: 0 });

    useLayoutEffect(() => {
        WindowSizeObserver.addListener(setSize);
        return () => WindowSizeObserver.removeListener(setSize);
    }, []);

    return { ...size };
}

export const useResizeObserver = (ref, callback) => {
    const [ width, setWidth ] = useState();
    const [ height, setHeight ] = useState();

    const handleResize = useCallback((entries) => {
        const entry = entries[0];
        if(Array.isArray(entry.borderBoxSize)) {
            setWidth(entry.borderBoxSize[0].inlineSize);
            setHeight(entry.borderBoxSize[0].blockSize);
        } else if(entry.contentRect) {
            setWidth(entry.contentRect.width);
            setHeight(entry.contentRect.height);
        } else{
            setWidth(document.documentElement?.clientWidth || window.innerWidth);
            setHeight(document.documentElement?.clientHeight || window.innerHeight);
        }

        if (callback) callback(entry.contentRect);
    }, [callback]);

    useLayoutEffect(() => {
        const resizeObserver = new ResizeObserver(entries => handleResize(entries));
        resizeObserver.observe(ref?.current);

        return () => { resizeObserver.disconnect() };
    }, [handleResize, ref]);

    return { width, height };
}
