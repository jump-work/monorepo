import { Icons16 } from '../icons';

export const ORDER_ASC = 'ORDER_ASC';
export const ORDER_DESC = 'ORDER_DESC';

export const orders = [
    {
        key: ORDER_ASC,
        desc: false,
        icon: Icons16.IconArrowUp
    },
    {
        key: ORDER_DESC,
        desc: true,
        icon: Icons16.IconArrowDown
    }
];
