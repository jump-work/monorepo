import React from 'react';
import PropTypes from 'prop-types';

import cx from './table-header.module.scss';

export const TableHeader = ({ header, children }) => {

    return (
        <div className={cx.base}>
            {header && <div className={cx.title}>{header}</div>}
            {children &&
                <div className={cx.toolbar}>
                    {children}
                </div>
            }
        </div>
    )
};

TableHeader.defaultProps = {
    isMediaScreen: false
};

TableHeader.propTypes = {
    header: PropTypes.string,
    children: PropTypes.node
};
