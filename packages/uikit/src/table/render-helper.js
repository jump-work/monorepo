export function renderColumn(columnInfo, rowData) {
    return columnInfo.Cell ?
        columnInfo.Cell({ cell: { value: rowData[columnInfo.accessor] } }) :
        rowData[columnInfo.accessor]
}
