import React, { useState } from 'react';
import PropTypes from 'prop-types';

import { useIfMediaScreen } from '../hooks';
import { FlexTable } from './flex-table';
import { MobTable } from './mob-table';

export const Table = ({ initialSort: initialSortProp, ...rest }) => {
    const [initialSort, setInitialSort] = useState(initialSortProp);
    const [sort, setSort] = useState(initialSortProp || {});

    if (initialSortProp !== initialSort) {
        setInitialSort(initialSortProp);
        setSort(initialSortProp);
    }

    const onSort = arg => {
        setSort(arg);
        rest.onSort(arg);
    };

    return useIfMediaScreen() ? <MobTable {...rest} sort={sort} onSort={onSort}/> :
        <FlexTable {...rest} sort={sort} onSort={onSort} />;
};

Table.defaultProps = {
    initialSort: {
        by: null
    },
    onSort: () => {}
};

Table.propTypes = {
    columns: PropTypes.array.isRequired,
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    onRowSelection: PropTypes.func,
    initialSort: PropTypes.shape({
        by: PropTypes.string,
        desc: PropTypes.bool
    }),
    onSort: PropTypes.func
};
