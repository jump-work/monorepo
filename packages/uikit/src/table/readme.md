# Table

## Импорт
```javascript
import { Table } from '@justlook/uikit';
```

## Использование
```javascript
const el = () => {
    return (
        <Table columns={columns}
               data={data}
               onRowSelection={row => alert(JSON.stringify(row, ' ', 2))}
               header={text('Header', 'Заголовок таблицы')}
               onAdd={() => alert("On add click")}
               initialSort={{ by: 'driver_name', desc: true }}
               onSort={sortObj => alert(JSON.stringify(sortObj, ' ', 2))}
        />
    )
}
```
##### Структура `columns[]` :
```
{
  Header: string, // обязательное - название колонки
  accessor: string, // обязательное - по какому ключу доставать значение в объекте из data[]
  Cell: ({ cell: { value } }) => React.node, // необязательное - произвольный рендерер для ячейки
  main: bool, // обязательно указать одну ячейку как true, по умолчанию false - используется в мобильной верстке как main-текст
  description: bool, // обязательно указать одну ячейку как true, по умолчанию false - используется в мобильной верстке как description-текст
  width: number, // ширина ячейки в десктоп-вёрстке, не обязательное
  sortable: bool // включение сортировки по этому столбцу
  ... остальные параметры в соответствии с документацией к https://github.com/tannerlinsley/react-table будут применимы только к десктоп-таблице 
}
```
