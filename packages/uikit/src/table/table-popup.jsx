import React from 'react';
import PropTypes from 'prop-types';

import { Popup } from '../popup';
import { Icons24 } from '../icons';
import cx from './table-popup.module.scss';
import { Button } from '../button';

const style = {
    top: '100%',
    left: 0,
    width: '100%',
    transform: 'translateY(-100%)',
    maxWidth: 'unset',
    borderRadius: 0
};

export const TablePopup = ({ show, onClose, children }) => {
    return (
        <>
            { show ?
                <Popup element='div'
                       style={style}
                >
                    <Button
                        styling={'hollow'}
                        icon={<Icons24.IconClose />}
                        className={cx.close}
                        style={{ padding: 0 }}
                        onClick={onClose}
                    />
                    <div className={cx.content}>
                        {children()}
                    </div>
                </Popup>

                : null
            }
        </>
    )
};

TablePopup.defaultProps = {
    show: false
};

TablePopup.propTypes = {
    show: PropTypes.bool,
    onClose: PropTypes.func.isRequired,
    children: PropTypes.func.isRequired
};
