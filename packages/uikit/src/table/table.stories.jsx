import React from 'react';
import { storiesOf } from '@storybook/react';
import { text } from '@storybook/addon-knobs';

import { Table } from './table';
import readme from './readme.md';

const data = [
    {
        id: 1,
        date: '01.09.2019',
        time: '06:00',
        driver: 'Прокопчук Марк Анатольевич',
        sex: 'М',
        birth: '1973'
    },
    {
        id: 2,
        date: '01.09.2019',
        time: '06:00 ',
        driver: 'Агарков Александр Анатольевич',
        sex: 'М',
        birth: '1972'
    },
    {
        id: 3,
        date: '01.09.2019',
        time: '06:00',
        driver: 'Рафальский Сергей Олегович',
        sex: 'М',
        birth: '1971'
    }
];

storiesOf('Компоненты/Таблица', module)
    .add(
        'Table',
        () => {
            const columns = [
                    {
                        Header: 'Дата',
                        accessor: 'date',
                        width: 50,
                        Cell: ({ cell: { value } }) => String(value) + 'TT'
                    },
                    {
                        Header: 'Время',
                        accessor: 'time',
                        width: 50,
                        description: true
                    },
                    {
                        Header: 'Водитель',
                        accessor: 'driver',
                        main: true
                    },
                    {
                        Header: 'Пол',
                        accessor: 'sex',
                        width: 50
                    },
                    {
                        Header: 'Год рождения',
                        accessor: 'birth',
                        width: 50
                    }
                ];
            return (
                <div style={{ position: 'relative', height: 600 }}>
                    <Table columns={columns}
                           data={data}
                           onRowSelection={(originalRow) => alert(JSON.stringify(originalRow, ' ', 2))}
                           header={text('Header', 'Заголовок таблицы')}
                           onAdd={() => alert('On add click')}
                           initialSort={{ by: 'driver', desc: true }}
                           onSort={sort => alert(JSON.stringify(sort, ' ', 2))}
                    />
                </div>
            )
        }, {
            notes: { readme }
        }
    );
