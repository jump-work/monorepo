import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { Icons24, Icons16 } from '../icons';
import { Description } from '../description';
import { Button } from '../button';
import cx from './upload.module.scss';

class Upload extends Component {
    constructor(props) {
        super(props);
        this.state = {
            uploadFiles: [],
        };
        this.fileInput = React.createRef();
    }

    onChange = () => {
        const { uploadFiles } = this.state;
        const uploadFile = this.fileInput.current.files;

        const files = uploadFiles;
        files.push(...uploadFile);

        this.setState({
            uploadFiles: files
        }, () => this.sendFiles(this.state.uploadFiles));
    };

    resetUpload = (file) => {
        const { uploadFiles } = this.state;

        const filter = uploadFiles.filter(item => item !== file);

        this.setState({
            uploadFiles: filter
        }, () => this.sendFiles(this.state.uploadFiles));
    };

    sendFiles = (files) => {
        const { onChange, isURI } = this.props;
        isURI
            ? files.map(file => {
                const arrResult = [];
                const reader = new FileReader();
                reader.readAsDataURL(file);
                reader.onload = (e) => {
                    arrResult.push({ file: e.target.result, type: file.type })
                    onChange(arrResult);
                };
                return arrResult
            })
            : onChange(files);
    };

    render() {
        const { uploadFiles } = this.state;
        const { error } = this.props;

        return (
            <div className={cx.wrapper}>
                {uploadFiles.map((element) => {
                    const expFile = element.name.split('.').pop().toUpperCase();

                    return (
                        <div className={cx.base} tabIndex={1} key={element.name}>
                            {
                                element.type === 'image/jpeg'
                                    ? <Icons24.IconImage className={cx.uploadFile}/>
                                    : <Icons24.IconDoc className={cx.uploadFile}/>
                            }

                            <Description.LH16 className={cx.description}>{expFile}</Description.LH16>

                            <Button
                                id={element.lastModified}
                                styling={'hollow'}
                                icon={<Icons16.IconClose/>}
                                className={cx.reset}
                                onClick={() => this.resetUpload(element)}
                            />
                        </div>
                    )
                })}
                {(!uploadFiles?.length || this.props?.multiple) &&
                <label className={cx.base} tabIndex={1}>
                    <input type={'file'} ref={this.fileInput}
                           onChange={(e) => this.onChange(e)}
                           className={cx.file}
                           multiple={this.props?.multiple}
                           accept={this.props?.accept}/>
                    <Icons24.IconPlus className={cx.notFile}/>
                </label>
                }
                {error && <Description.LH16 className={cx.error}>{error}</Description.LH16>}
            </div>
        )
    }
}

Upload.propTypes = {
    onChange: PropTypes.func.isRequired,
    error: PropTypes.string,
    multiple: PropTypes.bool,
    accept: PropTypes.string,
    isURI: PropTypes.bool,
};

export default Upload;
