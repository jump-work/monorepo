import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { text } from '@storybook/addon-knobs';

import { Upload } from './index';
import readme from './readme.md';
import { Header } from '../header';

storiesOf('Компоненты/Загрузка', module)
    .add(
        'Upload',
        () => {
            return (
                <>
                    <Header.H3>Одиночная загрузка</Header.H3>
                    <Upload
                        accept={text('MIME Types', '')}
                        onChange={action()}
                        error={text('Text error', '')}
                    />
                    <Header.H3>Несколько файлов</Header.H3>
                    <Upload
                        multiple={true}
                        accept={text('MIME Types', '')}
                        onChange={action()}
                        error={text('Text error', '')}
                    />
                </>
            )
        }, {
            notes: { readme }
        }
    );

