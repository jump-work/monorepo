import React from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';

import { useIfMediaScreen } from '../hooks';

import cx from './popupFooter.module.scss';

export const Footer = ({children, className, minorButton, onDismissButton, majorButton, ...attrs}) => {
    const cls = classes(cx.base, {
        [className]:className,
    });

    const isMedia = useIfMediaScreen();

    return(
        <div className={cls} {...attrs}>
            {minorButton && <div className={cx.minorButton}>{minorButton}</div>}
            {majorButton &&
                <div className={cx.majorButton}>
                    {isMedia && minorButton
                        ? null
                        : onDismissButton
                    }
                    {majorButton}
                </div>
            }

            {children && <div className={cx.children}>{children}</div>}
        </div>
    )
};

Footer.displayName = 'Popup.Footer';

Footer.propTypes = {
    children: PropTypes.any,
    className: PropTypes.string,
    minorButton: PropTypes.any,
    onDismissButton: PropTypes.any,
    majorButton: PropTypes.any,
};