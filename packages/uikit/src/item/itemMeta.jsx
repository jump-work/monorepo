import React from 'react';
import PropTypes from 'prop-types';

import { Paragraph } from '../paragraph';

import cx from './itemMeta.module.scss';

export const Meta = ({children, ...rest}) => {
    return (
        <div className={cx.base} {...rest}>
            <Paragraph className={cx.meta}>{children}</Paragraph>
        </div>
    )
};

Meta.displayName = 'Item.Meta';

Meta.propTypes = {
    children: PropTypes.any.isRequired,
};