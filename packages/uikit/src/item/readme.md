# Item

## Импорт

```javascript
import { Item } from '@justlook/uikit';
```

## Использование

```javascript
const el = () => {
    return <Item>
               <Item.Header>Hyundai Solaris</Item.Header>
               <Item.Meta>А001АА 154</Item.Meta>
               <Item.Description>На смене · Эконом · 85 600 км</Item.Description>
           </Item>
}
```

## Использование `<Item />` в качестве ссылки

```javascript
const el = () => {
    return <link to={''}>
                <Item>
                    <Item.Header>Hyundai Solaris</Item.Header>
                    <Item.Meta>А001АА 154</Item.Meta>
                    <Item.Description>На смене · Эконом · 85 600 км</Item.Description>
                </Item>
           </link>
}
```