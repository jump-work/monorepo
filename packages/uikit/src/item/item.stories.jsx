import React from 'react';
import { storiesOf } from '@storybook/react';
import { text } from '@storybook/addon-knobs';

import { Item } from './index';
import readme from './readme.md';
import cx from '../story.module.scss';

storiesOf('Компоненты/Вид', module)
    .add(
        'Item',
        () => {
            return (
                <div className={cx.narrowContent}>
                    <Item>
                        <Item.Notification />
                        <Item.Header>{text('Header', 'Hyundai Solaris')}</Item.Header>
                        <Item.Meta>{text('Meta', 'А001АА 154')}</Item.Meta>
                        <Item.Description>
                            {text('Description', 'На смене · Эконом · 85 600 км')}
                        </Item.Description>
                    </Item>
                    <br/>
                    <Item>
                        <Item.Notification alert />
                        <Item.Header>{text('Header', 'Hyundai Solaris')}</Item.Header>
                        <Item.Meta>{text('Meta', 'А001АА 154')}</Item.Meta>
                        <Item.Description>
                            {text('Description', 'На смене · Эконом · 85 600 км')}
                        </Item.Description>
                        <Item.Description>
                            {text('Description', 'На смене · Эконом · 85 600 км')}
                        </Item.Description>
                    </Item>
                </div>
            )
        }, {
            notes: { readme }
        }
    );

