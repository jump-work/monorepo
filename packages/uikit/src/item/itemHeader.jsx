import React from 'react';
import PropTypes from 'prop-types';

import { Header as BaseHeader} from '../header';

import cx from './itemHeader.module.scss';

export const Header = ({children, ...rest}) => {
    return (
        <div className={cx.base} {...rest}>
            <BaseHeader.H3>{children}</BaseHeader.H3>
        </div>
    )
};

Header.displayName = 'Item.Header';

Header.propTypes = {
    children: PropTypes.any.isRequired,
};