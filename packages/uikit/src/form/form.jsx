import React from 'react';
import PropTypes from 'prop-types';
import { Field } from './formField';
import { Title } from './formTitle';

export const Form = ({children, ...attr}) => {
    return (
        <form {...attr} >
            { children }
        </form>
    )
};

Form.propTypes = {
    children: PropTypes.any.isRequired
};

Form.Field = Field;
Form.Title = Title;

export default Form;