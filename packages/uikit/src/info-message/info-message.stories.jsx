import React from 'react';
import { storiesOf } from '@storybook/react';
import { select, text } from '@storybook/addon-knobs';

import { Icons24 } from '../icons';

import { InfoMessage } from './index';
import readme from './readme.md';

storiesOf('Компоненты/Вид', module)
    .add(
        'InfoMessage',
        () => {
            return (
                <InfoMessage
                    type={select('Тип', {
                        default: 'default',
                        info: 'info',
                        error: 'error',
                        success: 'success'
                    }, 'info')}
                    icon={<Icons24.IconInfo/>}
                >
                    {text('Children', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.')}
                </InfoMessage>
            )
        }, {
            notes: { readme }
        }
    );

