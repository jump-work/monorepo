import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import { ParagraphV2 } from '../new-paragraph';

import cx from './info-message.module.scss';

const classMap = {
    default: cx.default,
    info: cx.info,
    error: cx.error,
    success: cx.success,
};

export const InfoMessage = ({ children, icon, className, type, fluid, ...rest }) => {
    const cls = classNames(cx.base, classMap[type], {
        [cx.icon]: icon,
        [cx.fluid]: fluid,
        [cx.children]: children,
    }, className);

    return (
        <div className={cls} {...rest}>
            {icon}
            {children && <ParagraphV2.LH24>{children}</ParagraphV2.LH24>}
        </div>
    );
};

InfoMessage.defaultProps = {
    type: 'info',
    fluid: false,
};

InfoMessage.propTypes = {
    children: PropTypes.any,
    icon: PropTypes.node,
    className: PropTypes.string,
    type: PropTypes.oneOf(['default', 'info', 'error', 'success']),
    fluid: PropTypes.bool
};
