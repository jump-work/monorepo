import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, select, text } from '@storybook/addon-knobs';

import { Image } from './index';
import readme from './readme.md';

storiesOf('Компоненты/Картинки', module)
    .add(
        'Image',
        () => {
            return (
                <Image
                    size={select('Размер', {
                        none: null,
                        mini: 'mini',
                        middle: 'middle',
                        big: 'big',
                    }, null)}
                    src={text('Путь до картинки','https://cdn.iconscout.com/icon/free/png-256/react-2-458175.png')}
                    avatar={boolean('Аватар', false)}
                    round={boolean('Круглая картинка', false)}
                />
            )
        }, {
            notes: { readme }
        }
    );

