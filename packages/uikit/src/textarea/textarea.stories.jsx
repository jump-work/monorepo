import React from 'react';
import { storiesOf } from '@storybook/react';
import { boolean, text } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import { Textarea } from './index';
import readme from './readme.md';

storiesOf('Компоненты/Элементы формы', module)
    .add(
        'Textarea',
        () => {
            return (
                <Textarea
                    label={text('Label', 'Название поля')}
                    placeholder={text('Placeholder', 'Textarea')}
                    error={text('Error', '')}
                    onChange={action()}
                    disabled={boolean('Disabled', false)}
                />
            )
        }, {
            notes: { readme }
        }
    );
