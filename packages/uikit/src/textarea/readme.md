# Textarea

## Импорт

```javascript
import { Textarea } from '@justlook/uikit';
```

## Использование

```javascript
const el = () => {
    return <Textarea
                className={''}
                label={'Название поля'}
                placeholder={'textarea'}
                error={''}
                onChange={(value) => console.log(value)}
            />
}
```

В качестве дополнительных props, можно указать атрибуты которые поддерживает тег `<textarea></textarea>`

```javascript
const el = () => {
    return <Textarea
                maxlength={12}
                name={'textarea'}
                rows={10}
                value={'Значение поля'}
            />
}
```