import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Link } from '../index';
import { ErrorScreen } from './error-screen';

export const ErrorScreen503 = ({ footerContent }) => {
    return (
        <ErrorScreen
            error={{
                title: 'Сервис временно недоступен',
                message: 'Перезагрузите страницу или попробуйте зайти позже. Код ошибки 503',
            }}
            footerContent={() => (
                <Fragment>
                    <Link onClick={() => window.location.reload()} to='/'>
                        Обновить страницу
                    </Link>

                    {footerContent()}
                </Fragment>
            )}
        />
    );
};

ErrorScreen503.defaultProps = {
    footerContent: () => null,
};

ErrorScreen503.propTypes = {
    footerContent: PropTypes.func,
};