import React from 'react';
import PropTypes from 'prop-types';
import { Header, Paragraph } from '../index';

import cx from './error-screen.module.scss';

export const ErrorScreen = ({ error, footerContent }) => {
    return (
        <div className={cx.error}>
            <Header.H1>{error?.title}</Header.H1>

            <Paragraph>{error?.message}</Paragraph>

            {footerContent()}
        </div>
    );
};

ErrorScreen.defaultProps = {
    footerContent: () => null,
};

ErrorScreen.propTypes = {
    error: PropTypes.shape({
        title: PropTypes.string,
        message: PropTypes.string,
    }),
    footerContent: PropTypes.func,
};