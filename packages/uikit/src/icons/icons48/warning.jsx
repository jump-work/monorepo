import React from 'react';

export const IconWarning = (props) => {
    return (
        <svg {...props} width='48' height='48' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#F59F00' fillRule='evenodd' clipRule='evenodd' d='M24 44c11.046 0 20-8.954 20-20S35.046 4 24 4 4 12.954 4 24s8.954 20 20 20zm0-32a2 2 0 012 2v14a2 2 0 11-4 0V14a2 2 0 012-2zm0 24a2 2 0 100-4 2 2 0 000 4z'/>
        </svg>
    );
};
