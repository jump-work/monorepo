# Иконки 48*48

## Импорт
```javascript
import { Icons48 } from '@justlook/uikit';
```

## Использование
```javascript
const el = () => {
    return <Icons48.IconCheck/>
}
```

## Дополнительные атрибуты
В качестве props, омпоненту иконки можно передать атрибуты, которые поддерживает тег ```<svg></svg>``` 

```javascript
const el = () => {
    return <Icons48.IconCheck className={'class'}/>
}
```
