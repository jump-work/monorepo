import React from 'react';
import { storiesOf } from '@storybook/react';

import Icons16 from './icons16';
import Icons24 from './icons24';
import Icons48 from './icons48';

import cx from '../story.module.scss';
import '../index.scss';

import readme24 from './icons24/readme.md';
import readme16 from './icons16/readme.md';
import readme48 from './icons48/readme.md';

const icon24 = [];
for(let icon in Icons24) icon24.push(Icons24[icon]);

const icon16 = [];
for(let icon in Icons16) icon16.push(Icons16[icon]);

const icon48 = [];
for(let icon in Icons48) icon48.push(Icons48[icon]);

storiesOf('Иконки', module)
    .add(
        'Size 24x24',
        () => {
            return (
                <div className={cx['icon-grid']}>
                    {
                        icon24.map(Icon => (
                            <div key={Icon.name} className={cx['icon-item']}>
                                <Icon/>
                                <span className={cx['icon-name']}>{Icon.name.replace('Icon', '')}</span>
                            </div>
                        ))
                    }
                </div>
            )
        },
        {
            info: {
                text: 'Иконки 24x24',
            },
            notes: { readme24 },
        }
    ).add(
        'Size 16x16',
        () => {
            return (
                <div className={cx['icon-grid']}>
                    {
                        icon16.map(Icon => (
                            <div key={Icon.name} className={cx['icon-item']}>
                                <Icon/>
                                <span className={cx['icon-name']}>{Icon.name.replace('Icon', '')}</span>
                            </div>
                        ))
                    }
                </div>
            )
        },
    {
            info: {
                text: 'Иконки 16x16',
            },
            notes: { readme16 }
        }
    ).add(
        'Size 48x48',
        () => {
            return (
                <div className={cx['icon-grid']}>
                    {
                        icon48.map(Icon => (
                            <div key={Icon.name} className={cx['icon-item']}>
                                <Icon/>
                                <span className={cx['icon-name']}>{Icon.name.replace('Icon', '')}</span>
                            </div>
                        ))
                    }
                </div>
            )
        }, {
            info: {
                text: 'Иконки 48x48',
            },
            notes: { readme48 }
        }
)
