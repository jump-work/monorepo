import icons16 from './icons16';
import icons24 from './icons24';
import icons48 from './icons48'

export const Icons16 = {
    ...icons16
};

export const Icons24 = {
    ...icons24
};

export const Icons48 = {
    ...icons48
}
