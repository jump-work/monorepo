import React from 'react';

export const IconFly = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M4 3a1 1 0 00-1 1l3 8-3 8a1 1 0 001 1l16-8a1 1 0 000-2L4 3zm12 8L6 6l2 5h8zm0 2H8l-2 5 10-5z'/>
        </svg>
    );
};
