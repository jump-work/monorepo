import React from 'react';

export const IconChart = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M4 12a8 8 0 0015.938 1H12a1 1 0 01-1-1V4.062A8.001 8.001 0 004 12zm9-2.558l3.65-3.953A7.956 7.956 0 0013 4.062v5.38zm5.119-2.596L14.284 11h5.654a7.965 7.965 0 00-1.82-4.154zM22 11.993C21.996 6.473 17.52 2 12 2 6.477 2 2 6.477 2 12s4.477 10 10 10c5.52 0 9.996-4.473 10-9.993V12v-.007z'/>
        </svg>
    );
};