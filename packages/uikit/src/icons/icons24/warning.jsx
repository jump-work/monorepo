import React from 'react';

export const IconWarning = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M5 12a7 7 0 1114 0 7 7 0 01-14 0zm7 9a9 9 0 110-18 9 9 0 010 18zm0-7a1 1 0 001-1V8a1 1 0 10-2 0v5a1 1 0 001 1zm0 1a1 1 0 110 2 1 1 0 010-2z'/>
        </svg>
    );
};
