import React from 'react';

export const IconReceipt = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M4 4a1 1 0 011-1h14a1 1 0 011 1v16a1 1 0 01-1.371.928l-2.129-.851-2.129.851a1 1 0 01-.818-.034L12 20.118l-1.553.776a1 1 0 01-.818.035L7.5 20.076l-2.129.851A1 1 0 014 20V4zm2 1v13.523l1.129-.451a1 1 0 01.742 0l2.083.833 1.599-.8a1 1 0 01.894 0l1.599.8 2.083-.834a1 1 0 01.742 0l1.129.452V5H6z'/>
            <path fill='#333' d='M8 9a1 1 0 011-1h6a1 1 0 110 2H9a1 1 0 01-1-1zM8 12a1 1 0 011-1h6a1 1 0 110 2H9a1 1 0 01-1-1z'/>
        </svg>
    );
};
