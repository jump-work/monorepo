import React from 'react';

export const IconSetting = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M21 5H10.83a3.001 3.001 0 00-5.66 0H3a1 1 0 000 2h2.17a3.001 3.001 0 005.66 0H21a1 1 0 100-2zM8 7a1 1 0 110-2 1 1 0 010 2zm5.17 4H3a1 1 0 100 2h10.17a3.001 3.001 0 005.66 0H21a1 1 0 100-2h-2.17a3.001 3.001 0 00-5.66 0zM17 12a1 1 0 11-2 0 1 1 0 012 0zm-6.17 5H21a1 1 0 110 2H10.83a3.001 3.001 0 01-5.66 0H3a1 1 0 110-2h2.17a3.001 3.001 0 015.66 0zM7 18a1 1 0 102 0 1 1 0 00-2 0z'/>
        </svg>
    );
};