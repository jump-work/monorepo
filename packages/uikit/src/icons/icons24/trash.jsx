import React from 'react';

export const IconTrash = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M15 3H9v2h6V3zM5 6h14v2h-1v11a2 2 0 01-2 2H8a2 2 0 01-2-2V8H5V6zm3 2h8v11H8V8z'/>
        </svg>
    );
};
