import React from 'react';

export const IconCheckCircleFill = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M21 12a9 9 0 10-18 0 9 9 0 0018 0zm-4.232-2.36a1 1 0 10-1.536-1.28l-4.3 5.159-2.225-2.226a1 1 0 00-1.414 1.414l3 3a1 1 0 001.475-.067l5-6z'/>
        </svg>
    );
};
