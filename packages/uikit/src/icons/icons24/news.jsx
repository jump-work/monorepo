import React from 'react';

export const IconNews = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M5 4h14v16H5V4zM3 4a2 2 0 012-2h14a2 2 0 012 2v16a2 2 0 01-2 2H5a2 2 0 01-2-2V4zm5 3h2v4H8V7zM6 5h6v8H6V5zm7 0h5v2h-5V5zm5 3h-5v2h5V8zm-5 3h5v2h-5v-2zm5 5v-2H6v2h12z'/>
        </svg>
    );
};