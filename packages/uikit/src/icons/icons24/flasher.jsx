import React from 'react';

export const IconFlasher = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' d='M7 1a1 1 0 010 2 2 2 0 00-2 2 1 1 0 01-2 0 4 4 0 014-4z'/>
            <path fillRule='evenodd' clipRule='evenodd' fill='#333' d='M17 6l3 12a1 1 0 110 2H4a1 1 0 110-2L7 6l2-2h6l2 2zM9 6h6l3 12H6L9 6z'/>
        </svg>
    );
};
