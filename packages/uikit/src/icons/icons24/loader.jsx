import React from 'react';

import cx from '../animate.module.scss';

export const IconLoader = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path className={cx.base} fill='#333' fillRule='evenodd' clipRule='evenodd' d='M16.702 5.528A8 8 0 1020 12h2a10 10 0 11-4.122-8.09l-1.176 1.618z'/>
        </svg>
    );
};