import React from 'react';

export const IconBox = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path  fill='#333' fillRule='evenodd' clipRule='evenodd' d='M11 2h2l8 3 1 2v9l-1 2-8 3h-2l-8-3-1-2V8 7l1-2 8-3zm1 2l8 3-8 3-8-3 8-3zM4 16V9l7 3v7l-7-3zm9 3l7-3V9l-7 3v7z'/>
        </svg>
    );
};
