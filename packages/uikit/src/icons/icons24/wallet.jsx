import React from 'react';

export const IconWallet = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M20 6H4v2h6a4 4 0 010 8H4v2h16V6zM4 14v-4h6a2 2 0 110 4H4zM2 6v12a2 2 0 002 2h16a2 2 0 002-2V6a2 2 0 00-2-2H4a2 2 0 00-2 2zm8 7a1 1 0 100-2 1 1 0 000 2z'/>
        </svg>
    );
};