import React from 'react';

export const IconStore = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M20 4a1 1 0 00-1-1H5a1 1 0 000 2h14a1 1 0 001-1zM4.562 12l1-4h12.876l1 4H4.562zm16.157 2H20v6a1 1 0 11-2 0v-6h-5v6a1 1 0 01-1 1H5a1 1 0 01-1-1v-6h-.72a1 1 0 01-.97-1.242l1.5-6A1 1 0 014.78 6h14.44a1 1 0 01.97.757l1.5 6A1 1 0 0120.72 14zM6 14v5h5v-5H6z'/>
        </svg>
    );
};