import React from 'react';

export const IconSearch = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M15 10a5 5 0 11-10 0 5 5 0 0110 0zm-1.092 5.808a7 7 0 111.477-1.336l.03.028L20 19.086a1 1 0 01-1.414 1.414L14 15.914a1.004 1.004 0 01-.092-.106z'/>
        </svg>
    );
};