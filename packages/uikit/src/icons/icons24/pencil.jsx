import React from 'react';

export const IconPencil = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M16.728 6.272l-11.2 11.2-.82 2.235 2.234-.82 11.2-11.2-1.414-1.415zm.858-.858L19 4l1.414 1.414L19 6.828l-1.414-1.414z'/>
        </svg>
    );
};
