import React from 'react';

export const IconRouble = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M12 20a8 8 0 110-16 8 8 0 010 16zM2 12C2 6.477 6.477 2 12 2s10 4.477 10 10-4.477 10-10 10S2 17.523 2 12zm8-6H9v8h-.5a1 1 0 100 2H9v2h2v-2h3.5a1 1 0 100-2H11v-1h2.6c1.916 0 3.4-1.606 3.4-3.5S15.516 6 13.6 6H10zm1 2v3h2.6c.735 0 1.4-.633 1.4-1.5S14.335 8 13.6 8H11z'/>
        </svg>
    );
};