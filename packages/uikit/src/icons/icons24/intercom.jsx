import React from 'react';

export const IconIntercom = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M18 4H6a1 1 0 00-1 1v11a1 1 0 001 1h9.169a5 5 0 012.572.712l1.259.756V5a1 1 0 00-1-1zm3 1a3 3 0 00-3-3H6a3 3 0 00-3 3v11a3 3 0 003 3h9.169a3 3 0 011.543.427L21 22V5z' />
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M6.03 13.758a1 1 0 011.213-.728l1.09.272c1.75.438 5.583.438 7.335 0l1.09-.272a1 1 0 11.485 1.94l-1.09.272c-2.07.518-6.236.518-8.306 0l-1.09-.272a1 1 0 01-.727-1.213z' />
        </svg>
    );
};
