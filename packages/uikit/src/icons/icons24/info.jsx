import React from 'react';

export const IconInfo = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M5 12a7 7 0 1014 0 7 7 0 00-14 0zm7-9a9 9 0 100 18 9 9 0 000-18zm0 7a1 1 0 011 1v5a1 1 0 11-2 0v-5a1 1 0 011-1zm0-1a1 1 0 100-2 1 1 0 000 2z'/>
        </svg>
    );
};