import React from 'react';

export const IconMenu = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M5 8a1 1 0 000 2h14a1 1 0 100-2H5zm0 6a1 1 0 100 2h14a1 1 0 100-2H5z'/>
        </svg>
    );
};