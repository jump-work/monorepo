import React from 'react';

export const IconDoc = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M6 6a1 1 0 011-1h5v4a2 2 0 002 2h4v7a1 1 0 01-1 1H7a1 1 0 01-1-1V6zm11.761 3a.992.992 0 00-.076-.08L14 5.457V9h3.761zM7 3a3 3 0 00-3 3v12a3 3 0 003 3h10a3 3 0 003-3V9.648a3 3 0 00-.946-2.186l-3.883-3.648A3 3 0 0013.117 3H7z'/>
        </svg>
    );
};