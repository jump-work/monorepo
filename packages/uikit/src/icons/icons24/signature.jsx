import React from 'react';

export const IconSignature = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M5 21.003v-5.5L15.5 4.5c.5-.5 1.5-.5 2 0l3.5 3c.5.5.5 1.5 0 2L12.33 19h7.741c.513 0 .929.448.929 1s-.416 1-.929 1L5 21.003zM7 16.5L16.5 6l3 2.5L9.503 19 7 19.003V16.5z'/>
        </svg>
    );
};
