import React from 'react';

export const IconSort = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M4 8a1 1 0 011-1h14a1 1 0 110 2H5a1 1 0 01-1-1zm3 4a1 1 0 011-1h8a1 1 0 110 2H8a1 1 0 01-1-1zm4 3a1 1 0 100 2h2a1 1 0 100-2h-2z'/>
        </svg>
    );
};