import React from 'react';

export const IconPanel = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M11 6h9v12h-9V6zM9 6H4v12h5V6zM2 6a2 2 0 012-2h16a2 2 0 012 2v12a2 2 0 01-2 2H4a2 2 0 01-2-2V6zm3 2h3v2H5V8zm3 3H5v2h3v-2z'/>
        </svg>
    );
};