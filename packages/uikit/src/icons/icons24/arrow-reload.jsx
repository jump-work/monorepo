import React from 'react';

export const IconArrowReload = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' d='M16.486 3.336a1 1 0 10-1.973.328l.154.919c-.915-.39-1.911-.619-2.981-.577A8 8 0 1020 12a1 1 0 10-2 0 6 6 0 11-6.236-5.995 4.79 4.79 0 011.929.34l-.564.227a1 1 0 10.742 1.856l2.484-.993a.999.999 0 00.63-1.114l-.499-2.985z'/>
        </svg>
    );
};
