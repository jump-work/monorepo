import React from 'react';

export const IconCheck = (props) => {
    return (
        <svg {...props} width='24' height='24' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M18.6 6.2a1 1 0 01.2 1.4l-7.5 10a1 1 0 01-1.446.163l-4.5-3.81a1 1 0 111.292-1.526l3.69 3.124L17.2 6.4a1 1 0 011.4-.2z'/>
        </svg>
    );
};