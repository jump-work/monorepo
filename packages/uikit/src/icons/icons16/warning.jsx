import React from 'react';

export const IconWarning = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M8 0a8 8 0 110 16A8 8 0 018 0zm1 11a1 1 0 10-2 0 1 1 0 002 0zm0-3a1 1 0 11-2 0V5a1 1 0 012 0v3z'/>
        </svg>
    );
};
