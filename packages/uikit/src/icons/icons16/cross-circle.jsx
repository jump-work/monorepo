import React from 'react';

export const IconCrossCircle = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' d='M4.293 4.293a1 1 0 000 1.414L6.586 8l-2.293 2.293a1 1 0 101.414 1.414L8 9.414l2.293 2.293a1 1 0 001.414-1.414L9.414 8l2.293-2.293a1 1 0 00-1.414-1.414L8 6.586 5.707 4.293a1 1 0 00-1.414 0z'/>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M0 8a8 8 0 1116 0A8 8 0 010 8zm8 6A6 6 0 118 2a6 6 0 010 12z'/>
        </svg>
    );
};
