import React from 'react';

export const IconArrowDownSolid = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' d='M4 6l4 4 4-4H4z'/>
        </svg>
    );
};