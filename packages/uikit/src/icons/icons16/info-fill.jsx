import React from 'react';

export const IconInfoFill = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M8 16A8 8 0 108 0a8 8 0 000 16zM9 5a1 1 0 11-2 0 1 1 0 012 0zm0 3a1 1 0 10-2 0v3a1 1 0 102 0V8z'/>
        </svg>
    );
};
