import React from 'react';

export const IconCheckCircle = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' d='M11.809 6.588a1 1 0 10-1.618-1.176L6.754 8.513l-1.084-.88a1 1 0 10-1.34 1.484l1.91 1.625a1 1 0 001.478-.154l4.09-4z'/>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M0 8a8 8 0 1116 0A8 8 0 010 8zm8 6A6 6 0 118 2a6 6 0 010 12z'/>
        </svg>
    );
};
