import React from 'react';

export const IconArrowUp = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M12.696 10.718a1 1 0 01-1.414-.022L8.1 7.414l-3.404 3.304a1 1 0 01-1.392-1.436l4.12-4a1 1 0 011.415.022l3.879 4a1 1 0 01-.022 1.414z'/>
        </svg>
    );
};