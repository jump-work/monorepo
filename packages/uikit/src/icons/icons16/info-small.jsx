import React from 'react';

export const IconInfoSmall = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' d='M7.5 11V7h1v4h-1zM8.5 6V5h-1v1h1z'/>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M8 14A6 6 0 108 2a6 6 0 000 12zm0-1A5 5 0 108 3a5 5 0 000 10z'/>
        </svg>
    );
};
