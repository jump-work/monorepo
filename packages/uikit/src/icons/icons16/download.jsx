import React from 'react';

export const IconDownload = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M8 1a1 1 0 011 1v7l2.4-1.8a1 1 0 011.2 1.6l-3.995 2.996a.996.996 0 01-1.215-.004L3.4 8.8a1 1 0 111.2-1.6L7 9V2a1 1 0 011-1zm5 14a1 1 0 100-2H3a1 1 0 100 2h10z'/>
        </svg>
    );
};


