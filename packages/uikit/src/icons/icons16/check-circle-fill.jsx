import React from 'react';

export const IconCheckCircleFill = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M0 8a8 8 0 1116 0A8 8 0 010 8zm12.809-2.412a1 1 0 10-1.618-1.176L6.754 9.513 4.67 7.633a1 1 0 10-1.34 1.484l2.91 2.625a1 1 0 001.478-.154l5.09-6z'/>
        </svg>
    );
};
