import React from 'react';

export const IconSort = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M1 4a1 1 0 011-1h12a1 1 0 110 2H2a1 1 0 01-1-1zm2 4a1 1 0 011-1h8a1 1 0 110 2H4a1 1 0 01-1-1zm4 3a1 1 0 100 2h2a1 1 0 100-2H7z'/>
        </svg>
    );
};