import React from 'react';

export const IconLock = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fillRule='evenodd' clipRule='evenodd' fill='#333' d='M4 6V5a4 4 0 118 0v1l2 2v5l-2 2H4l-2-2V8l2-2zm2-1a2 2 0 114 0v1H6V5z'/>
        </svg>
    );
};
