import React from 'react';

export const IconExternal = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' d='M3 4l1-1h2a1 1 0 000-2H4C2 1 1 2 1 4v8c0 2 1 3 3 3h8c2 0 3-1 3-3v-2a1 1 0 10-2 0v2l-1 1H4l-1-1V4z'/>
            <path fill='#333' d='M10 1a1 1 0 000 2h2L6 8a1 1 0 002 2l5-6v2a1 1 0 102 0V2l-1-1h-4z'/>
        </svg>
    );
};
