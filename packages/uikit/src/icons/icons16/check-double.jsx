import React from 'react';

export const IconCheckDouble = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fillRule='evenodd' clipRule='evenodd' fill='#333' d='M10.588 3.191a1 1 0 01.22 1.397l-5.09 7a1 1 0 01-1.479.154L1.33 9.117a1 1 0 011.34-1.484l2.084 1.88 4.437-6.101a1 1 0 011.397-.22zM14.581 3.186a1 1 0 01.233 1.395l-5 7a1 1 0 01-1.628-1.162l5-7a1 1 0 011.395-.233z'/>
        </svg>
    );
};
