# Иконки 16*16

## Импорт

```javascript
import { Icons16 } from '@justlook/uikit';
```

## Использование

```javascript
const el = () => {
    return <Icons16.IconClose/>
}
```
## Дополнительные атрибуты

В качестве props, компоненту иконки можно передать атрибуты, которые поддерживает тег ```<svg></svg>```

```javascript
const el = () => {
    return <Icons16.IconClose className={'class'}/>
}
```