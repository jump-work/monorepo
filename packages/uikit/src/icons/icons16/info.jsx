import React from 'react';

export const IconInfo = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M2 8a6 6 0 1012 0A6 6 0 002 8zm6-8a8 8 0 100 16A8 8 0 008 0zm0 7a1 1 0 011 1v3a1 1 0 11-2 0V8a1 1 0 011-1zm0-1a1 1 0 100-2 1 1 0 000 2z'/>
        </svg>
    );
};
