import React from 'react';

export const IconCheck = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M12.588 3.191a1 1 0 01.22 1.397l-5.09 7a1 1 0 01-1.479.154L3.33 9.117a1 1 0 011.34-1.484l2.084 1.88 4.437-6.101a1 1 0 011.397-.22z'/>
        </svg>
    );
};