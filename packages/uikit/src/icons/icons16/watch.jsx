import React from 'react';

export const IconWatch = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M2 8a6 6 0 1012 0A6 6 0 002 8zm6-8a8 8 0 100 16A8 8 0 008 0zm0 3a1 1 0 011 1v3h2a1 1 0 110 2H8a1 1 0 01-1-1V4a1 1 0 011-1z'/>
        </svg>
    );
};