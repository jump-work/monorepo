import React from 'react';

export const IconCrossCircleFill = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path fill='#333' fillRule='evenodd' clipRule='evenodd' d='M8 0a8 8 0 100 16A8 8 0 008 0zm3.707 4.293a1 1 0 010 1.414L9.414 8l2.293 2.293a1 1 0 01-1.414 1.414L8 9.414l-2.293 2.293a1 1 0 01-1.414-1.414L6.586 8 4.293 5.707a1 1 0 011.414-1.414L8 6.586l2.293-2.293a1 1 0 011.414 0z'/>
        </svg>
    );
};
