import React from 'react';

import cx from '../animate.module.scss';

export const IconLoader = (props) => {
    return (
        <svg {...props} width='16' height='16' fill='none' xmlns='http://www.w3.org/2000/svg'>
            <path className={cx.base} fill='#333' fillRule='evenodd' clipRule='evenodd' d='M10.939 3.955A5 5 0 1013 8h2a7 7 0 11-2.886-5.663L10.94 3.955z'/>
        </svg>
    );
};