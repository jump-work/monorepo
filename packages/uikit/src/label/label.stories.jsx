import React from 'react';
import { storiesOf } from '@storybook/react';
import { text, select } from '@storybook/addon-knobs';

import { Label, LabelThemes } from './index';
import readme from './readme.md';

storiesOf('Компоненты/Метки', module)
    .add(
        'Label',
        () => {
            return <>
                {LabelThemes.map(theme => (<Label key={theme}
                                                  styleName={{display: 'block', margin: '0.5rem 0'}}
                                                  text={text('Label', 'Тестовая метка')}
                                                  theme={theme}/>))}
            </>
        }, {notes: { readme }}
    );
