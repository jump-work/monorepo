import React, { useState } from 'react';
import { storiesOf } from '@storybook/react';
import { text, select, boolean } from '@storybook/addon-knobs';

import { Input, Password } from './index';
import readme from './readme.md';

storiesOf('Компоненты/Элементы формы', module)
    .add(
        'Input',
        () => {
            const [value, setValue] = useState('');

            return(
                <Input
                    id={text('Id', '')}
                    name={text('Name', '')}
                    type={select('Type', {
                        email: 'email',
                        url: 'url',
                        tel: 'tel',
                        password: 'password',
                        text: 'text',
                        number: 'number',
                        date: 'date'
                    }, 'text')}
                    placeholder={text('Placeholder', 'example@example.com')}
                    label={text('Label', 'Эл. Почта')}
                    error={text('Error', '')}
                    onChange={setValue}
                    value={value}
                    isClearable={boolean('isClerable', true)}
                    disabled={boolean('Disabled', false)}
                    readOnly={boolean('readOnly', false)}
                />
            )
        }, {
            notes: { readme }
        }
    ).add(
        'Password',
        () => {
            const [password, setPassword] = useState('');
            return(
                <Password
                    value={password}
                    placeholder={text('Placeholder', '')}
                    label={text('Label', 'Пароль')}
                    error={text('Error', '')}
                    onChange={setPassword}
                    disabled={boolean('Disabled', false)}
                />
            )
        }, {
            notes: { readme }
        }
    );
