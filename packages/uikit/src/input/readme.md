# Input

## Импорт

```javascript
import { Input, Password } from '@justlook/uikit';
```

## Использование
### Input

```javascript
const el = () => {
    const [value, setValue] = useState('');
    return (
        <Input 
            type={'email'}
            placeholder={'Email'}
            label={'Email'}
            className={''}
            error={''}
            value={value}
            onChange={setValue}
        />
    )
}
```

### Password
```javascript
const el = () => {
    const [value, setValue] = useState('');
    return (
        <Password 
            placeholder={'Пароль'}
            label={'Пароль'}
            className={''}
            error={''}
            value={value}
            onChange={setValue}
        />
    )
}
```