import React from 'react';
import { storiesOf } from '@storybook/react';

import { Ellipsis } from './index';
import readme from './readme.md';

storiesOf('Компоненты/Загрузчик', module)
    .add(
        'Ellipsis',
        () => {
            return (
                <Ellipsis/>
            )
        }, {
            notes: { readme }
        }
    ).add(
    'Ellipsis-invert',
    () => {
        return (
            <div style={{background: '#258ffb', boxSizing: 'border-box'}}>
                <Ellipsis invert/>
            </div>
        )
    }, {
        notes: { readme }
    }
);