# Загрузчик

## Импорт

```javascript
import { Ellipsis } from '@justlook/uikit';
```

## Использование

```javascript
const el = () => {
    return <Ellipsis />
}
```
Инверсия цвета:

```javascript
const el = () => {
    return <Ellipsis invert />
}
```