import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import cx from './ellipsis.module.scss';

export const Ellipsis = ({invert}) => {
    const clsName = classNames(cx.container, {
        [cx.invert]: invert
    });

    return (
        <div className={clsName}>
            <div />
            <div />
            <div />
            <div />
        </div>
    )
};

Ellipsis.defaultProps = {
  invert: false,
};

Ellipsis.propTypes = {
    invert: PropTypes.bool,
};
