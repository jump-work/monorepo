# Chips

## Импорт

```javascript
import { Chips } from '@justlook/uikit';
```

## Использование

```javascript
const defaultChips = () => {
    return (
        <Chips
            label={'Статус'}
        />
    )
}

const deleteBtnChips = () => {
    return (
        <Chips
            label={'Статус'}
            onDelete={() => {}}
        />
    )
}

const propsChips = () => {
    return (
        <Chips
            label={'Статус'}
            avatar={<Icons16.IconCheck />}
            color={'alert'}
            onClick={() => {}}
        />
    )
}
```

## Props

- **label**: Текст внутри chips
- **onDelete**: Если передан данный props, у `Chips` появляется кнопка `close`
- **element**: Передает тег, по умолчанию `div`
- **color**: Определяет цвет `Chips` - `success`, `warning`, `alert`, `primary`
- **avatar**: Добавляет аватар для `Chips`
- **onClick**: Делает `Chips` кликабельным
