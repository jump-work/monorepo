import React from 'react';
import { storiesOf } from '@storybook/react';
import { action } from '@storybook/addon-actions';
import { text, select } from '@storybook/addon-knobs';

import { Chips } from './index';
import { Icons16 } from '../icons';
import readme from './readme.md';

storiesOf('Компоненты/Статус', module)
    .add(
        'Chips',
        () => {
            return (
                <Chips
                    label={text('Label', 'Статус')}
                    element={'a'}
                    href={'#'}
                />
            )
        }, {notes: { readme }}
    ).add(
    'Chips Close',
    () => {
        return (
            <Chips
                label={text('Label', 'Статус')}
                onDelete={action()}
            />
        )
    }, {notes: { readme }}
).add(
    'Chips Check',
    () => {
        return (
            <Chips
                label={text('Label', 'Статус')}
                avatar={<Icons16.IconCheck />}
                color={select('Color', {
                    alert: 'alert',
                    primary: 'primary',
                }, 'alert')}
                onClick={action()}
            />
        )
    }, {notes: { readme }}
);