import React, { Fragment } from 'react';
import { storiesOf } from '@storybook/react';
import { text } from '@storybook/addon-knobs';

import { ParagraphV2 } from './index';
import readme from './readme.md';

storiesOf('Типографика', module)
    .add(
        'Параграф с выбором line-height',
        () => {
            return (
                <Fragment>
                    <ParagraphV2.LH24>{text('Children LH24', 'Параграф')}</ParagraphV2.LH24>
                    <ParagraphV2.LH18>{text('Children LH18', 'Параграф')}</ParagraphV2.LH18>
                </Fragment>
            )
        }, {
            notes: { readme }
        }
    );