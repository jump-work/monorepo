import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import cx from './new-paragraph.module.scss'

const makeParagraph = (LH) => {
    const component = ({children, className, ...rest}) => {
        const cls = classNames(
            cx.base,
            className,
            {[cx['base-24']]: LH === '24', [cx['base-18']]: LH === '18'},
        );
        return <p className={cls} {...rest}>{children}</p>
    };

    component.displayName = 'Paragraph.LH' + LH.toUpperCase();
    component.propTypes = {
        children: PropTypes.any,
        className: PropTypes.string,
    };
    return component;
};

export const Paragraph = {
    LH24: makeParagraph('24'),
    LH18: makeParagraph('18'),
}