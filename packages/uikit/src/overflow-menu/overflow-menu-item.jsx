import React     from 'react';
import PropTypes from 'prop-types';

import cx from './overflow-menu-item.module.scss';

export const OverflowMenuItem = ({ onItemClick, value, index, children }) => {

    const _onItemClick = () => onItemClick(value, index);

    return (
        <div className={cx.base} onClick={_onItemClick}>
            <div className={cx.content}>{children}</div>
        </div>
    )
};

OverflowMenuItem.defaultProps = {
    onItemClick: () => {}
};

OverflowMenuItem.propTypes = {
    onItemClick: PropTypes.func,
    value: PropTypes.any,
    children: PropTypes.any,
    index: PropTypes.number
};
