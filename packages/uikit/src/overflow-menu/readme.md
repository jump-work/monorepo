# Pagination

## Импорт

```javascript
import { OverflowMenu } from '@justlook/uikit';
```

## Использование

```javascript
    <OverflowMenu
        icon={<Icons24.IconSort/>}
        onItemSelect={value => alert("Выбранный элемент " + value)}
    >
        <OverflowMenu.Item value='watermelon'>арбуз</OverflowMenu.Item>
        <OverflowMenu.Item value='tomato'>помидор</OverflowMenu.Item>
        <OverflowMenu.Item value='apple'>яблоко</OverflowMenu.Item>
    </OverflowMenu>
```
