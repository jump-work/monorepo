import React from 'react';
import PropTypes from 'prop-types';
import { storiesOf } from '@storybook/react';

import readme from './readme.md';
import cx from '../story.module.scss';
import { Search } from './search';
import { Button } from '../button';
import { Icons24 } from '../icons';

const data = [
    {
        main: 'Apple',
        description: '+7 (907) 345-67-89 example@example.com'
    },
    {
        main: 'Banana',
        description: '+7 (907) 345-67-89 example@example.com'
    },
    {
        main: 'Yogurt without sugar',
        description: '+7 (907) 345-67-89 example@example.com'
    },
    {
        main: 'Chocolate with sugar',
        description: '+7 (907) 345-67-89 example@example.com'
    },
    {
        main: 'Bread and butter',
        description: '+7 (907) 345-67-89 example@example.com'
    },
    {
        main: 'Butter',
        description: '+7 (907) 345-67-89 example@example.com'
    }
];

const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

const CustomItemList = (props) => {
    return <Search.ListItem {...{...props, main: props.main + ' (custom)'}} />
};

CustomItemList.propTypes = {
    main: PropTypes.any,
}

storiesOf('Компоненты/Поиск', module)
    .add(
        'Search',
        () => {
            const [open, setOpen] = React.useState(false);
            const [results, setResults] = React.useState([{ main: 'initial', description: 'initial'}]);
            const [loading, setLoading] = React.useState(false);
            const doSearch = async searchTerm => {
                setLoading(true);
                await wait(500);
                setLoading(false);
                setResults(data.filter(item => item.main.toLowerCase().includes(searchTerm)));
            };
            return (
                <div className={cx.position}>
                    <div className={cx.device}>
                        <div className={cx.header}>
                            <Button
                                icon={<Icons24.IconSearch/>}
                                onClick={() => setOpen(!open)}
                            />
                        </div>
                        <Search open={open}
                                loading={loading}
                                onSearch={doSearch}
                                results={results}
                                listItemType={CustomItemList}
                                hitSearchOnEnterOnly={true}
                                debounce={1000}
                        />
                        <div className={cx.content}>
                            content<br/>
                            content<br/>
                            content<br/>
                            content<br/>
                            content<br/>
                            content<br/>
                            content<br/>
                            content<br/>
                            content<br/>
                            content<br/>
                            content<br/>
                            content<br/>
                            content<br/>
                            content<br/>
                        </div>
                    </div>
                </div>
            )
        }, {
            notes: { readme }
        }

    );
