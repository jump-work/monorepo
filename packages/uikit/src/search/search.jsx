import React, { useEffect, useState, useCallback } from 'react';
import PropTypes from 'prop-types';

import cx from './search.module.scss';
import { useDebounce } from '../hooks';
import { Input } from '../input';
import { Ellipsis } from '../spinner';
import { SearchListItem } from './search-list-item';

export const Search = ({ open, loading, onSearch, results, debounce, hitSearchOnEnterOnly, style, ...rest }) => {

    const inputRef = React.createRef();
    const [didSearch, setDidSearch] = useState(false);
    const [innerResults, setInnerResults] = useState([]);
    const [prevResults, setPrevResults] = useState([]);
    const [searchTerm, setSearchTerm] = useState('');
    const [debouncedSearchTerm, cancel] = useDebounce(searchTerm, debounce);

    const doSearch = useCallback(str => {
        onSearch(str);
        setDidSearch(true);
    }, [onSearch, setDidSearch]);

    const onItemChange = value => {
        if (value === '') {
            setDidSearch(false);
        }
        setSearchTerm(value);
    };

    const onKeyPress= event => {
        if (event.key === 'Enter') {
            doSearch(searchTerm);
            if (cancel) {
                clearTimeout(cancel);
            }
        }
    };

    useEffect(() => {
        if (inputRef && inputRef.current) {
            inputRef.current.focus();
        }
    }, [inputRef]);

    useEffect(() => {
        if (!open) {
            setDidSearch(false);
        }
    }, [open]);

    useEffect(() => {
        setPrevResults(() => {
            if (innerResults.length > 0) {
                return [...innerResults];
            }
        })
        setInnerResults([...results]);
    }, [results, innerResults]);

    useEffect(() => {
        if (hitSearchOnEnterOnly) return;

        if (debouncedSearchTerm !== '') {
            doSearch(debouncedSearchTerm);
        }
    }, [debouncedSearchTerm, hitSearchOnEnterOnly, doSearch]);

    return open ? (
        <div className={cx.base} style={{ ...style }}>
            <div className={cx.searchInput}>
                <Input ref={inputRef}
                       type='text'
                       clearable={true}
                       onChange={onItemChange}
                       onKeyPress={onKeyPress}
                />
            </div>

            {loading &&
                <div className={cx.loading}>
                    <Ellipsis invert={false} />
                </div>
            }

            {!loading && didSearch && innerResults.length === 0 &&
                <div className={cx.noresult}>Никаких результатов</div>
            }

            {!loading &&
                <div className={cx.results}>
                    {
                        [...(innerResults.length > 0 ? innerResults : !didSearch ? prevResults : [])]
                            .map((item, index) => (<rest.listItemType key={index} {...item}/>))
                    }
                </div>
            }
        </div>
    ) : null;
};

Search.ListItem = SearchListItem;

Search.defaultProps = {
    open: false,
    loading: false,
    debounce: 500,
    listItemType: SearchListItem,
    hitSearchOnEnterOnly: false,
    style: {}
};

Search.propTypes = {
    open: PropTypes.bool,
    loading: PropTypes.bool,
    results: PropTypes.array,
    debounce: PropTypes.number,
    listItemType: PropTypes.elementType,
    hitSearchOnEnterOnly: PropTypes.bool,
    style: PropTypes.object,
    onSearch: PropTypes.func,
};
