# Поиск

## Импорт

```javascript
import { Search } from '@justlook/uikit';
```

## Использование

```javascript
<Search open={open}
        loading={loading}
        onSearch={doSearch}
        results={results}
        listItemType={CustomItemList}
        hitSearchOnEnterOnly={true}
        debounce={1000}
/>

Search.defaultProps = {
    open: false,
    loading: false,
    debounce: 500,
    listItemType: SearchListItem,
    hitSearchOnEnterOnly: false
};

Search.propTypes = {
    open: PropTypes.bool,
    loading: PropTypes.bool,
    results: PropTypes.array,
    debounce: PropTypes.number,
    listItemType: PropTypes.elementType,
    hitSearchOnEnterOnly: PropTypes.bool
}
```

Расчитан на размещение в абсолютной позиции от своего родителя, на высоте
`73px` от верхнего края, для исползования совместно с `MobileHeader`, и занимает
в раскрытом состоянии всё доступное пространство.

#####Описание пропсов:

`open`  - управление видимостью

`loading` - управление видимостью индикатора загрузки

`onSearch` - колбэк для вызова метода api поиска

`results` - массив с результатами поиска

`listItemType` - тип react-компонента для отображения в списке результатов,
`<ListItem {...T} />, где results: T[]`

`hitSearchOnEnterOnly` - флаг - осуществлять поиск только по Enter, по умолчанию `false`

`debounce` - задержка поиска при вводе поисковой строки
