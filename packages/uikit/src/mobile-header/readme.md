# MobileHeader

## Импорт

```javascript
import { MobileHeader } from '@justlook/uikit';
```

## Использование

```javascript
<MobileHeader header='Header'
              description='Description'
              onGoBack={action()}
>
    <MobileHeader.Button icon={<Icons24.IconPlus/>} onClick={() => alert('PLUS')}/>
    <MobileHeader.Button icon={<Icons24.IconSearch/>} onClick={() => alert('Search')}/>
    <MobileHeader.Button icon={<Icons24.IconMenu/>} onClick={() => alert('Menu')}/>
</MobileHeader>

MobileHeader.propTypes = {
    header: PropTypes.string.isRequired,
    description: PropTypes.string,
    onGoBack: PropTypes.func,
    children: PropTypes.node
};

MobileHeaderButton.propTypes = {
    icon: PropTypes.elementType,
    onClick: PropTypes.func
};
```
Если пропсы `description` и `onGoBack` не переданы, то и на компоненте не отрисовываются
соответствующие элементы интерфейса: область "description" и кнопка "назад"
