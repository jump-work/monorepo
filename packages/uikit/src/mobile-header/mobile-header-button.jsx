import React from 'react';
import PropTypes from 'prop-types';
import { Button } from '../button';

import cx from './mobile-header-button.module.scss';

export const MobileHeaderButton = ({ icon, onClick, ...rest }) => {

    return (
        <Button styling='hollow' onClick={onClick} icon={icon} className={cx.base} {...rest} />
    )
};

MobileHeaderButton.defaultProps = {
    onClick: () => {}
};

MobileHeaderButton.propTypes = {
    icon: PropTypes.any,
    onClick: PropTypes.func,
};
