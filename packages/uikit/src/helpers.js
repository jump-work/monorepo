export const idFromString = (str) => {
    let id = 'id_';

    for (let i = 0, l = str.length; i < l; i++) {
        id += str.charCodeAt(i);
    }

    return id;
};

export function isValidDate (d) {
    return d instanceof Date && !isNaN(d);
}

export const formatDate = (value) => {
    const d = value.split('.');
    const getDate = d[0];
    const getMonth = d[1] - 1;
    const getYear = d[2];

    if (!~value.indexOf('_')) {
        const date = new Date(getYear, getMonth, getDate);
        return isValidDate(date) ? date : undefined;
    } else {
        return undefined;
    }
};

export function addDays(date, days) {
    const result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}

export function declensionWords(num, labels) {
    num = Math.abs(num) % 100;
    const copy_num = num % 10;

    if (num > 10 && num < 20) { return labels[2] }
    if (copy_num > 1 && copy_num < 5) { return labels[1] }
    if (copy_num === 1) { return labels[0] }
    return labels[2];
}