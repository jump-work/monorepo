import React, { Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import classnames from 'classnames';

import cx from './font.module.scss';

const Font = ({ children, as: Component, size = 18, muted = false, className }) => {
    if (Component) {
        return (
            <Component className={classnames(cx.base, {[cx[`base${size}`]]: !!size, [cx.muted]: muted}, className)}>
                {children}
            </Component>
        )
    }

    if (children?.props && children.type) {
        return Children.map(children, child => {
            return cloneElement(child, {
                ...child.props,
                className: classnames(child.props?.className, cx.base, {[cx[`base${size}`]]: !!size}, className)
            });
        })
    }

    return <span className={classnames(cx.base, {[cx[`base${size}`]]: !!size}, className)}>{children}</span>
}

Font.propTypes = {
    className: PropTypes.string,
    as: PropTypes.string,
    children: PropTypes.any,
    size: PropTypes.oneOf([18, 24]),
    muted: PropTypes.bool,
}

export default Font;
