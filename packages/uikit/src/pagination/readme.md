# Pagination

## Импорт

```javascript
import { Pagination } from '@justlook/uikit';
```

## Использование

```javascript
<Pagination pageCount={100}
            handlePageClick={ data => console.log(data) }
            initialPage={49}
            fluid={true|false}/>
```
