import React from 'react';
import { storiesOf } from '@storybook/react';
import { number } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import { Pagination } from './pagination';

import readme from './readme.md';

storiesOf('Компоненты/Пагинатор', module)
    .add(
        'Pagination',
        () => {

            return (
                <Pagination pageCount={number('Page count', 10)}
                            handlePageClick={action()}
                            initialPage={number('Initial page', 5)}
                            marginPagesDisplayed={number('Margin Pages Displayed', 2)}
                            pageRangeDisplayed={number('Page Range Displayed', 5)}
                            fluid={false}
                />
            )
        }, {
            notes: { readme }
        }
    );
