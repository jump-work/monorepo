import React from 'react';
import PropTypes from 'prop-types';
import ReactPaginate from 'react-paginate';
import classnames from 'classnames';

import { useIfMediaScreen } from '../hooks';

import { Icons24 } from '../icons';

import cx from './pagination.module.scss';

export const Pagination = ({ handlePageClick, pageCount, marginPagesDisplayed, pageRangeDisplayed, initialPage, fluid = false }) => {
    const isMobile = useIfMediaScreen();

    const cxsPagination = classnames(cx.pagination, {[cx.fluid]: fluid})

    const breakLabel = isMobile ? null  : '...';
    const marginPages = isMobile ? 0 : marginPagesDisplayed;
    const pageRange = isMobile ? 0 : pageRangeDisplayed;

    return (
        <ReactPaginate
            previousLabel={<Icons24.IconArrowLeft/>}
            nextLabel={<Icons24.IconArrowRight/>}
            breakLabel={breakLabel}
            pageCount={pageCount}
            marginPagesDisplayed={marginPages}
            pageRangeDisplayed={pageRange}
            onPageChange={handlePageClick}
            initialPage={initialPage}
            containerClassName={cxsPagination}
            previousClassName={cx.previous}
            nextClassName={cx.next}
            pageClassName={cx.page}
            disabledClassName={cx.disabled}
            breakClassName={cx.break}
            activeClassName={cx.active}
            ariaLabelBuilder={() => pageCount}
        />
    )
};

Pagination.defaultProps = {
    handlePageClick: () => {},
    marginPagesDisplayed: 2,
    pageRangeDisplayed: 5
};

Pagination.propTypes = {
    handlePageClick: PropTypes.func,
    pageCount: PropTypes.number.isRequired,
    marginPagesDisplayed: PropTypes.number,
    pageRangeDisplayed: PropTypes.number,
    initialPage: PropTypes.number,
    fluid: PropTypes.bool
};
