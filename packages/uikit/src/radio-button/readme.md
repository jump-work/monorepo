# RadioButton 

## Импорт

```javascript
import { RadioButton } from '@justlook/uikit';
```

## Использование

```javascript
const el = () => {
    return <RadioButton 
                label={'label radiobutton'}
                id='id'
                name='name'
                value='value'
                className={'class'}
                error={false}
                checked={false}
                onChange={(value, name, changeEvent) => console.log(value, name)}
            />
}
```
