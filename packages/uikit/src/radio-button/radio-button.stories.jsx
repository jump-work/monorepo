import React from 'react';
import { action } from '@storybook/addon-actions';
import { boolean, text } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';

import { RadioButton } from './index';
import readme from './readme.md';

storiesOf('Компоненты/Элементы формы', module)
    .add(
        'RadioButton',
        () => {
            return (
                    <>
                        <RadioButton label={text('label', 'label')}
                                     onChange={action()}
                                     disabled={boolean('disabled', false)}
                                     error={boolean('error', false)}
                                     checked={boolean('checked', false)}
                        />
                    </>
                )
        }, {
            notes: { readme }
        }
    );
