# Большой заголовок

## Импорт

```javascript
import { BigHeader } from '@justlook/uikit';
```

## Использование

```javascript
const el = () => {
    return <BigHeader>350</BigHeader>
}
```

## Использование разных тегов

```javascript
const el = () => {
    return <BigHeader element={'span'}>350</BigHeader>
}
```