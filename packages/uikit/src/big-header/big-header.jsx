import React from 'react';
import PropTypes from 'prop-types';

import cx from './big-header.module.scss';

export const BigHeader = ({element: Tag, children, ...attrs}) => {
    return (
        <Tag className={cx.base} {...attrs}>
            {children}
        </Tag>
    )
};

BigHeader.defaultProps = {
    element: 'div',
};

BigHeader.propTypes = {
    children: PropTypes.any.isRequired,
    element: PropTypes.string
};