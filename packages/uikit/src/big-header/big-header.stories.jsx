import React from 'react';
import { storiesOf } from '@storybook/react';
import { select, text } from '@storybook/addon-knobs';

import { BigHeader } from './index';
import readme from './readme.md';

storiesOf('Типографика', module)
    .add(
        'Большой заголовок',
        () => {
            return (
                <BigHeader
                    element={select('Element', {
                        div: 'div',
                        span: 'span',
                        p: 'p',
                        etc : 'div',
                    })}
                >
                    {text('Children', '350')}
                </BigHeader>
            )
        }, {
            notes: { readme }
        }
    );