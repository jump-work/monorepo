import React from 'react';
import { Ellipsis } from '../spinner';

import cx from './splash-screen.module.scss';

export default () => {
    return (
        <div className={cx.container}>
            <Ellipsis/>
        </div>
    );
}
