import React from 'react';
import { action } from '@storybook/addon-actions';
import { boolean } from '@storybook/addon-knobs';
import { storiesOf } from '@storybook/react';

import { RadioButton } from '../radio-button';
import readme from './readme.md';
import { RadioButtonGroup } from './radio-button-group';

storiesOf('Компоненты/Элементы формы', module)
    .add(
        'RadioButtonGroup',
        () => (
            <RadioButtonGroup name='button-group'
                              onChange={action('onChange')}
                              defaultSelected='fixed'
                              disabled={boolean('disabled', false)}
                              vertical={boolean('vertical', false)}
            >
                <RadioButton value='free' label='Free' />
                <RadioButton value='monthly' label='Monthly' />
                <RadioButton value='weekly' label='Weekly' />
                <RadioButton value='fixed' label='Fixed' />
            </RadioButtonGroup>
        ), {
            notes: { readme }
        }
    );