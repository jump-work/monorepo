# RadioButtonGroup

## Импорт

```javascript
import { RadioButtonGroup, RadioButton } from '@justlook/uikit';
```

## Использование

```javascript
const el = () => {
    return <RadioButtonGroup 
                name='name'
                defaultSelected={string | number}
                valueSelected={string | number}
                className={'class'}
                onChange={(value, name, changeEvent) => console.log(value, name)}
                vertical={boolean}
            >
                <RadioButton value='free' label='Free' />
                <RadioButton value='monthly' label='Monthly' />
                <RadioButton value='weekly' label='Weekly' />
                <RadioButton value='fixed' label='Fixed' />
            </RadioButtonGroup>
}
```

