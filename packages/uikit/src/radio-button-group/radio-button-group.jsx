import React, { useState } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';

import { RadioButton } from '../radio-button';
import cls from './radio-button-group.module.scss';

export const RadioButtonGroup = (props) => {
    const [prevValueSelected, setPrevValueSelected] = useState(null);
    const [selected, setSelected] = useState(props.valueSelected || props.defaultSelected);

    if (prevValueSelected !== props.valueSelected) {
        setSelected(props.valueSelected || props.defaultSelected);
        setPrevValueSelected(props.valueSelected);
    }

    const handleChange = (newSelection, value, evt) => {
        if (newSelection !== selected) {
            setSelected(newSelection);
            props.onChange(newSelection, props.name, evt)
        }
    };

    const getRadioButtons = () => {
        return React.Children.map(props.children, radioButton => {
            const { value, ...other } = radioButton.props;
            return (
                <RadioButton
                    {...other}
                    name={props.name}
                    key={value}
                    value={value}
                    onChange={handleChange}
                    checked={value === selected}
                    disabled={props.disabled}
                />
            )
        });
    };

    const wrapperClasses = cn([cls.base], { [cls.vertical]: props.vertical });

    return (
        <div className={wrapperClasses}>
            {getRadioButtons()}
        </div>
    )
};

RadioButtonGroup.defaultProps = {
  onChange: () => {}
};

RadioButtonGroup.propTypes = {
    disabled: PropTypes.bool,
    name: PropTypes.string.isRequired,
    onChange: PropTypes.func,
    valueSelected: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    children: PropTypes.node,
    defaultSelected: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    vertical: PropTypes.bool
};
