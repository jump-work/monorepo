import React from 'react';
import PropTypes from 'prop-types';

import classNames from 'classnames';
import cx from './message.module.scss';

export const ErrorMessage = ({ className, children, ...attr }) => {
    const clsName = classNames(cx.error, className);

    return <p className={clsName} {...attr}>{children}</p>;
};

ErrorMessage.propTypes = {
    className: PropTypes.string,
    children: PropTypes.any,
};