# Select

## Импорт

```javascript
import { 
    Select, 
    SelectAsync 
} from '@justlook/uikit';
```

## Использование

```javascript
const options = [
    { value: 'chocolate', label: 'Chocolate', isDisabled: true},
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla', badge: '1' },
];

const el = () => {
    return <Select
                options={options}
                placeholder={'select'}
                label={'Название поля'}
                error={''}
                disabled={false}
                onChange={(value) => {console.log(value)}}
            />
}

const asyncElement = () => {
    // defaultOptions - 
    //      true - подгрузка опций, используя onLoadOptions
    //      Array[] - массив значений
    // loadOptions - selectedValue => Promise<Array[]>
    //      
    return <SelectAsync label={text('Label', 'Название поля')}
                        onChange={(value) => alert(value)}
                        placeholder={text('Placeholder', 'select')}
                        error={text('Error', '')}
                        disabled={false}
                        defaultOptions={bool | []}
                        loadOptions={onLoadOptions}
           />
}

const selectCompact = () => {
    return (
        <Select
            options={options}
            onChange={(value) => {console.log(value)}}
            isClearable={true}
            isSearchable={false}
            disabled={false}
            compact
            prefix={'Сортировать по:'}
        />
    )
}
```

## Props

- **option**: options для select
- **onChange**: функция возвращает выбраный элемент
- **placeholder**: placeholder для select
- **label**: label для select
- **error**: текст ошибки
- **disabled**: деактивация select
- **compact**: преобразует стили select
- **isClearable**: добавляет возможность очистить select, кнопкой close
- **isSearchable**: добавляет возможность поиска по options
- **prefix**: добавляет префикс (работает только для select-compact)
- **defaultValue**: дефолтное значение select
- **value**: Значение селекта; Отражает текущую выбранную опцию. Если указано, селект - managed, не
указано - unmanaged

