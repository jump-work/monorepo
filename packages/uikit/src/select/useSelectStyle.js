import { useMemo } from 'react';

export const useCustomStyle = (error, disabled) => useMemo(() => ({
    indicatorSeparator: () => ({
        display: 'none'
    }),
    option: (provided) => ({
        ...provided,
        padding: '0.5rem 0 0.5rem 1rem',
        fontSize: '0.9375rem',
        lineHeight: '1.5rem',
    }),
    control: (provided, state) => ({
        ...provided,
        transition: 'all 0.3s ease',
        boxShadow: state.isFocused
            ? (error ? '0 0 0.0625rem 0.1875rem rgba(236, 57, 61, 0.2)' : '0 0 0.0625rem 0.1875rem rgba(37, 143, 251, 0.3)')
            : null,

        background: error
            ? (state.isFocused ? '#FFFFFF' : 'rgba(93%, 22%, 24%, 0.2)')
            : (disabled ? '#e7eaee' : '#ffffff'),

        border: error
            ? (state.isFocused ? '0.0625rem solid #EC393D' : '0.0625rem solid transparent')
            : (state.isFocused ? '0.0625rem solid #2691FF' : '0.0625rem solid #E7EAEE'),

        borderColor: state.isFocused ? 'transparent' : '#E7EAEE',
        '&:hover': {
            borderColor: error
                ? (state.isFocused ? '#EC393D' : '#E7EAEE')
                : (state.isFocused ? '#2691FF' : '#E7EAEE'),
        },
    }),
    placeholder: (provided, state) => ({
        ...provided,
        display: state.isFocused ? 'none' : 'block',
        color: error ? '#ABADBA' : disabled ? '#CCCFDB' : '#ABADBA'
    }),
    indicatorsContainer: () => ({
        padding: '0.4375rem 0.5rem',
        display: 'flex'
    }),
    dropdownIndicator: () => ({
        color: '#333333',
        width: '1.5rem',
        height: '1.5rem',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    }),
    loadingIndicator: () => ({
        display: 'none',
    })
}), [error, disabled]);

export const useCustomCompactStyle = () => useMemo(() => ({
    indicatorSeparator: () => ({
        display: 'none'
    }),
    dropdownIndicator: () => ({
        display: 'none'
    }),
    menu: () => ({
        margin: '0',
        maxWidth: '13.5625rem',
        width: '100%',
        position: 'absolute',
        top: 'calc(100% - 0.1875rem)',
        left: '0.75rem',
        boxShadow: '0 0.25rem 0.75rem rgba(204, 207, 219, 0.5)',
        borderRadius: '0.25rem',
        background: '#FFFFFF'
    }),
}), []);
