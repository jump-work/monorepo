export const getAlongActiveOption = (options = []) => {
    const active = options.filter(option => option.isDisabled !== true);

    return active.length === 1 ? active[0] : null;
}
