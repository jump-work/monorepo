import React            from 'react';
import { storiesOf }    from '@storybook/react';
import { action }       from '@storybook/addon-actions';
import { text, select } from '@storybook/addon-knobs';

import { Select }      from './index';
import { SelectAsync } from './select-async';
import readme          from './readme.md';

import cx from '../story.module.scss';

const goods = [
    { value: 'chocolate', label: 'Chocolate', isDisabled: true },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla', badge: '1' },
];

const sort = [
    { value: 'chocolate', label: 'Chocolate' },
    { value: 'strawberry', label: 'Strawberry' },
    { value: 'vanilla', label: 'Vanilla' },
];

storiesOf('Компоненты/Элементы формы', module)
    .add(
        'Select',
        () => {
            const [ value, setValue ] = React.useState(null);
            return (
                <Select
                    label={text('Label', 'Название поля')}
                    options={goods}
                    onChange={setValue}
                    placeholder={text('Placeholder', 'select')}
                    error={text('Error', '')}
                    disabled={select('Disabled', {
                        FALSE: false,
                        TRUE: true,
                    })}
                    isSearchable={false}
                    value={value}
                />
            )
        }, {
            notes: { readme }
        }
    )
    .add(
        'SelectAsync',
        () => {
            const filterGoods = (inputValue) => {
                return goods.filter(i =>
                    i.label.toLowerCase().includes(inputValue.toLowerCase())
                );
            };

            const onLoadOptions = inputValue =>
                new Promise(resolve => {
                    setTimeout(() => {
                        resolve(filterGoods(inputValue));
                    }, 1000);
                });

            return (
                <SelectAsync label={text('Label', 'Название поля')}
                             onChange={action()}
                             placeholder={text('Placeholder', 'select')}
                             error={text('Error', '')}
                             disabled={select('Disabled', {
                                 FALSE: false,
                                 TRUE: true,
                             })}
                             defaultOptions
                             loadOptions={onLoadOptions}
                />
            )
        }, {
            notes: { readme }
        }
    ).add(
    'SelectCompact',
    () => {
        const [ valueSort, setValueSort ] = React.useState(sort[ 1 ]);
        return (
            <div className={`${cx.content} ${cx.narrowContent}`}>
                <Select
                    options={sort}
                    onChange={setValueSort}
                    value={valueSort}
                    isClearable={false}
                    isSearchable={false}
                    disabled={false}
                    compact
                    prefix={text('Префикс', '')}
                />
            </div>
        )
    }, {
        notes: { readme }
    }
);

