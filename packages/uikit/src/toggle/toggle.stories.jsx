import React from 'react';
import { storiesOf } from '@storybook/react';
import { text, select, boolean } from '@storybook/addon-knobs';
import { action } from '@storybook/addon-actions';

import { Toggle } from './index';
import readme from './readme.md';

storiesOf('Компоненты/Переключатель', module)
    .add(
        'Toggle',
        () => {
            return (
                <Toggle
                    label={text('label', 'Возместить налоги исполнителям')}
                    isLoading={boolean('isLoading', false, 'first')}
                    defaultChecked={true}
                    positionToggle={select('positionToggle', {
                        before: 'before',
                        after: 'after',
                    }, 'before')}
                    onChange={action('Toggle state')}
                    disabled={boolean('disabled', false)}
                />
            );
        }, {
            notes: { readme },
        },
    );
