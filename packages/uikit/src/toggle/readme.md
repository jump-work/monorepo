# Переключатель

## Импорт
```javascript
import { Toggle } from '@justlook/uikit';
```

## Использование

```javascript
<Toggle
    label='Переключатель'
    positionToggle='before'
    onChange={() => {}}
/>
```

##Props
### ***positionToggle*** 
Определяет позицию переключателя относительно label. Принимает два значения: `'before'`, `'after'`. 
<br/>
<br/>
type: `string` <br/>

### ***onChange*** 
Callback - функция. Вызывается при изменении `<Toggle/>` 
<br/>
<br/>
type: `func` <br/>

### ***label***  
type: `string` <br/>

### ***id***  
type: `string || number` <br/>

### ***name***  
type: `string` <br/>
