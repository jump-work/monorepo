# Табы

## Импорт
```javascript
import { Tab } from '@justlook/uikit';
import { Item } from '@justlook/uikit'; // notification
```

## Использование

```javascript
const el = () => {
    return <Tab>
               <Tab.Content label={'Tab-1'} disabled={true}>
                   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, nostrum!
               </Tab.Content>
               <Tab.Content label={'Tab-2'} disabled={false} notification={<Item.Notification alert/>}>
                   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aut cumque cupiditate debitis dolor error in, ipsa molestiae odit sint?
               </Tab.Content>
               <Tab.Content label={'Tab-3'} disabled={false}>
                   Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis eaque qui quis suscipit voluptatum? Cumque in laudantium nesciunt praesentium voluptatem.
               </Tab.Content>
           </Tab>
}
```