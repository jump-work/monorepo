import React from 'react';
import { storiesOf } from '@storybook/react';
import { text, boolean } from '@storybook/addon-knobs';

import { Item } from '../item';
import { Tab } from './index';
import readme from './readme.md';

storiesOf('Компоненты/Табы', module)
    .add(
        'Tabs',
        () => {
            return (
                <Tab
                    actionChangeTab={(value) => console.log(value)}
                    mobileFormat={'select'}
                >
                    <Tab.Content label={text('Label Tab-1','Tab-1')} disabled={boolean('Disabled Tab-1', true)} value={'tabNav1'}>
                        {text('Text Tab-1', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Et, nostrum!')}
                    </Tab.Content>
                    <Tab.Content label={text('Label Tab-2','Tab-2')} disabled={boolean('Disabled Tab-2', false)} notification={<Item.Notification alert/>} value={'tabNav2'}>
                        {text('Text Tab-2', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquam aut cumque cupiditate debitis dolor error in, ipsa molestiae odit sint?')}
                    </Tab.Content>
                    <Tab.Content label={text('Label Tab-3','Tab-3')} disabled={boolean('Disabled Tab-3', false)} value={'tabNav3'}>
                        {text('Text Tab-3', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Debitis eaque qui quis suscipit voluptatum? Cumque in laudantium nesciunt praesentium voluptatem.')}
                    </Tab.Content>
                </Tab>
            )
        }, {
            notes: { readme }
        }
    );
