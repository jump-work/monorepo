import React, {Component} from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import debounce from 'lodash.debounce';

import { Navigation } from './tabsNavigation';
import { Content } from './tabsContent';
import { Select } from '../select';
import cx from './tabs.module.scss';

class Tab extends Component {
    state = {
        activeTab: null,
        mediaScreen: false,
    };

    componentDidMount() {
        const {children = []} = this.props;
        this.findDisabledTab(children);
        window.addEventListener('resize', this.resizeWindow);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.resizeWindow);
    }

    resizeWindow = debounce(() => {
        this.setState({
            mediaScreen: window.matchMedia('(max-width: 768px)').matches
        })
    }, 200);

    findDisabledTab = (children, variable) => {
        for(let i = 0; i < children.length; i++) {
            const props = children[i].props;
            if(!props.disabled) {
                if(variable) {
                    return children[i].props;
                } else {
                    return this.setActiveTab(children[i].props.label);
                }
            }
        }
    };

    getChildren = (children) => {
        return children.map(({props}) => (props))
    };

    setActiveTab = (activeTab) => {
        const {activeTab: currentTab} = this.state;

        if (currentTab !== activeTab) {
            this.setState({
                activeTab
            });
        }
    };

    renderTabs = (children = [], actionChangeTab) => {
        const { activeTab } = this.state;

        return this.getChildren(children).map((props) => (
            <Navigation
                key={props.label}
                navLabel={props.label}
                value={props.value}
                isActive={activeTab === props.label}
                onChangeActiveTab={(activeTab, value = '') => {
                    actionChangeTab(value)
                    return this.setActiveTab(activeTab)
                }}
                disabled={props.disabled}
                notification={props.notification}
            />
        ));
    };

    renderMediaScreenTabs = (children = [], actionChangeTab) => {
        const options = [];

        children.map((e) => (
            options.push({
                value: e.props.label,
                label: e.props.label,
                tabValue: e.props.value || '',
                isDisabled: e.props.disabled || null,
            })
        ));

        function defaultPropsSelect(options) {
            for(let i = 0; i < options.length; i++) {
                if(options[i].isDisabled === null) {
                    return options[i]
                }
            }
        }

        return <Select
                    defaultValue={defaultPropsSelect(options)}
                    options={options}
                    onChange={(obj) => {
                        actionChangeTab(obj.tabValue)
                        return this.setActiveTab(obj.value)
                    }}
                    placeholder={'Заблокировано'}/>
    };

    getContent = () => {
        let activeTabContent = null;
        const {activeTab} = this.state;
        const {children} = this.props;

        React.Children.forEach(children, child => {
            if (child.props.label === activeTab) {
                activeTabContent = child;
            }
        });

        return activeTabContent;
    };

    render() {
        const {mediaScreen} = this.state;
        const {children, className, mobileFormat, emptyContent, actionChangeTab, ...attrs} = this.props;

        const classes = classNames(cx.base, className);

        return (
            <div className={classes} {...attrs}>
                <div
                    className={cx.navigation}
                    style={{
                        gridTemplateColumns: mediaScreen && mobileFormat === 'select' ? '1fr' : `repeat(${children.length}, auto)`,
                        width: mediaScreen && mobileFormat === 'select' ? '100%' : 'auto',
                    }}
                >
                    {
                        mediaScreen && mobileFormat === 'select'
                            ? this.renderMediaScreenTabs(children, actionChangeTab)
                            : this.renderTabs(children, actionChangeTab)
                    }
                </div>
                <div className={cx.container}>
                    {
                        this.findDisabledTab(children, true) === undefined
                        ? emptyContent
                        : this.getContent()
                    }
                </div>
            </div>
        )
    }
}

Tab.defaultProps = {
    className: '',
    actionChangeTab: () => {},
    mobileFormat: 'select',
};

Tab.propTypes = {
    children: PropTypes.any.isRequired,
    className: PropTypes.string,
    actionChangeTab: PropTypes.func,
    emptyContent: PropTypes.any,
    mobileFormat: PropTypes.oneOf(['tabs', 'select']),
};

Tab.Content = Content;

export default Tab;
