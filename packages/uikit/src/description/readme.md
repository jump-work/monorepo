# Описание

## Импорт

```javascript
import { Description } from '@justlook/uikit';
```

## Использование
Межстрочное растояние ```line-height: 24px;```

```javascript
const el = () => {
    return <Description.LH24></Description.LH24>
}
```

Межстрочное растояние ```line-height: 16px;```

```javascript
const el = () => {
    return <Description.LH16></Description.LH16>
}
```