import React         from 'react';
import { storiesOf } from '@storybook/react'
import { text }      from '@storybook/addon-knobs';

import { Description } from './index';
import readme          from './readme.md';

storiesOf('Типографика', module)
    .add(
        'Описание',
        () => {
            return (
                <div>
                    <Description.LH24 element='span'>{text('Children', 'Описание')}</Description.LH24>
                    <Description.LH16>{text('Children', 'Описание')}</Description.LH16>
                </div>
            );
        }, {
            notes: { readme }
        }
    );

