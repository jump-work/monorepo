import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import cx from './paragraph.module.scss';

export const Paragraph = ({className, children, ...attr}) => {
    const clsName = classNames(cx.base, className);

    return <p className={clsName} {...attr}>{children}</p>
};

Paragraph.propTypes = {
    className: PropTypes.string,
    children: PropTypes.any,
};