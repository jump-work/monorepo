# Параграф

## Импорт

```javascript
import { Paragraph } from '@justlook/uikit';
```

## Использование

```javascript
const el = () => {
    return <Paragraph>Children</Paragraph>
}
```