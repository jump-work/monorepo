import React      from 'react';
import PropTypes  from 'prop-types';
import classNames from 'classnames';

import { ParagraphV2 as Paragraph } from '../new-paragraph';

import { idFromString }   from '../helpers';
import { useTabListener } from '../hooks';

import cx from './checkbox.module.scss';

export const Checkbox = ({ id, name, label, description, checked, error, onChange, className, mixed, ...rest }) => {

    const { focusedByTab } = useTabListener();

    const key = id || idFromString(name || label);
    const clsName = classNames(cx.base, className, {
        [cx.error]: error,
        [cx.mixed]: mixed,
    });

    const clsNameInput = classNames(cx.inputCheckbox, {
        [cx.focus]: focusedByTab,
    });

    return (
        <div className={clsName}>
            <input
                id={key}
                type='checkbox'
                className={clsNameInput}
                checked={checked}
                onChange={(e) => onChange(e.target.checked)}
                {...rest}
            />

            <label htmlFor={key} className={cx.labelCheckbox}>
                <Paragraph.LH24>{label}</Paragraph.LH24>
                {description && <Paragraph.LH18>{description}</Paragraph.LH18>}
            </label>
        </div>
    );
};

Checkbox.defaultProps = {
    label: '',
    description: '',
    checked: false,
    onChange: () => {},
    className: '',
    error: false,
    mixed: false,
};

Checkbox.propTypes = {
    id: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    name: PropTypes.string,
    label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    description: PropTypes.string,
    checked: PropTypes.bool,
    onChange: PropTypes.func,
    className: PropTypes.string,
    error: PropTypes.bool,
    mixed: PropTypes.bool,
};
