import React, { useState } from 'react';
import { storiesOf }       from '@storybook/react';
import { text, boolean }   from '@storybook/addon-knobs';

import { Checkbox } from './index';

import readme from './readme.md';

storiesOf('Компоненты/Элементы формы', module)
    .add(
        'Checkbox',
        () => {
            const [ checked, setChecked ] = useState(false);

            return (
                <Checkbox
                    label={text('Label', 'label checkbox')}
                    description={text('Description', '')}
                    error={boolean('Error', false)}
                    className={'class'}
                    checked={checked}
                    onChange={setChecked}
                    disabled={boolean('Disabled', false)}
                    mixed={boolean('Mixed', false)}
                    defaultChecked={true}
                />
            )
        }, {
            notes: { readme }
        }
    );

