# Checkbox 

## Импорт

```javascript
import { Checkbox } from '@justlook/uikit';
```

## Использование

```javascript
const el = () => {
    return <Checkbox 
                label={'label checkbox'}
                className={'class'}
                error={false}
                checked={false}
                onChange={(value) => console.log(value)}
            />
}
```

В качестве дополнительных props можно использовать атрибуты, 
которые поддерживает `<input type='checkbox' />`