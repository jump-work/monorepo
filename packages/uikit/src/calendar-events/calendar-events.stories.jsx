import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { storiesOf } from '@storybook/react';

import { CalendarEvents } from './index';
import { Button } from '../button';
import { Input } from '../input';
import { Icons24 } from '../icons';
import { ParagraphV2 as Paragraph } from '../new-paragraph';
import { Header } from '../header';
import { Link } from '../link';
import { addDays } from '../helpers';
import { Popup } from '../popup';

import { useModalState, useIfMediaScreen } from '../hooks';

import readme from './readme.md';

const shifts = [
    {
        count_shifts: 3,
        documents: [
            {
                attache_file: 'putevoy-list-96.xlsx',
                id: 321,
                shift_id: 97,
                title: 'Путевой лист',
                type: 'route_contract',
                url: 'https://jump.rent/api/drivers/53/documents/320',
            },
            {
                attache_file: 'akt-priema-peredachi-avto-prilozhenie-2-96.docx',
                id: 325,
                shift_id: 97,
                title: 'Акт приема-передачи авто приложение 2',
                type: 'car_acceptance_act_2',
                url: 'https://jump.rent/api/drivers/53/documents/319',
            },
            {
                attache_file: 'akt-priema-peredachi-avto-prilozhenie-1-96.docx',
                id: 330,
                shift_id: 97,
                title: 'Акт приема-передачи авто приложение 1',
                type: 'car_acceptance_act_1',
                url: 'https://jump.rent/api/drivers/53/documents/318',
            }
        ],
        expire_date: new Date(2020, 5, 11),
        id: 97,
        start_date: new Date(2020, 5, 8),
        status: {name: 'close', title: 'Смена закрыта'}
    },
    {
        count_shifts: 5,
        documents: [
            {
                attache_file: 'putevoy-list-96.xlsx',
                id: 320,
                shift_id: 96,
                title: 'Путевой лист',
                type: 'route_contract',
                url: 'https://jump.rent/api/drivers/53/documents/320',
            },
            {
                attache_file: 'akt-priema-peredachi-avto-prilozhenie-2-96.docx',
                id: 319,
                shift_id: 96,
                title: 'Акт приема-передачи авто приложение 2',
                type: 'car_acceptance_act_2',
                url: 'https://jump.rent/api/drivers/53/documents/319',
            },
            {
                attache_file: 'akt-priema-peredachi-avto-prilozhenie-1-96.docx',
                id: 318,
                shift_id: 96,
                title: 'Акт приема-передачи авто приложение 1',
                type: 'car_acceptance_act_1',
                url: 'https://jump.rent/api/drivers/53/documents/318',
            }
        ],
        expire_date: null,
        id: 96,
        start_date: new Date(2020, 5, 18),
        status: {name: 'open', title: 'Смена открыта'}
    }
].map(item => ({
    ...item,
    start: item.start_date,
    expire_date: item.expire_date || addDays(item.start_date, item.count_shifts),
    end: item.expire_date || addDays(item.start_date, item.count_shifts)
}));

const ExamplePopupShift = ({shift}) => {
    const { start_date, expire_date, count_shifts, status = {} } = shift;

    return (
        <div>
            <Header.H3>{status.title}</Header.H3>

            <Paragraph.LH24>Начало: {start_date.toLocaleString()}</Paragraph.LH24>
            <Paragraph.LH24>Конец: {expire_date.toLocaleString()}</Paragraph.LH24>
            <Link>Путевой лист ({count_shifts})</Link>

            <div style={{display:'grid', gridTemplateColumns: `repeat(${status.name !== 'close' ? 3 : 2}, auto)`}}>
                { status.name !== 'close' && <Button key='close' styling='hollow-border'>Закрыть смену</Button>}
                <Button key='edit' styling='hollow-border' icon={<Icons24.IconPencil />}/>
                <Button key='delete' styling='hollow-border' icon={<Icons24.IconTrash />}/>
            </div>
        </div>
    )
}

const ExampleRenderControl = ({renderControl}) => {
    const { isShow, onOpenForm, onCloseForm } = useModalState(false);
    return (
        <Fragment>
            {renderControl(onOpenForm)}

            {isShow &&
                <Popup onDismiss={onCloseForm}>
                    <Popup.Header>Новая смена</Popup.Header>
                    <Popup.Content>
                        <Input
                            label='Автомобиль'
                            placeholder='Введите название'
                        />
                        <Input
                            label='Колличество смен'
                            placeholder='Введите название'
                        />
                    </Popup.Content>
                    <Popup.Footer>
                        <Button onClick={onCloseForm} styling='hollow-border'>Отмена</Button>
                        <Button>Открыть смену</Button>
                    </Popup.Footer>
                </Popup>
            }
        </Fragment>
    )
}

ExampleRenderControl.propTypes = {
    renderControl: PropTypes.func,
}

ExamplePopupShift.propTypes = {
    shift: PropTypes.object
}

storiesOf('Компоненты/Календарь событий', module)
    .add(
        'CalendarEvents',
        () => {
            const isMedia = useIfMediaScreen()
            return (
                <Fragment>
                    <CalendarEvents
                        events={shifts}
                        createEventControl={
                            <ExampleRenderControl
                                renderControl={(open) => (
                                    isMedia
                                        ? <Button onClick={open} icon={<Icons24.IconPlus/>}/>
                                        : <Button onClick={open}>Открыть смену</Button>
                                )}
                            />
                        }
                        declensionNameCount={['смена', 'смены', 'смен']}
                        renderPopup={(shift) => <ExamplePopupShift shift={shift} />}
                    />
                </Fragment>
            )
        }, {
            notes: {readme}
        }
    )
