import React from 'react';
import classes from 'classnames';
import PropTypes from 'prop-types';

import cx from './cardBody.module.scss';

export const Body = ({children, noPadding, ...attrs}) => {
    const cls = classes(cx.base, {
        [cx.padding]: !noPadding
    });

    return (
        <div className={cls} {...attrs}>
            {children}
        </div>
    )
};

Body.displayName = 'Card.Body';

Body.defaultProps = {
    noPadding: false,
};

Body.propTypes = {
    children: PropTypes.any.isRequired,
    noPadding: PropTypes.bool,
};