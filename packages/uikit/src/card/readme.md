# Карточка

## Импорт

```javascript
import { Card } from '@justlook/uikit';
```

## Использование

 ```javascript
const el = () => {
    return (
        <Card noPadding={false}>
            <Card.Header>Lorem ipsum dolor sit amet.</Card.Header>
            <Card.Body>Контент</Card.Body>
        </Card>
    )
}
```
 
## Компонент содержит модификации:

- **`<Card.Header></ Card.Header>`**
- **`<Card.Body></ Card.Body>`**