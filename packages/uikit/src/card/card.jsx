import React from 'react';
import PropTypes from 'prop-types';
import { Header } from './cardHeader';
import { Body } from './cardBody';

import classes from 'classnames';
import cx from './card.module.scss';

export const Card = ({children, noPadding, ...attrs}) => {
    const cls = classes(cx.base, {
        [cx.padding]: !noPadding
    });

    return (
        <div className={cls} {...attrs}>
            {children}
        </div>
    )
};

Card.Header = Header;
Card.Body = Body;

Card.defaultProps = {
    noPadding: false,
};

Card.propTypes = {
    children: PropTypes.any.isRequired,
    noPadding: PropTypes.bool,
};