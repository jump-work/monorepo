import React, { Fragment, useState } from 'react';
import { action }                    from '@storybook/addon-actions';
import { storiesOf }                 from '@storybook/react';
import { text, select, boolean }              from '@storybook/addon-knobs';

import { CalendarPeriod, Calendar, CalendarV2 } from './index';

import readme            from './readme-calendarPeriod.md';
import readmeCalendar    from './readme-calendar.md';
import readmeNewCalendar from './readme-new-calendar.md';

storiesOf('Компоненты/Элементы формы', module)
    .add(
        'CalendarPeriod',
        () => {
            const [ values, setValues ] = useState({ from: new Date(), to: new Date(2020, 6, 6) });
            const onChange = (e) => {
                setValues(e)
            }

            return (
                <CalendarPeriod
                    label={text('Label', '')}
                    placeholder={text('Placeholder', 'Дата')}
                    labelPosition={text('Положение label', 'top')}
                    error={text('Ошибка', '')}
                    small={boolean('small', false)}
                    value={values}
                    onChange={onChange}
                    onApply={action()}
                    disabled={boolean('disabled', false)}
                />
            )
        }, {
            notes: { readme }
        }
    ).add(
    'Calendar',
    () => {
        return (
            <Calendar
                label={text('Label', 'Дата')}
                placeholder={text('Placeholder', 'Выберите дату')}
                onChange={action()}
                labelPosition={text('Положение label', 'top')}
            />
        )
    }, {
        notes: { readmeCalendar }
    }
).add(
    'NewCalendar',
    () => {
        const [ value1, setValue1 ] = useState('');
        const [ value2, setValue2 ] = useState(new Date());

        const action1 = action();
        const action2 = action();

        const onChange1 = (e) => {
            setValue1(e);
            action1(e);
        };

        const onChange2 = (e) => {
            setValue2(e);
            action2(e);
        };

        return (
            <Fragment>
                <CalendarV2
                    label='Календарь 1'
                    labelPosition={select('Позиция label', {
                        default: null,
                        left: 'left',
                    }, null)}
                    placeholder={text('Placeholder', 'Выберите дату')}
                    value={value1}
                    error={text('Ошибка', '')}
                    onChange={onChange1}
                    disabled={boolean('disabled', false)}
                />

                <br/>

                <CalendarV2
                    label='Календарь 2'
                    labelPosition={select('Позиция label', {
                        default: null,
                        left: 'left',
                    }, null)}
                    placeholder={text('Placeholder', 'Выберите дату')}
                    value={value2}
                    error={text('Ошибка', '')}
                    onChange={onChange2}
                    disabled={boolean('disabled', false)}
                />
            </Fragment>
        );
    }, {
        notes: { readmeNewCalendar }
    }
);
