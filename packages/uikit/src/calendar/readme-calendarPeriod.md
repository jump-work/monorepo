# Календарь

## Импорт
````javascript
import { CalendarPeriod } from '@justlook/uikit';
````

## Использование
```javascript
const el = () => {  
    const [ values, setValues ] = useState({ from: new Date(), to: new Date(2020, 6, 6) });
    const onChange = (e) => { setValues(e) }

    return (
        <CalendarPeriod
            label='Дата'
            placeholder='Выберите дату'
            labelPosition='top'
            value={values}
            onChange={onChange}
        />
    )   
}
```

## Props

- **label**: текст тега label
- **placeholder**: текст placeholder 
- **onChange**: функция возвращает объект `{from:'', to:''}`
- **value**: объект формата `{from: new Date, to: new Date}`
- **error**: ошибка
- **labelPosition**: позиция label, есть 2 значения `top` и `left`
