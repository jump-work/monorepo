import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';
import { Calendar } from 'react-calendar';

import { Icons24 } from '../icons';
import { Input } from '../input';
import { formatDate, idFromString } from '../helpers';

import './custom-style-new-calendar.scss';
import cx from './new-calendar.module.scss';

export const NewCalendar = ({ id, name, label, placeholder, onChange, labelPosition, value, error, disabled, ...rest }) => {
    const [inputValue, setInputValue] = useState('');
    const [isOpen, setIsOpen] = useState(false);
    const calendarRef = useRef();
    const key = id || idFromString(name || label || placeholder);

    const clsWrapper = classes(cx.wrapper, {
        [cx.grid]: labelPosition === 'left',
    });

    const positionBase = classes(cx.overlay, {
        [cx.normal]: !label,
        [cx.left]: labelPosition === 'left',
    });

    useEffect(() => {
        const closeHandler = (e) => {
            if (calendarRef.current !== e.target && calendarRef.current &&
                !calendarRef.current.contains(e.target) && isOpen) {
                setIsOpen(false);
            }
        };

        if (isOpen) {
            document.addEventListener('mousedown', closeHandler);
            document.addEventListener('touchstart', closeHandler);
        } else {
            document.removeEventListener('mousedown', closeHandler);
            document.removeEventListener('touchstart', closeHandler);
        }

        return () => {
            document.removeEventListener('mousedown', closeHandler);
            document.removeEventListener('touchstart', closeHandler);
        };
    }, [isOpen]);

    const onSelectDate = (date) => {
        onChange(date);
        setIsOpen(false);
    };

    const onChangeValue = (value) => {
        setInputValue(value);
        onChange(formatDate(value) || '');
    };

    const handleSetIsOpenCalendar = () => {
        if(!disabled) setIsOpen(true)
    }

    return (
        <div className={clsWrapper} {...rest}>
            {
                label.length > 1
                ? <label htmlFor={key} className={cx.label}>{label}</label>
                : ''
            }

            <div className={cx['input-wrapper']} onClick={handleSetIsOpenCalendar}>
                <Input
                    id={key}
                    value={value instanceof Date
                        ? value.toLocaleDateString('ru-RU')
                        : inputValue}
                    placeholder={placeholder}
                    onChange={onChangeValue}
                    onFocus={() => setIsOpen(true)}
                    error={error}
                    isClearable={false}
                    mask='99.99.9999'
                    disabled={disabled}
                />
                <Icons24.IconCalendar className={cx.icon}/>
            </div>
            {
                isOpen &&
                <div className={positionBase} ref={calendarRef}>
                    <Calendar
                        className={cx.base}
                        nextLabel={<Icons24.IconArrowRight/>}
                        prevLabel={<Icons24.IconArrowLeft/>}
                        onClickDay={onSelectDate}
                        value={value instanceof Date ? value : ''}
                    />
                </div>
            }
        </div>
    );
};

NewCalendar.defaultProps = {
    label: '',
    value: '',
    placeholder: '',
    onChange: () => {},
    id: null,
    labelPosition: null,
    error: null,
};

NewCalendar.propTypes = {
    label: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
    ]),
    placeholder: PropTypes.string,
    onChange: PropTypes.func,
    id: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string,
    ]),
    name: PropTypes.string,
    labelPosition: PropTypes.oneOf(['left']),
    value: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.instanceOf(Date)
    ]),
    error: PropTypes.any,
    disabled: PropTypes.bool,
};
