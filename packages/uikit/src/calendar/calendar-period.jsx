import React, { useState, useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import classes from 'classnames';
import DayPicker, { DateUtils } from 'react-day-picker';
import { format, isLastDayOfMonth } from 'date-fns';
import ru from 'date-fns/locale/ru';

import { Icons24 } from '../icons';
import { Button } from '../button';
import { Input } from '../input';

import { idFromString, formatDate } from '../helpers';

import { WEEKDAYS_SHORT, WEEKDAYS_LONG, MONTHS } from './russification-calendar';

import 'react-day-picker/lib/style.css';
import './custom-style--calendar.scss';

import cx from './calendar-period.module.scss';

const Navigations = ({ onPreviousClick, onNextClick, className }) => {
    return (
        <div className={className}>
            <Button
                styling={'hollow'}
                icon={<Icons24.IconArrowLeft/>}
                onClick={() => onPreviousClick()}
                className={cx.prevBtn}
            />
            <Button
                styling={'hollow'}
                icon={<Icons24.IconArrowRight/>}
                onClick={() => onNextClick()}
                className={cx.nextBtn}
            />
        </div>
    )
};

export const CalendarPeriod = ({ id, name, label, small, placeholder, labelPosition, error, value, onChange, disabled, onApply, ...rest }) => {
    const [ inputValueRange, setInputValueRange ] = useState('');
    const [ inputValueFrom, setInputValueFrom ] = useState('');
    const [ inputValueTo, setInputValueTo ] = useState('');
    const [ isOpen, setIsOpen ] = useState(false);

    const modifiers = {
        start: value.from,
        end: value.to,
        isLastDayOfMonth,
    };

    const calendarRef = useRef();
    const key = id || idFromString(name || label || placeholder);

    useEffect(() => {
        prepareInputValue(value)

        const closeHandler = (e) => {
            if (calendarRef.current !== e.target && calendarRef.current &&
                !calendarRef.current.contains(e.target) && isOpen) {
                setIsOpen(false);
            }
        };

        if (isOpen) {
            document.addEventListener('mousedown', closeHandler);
            document.addEventListener('touchstart', closeHandler);
        } else {
            document.removeEventListener('mousedown', closeHandler);
            document.removeEventListener('touchstart', closeHandler);
        }

        return () => {
            document.removeEventListener('mousedown', closeHandler);
            document.removeEventListener('touchstart', closeHandler);
        };
    }, [ isOpen, value ]);

    const clsWrapper = classes(cx.wrapper, {
        [ cx.grid ]: labelPosition === 'left',
    });
    const positionBase = classes(cx.base, {
        [ cx.normal ]: !label,
        [ cx.left ]: labelPosition === 'left',
    });

    const handleDayClick = (day) => {
        const range = DateUtils.addDayToRange(day, value);

        onChange(range)
        prepareInputValue(range)
    };

    const handleReset = () => {
        onChange({ from: undefined, to: undefined })
        setInputValueFrom('')
        setInputValueTo('')
        setInputValueRange('')
    }

    const handleApply = () => {
        setIsOpen(false);
        onApply(value);
    }

    const handleFrom = (date) => {
        setInputValueFrom(date)
        if(formatDate(date)) {
            onChange({
                from: formatDate(date),
                to: value.to || undefined
            })
        } else {
            onChange({
                from: undefined,
                to: value.to || undefined
            })
        }
    }

    const handleTo = (date) => {
        setInputValueTo(date)
        if(formatDate(date)) {
            onChange({
                from: value.from || undefined,
                to: formatDate(date) || undefined
            })
        } else {
            onChange({
                from: value.from || undefined,
                to: undefined
            })
        }
    }

    const prepareInputValue = ({from, to}) => {
        const startDate = from instanceof Date ? format(from, 'dd MMM yyyy', { locale: ru }) : '';
        const expireDate = to instanceof Date ? ' – ' + format(to, 'dd MMM yyyy', { locale: ru }) : '';

        setInputValueRange(startDate + expireDate);
    }

    const renderFooterOverlay = () => {
        const cls = classes(cx.footer, {[cx.small]: small});

        return (
            <div className={cls}>
                <Input
                    id='from'
                    value={value.from instanceof Date
                        ? value.from.toLocaleDateString('ru-RU')
                        : inputValueFrom}
                    onChange={handleFrom}
                    placeholder='01.01.1970'
                    mask='99.99.9999'
                    className={cx.from}
                    isClearable={false}
                />
                <Input
                    id='to'
                    value={value.to instanceof Date
                        ? value.to.toLocaleDateString('ru-RU')
                        : inputValueTo}
                    onChange={handleTo}
                    placeholder='01.01.1970'
                    mask='99.99.9999'
                    className={cx.to}
                    isClearable={false}
                />

                <Button
                    styling='hollow-border'
                    onClick={handleReset}
                    className={cx.close}
                >
                    Сбросить
                </Button>
                <Button
                    onClick={handleApply}
                    className={cx.apply}
                >
                    Применить
                </Button>
            </div>
        )
    };

    return (
        <div className={clsWrapper}>

            {
                label.length > 1
                    ? <label htmlFor={key} className={cx.label}>{label}</label>
                    : ''
            }

            <div className={cx.input} onClick={() => setIsOpen(true)}>
                <Input
                    id={key}
                    value={inputValueRange}
                    placeholder={placeholder}
                    onFocus={() => setIsOpen(true)}
                    error={error}
                    isClearable={false}
                    readOnly={true}
                    disabled={disabled}
                />
                <Icons24.IconCalendar className={cx.icon}/>
            </div>

            {isOpen && !disabled &&
                <div className={positionBase} ref={calendarRef}>
                    <DayPicker
                        className='Selectable'
                        numberOfMonths={small ? 1 : 2}
                        month={value.from}
                        selectedDays={[ value.from, { from: value.from, to: value.to } ]}
                        onDayClick={handleDayClick}
                        modifiers={modifiers}
                        months={MONTHS}
                        weekdaysLong={WEEKDAYS_LONG}
                        weekdaysShort={WEEKDAYS_SHORT}
                        firstDayOfWeek={1}
                        navbarElement={<Navigations/>}
                        fixedWeeks={true}
                        showOutsideDays={false}
                        {...rest}
                    />

                    {renderFooterOverlay(value)}
                </div>
            }
        </div>
    )
}

CalendarPeriod.propTypes = {
    label: PropTypes.string,
    id: PropTypes.string,
    name: PropTypes.string,
    small: PropTypes.bool,
    placeholder: PropTypes.string,
    labelPosition: PropTypes.oneOf(['top', 'left']),
    onChange: PropTypes.func.isRequired,
    value: PropTypes.exact({
        from: PropTypes.instanceOf(Date),
        to: PropTypes.instanceOf(Date)
    }),
    error: PropTypes.any,
    disabled: PropTypes.bool,
    onApply: PropTypes.func,
};

CalendarPeriod.defaultProps = {
    label: '',
    id: '',
    placeholder: '',
    small: false,
    labelPosition: 'top',
    onChange: () => {},
    value: { from: undefined, to: undefined }
};

Navigations.propTypes = {
    onPreviousClick: PropTypes.func,
    onNextClick: PropTypes.func,
    className: PropTypes.string,
}
