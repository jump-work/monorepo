# Table

##Импорт

```javascript
import { NewTable } from '@justlook/uikit';
```

##Использование

```javascript
const [sortField, setSortField] = useState('+name');
    let data = [...array];

    if (sortField) {
        data = array.sort((a, b) => a[sortField] > b[sortField] ? 1 : -1);
    }

<Table
    title='Заголовок таблицы'
    loading={boolean('Загрузка', false)}
    headerCell={[
        {label: 'Имя', sort: 'name'},
        {label: 'Год рождения', sort: 'year'},
        {label: 'Информация'}
    ]}
    sortField={sortField}
    onSort={setSortField}
    accessor={['name', 'year', 'info']}
    data={data}
    onDefaultAction={(item) => console.log('--default', item)}
    renderCustomCells={(item) => (
        <div style={{position:'absolute', top:0, right:0, bottom:0}}>
            <Button styling='hollow'
                    icon={<Icons24.IconPencil />}
                    onClick={() => console.log('--edit', item)} />

            <Button styling='hollow'
                    icon={<Icons24.IconTrash />}
                    onClick={() => console.log('--delete', item)} />

            <Button styling='hollow'
                    icon={<Icons24.IconChart />}
                    onClick={() => console.log('--statistic', item)} />
        </div>
    )}
    onAdd={() => alert('onAdd')}
/>
```

- **title**: Заголовок таблицы.
- **loading**: Состояние таблицы.
- **headerCell**: Ячейки заголовка таблицы, ключ `sort` указывает на возможность сортировать, по заданому значению.
- **sortField**: Внешняя зависимость, состояние определяющее отсортированый параметр.
- **onSort**: Функция для определения состояния `sortField`. 
- **accessor**: Значения ключей, по которым нужно взять данные для ячеек таблицы.
- **data**: Данные для таблицы.
- **renderCustomCells**: Функция позволяет передать кастомную ячейку в таблицу.
 


