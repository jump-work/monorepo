import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import { useModalState } from '../hooks';
import { Popup } from '../popup';
import { Button } from '../button';
import { Paragraph } from '../paragraph';
import { Icons16 } from '../icons';

import cx from './new-table-popup-sort.module.scss';

export const TablePopupSort = ({headerCell, renderProps, field, onClickSort}) => {
    const { isShow, onOpenForm, onCloseForm } = useModalState(false);
    const [ selectedSort, setSelectedSort ] = useState(field);

    const onSort = async () => {
        await onClickSort(selectedSort);
        onCloseForm();
    }

    const onSelectSort = (sort) => {
        if(sort === selectedSort) {
            setSelectedSort(null)
        } else {
            setSelectedSort(sort)
        }
    }

    return (
        <Fragment>
            {renderProps(onOpenForm)}
            {isShow &&
                <Popup onDismiss={() => onCloseForm()}>
                    <Popup.Header>
                        Сортировка
                    </Popup.Header>
                    <Popup.Content>
                        {headerCell.map((item, index) => (
                            <Fragment key={index}>
                                {item.sort &&
                                    <div
                                        className={cx.itemSortWrap}
                                        onClick={() => onSelectSort(item.sort)}
                                    >
                                        <Paragraph>{item.label}</Paragraph>

                                        {item.sort === selectedSort &&
                                            <Icons16.IconCheck/>
                                        }
                                    </div>
                                }
                            </Fragment>
                        ))}
                    </Popup.Content>
                    <Popup.Footer>
                        <Button className={cx.button} onClick={() => onSort()}>Применить</Button>
                    </Popup.Footer>
                </Popup>
            }
        </Fragment>
    )
}

TablePopupSort.propTypes = {
    headerCell: PropTypes.array,
    renderProps: PropTypes.func,
    field: PropTypes.any,
    onClickSort: PropTypes.func,
}