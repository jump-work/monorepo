import React, { useState } from 'react';
import { storiesOf }       from '@storybook/react';
import { boolean }         from '@storybook/addon-knobs';
import { Button }          from '../button';
import { Icons24 }         from '../icons';

import { NewTable as Table } from './new-table';
import readme                from './readme.md';

const array = [
    {
        id: 1,
        name: 'Jone',
        year: 1993,
        info: 'Programmer'
    },
    {
        id: 2,
        name: 'Tamara',
        year: 1994,
        info: 'dancer'
    },
    {
        id: 3,
        name: 'Adelina',
        year: 1991,
        info: 'dancer'
    },
    {
        id: 4,
        name: 'Misha',
        year: 1958,
        info: 'seller'
    }
]

storiesOf('Компоненты/Новая таблица', module)
    .add(
        'Table',
        () => {
            const [ sortField, setSortField ] = useState('+name');
            let data = [ ...array ];

            if (sortField) {
                data = array.sort((a, b) => a[ sortField ] > b[ sortField ] ? 1 : -1);
            }

            return (
                <Table
                    title='Заголовок таблицы'
                    loading={boolean('Загрузка', false)}
                    headerCell={[
                        { label: 'Имя', sort: 'name' },
                        { label: 'Год рождения', sort: 'year' },
                        { label: 'Информация' },
                    ]}
                    sortField={sortField}
                    onSort={setSortField}
                    accessor={[ 'name', 'year', 'info' ]}
                    data={data}
                    onDefaultAction={(item) => console.log('--default', item)}
                    renderCustomCells={(item) => (
                        <div style={{ position: 'absolute', top: 0, right: 0, bottom: 0 }}>
                            <Button styling='hollow'
                                    icon={<Icons24.IconPencil/>}
                                    onClick={() => console.log('--edit', item)}/>

                            <Button styling='hollow'
                                    icon={<Icons24.IconTrash/>}
                                    onClick={() => console.log('--delete', item)}/>

                            <Button styling='hollow'
                                    icon={<Icons24.IconChart/>}
                                    onClick={() => console.log('--statistic', item)}/>
                        </div>
                    )}
                    onAdd={() => alert('onAdd')}
                />
            )
        }, {
            notes: { readme }
        }
    )
