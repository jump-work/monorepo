import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { useIfMediaScreen } from '../hooks';
import { Paragraph } from '../paragraph';
import { Description } from '../description';
import { Button } from '../button';
import { Icons24 } from '../icons';

import { TableRow } from './new-table-row';
import { TableCell } from './new-table-cell';
import { TableMobileRow } from './new-table-mobile-row';
import { TablePopup } from './new-table-popup';

import cx from './new-table-body.module.scss';

export const TableBody = ({data, accessor, headerCell, renderCustomCells, onDefaultAction}) => {
    const isMobile = useIfMediaScreen();
    const [hoverItemId, onHover] = useState(null);
    return (
        <div className={cx.base}>
            {data.map((object, index) => {

                if(isMobile) {
                    return (
                        <TableMobileRow key={index}>
                            <Paragraph className={cx.paragraph}>{object[accessor[0]]}</Paragraph>
                            <Description.LH24 className={cx.description}>{object[accessor[1]]}</Description.LH24>

                            <TablePopup
                                renderProps={(open) => (
                                    <Button
                                        className={cx.button}
                                        styling='hollow'
                                        icon={ <Icons24.IconInfo className={cx.icon} /> }
                                        onClick={open}
                                    />
                                )}
                                data={object}
                                accessor={accessor}
                                headerCell={headerCell}
                                renderCustomCells={renderCustomCells}
                            />
                        </TableMobileRow>
                    )
                }

                return (
                    <TableRow
                        key={index}
                        style={{gridTemplateColumns: `repeat(${accessor.length}, 1fr)`}}
                        onClick={() => onDefaultAction(object)}
                        onMouseEnter={() => onHover(object.id)}
                        onMouseLeave={() => onHover(null)}
                    >
                        {accessor.map((item, index) => (
                            <TableCell key={index}>{object[item]}</TableCell>
                        ))}

                        {hoverItemId === object.id && renderCustomCells(object)}
                    </TableRow>
                )
            })}
        </div>
    )
}

TableBody.defaultProps = {
    data: [],
    accessor: [],
    renderCustomCells: () => {}
}

TableBody.propTypes = {
    data: PropTypes.array,
    accessor: PropTypes.array,
    renderCustomCells: PropTypes.func,
    headerCell: PropTypes.array,
    onDefaultAction: PropTypes.func,
}