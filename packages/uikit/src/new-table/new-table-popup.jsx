import React, { Fragment } from 'react';
import PropTypes           from 'prop-types';
import { Button }          from '../button';
import { Icons24 }         from '../icons';
import { Header }          from '../header';
import { Paragraph }       from '../paragraph';
import { useModalState }   from '../hooks';

import cx from './new-table-popup.module.scss';

export const TablePopup = ({ data, accessor, headerCell, renderCustomCells, renderProps }) => {

    const { isShow, onOpenForm, onCloseForm } = useModalState(false);

    return (
        <Fragment>
            {renderProps(onOpenForm)}
            {isShow &&
            <div className={cx.base}>
                <div className={cx.shadow}/>
                <div className={cx.content}>
                    <div className={cx.header}>
                        <Header.H3>{data[ accessor[ 0 ] ]}</Header.H3>
                        <Button styling='hollow' icon={<Icons24.IconClose/>} onClick={() => onCloseForm()}/>
                    </div>

                    {headerCell.map((item, index) => (
                        <Paragraph key={index} className={cx.paragraph}>
                            {item.label}: {data[ accessor[ index ] ]}
                        </Paragraph>
                    ))}

                    <div className={cx.buttons}>
                        {renderCustomCells(data)}
                    </div>
                </div>
            </div>
            }
        </Fragment>
    )
}

TablePopup.propTypes = {
    data: PropTypes.object,
    accessor: PropTypes.array,
    headerCell: PropTypes.array,
    renderCustomCells: PropTypes.func,
    renderProps: PropTypes.func,

}
