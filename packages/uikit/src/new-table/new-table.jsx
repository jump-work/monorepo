import React, { Fragment } from 'react';
import PropTypes           from 'prop-types';
import { Ellipsis }        from '../spinner';

import { TableHeader } from './new-table-header';
import { TableBody }   from './new-table-body';

import cx from './new-table.module.scss'

export const NewTable = ({ loading, title, headerCell, data, accessor, renderCustomCells, onDefaultAction, onAdd, sortField, onSort }) => {
    return (
        <div className={cx.base}>
            {
                loading ? <div className={cx.spinner}><Ellipsis/></div> : (
                    <Fragment>
                        <TableHeader
                            headerCell={headerCell}
                            sortField={sortField}
                            onSort={onSort}
                            title={title}
                            onAdd={onAdd}
                        />
                        <TableBody
                            data={data}
                            accessor={accessor}
                            headerCell={headerCell}
                            renderCustomCells={renderCustomCells}
                            onDefaultAction={onDefaultAction}
                        />
                    </Fragment>
                )
            }

        </div>
    )
}

NewTable.defaultProps = {
    loading: false,
    title: null,
    headerCell: [],
    data: [],
    accessor: [],
    renderCustomCells: () => {},
    onDefaultAction: () => {},
    sortField: '',
    onSort: () => {},
}

NewTable.propTypes = {
    title: PropTypes.string,
    headerCell: PropTypes.array.isRequired,
    data: PropTypes.arrayOf(PropTypes.object).isRequired,
    accessor: PropTypes.array.isRequired,
    loading: PropTypes.bool,
    renderCustomCells: PropTypes.func,
    onDefaultAction: PropTypes.func,
    onAdd: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.bool,
    ]),
    sortField: PropTypes.string,
    onSort: PropTypes.any,
}
