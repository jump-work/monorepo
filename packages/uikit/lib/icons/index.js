"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Icons48 = exports.Icons24 = exports.Icons16 = void 0;

var _icons = _interopRequireDefault(require("./icons16"));

var _icons2 = _interopRequireDefault(require("./icons24"));

var _icons3 = _interopRequireDefault(require("./icons48"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Icons16 = _objectSpread({}, _icons["default"]);

exports.Icons16 = Icons16;

var Icons24 = _objectSpread({}, _icons2["default"]);

exports.Icons24 = Icons24;

var Icons48 = _objectSpread({}, _icons3["default"]);

exports.Icons48 = Icons48;