"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconEyeOpen = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconEyeOpen = function IconEyeOpen(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M12 19c-5.97 0-9-7-9-7s3.03-7 9-7c5.97 0 9 7 9 7s-3.03 7-9 7zm-6.7-6.903A14.34 14.34 0 015.246 12l.056-.097c.332-.579.828-1.344 1.477-2.1C8.109 8.254 9.854 7 12 7c2.146 0 3.89 1.254 5.222 2.803A13.878 13.878 0 0118.755 12l-.055.097a13.88 13.88 0 01-1.478 2.1C15.891 15.746 14.145 17 12 17c-2.146 0-3.89-1.254-5.222-2.803a13.885 13.885 0 01-1.477-2.1zM12 15a3 3 0 100-6 3 3 0 000 6z"
  }));
};

exports.IconEyeOpen = IconEyeOpen;