"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconFlasher = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconFlasher = function IconFlasher(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    d: "M7 1a1 1 0 010 2 2 2 0 00-2 2 1 1 0 01-2 0 4 4 0 014-4z"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fillRule: "evenodd",
    clipRule: "evenodd",
    fill: "#333",
    d: "M17 6l3 12a1 1 0 110 2H4a1 1 0 110-2L7 6l2-2h6l2 2zM9 6h6l3 12H6L9 6z"
  }));
};

exports.IconFlasher = IconFlasher;