"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconCheckCircle = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconCheckCircle = function IconCheckCircle(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M5 12a7 7 0 1114 0 7 7 0 01-14 0zm7 9a9 9 0 110-18 9 9 0 010 18z"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    d: "M16.768 9.64a1 1 0 10-1.536-1.28l-4.3 5.159-2.225-2.226a1 1 0 00-1.414 1.414l3 3a1 1 0 001.475-.067l5-6z"
  }));
};

exports.IconCheckCircle = IconCheckCircle;