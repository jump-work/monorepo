"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconDownload = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconDownload = function IconDownload(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M12 3a1 1 0 011 1v10.92l3.375-2.7a1 1 0 111.25 1.56l-4.997 3.998a.996.996 0 01-1.26-.004l-4.993-3.993a1 1 0 011.25-1.562L11 14.92V4a1 1 0 011-1zm5 18a1 1 0 100-2H7a1 1 0 100 2h10z"
  }));
};

exports.IconDownload = IconDownload;