"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconInfoFill = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconInfoFill = function IconInfoFill(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M12 3a9 9 0 110 18 9 9 0 010-18zm1 8a1 1 0 10-2 0v5a1 1 0 102 0v-5zm0-3a1 1 0 11-2 0 1 1 0 012 0z"
  }));
};

exports.IconInfoFill = IconInfoFill;