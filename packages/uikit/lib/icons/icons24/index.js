"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _arrowDown = require("./arrow-down");

var _arrowLeft = require("./arrow-left");

var _arrowLeftLine = require("./arrow-left-line");

var _arrowReload = require("./arrow-reload");

var _arrowRight = require("./arrow-right");

var _arrowRightLine = require("./arrow-right-line");

var _arrowTop = require("./arrow-top");

var _auto = require("./auto");

var _back = require("./back");

var _bell = require("./bell");

var _board = require("./board");

var _box = require("./box");

var _calendar = require("./calendar");

var _chart = require("./chart");

var _chat = require("./chat");

var _check = require("./check");

var _checkCircle = require("./check-circle");

var _checkCircleFill = require("./check-circle-fill");

var _clients = require("./clients");

var _close = require("./close");

var _copyright = require("./copyright");

var _cross = require("./cross");

var _doc = require("./doc");

var _download = require("./download");

var _eyeClose = require("./eye-close");

var _eyeOpen = require("./eye-open");

var _flag = require("./flag");

var _flasher = require("./flasher");

var _fly = require("./fly");

var _forward = require("./forward");

var _image = require("./image");

var _info = require("./info");

var _infoFill = require("./info-fill");

var _intercom = require("./intercom");

var _kebab = require("./kebab");

var _kebabVertical = require("./kebab-vertical");

var _loader = require("./loader");

var _loaderSmall = require("./loader-small");

var _menu = require("./menu");

var _minus = require("./minus");

var _news = require("./news");

var _panel = require("./panel");

var _pencil = require("./pencil");

var _plus = require("./plus");

var _progress = require("./progress");

var _receipt = require("./receipt");

var _reloadBack = require("./reload-back");

var _rouble = require("./rouble");

var _search = require("./search");

var _setting = require("./setting");

var _signature = require("./signature");

var _sort = require("./sort");

var _stick = require("./stick");

var _store = require("./store");

var _trash = require("./trash");

var _user = require("./user");

var _wallet = require("./wallet");

var _warning = require("./warning");

var _warningFill = require("./warning-fill");

var _watch = require("./watch");

var _watchRhombus = require("./watch-rhombus");

var _default = {
  IconArrowDown: _arrowDown.IconArrowDown,
  IconArrowLeft: _arrowLeft.IconArrowLeft,
  IconArrowLeftLine: _arrowLeftLine.IconArrowLeftLine,
  IconArrowReload: _arrowReload.IconArrowReload,
  IconArrowRight: _arrowRight.IconArrowRight,
  IconArrowRightLine: _arrowRightLine.IconArrowRightLine,
  IconArrowTop: _arrowTop.IconArrowTop,
  IconAuto: _auto.IconAuto,
  IconBack: _back.IconBack,
  IconBell: _bell.IconBell,
  IconBoard: _board.IconBoard,
  IconBox: _box.IconBox,
  IconCalendar: _calendar.IconCalendar,
  IconChart: _chart.IconChart,
  IconChat: _chat.IconChat,
  IconCheck: _check.IconCheck,
  IconCheckCircle: _checkCircle.IconCheckCircle,
  IconCheckCircleFill: _checkCircleFill.IconCheckCircleFill,
  IconClients: _clients.IconClients,
  IconClose: _close.IconClose,
  IconCopyright: _copyright.IconCopyright,
  IconCross: _cross.IconCross,
  IconDoc: _doc.IconDoc,
  IconDownload: _download.IconDownload,
  IconEyeClose: _eyeClose.IconEyeClose,
  IconEyeOpen: _eyeOpen.IconEyeOpen,
  IconFlag: _flag.IconFlag,
  IconFlasher: _flasher.IconFlasher,
  IconFly: _fly.IconFly,
  IconForward: _forward.IconForward,
  IconImage: _image.IconImage,
  IconInfo: _info.IconInfo,
  IconInfoFill: _infoFill.IconInfoFill,
  IconIntercom: _intercom.IconIntercom,
  IconKebab: _kebab.IconKebab,
  IconKebabVertical: _kebabVertical.IconKebabVertical,
  IconLoader: _loader.IconLoader,
  IconLoaderSmall: _loaderSmall.IconLoaderSmall,
  IconMenu: _menu.IconMenu,
  IconMinus: _minus.IconMinus,
  IconNews: _news.IconNews,
  IconPanel: _panel.IconPanel,
  IconPencil: _pencil.IconPencil,
  IconPlus: _plus.IconPlus,
  IconProgress: _progress.IconProgress,
  IconReceipt: _receipt.IconReceipt,
  IconReloadBack: _reloadBack.IconReloadBack,
  IconRouble: _rouble.IconRouble,
  IconSearch: _search.IconSearch,
  IconSetting: _setting.IconSetting,
  IconSignature: _signature.IconSignature,
  IconSort: _sort.IconSort,
  IconStick: _stick.IconStick,
  IconStore: _store.IconStore,
  IconTrash: _trash.IconTrash,
  IconUser: _user.IconUser,
  IconWallet: _wallet.IconWallet,
  IconWarning: _warning.IconWarning,
  IconWarningFill: _warningFill.IconWarningFill,
  IconWatch: _watch.IconWatch,
  IconWatchRhombus: _watchRhombus.IconWatchRhombus
};
exports["default"] = _default;