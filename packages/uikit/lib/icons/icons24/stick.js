"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconStick = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconStick = function IconStick(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    d: "M6.129 4A2.129 2.129 0 004 6.129v11.742C4 19.047 4.953 20 6.129 20h7.403c.565 0 1.107-.224 1.506-.624l4.338-4.338c.4-.4.624-.941.624-1.506V6.13A2.129 2.129 0 0017.871 4H6.129zM6 6.129A.13.13 0 016.129 6h11.742a.13.13 0 01.129.129V12h-3.871A2.129 2.129 0 0012 14.129V18H6.129A.129.129 0 016 17.871V6.129zM14 17.5l3.5-3.5h-3.371a.13.13 0 00-.129.129V17.5z"
  }));
};

exports.IconStick = IconStick;