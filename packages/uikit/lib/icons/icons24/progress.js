"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconProgress = void 0;

var _react = _interopRequireDefault(require("react"));

var _animateModule = _interopRequireDefault(require("../animate.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconProgress = function IconProgress(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#DBDDE6",
    opacity: ".3",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M12 4a8 8 0 100 16 8 8 0 000-16zM2 12C2 6.477 6.477 2 12 2s10 4.477 10 10-4.477 10-10 10S2 17.523 2 12z"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    className: _animateModule["default"].base,
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M5.917 17.196A8 8 0 1012 4V2a10 10 0 11-7.604 16.494l1.52-1.298z"
  }));
};

exports.IconProgress = IconProgress;