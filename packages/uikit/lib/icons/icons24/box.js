"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconBox = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconBox = function IconBox(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M11 2h2l8 3 1 2v9l-1 2-8 3h-2l-8-3-1-2V8 7l1-2 8-3zm1 2l8 3-8 3-8-3 8-3zM4 16V9l7 3v7l-7-3zm9 3l7-3V9l-7 3v7z"
  }));
};

exports.IconBox = IconBox;