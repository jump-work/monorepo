"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconSearch = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconSearch = function IconSearch(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M15 10a5 5 0 11-10 0 5 5 0 0110 0zm-1.092 5.808a7 7 0 111.477-1.336l.03.028L20 19.086a1 1 0 01-1.414 1.414L14 15.914a1.004 1.004 0 01-.092-.106z"
  }));
};

exports.IconSearch = IconSearch;