"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconCheck = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconCheck = function IconCheck(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M18.6 6.2a1 1 0 01.2 1.4l-7.5 10a1 1 0 01-1.446.163l-4.5-3.81a1 1 0 111.292-1.526l3.69 3.124L17.2 6.4a1 1 0 011.4-.2z"
  }));
};

exports.IconCheck = IconCheck;