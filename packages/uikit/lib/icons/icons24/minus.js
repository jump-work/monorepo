"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconMinus = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconMinus = function IconMinus(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    d: "M4 12a1 1 0 011-1h14a1 1 0 110 2H5a1 1 0 01-1-1z",
    fill: "#333"
  }));
};

exports.IconMinus = IconMinus;