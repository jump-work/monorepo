"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconCheckCircleFill = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconCheckCircleFill = function IconCheckCircleFill(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M21 12a9 9 0 10-18 0 9 9 0 0018 0zm-4.232-2.36a1 1 0 10-1.536-1.28l-4.3 5.159-2.225-2.226a1 1 0 00-1.414 1.414l3 3a1 1 0 001.475-.067l5-6z"
  }));
};

exports.IconCheckCircleFill = IconCheckCircleFill;