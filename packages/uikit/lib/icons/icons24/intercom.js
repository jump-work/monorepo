"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconIntercom = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconIntercom = function IconIntercom(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M18 4H6a1 1 0 00-1 1v11a1 1 0 001 1h9.169a5 5 0 012.572.712l1.259.756V5a1 1 0 00-1-1zm3 1a3 3 0 00-3-3H6a3 3 0 00-3 3v11a3 3 0 003 3h9.169a3 3 0 011.543.427L21 22V5z"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M6.03 13.758a1 1 0 011.213-.728l1.09.272c1.75.438 5.583.438 7.335 0l1.09-.272a1 1 0 11.485 1.94l-1.09.272c-2.07.518-6.236.518-8.306 0l-1.09-.272a1 1 0 01-.727-1.213z"
  }));
};

exports.IconIntercom = IconIntercom;