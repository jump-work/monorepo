"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconArrowReload = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconArrowReload = function IconArrowReload(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    d: "M16.486 3.336a1 1 0 10-1.973.328l.154.919c-.915-.39-1.911-.619-2.981-.577A8 8 0 1020 12a1 1 0 10-2 0 6 6 0 11-6.236-5.995 4.79 4.79 0 011.929.34l-.564.227a1 1 0 10.742 1.856l2.484-.993a.999.999 0 00.63-1.114l-.499-2.985z"
  }));
};

exports.IconArrowReload = IconArrowReload;