"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconReloadBack = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconReloadBack = function IconReloadBack(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    d: "M7.514 3.336a1 1 0 011.972.328l-.153.919c.915-.39 1.911-.619 2.981-.577A8 8 0 114 12a1 1 0 012 0 6 6 0 106.236-5.995 4.79 4.79 0 00-1.929.34l.564.227a1 1 0 11-.742 1.856l-2.484-.993a1 1 0 01-.63-1.114l.499-2.985z"
  }));
};

exports.IconReloadBack = IconReloadBack;