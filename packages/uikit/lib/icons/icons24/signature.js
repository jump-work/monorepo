"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconSignature = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconSignature = function IconSignature(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M5 21.003v-5.5L15.5 4.5c.5-.5 1.5-.5 2 0l3.5 3c.5.5.5 1.5 0 2L12.33 19h7.741c.513 0 .929.448.929 1s-.416 1-.929 1L5 21.003zM7 16.5L16.5 6l3 2.5L9.503 19 7 19.003V16.5z"
  }));
};

exports.IconSignature = IconSignature;