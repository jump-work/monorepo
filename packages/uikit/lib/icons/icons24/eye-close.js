"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconEyeClose = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconEyeClose = function IconEyeClose(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M17.852 7.563l1.855-1.856a1 1 0 00-1.414-1.414L16.276 6.31C15.076 5.535 13.651 5 12 5c-5.97 0-9 7-9 7s1.055 2.439 3.148 4.438l-1.855 1.855a1 1 0 101.414 1.414l2.017-2.017C8.924 18.465 10.349 19 12 19c5.97 0 9-7 9-7s-1.055-2.439-3.148-4.437zm-3.032.203C13.974 7.29 13.033 7 12 7 9.854 7 8.11 8.254 6.778 9.803A13.884 13.884 0 005.245 12l.056.097c.332.579.828 1.344 1.477 2.1.247.287.509.565.785.826l7.257-7.257zm-5.64 8.468l7.257-7.257c.276.261.538.539.785.826A13.878 13.878 0 0118.755 12l-.055.097a13.88 13.88 0 01-1.478 2.1C15.891 15.746 14.145 17 12 17c-1.033 0-1.974-.291-2.82-.766z"
  }));
};

exports.IconEyeClose = IconEyeClose;