"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconUser = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconUser = function IconUser(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M6.81 19.354A8.96 8.96 0 0012 21c1.932 0 3.723-.61 5.19-1.646.05-.13.113-.315.168-.534.12-.477.18-1.047.047-1.578-.123-.492-.425-1.012-1.167-1.436C15.461 15.362 14.155 15 12 15c-2.155 0-3.461.362-4.238.806-.742.424-1.044.944-1.167 1.437-.133.53-.072 1.1.047 1.577.055.219.118.403.169.534zM4.585 17.1a9 9 0 1114.833 0 4.39 4.39 0 00-.072-.343c-.252-1.007-.887-1.987-2.114-2.688-.73-.417-1.649-.724-2.79-.9a4 4 0 10-4.882 0c-1.141.176-2.06.483-2.79.9-1.227.701-1.862 1.68-2.114 2.688-.029.115-.052.23-.071.343zM12 1C5.925 1 1 5.925 1 12s4.925 11 11 11 11-4.925 11-11S18.075 1 12 1zm0 11a2 2 0 100-4 2 2 0 000 4z"
  }));
};

exports.IconUser = IconUser;