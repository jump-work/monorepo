"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconDoc = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconDoc = function IconDoc(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M6 6a1 1 0 011-1h5v4a2 2 0 002 2h4v7a1 1 0 01-1 1H7a1 1 0 01-1-1V6zm11.761 3a.992.992 0 00-.076-.08L14 5.457V9h3.761zM7 3a3 3 0 00-3 3v12a3 3 0 003 3h10a3 3 0 003-3V9.648a3 3 0 00-.946-2.186l-3.883-3.648A3 3 0 0013.117 3H7z"
  }));
};

exports.IconDoc = IconDoc;