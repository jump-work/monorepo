"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconAuto = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconAuto = function IconAuto(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "24",
    height: "24",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M18.05 17.264l-.193-.406a1.5 1.5 0 00-2.714 0l-.193.406.065.453a1.5 1.5 0 002.97 0l.064-.453zM19.662 16H21a1 1 0 001-1v-1.532a1 1 0 00-.807-.981l-1.693-.333a5 5 0 01-2.742-1.55L14.697 8.33a1 1 0 00-.74-.329H5.892a1 1 0 00-.738.325L2.262 11.49a1 1 0 00-.262.675V15a.997.997 0 001 1h.337a3.5 3.5 0 016.326 0h3.674a3.5 3.5 0 016.326 0zm.302 2a3.501 3.501 0 01-6.93 0h-3.07a3.5 3.5 0 01-6.93 0H3a3 3 0 01-3-3v-2.835a3 3 0 01.786-2.025l2.893-3.164A3 3 0 015.893 6h8.062a3 3 0 012.224.986l2.062 2.276a3 3 0 001.645.93l1.692.332A3 3 0 0124 13.468V15a3 3 0 01-3 3h-1.035zM4.95 17.264l.192-.406a1.5 1.5 0 012.714 0l.192.406-.064.453a1.5 1.5 0 01-2.97 0l-.064-.453z"
  }));
};

exports.IconAuto = IconAuto;