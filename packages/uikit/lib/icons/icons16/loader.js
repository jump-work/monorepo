"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconLoader = void 0;

var _react = _interopRequireDefault(require("react"));

var _animateModule = _interopRequireDefault(require("../animate.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconLoader = function IconLoader(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    className: _animateModule["default"].base,
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M10.939 3.955A5 5 0 1013 8h2a7 7 0 11-2.886-5.663L10.94 3.955z"
  }));
};

exports.IconLoader = IconLoader;