"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconArrowUp = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconArrowUp = function IconArrowUp(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M12.696 10.718a1 1 0 01-1.414-.022L8.1 7.414l-3.404 3.304a1 1 0 01-1.392-1.436l4.12-4a1 1 0 011.415.022l3.879 4a1 1 0 01-.022 1.414z"
  }));
};

exports.IconArrowUp = IconArrowUp;