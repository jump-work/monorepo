"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconCrossCircle = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconCrossCircle = function IconCrossCircle(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    d: "M4.293 4.293a1 1 0 000 1.414L6.586 8l-2.293 2.293a1 1 0 101.414 1.414L8 9.414l2.293 2.293a1 1 0 001.414-1.414L9.414 8l2.293-2.293a1 1 0 00-1.414-1.414L8 6.586 5.707 4.293a1 1 0 00-1.414 0z"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M0 8a8 8 0 1116 0A8 8 0 010 8zm8 6A6 6 0 118 2a6 6 0 010 12z"
  }));
};

exports.IconCrossCircle = IconCrossCircle;