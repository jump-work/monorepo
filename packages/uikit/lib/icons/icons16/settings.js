"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconSettings = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconSettings = function IconSettings(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M5 6a1 1 0 100-2 1 1 0 000 2zm0-4c1.306 0 2.417.835 2.83 2H13a1 1 0 110 2H7.83A3.001 3.001 0 115 2zm3.17 8a3.001 3.001 0 110 2H3a1 1 0 110-2h5.17zM10 11a1 1 0 112 0 1 1 0 01-2 0z"
  }));
};

exports.IconSettings = IconSettings;