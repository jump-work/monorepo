"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconWatch = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconWatch = function IconWatch(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M2 8a6 6 0 1012 0A6 6 0 002 8zm6-8a8 8 0 100 16A8 8 0 008 0zm0 3a1 1 0 011 1v3h2a1 1 0 110 2H8a1 1 0 01-1-1V4a1 1 0 011-1z"
  }));
};

exports.IconWatch = IconWatch;