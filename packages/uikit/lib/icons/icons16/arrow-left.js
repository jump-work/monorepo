"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconArrowLeft = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconArrowLeft = function IconArrowLeft(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M9.718 3.304a1 1 0 01-.022 1.414L6.414 7.9l3.304 3.403a1 1 0 01-1.436 1.393l-4-4.12a1 1 0 01.022-1.415l4-3.879a1 1 0 011.414.022z"
  }));
};

exports.IconArrowLeft = IconArrowLeft;