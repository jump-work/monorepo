"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconWatchFill = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconWatchFill = function IconWatchFill(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M0 8a8 8 0 1116 0A8 8 0 010 8zm9-4a1 1 0 10-2 0v4a1 1 0 001 1h3a1 1 0 100-2H9V4z"
  }));
};

exports.IconWatchFill = IconWatchFill;