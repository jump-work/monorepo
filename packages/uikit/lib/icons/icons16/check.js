"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconCheck = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconCheck = function IconCheck(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M12.588 3.191a1 1 0 01.22 1.397l-5.09 7a1 1 0 01-1.479.154L3.33 9.117a1 1 0 011.34-1.484l2.084 1.88 4.437-6.101a1 1 0 011.397-.22z"
  }));
};

exports.IconCheck = IconCheck;