"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconReceipt = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconReceipt = function IconReceipt(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M2 15V2a1 1 0 011-1h10a1 1 0 011 1v13l-2-1-2 1-2-1-2 1-2-1-2 1zm2-9.5a.5.5 0 01.5-.5h7a.5.5 0 010 1h-7a.5.5 0 01-.5-.5zM4.5 7a.5.5 0 000 1h7a.5.5 0 000-1h-7z"
  }));
};

exports.IconReceipt = IconReceipt;