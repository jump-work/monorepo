"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconInfo = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconInfo = function IconInfo(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M2 8a6 6 0 1012 0A6 6 0 002 8zm6-8a8 8 0 100 16A8 8 0 008 0zm0 7a1 1 0 011 1v3a1 1 0 11-2 0V8a1 1 0 011-1zm0-1a1 1 0 100-2 1 1 0 000 2z"
  }));
};

exports.IconInfo = IconInfo;