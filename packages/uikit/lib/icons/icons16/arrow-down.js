"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconArrowDown = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconArrowDown = function IconArrowDown(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "16",
    height: "16",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#333",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M3.304 5.282a1 1 0 011.414.022L7.9 8.586l3.403-3.304a1 1 0 011.393 1.436l-4.12 4a1 1 0 01-1.415-.022l-3.879-4a1 1 0 01.022-1.414z"
  }));
};

exports.IconArrowDown = IconArrowDown;