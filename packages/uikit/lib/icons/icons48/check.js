"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconCheck = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconCheck = function IconCheck(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "48",
    height: "48",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#26D962",
    d: "M24 4C12.96 4 4 12.96 4 24s8.96 20 20 20 20-8.96 20-20S35.04 4 24 4zm-2.586 28.586a2 2 0 01-2.828 0L11.41 25.41a1.995 1.995 0 012.82-2.824L20 28.34l13.76-13.76a2.001 2.001 0 012.83 2.83L21.415 32.585z"
  }));
};

exports.IconCheck = IconCheck;