"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _check = require("./check");

var _cross = require("./cross");

var _docDownload = require("./doc-download");

var _warning = require("./warning");

var Icons = {
  IconCheck: _check.IconCheck,
  IconCross: _cross.IconCross,
  IconDocDownload: _docDownload.IconDocDownload,
  IconWarning: _warning.IconWarning
};
var _default = Icons;
exports["default"] = _default;