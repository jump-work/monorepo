"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconCross = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconCross = function IconCross(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "48",
    height: "48",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#EC393D",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M24 44a20 20 0 100-40 20 20 0 000 40zm-5.6-28.4a2 2 0 10-2.8 2.8l5.6 5.6-5.6 5.6a2 2 0 102.8 2.8l5.6-5.6 5.6 5.6a2 2 0 102.8-2.8L26.8 24l5.6-5.6a2 2 0 10-2.8-2.8L24 21.2l-5.6-5.6z"
  }));
};

exports.IconCross = IconCross;