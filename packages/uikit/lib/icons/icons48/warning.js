"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.IconWarning = void 0;

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

var IconWarning = function IconWarning(props) {
  return /*#__PURE__*/_react["default"].createElement("svg", _extends({}, props, {
    width: "48",
    height: "48",
    fill: "none",
    xmlns: "http://www.w3.org/2000/svg"
  }), /*#__PURE__*/_react["default"].createElement("path", {
    fill: "#F59F00",
    fillRule: "evenodd",
    clipRule: "evenodd",
    d: "M24 44c11.046 0 20-8.954 20-20S35.046 4 24 4 4 12.954 4 24s8.954 20 20 20zm0-32a2 2 0 012 2v14a2 2 0 11-4 0V14a2 2 0 012-2zm0 24a2 2 0 100-4 2 2 0 000 4z"
  }));
};

exports.IconWarning = IconWarning;