"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "BigHeader", {
  enumerable: true,
  get: function get() {
    return _bigHeader.BigHeader;
  }
});

var _bigHeader = require("./big-header");