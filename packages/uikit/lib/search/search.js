"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Search = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _searchModule = _interopRequireDefault(require("./search.module.scss"));

var _hooks = require("../hooks");

var _input = require("../input");

var _spinner = require("../spinner");

var _searchListItem = require("./search-list-item");

var _excluded = ["open", "loading", "onSearch", "results", "debounce", "hitSearchOnEnterOnly", "style"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Search = function Search(_ref) {
  var open = _ref.open,
      loading = _ref.loading,
      onSearch = _ref.onSearch,
      results = _ref.results,
      debounce = _ref.debounce,
      hitSearchOnEnterOnly = _ref.hitSearchOnEnterOnly,
      style = _ref.style,
      rest = _objectWithoutProperties(_ref, _excluded);

  var inputRef = /*#__PURE__*/_react["default"].createRef();

  var _useState = (0, _react.useState)(false),
      _useState2 = _slicedToArray(_useState, 2),
      didSearch = _useState2[0],
      setDidSearch = _useState2[1];

  var _useState3 = (0, _react.useState)([]),
      _useState4 = _slicedToArray(_useState3, 2),
      innerResults = _useState4[0],
      setInnerResults = _useState4[1];

  var _useState5 = (0, _react.useState)([]),
      _useState6 = _slicedToArray(_useState5, 2),
      prevResults = _useState6[0],
      setPrevResults = _useState6[1];

  var _useState7 = (0, _react.useState)(''),
      _useState8 = _slicedToArray(_useState7, 2),
      searchTerm = _useState8[0],
      setSearchTerm = _useState8[1];

  var _useDebounce = (0, _hooks.useDebounce)(searchTerm, debounce),
      _useDebounce2 = _slicedToArray(_useDebounce, 2),
      debouncedSearchTerm = _useDebounce2[0],
      cancel = _useDebounce2[1];

  var doSearch = (0, _react.useCallback)(function (str) {
    onSearch(str);
    setDidSearch(true);
  }, [onSearch, setDidSearch]);

  var onItemChange = function onItemChange(value) {
    if (value === '') {
      setDidSearch(false);
    }

    setSearchTerm(value);
  };

  var onKeyPress = function onKeyPress(event) {
    if (event.key === 'Enter') {
      doSearch(searchTerm);

      if (cancel) {
        clearTimeout(cancel);
      }
    }
  };

  (0, _react.useEffect)(function () {
    if (inputRef && inputRef.current) {
      inputRef.current.focus();
    }
  }, [inputRef]);
  (0, _react.useEffect)(function () {
    if (!open) {
      setDidSearch(false);
    }
  }, [open]);
  (0, _react.useEffect)(function () {
    setPrevResults(function () {
      if (innerResults.length > 0) {
        return _toConsumableArray(innerResults);
      }
    });
    setInnerResults(_toConsumableArray(results));
  }, [results, innerResults]);
  (0, _react.useEffect)(function () {
    if (hitSearchOnEnterOnly) return;

    if (debouncedSearchTerm !== '') {
      doSearch(debouncedSearchTerm);
    }
  }, [debouncedSearchTerm, hitSearchOnEnterOnly, doSearch]);
  return open ? /*#__PURE__*/_react["default"].createElement("div", {
    className: _searchModule["default"].base,
    style: _objectSpread({}, style)
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: _searchModule["default"].searchInput
  }, /*#__PURE__*/_react["default"].createElement(_input.Input, {
    ref: inputRef,
    type: "text",
    clearable: true,
    onChange: onItemChange,
    onKeyPress: onKeyPress
  })), loading && /*#__PURE__*/_react["default"].createElement("div", {
    className: _searchModule["default"].loading
  }, /*#__PURE__*/_react["default"].createElement(_spinner.Ellipsis, {
    invert: false
  })), !loading && didSearch && innerResults.length === 0 && /*#__PURE__*/_react["default"].createElement("div", {
    className: _searchModule["default"].noresult
  }, "\u041D\u0438\u043A\u0430\u043A\u0438\u0445 \u0440\u0435\u0437\u0443\u043B\u044C\u0442\u0430\u0442\u043E\u0432"), !loading && /*#__PURE__*/_react["default"].createElement("div", {
    className: _searchModule["default"].results
  }, _toConsumableArray(innerResults.length > 0 ? innerResults : !didSearch ? prevResults : []).map(function (item, index) {
    return /*#__PURE__*/_react["default"].createElement(rest.listItemType, _extends({
      key: index
    }, item));
  }))) : null;
};

exports.Search = Search;
Search.ListItem = _searchListItem.SearchListItem;
Search.defaultProps = {
  open: false,
  loading: false,
  debounce: 500,
  listItemType: _searchListItem.SearchListItem,
  hitSearchOnEnterOnly: false,
  style: {}
};
Search.propTypes = {
  open: _propTypes["default"].bool,
  loading: _propTypes["default"].bool,
  results: _propTypes["default"].array,
  debounce: _propTypes["default"].number,
  listItemType: _propTypes["default"].elementType,
  hitSearchOnEnterOnly: _propTypes["default"].bool,
  style: _propTypes["default"].object,
  onSearch: _propTypes["default"].func
};