"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Label = exports.LabelThemes = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames2 = _interopRequireDefault(require("classnames"));

var _labelModule = _interopRequireDefault(require("./label.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var LabelThemes = ['success', 'info', 'warning', 'danger'];
exports.LabelThemes = LabelThemes;

var Label = function Label(_ref) {
  var _classnames;

  var text = _ref.text,
      theme = _ref.theme,
      className = _ref.className;
  var classes = (0, _classnames2["default"])(_labelModule["default"].label, className, (_classnames = {}, _defineProperty(_classnames, _labelModule["default"].success, theme === 'success'), _defineProperty(_classnames, _labelModule["default"].info, theme === 'info'), _defineProperty(_classnames, _labelModule["default"].warning, theme === 'warning'), _defineProperty(_classnames, _labelModule["default"].danger, theme === 'danger'), _classnames));
  if (!text) return;
  return /*#__PURE__*/_react["default"].createElement("span", {
    className: classes
  }, text);
};

exports.Label = Label;
Label.propTypes = {
  text: _propTypes["default"].string,
  theme: _propTypes["default"].oneOf(LabelThemes),
  className: _propTypes["default"].string
};