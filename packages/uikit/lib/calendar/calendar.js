"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactDayPicker = _interopRequireDefault(require("react-day-picker"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactClickOutside = _interopRequireDefault(require("react-click-outside"));

require("react-day-picker/lib/style.css");

var _input = require("../input");

var _button = require("../button");

var _icons = require("../icons");

var _helpers = require("../helpers");

require("./custom-style--calendar.scss");

var _calendarModule = _interopRequireDefault(require("./calendar.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var WEEKDAYS_SHORT = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
var MONTHS = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
var WEEKDAYS_LONG = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];

var Navbar = function Navbar(_ref) {
  var onPreviousClick = _ref.onPreviousClick,
      onNextClick = _ref.onNextClick,
      className = _ref.className;
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: className
  }, /*#__PURE__*/_react["default"].createElement(_button.Button, {
    styling: 'hollow',
    icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconArrowLeft, null),
    onClick: function onClick() {
      return onPreviousClick();
    },
    className: _calendarModule["default"].prevBtn
  }), /*#__PURE__*/_react["default"].createElement(_button.Button, {
    styling: 'hollow',
    icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconArrowRight, null),
    onClick: function onClick() {
      return onNextClick();
    },
    className: _calendarModule["default"].nextBtn
  }));
};

var Calendar = /*#__PURE__*/function (_Component) {
  _inherits(Calendar, _Component);

  var _super = _createSuper(Calendar);

  function Calendar(props) {
    var _this;

    _classCallCheck(this, Calendar);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "handleClickOutside", function () {
      var selectedDay = _this.state.selectedDay;
      var onChange = _this.props.onChange;

      _this.setState({
        focus: false
      });

      onChange(selectedDay);
    });

    _defineProperty(_assertThisInitialized(_this), "handleDayClick", function (day, _ref2) {
      var selected = _ref2.selected;
      var onChange = _this.props.onChange;

      _this.setState({
        selectedDay: selected ? undefined : day,
        focus: false
      });

      onChange(day);
    });

    _defineProperty(_assertThisInitialized(_this), "handleSelectedDay", function (value) {
      _this.setState({
        selectedDay: (0, _helpers.formatDate)(value)
      });
    });

    _this.state = {
      selectedDay: (0, _helpers.formatDate)(_this.props.selectedDay),
      isVisible: false,
      focus: false
    };
    return _this;
  }

  _createClass(Calendar, [{
    key: "render",
    value: function render() {
      var _classes3,
          _this2 = this;

      var _this$state = this.state,
          selectedDay = _this$state.selectedDay,
          focus = _this$state.focus;
      var _this$props = this.props,
          label = _this$props.label,
          placeholder = _this$props.placeholder,
          id = _this$props.id,
          labelPosition = _this$props.labelPosition;
      var key = id || (0, _helpers.idFromString)(label) || (0, _helpers.idFromString)(placeholder);
      var cls = (0, _classnames["default"])(_calendarModule["default"].input, _defineProperty({}, _calendarModule["default"].focus, focus));
      var clsWrapper = (0, _classnames["default"])(_calendarModule["default"].wrapper, _defineProperty({}, _calendarModule["default"].grid, labelPosition === 'left'));
      var positionBase = (0, _classnames["default"])(_calendarModule["default"].overlay, (_classes3 = {}, _defineProperty(_classes3, _calendarModule["default"].normal, !label), _defineProperty(_classes3, _calendarModule["default"].left, labelPosition === 'left'), _classes3));
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: clsWrapper
      }, label.length > 1 ? /*#__PURE__*/_react["default"].createElement("label", {
        htmlFor: key,
        className: _calendarModule["default"].label
      }, label) : '', /*#__PURE__*/_react["default"].createElement("div", {
        className: cls
      }, /*#__PURE__*/_react["default"].createElement(_input.Input, {
        id: key,
        value: selectedDay && selectedDay.toLocaleDateString('ru-RU'),
        className: _calendarModule["default"].field,
        placeholder: placeholder,
        onFocus: function onFocus() {
          return _this2.setState({
            focus: true
          });
        },
        onChange: this.handleSelectedDay
      }), /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconCalendar, null)), focus && /*#__PURE__*/_react["default"].createElement("div", {
        className: positionBase
      }, /*#__PURE__*/_react["default"].createElement(_reactDayPicker["default"], {
        className: 'input',
        selectedDays: selectedDay,
        onDayClick: this.handleDayClick,
        month: selectedDay,
        months: MONTHS,
        weekdaysLong: WEEKDAYS_LONG,
        weekdaysShort: WEEKDAYS_SHORT,
        firstDayOfWeek: 1,
        navbarElement: /*#__PURE__*/_react["default"].createElement(Navbar, null)
      })));
    }
  }]);

  return Calendar;
}(_react.Component);

Calendar.defaultProps = {
  label: '',
  placeholder: '',
  onChange: function onChange() {},
  labelPosition: 'top',
  id: null,
  selectedDay: ''
};
Calendar.propTypes = {
  id: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
  label: _propTypes["default"].string,
  placeholder: _propTypes["default"].string,
  onChange: _propTypes["default"].func.isRequired,
  labelPosition: _propTypes["default"].string,
  selectedDay: _propTypes["default"].string
};
Navbar.propTypes = {
  onPreviousClick: _propTypes["default"].func,
  onNextClick: _propTypes["default"].func,
  className: _propTypes["default"].string
};

var _default = (0, _reactClickOutside["default"])(Calendar);

exports["default"] = _default;