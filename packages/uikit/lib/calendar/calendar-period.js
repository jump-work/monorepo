"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CalendarPeriod = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactDayPicker = _interopRequireWildcard(require("react-day-picker"));

var _dateFns = require("date-fns");

var _ru = _interopRequireDefault(require("date-fns/locale/ru"));

var _icons = require("../icons");

var _button = require("../button");

var _input = require("../input");

var _helpers = require("../helpers");

var _russificationCalendar = require("./russification-calendar");

require("react-day-picker/lib/style.css");

require("./custom-style--calendar.scss");

var _calendarPeriodModule = _interopRequireDefault(require("./calendar-period.module.scss"));

var _excluded = ["id", "name", "label", "small", "placeholder", "labelPosition", "error", "value", "onChange", "disabled", "onApply"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Navigations = function Navigations(_ref) {
  var onPreviousClick = _ref.onPreviousClick,
      onNextClick = _ref.onNextClick,
      className = _ref.className;
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: className
  }, /*#__PURE__*/_react["default"].createElement(_button.Button, {
    styling: 'hollow',
    icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconArrowLeft, null),
    onClick: function onClick() {
      return onPreviousClick();
    },
    className: _calendarPeriodModule["default"].prevBtn
  }), /*#__PURE__*/_react["default"].createElement(_button.Button, {
    styling: 'hollow',
    icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconArrowRight, null),
    onClick: function onClick() {
      return onNextClick();
    },
    className: _calendarPeriodModule["default"].nextBtn
  }));
};

var CalendarPeriod = function CalendarPeriod(_ref2) {
  var _classes2;

  var id = _ref2.id,
      name = _ref2.name,
      label = _ref2.label,
      small = _ref2.small,
      placeholder = _ref2.placeholder,
      labelPosition = _ref2.labelPosition,
      error = _ref2.error,
      value = _ref2.value,
      onChange = _ref2.onChange,
      disabled = _ref2.disabled,
      onApply = _ref2.onApply,
      rest = _objectWithoutProperties(_ref2, _excluded);

  var _useState = (0, _react.useState)(''),
      _useState2 = _slicedToArray(_useState, 2),
      inputValueRange = _useState2[0],
      setInputValueRange = _useState2[1];

  var _useState3 = (0, _react.useState)(''),
      _useState4 = _slicedToArray(_useState3, 2),
      inputValueFrom = _useState4[0],
      setInputValueFrom = _useState4[1];

  var _useState5 = (0, _react.useState)(''),
      _useState6 = _slicedToArray(_useState5, 2),
      inputValueTo = _useState6[0],
      setInputValueTo = _useState6[1];

  var _useState7 = (0, _react.useState)(false),
      _useState8 = _slicedToArray(_useState7, 2),
      isOpen = _useState8[0],
      setIsOpen = _useState8[1];

  var modifiers = {
    start: value.from,
    end: value.to,
    isLastDayOfMonth: _dateFns.isLastDayOfMonth
  };
  var calendarRef = (0, _react.useRef)();
  var key = id || (0, _helpers.idFromString)(name || label || placeholder);
  (0, _react.useEffect)(function () {
    prepareInputValue(value);

    var closeHandler = function closeHandler(e) {
      if (calendarRef.current !== e.target && calendarRef.current && !calendarRef.current.contains(e.target) && isOpen) {
        setIsOpen(false);
      }
    };

    if (isOpen) {
      document.addEventListener('mousedown', closeHandler);
      document.addEventListener('touchstart', closeHandler);
    } else {
      document.removeEventListener('mousedown', closeHandler);
      document.removeEventListener('touchstart', closeHandler);
    }

    return function () {
      document.removeEventListener('mousedown', closeHandler);
      document.removeEventListener('touchstart', closeHandler);
    };
  }, [isOpen, value]);
  var clsWrapper = (0, _classnames["default"])(_calendarPeriodModule["default"].wrapper, _defineProperty({}, _calendarPeriodModule["default"].grid, labelPosition === 'left'));
  var positionBase = (0, _classnames["default"])(_calendarPeriodModule["default"].base, (_classes2 = {}, _defineProperty(_classes2, _calendarPeriodModule["default"].normal, !label), _defineProperty(_classes2, _calendarPeriodModule["default"].left, labelPosition === 'left'), _classes2));

  var handleDayClick = function handleDayClick(day) {
    var range = _reactDayPicker.DateUtils.addDayToRange(day, value);

    onChange(range);
    prepareInputValue(range);
  };

  var handleReset = function handleReset() {
    onChange({
      from: undefined,
      to: undefined
    });
    setInputValueFrom('');
    setInputValueTo('');
    setInputValueRange('');
  };

  var handleApply = function handleApply() {
    setIsOpen(false);
    onApply(value);
  };

  var handleFrom = function handleFrom(date) {
    setInputValueFrom(date);

    if ((0, _helpers.formatDate)(date)) {
      onChange({
        from: (0, _helpers.formatDate)(date),
        to: value.to || undefined
      });
    } else {
      onChange({
        from: undefined,
        to: value.to || undefined
      });
    }
  };

  var handleTo = function handleTo(date) {
    setInputValueTo(date);

    if ((0, _helpers.formatDate)(date)) {
      onChange({
        from: value.from || undefined,
        to: (0, _helpers.formatDate)(date) || undefined
      });
    } else {
      onChange({
        from: value.from || undefined,
        to: undefined
      });
    }
  };

  var prepareInputValue = function prepareInputValue(_ref3) {
    var from = _ref3.from,
        to = _ref3.to;
    var startDate = from instanceof Date ? (0, _dateFns.format)(from, 'dd MMM yyyy', {
      locale: _ru["default"]
    }) : '';
    var expireDate = to instanceof Date ? ' – ' + (0, _dateFns.format)(to, 'dd MMM yyyy', {
      locale: _ru["default"]
    }) : '';
    setInputValueRange(startDate + expireDate);
  };

  var renderFooterOverlay = function renderFooterOverlay() {
    var cls = (0, _classnames["default"])(_calendarPeriodModule["default"].footer, _defineProperty({}, _calendarPeriodModule["default"].small, small));
    return /*#__PURE__*/_react["default"].createElement("div", {
      className: cls
    }, /*#__PURE__*/_react["default"].createElement(_input.Input, {
      id: "from",
      value: value.from instanceof Date ? value.from.toLocaleDateString('ru-RU') : inputValueFrom,
      onChange: handleFrom,
      placeholder: "01.01.1970",
      mask: "99.99.9999",
      className: _calendarPeriodModule["default"].from,
      isClearable: false
    }), /*#__PURE__*/_react["default"].createElement(_input.Input, {
      id: "to",
      value: value.to instanceof Date ? value.to.toLocaleDateString('ru-RU') : inputValueTo,
      onChange: handleTo,
      placeholder: "01.01.1970",
      mask: "99.99.9999",
      className: _calendarPeriodModule["default"].to,
      isClearable: false
    }), /*#__PURE__*/_react["default"].createElement(_button.Button, {
      styling: "hollow-border",
      onClick: handleReset,
      className: _calendarPeriodModule["default"].close
    }, "\u0421\u0431\u0440\u043E\u0441\u0438\u0442\u044C"), /*#__PURE__*/_react["default"].createElement(_button.Button, {
      onClick: handleApply,
      className: _calendarPeriodModule["default"].apply
    }, "\u041F\u0440\u0438\u043C\u0435\u043D\u0438\u0442\u044C"));
  };

  return /*#__PURE__*/_react["default"].createElement("div", {
    className: clsWrapper
  }, label.length > 1 ? /*#__PURE__*/_react["default"].createElement("label", {
    htmlFor: key,
    className: _calendarPeriodModule["default"].label
  }, label) : '', /*#__PURE__*/_react["default"].createElement("div", {
    className: _calendarPeriodModule["default"].input,
    onClick: function onClick() {
      return setIsOpen(true);
    }
  }, /*#__PURE__*/_react["default"].createElement(_input.Input, {
    id: key,
    value: inputValueRange,
    placeholder: placeholder,
    onFocus: function onFocus() {
      return setIsOpen(true);
    },
    error: error,
    isClearable: false,
    readOnly: true,
    disabled: disabled
  }), /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconCalendar, {
    className: _calendarPeriodModule["default"].icon
  })), isOpen && !disabled && /*#__PURE__*/_react["default"].createElement("div", {
    className: positionBase,
    ref: calendarRef
  }, /*#__PURE__*/_react["default"].createElement(_reactDayPicker["default"], _extends({
    className: "Selectable",
    numberOfMonths: small ? 1 : 2,
    month: value.from,
    selectedDays: [value.from, {
      from: value.from,
      to: value.to
    }],
    onDayClick: handleDayClick,
    modifiers: modifiers,
    months: _russificationCalendar.MONTHS,
    weekdaysLong: _russificationCalendar.WEEKDAYS_LONG,
    weekdaysShort: _russificationCalendar.WEEKDAYS_SHORT,
    firstDayOfWeek: 1,
    navbarElement: /*#__PURE__*/_react["default"].createElement(Navigations, null),
    fixedWeeks: true,
    showOutsideDays: false
  }, rest)), renderFooterOverlay(value)));
};

exports.CalendarPeriod = CalendarPeriod;
CalendarPeriod.propTypes = {
  label: _propTypes["default"].string,
  id: _propTypes["default"].string,
  name: _propTypes["default"].string,
  small: _propTypes["default"].bool,
  placeholder: _propTypes["default"].string,
  labelPosition: _propTypes["default"].oneOf(['top', 'left']),
  onChange: _propTypes["default"].func.isRequired,
  value: _propTypes["default"].exact({
    from: _propTypes["default"].instanceOf(Date),
    to: _propTypes["default"].instanceOf(Date)
  }),
  error: _propTypes["default"].any,
  disabled: _propTypes["default"].bool,
  onApply: _propTypes["default"].func
};
CalendarPeriod.defaultProps = {
  label: '',
  id: '',
  placeholder: '',
  small: false,
  labelPosition: 'top',
  onChange: function onChange() {},
  value: {
    from: undefined,
    to: undefined
  }
};
Navigations.propTypes = {
  onPreviousClick: _propTypes["default"].func,
  onNextClick: _propTypes["default"].func,
  className: _propTypes["default"].string
};