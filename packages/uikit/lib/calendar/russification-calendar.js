"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.WEEKDAYS_LONG = exports.MONTHS = exports.WEEKDAYS_SHORT = void 0;
var WEEKDAYS_SHORT = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];
exports.WEEKDAYS_SHORT = WEEKDAYS_SHORT;
var MONTHS = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
exports.MONTHS = MONTHS;
var WEEKDAYS_LONG = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
exports.WEEKDAYS_LONG = WEEKDAYS_LONG;