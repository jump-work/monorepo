"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.NewCalendar = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactCalendar = require("react-calendar");

var _icons = require("../icons");

var _input = require("../input");

var _helpers = require("../helpers");

require("./custom-style-new-calendar.scss");

var _newCalendarModule = _interopRequireDefault(require("./new-calendar.module.scss"));

var _excluded = ["id", "name", "label", "placeholder", "onChange", "labelPosition", "value", "error", "disabled"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var NewCalendar = function NewCalendar(_ref) {
  var _classes2;

  var id = _ref.id,
      name = _ref.name,
      label = _ref.label,
      placeholder = _ref.placeholder,
      onChange = _ref.onChange,
      labelPosition = _ref.labelPosition,
      value = _ref.value,
      error = _ref.error,
      disabled = _ref.disabled,
      rest = _objectWithoutProperties(_ref, _excluded);

  var _useState = (0, _react.useState)(''),
      _useState2 = _slicedToArray(_useState, 2),
      inputValue = _useState2[0],
      setInputValue = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      isOpen = _useState4[0],
      setIsOpen = _useState4[1];

  var calendarRef = (0, _react.useRef)();
  var key = id || (0, _helpers.idFromString)(name || label || placeholder);
  var clsWrapper = (0, _classnames["default"])(_newCalendarModule["default"].wrapper, _defineProperty({}, _newCalendarModule["default"].grid, labelPosition === 'left'));
  var positionBase = (0, _classnames["default"])(_newCalendarModule["default"].overlay, (_classes2 = {}, _defineProperty(_classes2, _newCalendarModule["default"].normal, !label), _defineProperty(_classes2, _newCalendarModule["default"].left, labelPosition === 'left'), _classes2));
  (0, _react.useEffect)(function () {
    var closeHandler = function closeHandler(e) {
      if (calendarRef.current !== e.target && calendarRef.current && !calendarRef.current.contains(e.target) && isOpen) {
        setIsOpen(false);
      }
    };

    if (isOpen) {
      document.addEventListener('mousedown', closeHandler);
      document.addEventListener('touchstart', closeHandler);
    } else {
      document.removeEventListener('mousedown', closeHandler);
      document.removeEventListener('touchstart', closeHandler);
    }

    return function () {
      document.removeEventListener('mousedown', closeHandler);
      document.removeEventListener('touchstart', closeHandler);
    };
  }, [isOpen]);

  var onSelectDate = function onSelectDate(date) {
    onChange(date);
    setIsOpen(false);
  };

  var onChangeValue = function onChangeValue(value) {
    setInputValue(value);
    onChange((0, _helpers.formatDate)(value) || '');
  };

  var handleSetIsOpenCalendar = function handleSetIsOpenCalendar() {
    if (!disabled) setIsOpen(true);
  };

  return /*#__PURE__*/_react["default"].createElement("div", _extends({
    className: clsWrapper
  }, rest), label.length > 1 ? /*#__PURE__*/_react["default"].createElement("label", {
    htmlFor: key,
    className: _newCalendarModule["default"].label
  }, label) : '', /*#__PURE__*/_react["default"].createElement("div", {
    className: _newCalendarModule["default"]['input-wrapper'],
    onClick: handleSetIsOpenCalendar
  }, /*#__PURE__*/_react["default"].createElement(_input.Input, {
    id: key,
    value: value instanceof Date ? value.toLocaleDateString('ru-RU') : inputValue,
    placeholder: placeholder,
    onChange: onChangeValue,
    onFocus: function onFocus() {
      return setIsOpen(true);
    },
    error: error,
    isClearable: false,
    mask: "99.99.9999",
    disabled: disabled
  }), /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconCalendar, {
    className: _newCalendarModule["default"].icon
  })), isOpen && /*#__PURE__*/_react["default"].createElement("div", {
    className: positionBase,
    ref: calendarRef
  }, /*#__PURE__*/_react["default"].createElement(_reactCalendar.Calendar, {
    className: _newCalendarModule["default"].base,
    nextLabel: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconArrowRight, null),
    prevLabel: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconArrowLeft, null),
    onClickDay: onSelectDate,
    value: value instanceof Date ? value : ''
  })));
};

exports.NewCalendar = NewCalendar;
NewCalendar.defaultProps = {
  label: '',
  value: '',
  placeholder: '',
  onChange: function onChange() {},
  id: null,
  labelPosition: null,
  error: null
};
NewCalendar.propTypes = {
  label: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
  placeholder: _propTypes["default"].string,
  onChange: _propTypes["default"].func,
  id: _propTypes["default"].oneOfType([_propTypes["default"].number, _propTypes["default"].string]),
  name: _propTypes["default"].string,
  labelPosition: _propTypes["default"].oneOf(['left']),
  value: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].instanceOf(Date)]),
  error: _propTypes["default"].any,
  disabled: _propTypes["default"].bool
};