"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Input", {
  enumerable: true,
  get: function get() {
    return _input.Input;
  }
});
Object.defineProperty(exports, "Password", {
  enumerable: true,
  get: function get() {
    return _input.Password;
  }
});

var _input = require("./input");