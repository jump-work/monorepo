"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Input = exports.Password = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactInputMask = _interopRequireDefault(require("react-input-mask"));

var _helpers = require("../helpers");

var _icons = require("../icons");

var _inputModule = _interopRequireDefault(require("./input.module.scss"));

var _excluded = ["id", "name", "label", "error", "className", "onChange"],
    _excluded2 = ["id", "name", "label", "value", "error", "className", "onChange", "isClearable", "disabled", "mask", "readOnly"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var idType = _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]);

var Password = function Password(_ref) {
  var id = _ref.id,
      name = _ref.name,
      label = _ref.label,
      error = _ref.error,
      className = _ref.className,
      onChange = _ref.onChange,
      rest = _objectWithoutProperties(_ref, _excluded);

  var key = id || (0, _helpers.idFromString)(name || label || rest.placeholder);
  var inputRef = (0, _react.useRef)(null);

  var _useState = (0, _react.useState)('password'),
      _useState2 = _slicedToArray(_useState, 2),
      controlType = _useState2[0],
      changeControlType = _useState2[1];

  var isPassword = controlType === 'password';

  var onInvert = function onInvert() {
    changeControlType(isPassword ? 'text' : 'password');
    inputRef.current.focus();
  };

  var handlerOnChange = function handlerOnChange(e) {
    return onChange(e.target.value || '');
  };

  var clsName = (0, _classnames["default"])(_inputModule["default"].base, className, _defineProperty({}, _inputModule["default"].error, error));
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: clsName
  }, label && /*#__PURE__*/_react["default"].createElement("label", {
    htmlFor: key,
    className: _inputModule["default"].label
  }, label), /*#__PURE__*/_react["default"].createElement("div", {
    className: _inputModule["default"].inputWrapper
  }, /*#__PURE__*/_react["default"].createElement("input", _extends({
    id: key,
    name: name,
    ref: inputRef,
    className: _inputModule["default"].input,
    autoComplete: "on",
    type: controlType,
    onChange: handlerOnChange
  }, rest)), !rest.disabled && /*#__PURE__*/_react["default"].createElement("div", {
    className: _inputModule["default"].control,
    onClick: onInvert
  }, isPassword ? /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconEyeClose, null) : /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconEyeOpen, null))), error && /*#__PURE__*/_react["default"].createElement("span", {
    className: _inputModule["default"]['error-message']
  }, error));
};

exports.Password = Password;
Password.defaultProps = {
  label: '',
  className: '',
  onChange: function onChange() {}
};
Password.propTypes = {
  id: idType,
  name: _propTypes["default"].string,
  label: _propTypes["default"].string,
  className: _propTypes["default"].string,
  onChange: _propTypes["default"].func,
  error: _propTypes["default"].any
};

var Input = function Input(_ref2) {
  var id = _ref2.id,
      name = _ref2.name,
      label = _ref2.label,
      value = _ref2.value,
      error = _ref2.error,
      className = _ref2.className,
      onChange = _ref2.onChange,
      isClearable = _ref2.isClearable,
      disabled = _ref2.disabled,
      mask = _ref2.mask,
      readOnly = _ref2.readOnly,
      rest = _objectWithoutProperties(_ref2, _excluded2);

  var ref = (0, _react.useRef)(null);
  var key = id || (0, _helpers.idFromString)(name || label || rest.placeholder);
  var clsName = (0, _classnames["default"])(_inputModule["default"].base, className, _defineProperty({}, _inputModule["default"].error, error));

  var onClear = function onClear() {
    onChange('');
    ref.current.focus();
  };

  var handlerOnChange = function handlerOnChange(e) {
    return onChange(e.target.value || '');
  };

  return /*#__PURE__*/_react["default"].createElement("div", {
    className: clsName
  }, label && /*#__PURE__*/_react["default"].createElement("label", {
    htmlFor: key,
    className: _inputModule["default"].label
  }, label), /*#__PURE__*/_react["default"].createElement("div", {
    className: _inputModule["default"].inputWrapper
  }, /*#__PURE__*/_react["default"].createElement(_reactInputMask["default"], _extends({
    id: key,
    name: name,
    mask: mask,
    className: _inputModule["default"].input,
    autoComplete: "off",
    onChange: handlerOnChange,
    value: value,
    disabled: disabled,
    readOnly: readOnly
  }, rest), function (props) {
    return /*#__PURE__*/_react["default"].createElement("input", _extends({}, props, {
      ref: ref,
      disabled: disabled,
      readOnly: readOnly,
      onChange: handlerOnChange,
      value: value
    }));
  }), value && !disabled && isClearable && /*#__PURE__*/_react["default"].createElement("div", {
    className: _inputModule["default"].control,
    onClick: onClear
  }, /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconClose, null))), error && /*#__PURE__*/_react["default"].createElement("span", {
    className: _inputModule["default"]['error-message']
  }, error));
};

exports.Input = Input;
Input.defaultProps = {
  label: '',
  value: '',
  className: '',
  type: 'text',
  onChange: function onChange() {},
  isClearable: true,
  readOnly: false
};
Input.propTypes = {
  id: idType,
  name: _propTypes["default"].string,
  label: _propTypes["default"].string,
  value: _propTypes["default"].any,
  className: _propTypes["default"].string,
  onChange: _propTypes["default"].func,
  error: _propTypes["default"].any,
  type: _propTypes["default"].string,
  isClearable: _propTypes["default"].bool,
  disabled: _propTypes["default"].bool,
  mask: _propTypes["default"].string,
  readOnly: _propTypes["default"].bool
};