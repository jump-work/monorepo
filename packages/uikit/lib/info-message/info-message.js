"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.InfoMessage = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _newParagraph = require("../new-paragraph");

var _infoMessageModule = _interopRequireDefault(require("./info-message.module.scss"));

var _excluded = ["children", "icon", "className", "type", "fluid"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var classMap = {
  "default": _infoMessageModule["default"]["default"],
  info: _infoMessageModule["default"].info,
  error: _infoMessageModule["default"].error,
  success: _infoMessageModule["default"].success
};

var InfoMessage = function InfoMessage(_ref) {
  var _classNames;

  var children = _ref.children,
      icon = _ref.icon,
      className = _ref.className,
      type = _ref.type,
      fluid = _ref.fluid,
      rest = _objectWithoutProperties(_ref, _excluded);

  var cls = (0, _classnames["default"])(_infoMessageModule["default"].base, classMap[type], (_classNames = {}, _defineProperty(_classNames, _infoMessageModule["default"].icon, icon), _defineProperty(_classNames, _infoMessageModule["default"].fluid, fluid), _defineProperty(_classNames, _infoMessageModule["default"].children, children), _classNames), className);
  return /*#__PURE__*/_react["default"].createElement("div", _extends({
    className: cls
  }, rest), icon, children && /*#__PURE__*/_react["default"].createElement(_newParagraph.ParagraphV2.LH24, null, children));
};

exports.InfoMessage = InfoMessage;
InfoMessage.defaultProps = {
  type: 'info',
  fluid: false
};
InfoMessage.propTypes = {
  children: _propTypes["default"].any,
  icon: _propTypes["default"].node,
  className: _propTypes["default"].string,
  type: _propTypes["default"].oneOf(['default', 'info', 'error', 'success']),
  fluid: _propTypes["default"].bool
};