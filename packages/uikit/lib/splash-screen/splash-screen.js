"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _spinner = require("../spinner");

var _splashScreenModule = _interopRequireDefault(require("./splash-screen.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = function _default() {
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: _splashScreenModule["default"].container
  }, /*#__PURE__*/_react["default"].createElement(_spinner.Ellipsis, null));
};

exports["default"] = _default;