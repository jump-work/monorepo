"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "SplashScreen", {
  enumerable: true,
  get: function get() {
    return _splashScreen["default"];
  }
});

var _splashScreen = _interopRequireDefault(require("./splash-screen"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }