"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "CalendarEvents", {
  enumerable: true,
  get: function get() {
    return _calendarEvents.CalendarEvents;
  }
});

var _calendarEvents = require("./calendar-events");