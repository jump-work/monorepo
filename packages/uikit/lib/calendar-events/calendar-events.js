"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CalendarEvents = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactBigCalendar = require("react-big-calendar");

var _dateFns = require("date-fns");

var _ru = _interopRequireDefault(require("date-fns/locale/ru"));

var _icons = require("../icons");

var _button = require("../button");

var _helpers = require("../helpers");

var _hooks = require("../hooks");

require("./custom-style-calendar-events.scss");

var _calendarEventsModule = _interopRequireDefault(require("./calendar-events.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var Toolbar = function Toolbar(_ref) {
  var _ref$props = _ref.props,
      date = _ref$props.date,
      label = _ref$props.label,
      onNavigate = _ref$props.onNavigate,
      count = _ref.count,
      declensionNameCount = _ref.declensionNameCount;

  var goToBack = function goToBack() {
    var mDate = date;
    var newDate = new Date(mDate.getFullYear(), mDate.getMonth() - 1, 1);
    onNavigate('prev', newDate);
  };

  var goToNext = function goToNext() {
    var mDate = date;
    var newDate = new Date(mDate.getFullYear(), mDate.getMonth() + 1, 1);
    onNavigate('next', newDate);
  };

  return /*#__PURE__*/_react["default"].createElement("div", {
    className: _calendarEventsModule["default"].toolbar
  }, /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconArrowLeft, {
    onClick: function onClick() {
      return goToBack();
    },
    className: _calendarEventsModule["default"].control
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: _calendarEventsModule["default"]['toolbar-label']
  }, label), /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconArrowRight, {
    onClick: function onClick() {
      return goToNext();
    },
    className: _calendarEventsModule["default"].control
  }), count > 0 && /*#__PURE__*/_react["default"].createElement("div", {
    className: _calendarEventsModule["default"].count
  }, count, " ", (0, _helpers.declensionWords)(count, declensionNameCount)));
};

var eventStyleGetter = function eventStyleGetter(_ref2) {
  var status = _ref2.status;
  var closeBackgroundColor = status.name === 'close' ? '#E7EAEE' : '#FFDD99';
  var closeBorderColor = status.name === 'close' ? '#E7EAEE' : '#FFCC66';
  var style = {
    backgroundColor: closeBackgroundColor,
    borderBottom: '0.0625rem solid ' + closeBorderColor
  };
  return {
    style: style
  };
};

var CalendarEvents = function CalendarEvents(_ref3) {
  var events = _ref3.events,
      renderPopup = _ref3.renderPopup,
      declensionNameCount = _ref3.declensionNameCount,
      onClickByEvent = _ref3.onClickByEvent,
      createEventControl = _ref3.createEventControl;

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      selectedEvent = _useState2[0],
      selectEvent = _useState2[1];

  var isMedia = (0, _hooks.useIfMediaScreen)();
  var ref = (0, _react.useRef)(null);
  var locales = {
    'ru-RU': _ru["default"]
  };
  var localizer = (0, _reactBigCalendar.dateFnsLocalizer)({
    format: _dateFns.format,
    parse: _dateFns.parse,
    startOfWeek: _dateFns.startOfWeek,
    getDay: _dateFns.getDay,
    locales: locales
  });
  var formatWeekDay = {
    weekdayFormat: function weekdayFormat(date, culture, localizer) {
      return localizer.format(date, isMedia ? 'EEEEEE' : 'EEEE', culture);
    }
  };

  var onSelectEvent = function onSelectEvent(e) {
    selectEvent(e);
    onClickByEvent(e);
  };

  var onClickOutside = function onClickOutside() {
    selectEvent(null);
  };

  (0, _hooks.useOnClickOutside)(ref, onClickOutside);
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: _calendarEventsModule["default"].base,
    ref: ref
  }, createEventControl && /*#__PURE__*/_react["default"].createElement("div", {
    className: _calendarEventsModule["default"]['event-control']
  }, createEventControl), /*#__PURE__*/_react["default"].createElement(_reactBigCalendar.Calendar, {
    localizer: localizer,
    culture: "ru-RU",
    formats: formatWeekDay,
    eventPropGetter: eventStyleGetter,
    events: events,
    onNavigate: onClickOutside,
    onSelectEvent: onSelectEvent,
    onSelectSlot: onClickOutside,
    selectable: true,
    components: {
      toolbar: function toolbar(props) {
        return /*#__PURE__*/_react["default"].createElement(Toolbar, {
          props: props
          /* eslint-disable-next-line react/prop-types */
          ,
          count: events.length,
          declensionNameCount: declensionNameCount
        });
      }
    }
  }), selectedEvent && /*#__PURE__*/_react["default"].createElement("div", {
    className: _calendarEventsModule["default"].popup
  }, isMedia && /*#__PURE__*/_react["default"].createElement("div", {
    className: _calendarEventsModule["default"].shadow
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: _calendarEventsModule["default"]['popup-content']
  }, isMedia && /*#__PURE__*/_react["default"].createElement(_button.Button, {
    icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconClose, null),
    styling: "hollow",
    className: _calendarEventsModule["default"]['popup-close'],
    onClick: function onClick() {
      return selectEvent(null);
    }
  }), renderPopup(selectedEvent))));
};

exports.CalendarEvents = CalendarEvents;
CalendarEvents.defaultProps = {
  onClickByEvent: function onClickByEvent() {}
};
CalendarEvents.propTypes = {
  events: _propTypes["default"].array.isRequired,
  renderPopup: _propTypes["default"].func,
  createEventControl: _propTypes["default"].node,
  onClickByEvent: _propTypes["default"].func,
  declensionNameCount: _propTypes["default"].array
};
Toolbar.propTypes = {
  props: _propTypes["default"].object,
  count: _propTypes["default"].number,
  declensionNameCount: _propTypes["default"].array
};