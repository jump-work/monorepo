"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MobileHeader = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _icons = require("../icons");

var _header = require("../header");

var _description = require("../description");

var _button = require("../button");

var _mobileHeaderButton = require("./mobile-header-button");

var _mobileHeaderModule = _interopRequireDefault(require("./mobile-header.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var MobileHeader = function MobileHeader(_ref) {
  var header = _ref.header,
      className = _ref.className,
      description = _ref.description,
      onGoBack = _ref.onGoBack,
      children = _ref.children;
  var cls = (0, _classnames["default"])(_mobileHeaderModule["default"].base, className, _defineProperty({}, _mobileHeaderModule["default"].description, description));
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: cls
  }, onGoBack && /*#__PURE__*/_react["default"].createElement("div", {
    className: _mobileHeaderModule["default"].backBtn
  }, /*#__PURE__*/_react["default"].createElement(_button.Button, {
    styling: "hollow",
    icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconArrowLeftLine, null),
    onClick: onGoBack
  })), /*#__PURE__*/_react["default"].createElement("div", {
    className: _mobileHeaderModule["default"].descArea
  }, /*#__PURE__*/_react["default"].createElement(_header.Header.H1, {
    className: _mobileHeaderModule["default"].title
  }, header), description && /*#__PURE__*/_react["default"].createElement(_description.Description.LH24, {
    className: _mobileHeaderModule["default"]['description-text']
  }, description)), /*#__PURE__*/_react["default"].createElement("div", {
    className: _mobileHeaderModule["default"].toolbox
  }, children));
};

exports.MobileHeader = MobileHeader;
MobileHeader.Button = _mobileHeaderButton.MobileHeaderButton;
MobileHeader.propTypes = {
  header: _propTypes["default"].string.isRequired,
  description: _propTypes["default"].string,
  onGoBack: _propTypes["default"].func,
  children: _propTypes["default"].node,
  className: _propTypes["default"].string
};