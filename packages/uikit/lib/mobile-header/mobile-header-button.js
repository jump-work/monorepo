"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MobileHeaderButton = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _button = require("../button");

var _mobileHeaderButtonModule = _interopRequireDefault(require("./mobile-header-button.module.scss"));

var _excluded = ["icon", "onClick"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var MobileHeaderButton = function MobileHeaderButton(_ref) {
  var icon = _ref.icon,
      onClick = _ref.onClick,
      rest = _objectWithoutProperties(_ref, _excluded);

  return /*#__PURE__*/_react["default"].createElement(_button.Button, _extends({
    styling: "hollow",
    onClick: onClick,
    icon: icon,
    className: _mobileHeaderButtonModule["default"].base
  }, rest));
};

exports.MobileHeaderButton = MobileHeaderButton;
MobileHeaderButton.defaultProps = {
  onClick: function onClick() {}
};
MobileHeaderButton.propTypes = {
  icon: _propTypes["default"].any,
  onClick: _propTypes["default"].func
};