"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "MobileHeader", {
  enumerable: true,
  get: function get() {
    return _mobileHeader.MobileHeader;
  }
});

var _mobileHeader = require("./mobile-header");