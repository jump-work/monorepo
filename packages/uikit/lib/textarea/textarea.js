"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Textarea = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _description = require("../description");

var _helpers = require("../helpers");

var _textareaModule = _interopRequireDefault(require("./textarea.module.scss"));

var _excluded = ["id", "name", "label", "placeholder", "error", "className", "onChange"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Textarea = function Textarea(_ref) {
  var id = _ref.id,
      name = _ref.name,
      label = _ref.label,
      placeholder = _ref.placeholder,
      error = _ref.error,
      className = _ref.className,
      _onChange = _ref.onChange,
      rest = _objectWithoutProperties(_ref, _excluded);

  var key = id || (0, _helpers.idFromString)(name || label || placeholder);
  var clsName = (0, _classnames["default"])(_textareaModule["default"].base, _defineProperty({}, _textareaModule["default"].error, error));
  var clsNameTextArea = (0, _classnames["default"])(_textareaModule["default"].textarea, className);
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: clsName
  }, label === '' ? null : /*#__PURE__*/_react["default"].createElement("label", {
    htmlFor: key,
    className: _textareaModule["default"].label
  }, label), /*#__PURE__*/_react["default"].createElement("textarea", _extends({
    id: key,
    name: name,
    placeholder: placeholder,
    className: clsNameTextArea,
    onChange: function onChange(e) {
      return _onChange(e.target.value);
    }
  }, rest)), error && /*#__PURE__*/_react["default"].createElement(_description.Description.LH16, {
    className: _textareaModule["default"]['error-message']
  }, error));
};

exports.Textarea = Textarea;
Textarea.defaultProps = {
  id: null,
  label: '',
  onChange: function onChange() {}
};
Textarea.propTypes = {
  id: _propTypes["default"].string,
  name: _propTypes["default"].string,
  label: _propTypes["default"].string,
  placeholder: _propTypes["default"].string,
  className: _propTypes["default"].string,
  error: _propTypes["default"].any,
  onChange: _propTypes["default"].func
};