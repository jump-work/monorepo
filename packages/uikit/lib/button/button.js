"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _hooks = require("../hooks");

var _paragraph = require("../paragraph");

var _icons = require("../icons");

var _buttonModule = _interopRequireDefault(require("./button.module.scss"));

var _excluded = ["element", "icon", "children", "className", "wide", "styling", "loading"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Button = function Button(_ref) {
  var _classNames;

  var Btn = _ref.element,
      icon = _ref.icon,
      children = _ref.children,
      className = _ref.className,
      wide = _ref.wide,
      styling = _ref.styling,
      loading = _ref.loading,
      attrs = _objectWithoutProperties(_ref, _excluded);

  var _useTabListener = (0, _hooks.useTabListener)(),
      focusedByTab = _useTabListener.focusedByTab;

  var cl = (0, _classnames["default"])(className, _buttonModule["default"].base, (_classNames = {}, _defineProperty(_classNames, _buttonModule["default"].primary, styling === 'primary'), _defineProperty(_classNames, _buttonModule["default"].hollow, styling === 'hollow'), _defineProperty(_classNames, _buttonModule["default"]['hollow-border'], styling === 'hollow-border'), _defineProperty(_classNames, _buttonModule["default"].icon, icon), _defineProperty(_classNames, _buttonModule["default"].text, icon && children), _defineProperty(_classNames, _buttonModule["default"].disabled, attrs.disabled), _defineProperty(_classNames, _buttonModule["default"].wide, wide), _defineProperty(_classNames, _buttonModule["default"].loading, loading), _defineProperty(_classNames, _buttonModule["default"].focus, focusedByTab), _classNames));

  var renderChildren = function renderChildren() {
    return /*#__PURE__*/_react["default"].createElement(_react.Fragment, null, icon, loading ? /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconLoader, null) : /*#__PURE__*/_react["default"].createElement(_paragraph.Paragraph, null, " ", children, " "));
  };

  var renderChildrenWithIcon = function renderChildrenWithIcon() {
    return /*#__PURE__*/_react["default"].createElement(_react.Fragment, null, /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconLoader, null), /*#__PURE__*/_react["default"].createElement(_paragraph.Paragraph, null, children));
  };

  return /*#__PURE__*/_react["default"].createElement(Btn, _extends({
    className: cl
  }, attrs), loading && icon ? renderChildrenWithIcon() : renderChildren());
};

Button.defaultProps = {
  type: 'button',
  styling: 'primary',
  element: 'button'
};
Button.propTypes = {
  className: _propTypes["default"].string,
  children: _propTypes["default"].any,
  element: _propTypes["default"].string,
  type: _propTypes["default"].string,
  icon: _propTypes["default"].node,
  styling: _propTypes["default"].oneOf(['primary', 'hollow', 'hollow-border']),
  loading: _propTypes["default"].bool,
  wide: _propTypes["default"].bool
};
var _default = Button;
exports["default"] = _default;