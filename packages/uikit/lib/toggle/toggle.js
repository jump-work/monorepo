"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Toggle = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _icons = require("../icons");

var _helpers = require("../helpers");

var _toggleModule = _interopRequireDefault(require("./toggle.module.scss"));

var _excluded = ["id", "name", "label", "onChange", "positionToggle", "isLoading", "disabled"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Toggle = function Toggle(_ref) {
  var _classes;

  var id = _ref.id,
      name = _ref.name,
      label = _ref.label,
      onChange = _ref.onChange,
      positionToggle = _ref.positionToggle,
      isLoading = _ref.isLoading,
      disabled = _ref.disabled,
      rest = _objectWithoutProperties(_ref, _excluded);

  var key = id || (0, _helpers.idFromString)(name || label || '');
  var cls = (0, _classnames["default"])(_toggleModule["default"].base, (_classes = {}, _defineProperty(_classes, _toggleModule["default"].before, positionToggle === 'before'), _defineProperty(_classes, _toggleModule["default"].after, positionToggle === 'after'), _defineProperty(_classes, _toggleModule["default"].disabled, disabled), _classes));
  var toggleCls = (0, _classnames["default"])(_toggleModule["default"].toggle, _defineProperty({}, _toggleModule["default"].loading, isLoading));

  var handlerOnChange = function handlerOnChange(e) {
    return onChange(e.target.checked);
  };

  return /*#__PURE__*/_react["default"].createElement("label", {
    htmlFor: key,
    className: cls
  }, label && /*#__PURE__*/_react["default"].createElement("div", {
    className: _toggleModule["default"].label
  }, label), /*#__PURE__*/_react["default"].createElement("input", _extends({
    id: key,
    type: "checkbox",
    name: name,
    className: _toggleModule["default"].input,
    onChange: handlerOnChange,
    disabled: disabled,
    hidden: true
  }, rest)), /*#__PURE__*/_react["default"].createElement("div", {
    className: toggleCls
  }));
};

exports.Toggle = Toggle;
Toggle.defaultProps = {
  positionToggle: 'before',
  isLoading: false,
  onChange: function onChange() {}
};
Toggle.propTypes = {
  label: _propTypes["default"].string,
  id: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
  name: _propTypes["default"].string,
  onChange: _propTypes["default"].func,
  isLoading: _propTypes["default"].bool,
  positionToggle: _propTypes["default"].oneOf(['before', 'after']),
  disabled: _propTypes["default"].bool
};