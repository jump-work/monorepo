"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _icons = require("../icons");

var _description = require("../description");

var _button = require("../button");

var _uploadModule = _interopRequireDefault(require("./upload.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function _createSuperInternal() { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } else if (call !== void 0) { throw new TypeError("Derived constructors may only return object or undefined"); } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Upload = /*#__PURE__*/function (_Component) {
  _inherits(Upload, _Component);

  var _super = _createSuper(Upload);

  function Upload(props) {
    var _this;

    _classCallCheck(this, Upload);

    _this = _super.call(this, props);

    _defineProperty(_assertThisInitialized(_this), "onChange", function () {
      var uploadFiles = _this.state.uploadFiles;
      var uploadFile = _this.fileInput.current.files;
      var files = uploadFiles;
      files.push.apply(files, _toConsumableArray(uploadFile));

      _this.setState({
        uploadFiles: files
      }, function () {
        return _this.sendFiles(_this.state.uploadFiles);
      });
    });

    _defineProperty(_assertThisInitialized(_this), "resetUpload", function (file) {
      var uploadFiles = _this.state.uploadFiles;
      var filter = uploadFiles.filter(function (item) {
        return item !== file;
      });

      _this.setState({
        uploadFiles: filter
      }, function () {
        return _this.sendFiles(_this.state.uploadFiles);
      });
    });

    _defineProperty(_assertThisInitialized(_this), "sendFiles", function (files) {
      var _this$props = _this.props,
          onChange = _this$props.onChange,
          isURI = _this$props.isURI;
      isURI ? files.map(function (file) {
        var arrResult = [];
        var reader = new FileReader();
        reader.readAsDataURL(file);

        reader.onload = function (e) {
          arrResult.push({
            file: e.target.result,
            type: file.type
          });
          onChange(arrResult);
        };

        return arrResult;
      }) : onChange(files);
    });

    _this.state = {
      uploadFiles: []
    };
    _this.fileInput = /*#__PURE__*/_react["default"].createRef();
    return _this;
  }

  _createClass(Upload, [{
    key: "render",
    value: function render() {
      var _this2 = this,
          _this$props2,
          _this$props3,
          _this$props4;

      var uploadFiles = this.state.uploadFiles;
      var error = this.props.error;
      return /*#__PURE__*/_react["default"].createElement("div", {
        className: _uploadModule["default"].wrapper
      }, uploadFiles.map(function (element) {
        var expFile = element.name.split('.').pop().toUpperCase();
        return /*#__PURE__*/_react["default"].createElement("div", {
          className: _uploadModule["default"].base,
          tabIndex: 1,
          key: element.name
        }, element.type === 'image/jpeg' ? /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconImage, {
          className: _uploadModule["default"].uploadFile
        }) : /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconDoc, {
          className: _uploadModule["default"].uploadFile
        }), /*#__PURE__*/_react["default"].createElement(_description.Description.LH16, {
          className: _uploadModule["default"].description
        }, expFile), /*#__PURE__*/_react["default"].createElement(_button.Button, {
          id: element.lastModified,
          styling: 'hollow',
          icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons16.IconClose, null),
          className: _uploadModule["default"].reset,
          onClick: function onClick() {
            return _this2.resetUpload(element);
          }
        }));
      }), (!(uploadFiles !== null && uploadFiles !== void 0 && uploadFiles.length) || ((_this$props2 = this.props) === null || _this$props2 === void 0 ? void 0 : _this$props2.multiple)) && /*#__PURE__*/_react["default"].createElement("label", {
        className: _uploadModule["default"].base,
        tabIndex: 1
      }, /*#__PURE__*/_react["default"].createElement("input", {
        type: 'file',
        ref: this.fileInput,
        onChange: function onChange(e) {
          return _this2.onChange(e);
        },
        className: _uploadModule["default"].file,
        multiple: (_this$props3 = this.props) === null || _this$props3 === void 0 ? void 0 : _this$props3.multiple,
        accept: (_this$props4 = this.props) === null || _this$props4 === void 0 ? void 0 : _this$props4.accept
      }), /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconPlus, {
        className: _uploadModule["default"].notFile
      })), error && /*#__PURE__*/_react["default"].createElement(_description.Description.LH16, {
        className: _uploadModule["default"].error
      }, error));
    }
  }]);

  return Upload;
}(_react.Component);

Upload.propTypes = {
  onChange: _propTypes["default"].func.isRequired,
  error: _propTypes["default"].string,
  multiple: _propTypes["default"].bool,
  accept: _propTypes["default"].string,
  isURI: _propTypes["default"].bool
};
var _default = Upload;
exports["default"] = _default;