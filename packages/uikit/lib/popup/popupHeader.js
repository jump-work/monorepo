"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Header = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _description = require("../description");

var _header = require("../header");

var _popupHeaderModule = _interopRequireDefault(require("./popupHeader.module.scss"));

var _excluded = ["children", "label", "description", "className"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Header = function Header(_ref) {
  var children = _ref.children,
      label = _ref.label,
      description = _ref.description,
      className = _ref.className,
      attrs = _objectWithoutProperties(_ref, _excluded);

  var style = {
    gridRow: description ? '1/2' : '1/3'
  };
  var cls = (0, _classnames["default"])(_popupHeaderModule["default"].base, _defineProperty({}, className, className));
  return /*#__PURE__*/_react["default"].createElement("div", _extends({
    className: cls
  }, attrs), /*#__PURE__*/_react["default"].createElement(_header.Header.H1, {
    style: style,
    className: _popupHeaderModule["default"]['header']
  }, label || children), description && /*#__PURE__*/_react["default"].createElement(_description.Description.LH24, {
    className: _popupHeaderModule["default"]['description']
  }, description));
};

exports.Header = Header;
Header.displayName = 'Popup.Header';
Header.propTypes = {
  children: _propTypes["default"].any,
  label: _propTypes["default"].string,
  description: _propTypes["default"].string,
  className: _propTypes["default"].string
};