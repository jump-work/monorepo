"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Footer = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _hooks = require("../hooks");

var _popupFooterModule = _interopRequireDefault(require("./popupFooter.module.scss"));

var _excluded = ["children", "className", "minorButton", "onDismissButton", "majorButton"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Footer = function Footer(_ref) {
  var children = _ref.children,
      className = _ref.className,
      minorButton = _ref.minorButton,
      onDismissButton = _ref.onDismissButton,
      majorButton = _ref.majorButton,
      attrs = _objectWithoutProperties(_ref, _excluded);

  var cls = (0, _classnames["default"])(_popupFooterModule["default"].base, _defineProperty({}, className, className));
  var isMedia = (0, _hooks.useIfMediaScreen)();
  return /*#__PURE__*/_react["default"].createElement("div", _extends({
    className: cls
  }, attrs), minorButton && /*#__PURE__*/_react["default"].createElement("div", {
    className: _popupFooterModule["default"].minorButton
  }, minorButton), majorButton && /*#__PURE__*/_react["default"].createElement("div", {
    className: _popupFooterModule["default"].majorButton
  }, isMedia && minorButton ? null : onDismissButton, majorButton), children && /*#__PURE__*/_react["default"].createElement("div", {
    className: _popupFooterModule["default"].children
  }, children));
};

exports.Footer = Footer;
Footer.displayName = 'Popup.Footer';
Footer.propTypes = {
  children: _propTypes["default"].any,
  className: _propTypes["default"].string,
  minorButton: _propTypes["default"].any,
  onDismissButton: _propTypes["default"].any,
  majorButton: _propTypes["default"].any
};