"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Popup = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _button = require("../button");

var _icons = require("../icons");

var _popupHeader = require("./popupHeader");

var _popupContent = require("./popupContent");

var _popupFooter = require("./popupFooter");

var _hooks = require("../hooks");

var _popupModule = _interopRequireDefault(require("./popup.module.scss"));

var _excluded = ["children", "onDismiss", "className"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Popup = function Popup(_ref) {
  var children = _ref.children,
      onDismiss = _ref.onDismiss,
      className = _ref.className,
      attrs = _objectWithoutProperties(_ref, _excluded);

  var contentRef = (0, _react.useRef)(null);

  var _useWindowSize = (0, _hooks.useWindowSize)(),
      windowHeight = _useWindowSize.height;

  var _useResizeObserver = (0, _hooks.useResizeObserver)(contentRef),
      height = _useResizeObserver.height;

  var cls = (0, _classnames["default"])(_popupModule["default"].window, className, _defineProperty({}, _popupModule["default"].fullHeight, height + 146 > windowHeight));

  var cloneContent = function cloneContent(child) {
    if (child.type.displayName === 'Popup.Content') {
      return /*#__PURE__*/_react["default"].cloneElement(child, {
        ref: function ref(node) {
          return contentRef.current = node;
        }
      });
    }

    return child;
  };

  return /*#__PURE__*/_react["default"].createElement("div", {
    className: _popupModule["default"].base
  }, /*#__PURE__*/_react["default"].createElement("div", {
    className: _popupModule["default"].shadow,
    onClick: onDismiss
  }), /*#__PURE__*/_react["default"].createElement("div", _extends({
    className: cls
  }, attrs), onDismiss && /*#__PURE__*/_react["default"].createElement(_button.Button, {
    styling: "hollow",
    icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconClose, null),
    className: _popupModule["default"].close,
    onClick: function onClick() {
      return onDismiss();
    }
  }), _react["default"].Children.map(children, cloneContent)));
};

exports.Popup = Popup;
Popup.Header = _popupHeader.Header;
Popup.Content = _popupContent.Content;
Popup.Footer = _popupFooter.Footer;
Popup.propTypes = {
  children: _propTypes["default"].any.isRequired,
  onDismiss: _propTypes["default"].func,
  className: _propTypes["default"].string
};