"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Description", {
  enumerable: true,
  get: function get() {
    return _description.Description;
  }
});

var _description = require("./description");