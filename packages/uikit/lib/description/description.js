"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Description = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _descriptionModule = _interopRequireDefault(require("./description.module.scss"));

var _excluded = ["element", "children", "className"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var makeDescription = function makeDescription(LH) {
  var component = function component(_ref) {
    var _classNames;

    var Tag = _ref.element,
        children = _ref.children,
        className = _ref.className,
        attr = _objectWithoutProperties(_ref, _excluded);

    var clsName = (0, _classnames["default"])(_descriptionModule["default"].base, (_classNames = {}, _defineProperty(_classNames, _descriptionModule["default"]['base-24'], LH === '24'), _defineProperty(_classNames, _descriptionModule["default"]['base-16'], LH === '16'), _classNames), className);
    return /*#__PURE__*/_react["default"].createElement(Tag, _extends({
      className: clsName
    }, attr), children);
  };

  component.displayName = 'Description.LH' + LH.toUpperCase();
  component.defaultProps = {
    element: 'p'
  };
  component.propTypes = {
    className: _propTypes["default"].string,
    children: _propTypes["default"].any,
    element: _propTypes["default"].string
  };
  return component;
};

var Description = {
  LH24: makeDescription('24'),
  LH16: makeDescription('16')
};
exports.Description = Description;