"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Checkbox = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _newParagraph = require("../new-paragraph");

var _helpers = require("../helpers");

var _hooks = require("../hooks");

var _checkboxModule = _interopRequireDefault(require("./checkbox.module.scss"));

var _excluded = ["id", "name", "label", "description", "checked", "error", "onChange", "className", "mixed"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Checkbox = function Checkbox(_ref) {
  var _classNames;

  var id = _ref.id,
      name = _ref.name,
      label = _ref.label,
      description = _ref.description,
      checked = _ref.checked,
      error = _ref.error,
      _onChange = _ref.onChange,
      className = _ref.className,
      mixed = _ref.mixed,
      rest = _objectWithoutProperties(_ref, _excluded);

  var _useTabListener = (0, _hooks.useTabListener)(),
      focusedByTab = _useTabListener.focusedByTab;

  var key = id || (0, _helpers.idFromString)(name || label);
  var clsName = (0, _classnames["default"])(_checkboxModule["default"].base, className, (_classNames = {}, _defineProperty(_classNames, _checkboxModule["default"].error, error), _defineProperty(_classNames, _checkboxModule["default"].mixed, mixed), _classNames));
  var clsNameInput = (0, _classnames["default"])(_checkboxModule["default"].inputCheckbox, _defineProperty({}, _checkboxModule["default"].focus, focusedByTab));
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: clsName
  }, /*#__PURE__*/_react["default"].createElement("input", _extends({
    id: key,
    type: "checkbox",
    className: clsNameInput,
    checked: checked,
    onChange: function onChange(e) {
      return _onChange(e.target.checked);
    }
  }, rest)), /*#__PURE__*/_react["default"].createElement("label", {
    htmlFor: key,
    className: _checkboxModule["default"].labelCheckbox
  }, /*#__PURE__*/_react["default"].createElement(_newParagraph.ParagraphV2.LH24, null, label), description && /*#__PURE__*/_react["default"].createElement(_newParagraph.ParagraphV2.LH18, null, description)));
};

exports.Checkbox = Checkbox;
Checkbox.defaultProps = {
  label: '',
  description: '',
  checked: false,
  onChange: function onChange() {},
  className: '',
  error: false,
  mixed: false
};
Checkbox.propTypes = {
  id: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
  name: _propTypes["default"].string,
  label: _propTypes["default"].oneOfType([_propTypes["default"].string, _propTypes["default"].number]),
  description: _propTypes["default"].string,
  checked: _propTypes["default"].bool,
  onChange: _propTypes["default"].func,
  className: _propTypes["default"].string,
  error: _propTypes["default"].bool,
  mixed: _propTypes["default"].bool
};