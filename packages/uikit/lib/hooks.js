"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useIfMediaScreen = useIfMediaScreen;
exports.useOnClickOutside = useOnClickOutside;
exports.useDebounce = useDebounce;
exports.useModalState = useModalState;
exports.useTabListener = useTabListener;
exports.useResizeObserver = exports.useWindowSize = void 0;

var _react = require("react");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function useIfMediaScreen() {
  function checkIfMediaScreen() {
    return window.matchMedia('(max-width: 768px)').matches;
  }

  var _useState = (0, _react.useState)(checkIfMediaScreen),
      _useState2 = _slicedToArray(_useState, 2),
      isMediaScreen = _useState2[0],
      setIsMediaScreen = _useState2[1];

  (0, _react.useEffect)(function () {
    var time;

    function handleResize() {
      if (time) {
        clearTimeout(time);
      }

      time = setTimeout(function () {
        setIsMediaScreen(checkIfMediaScreen());
      }, 200);
    }

    window.addEventListener('resize', handleResize);
    return function () {
      return window.removeEventListener('resize', handleResize);
    };
  }, []);
  return isMediaScreen;
}

function useOnClickOutside(ref, handler) {
  (0, _react.useEffect)(function () {
    var listener = function listener(event) {
      if (!ref.current || ref.current.contains(event.target)) {
        return;
      }

      handler(event);
    };

    document.addEventListener('mousedown', listener);
    document.addEventListener('touchstart', listener);
    return function () {
      document.removeEventListener('mousedown', listener);
      document.removeEventListener('touchstart', listener);
    };
  }, [ref, handler]);
}

function useDebounce(value, delay) {
  var _useState3 = (0, _react.useState)(value),
      _useState4 = _slicedToArray(_useState3, 2),
      debouncedValue = _useState4[0],
      setDebouncedValue = _useState4[1];

  var _useState5 = (0, _react.useState)(null),
      _useState6 = _slicedToArray(_useState5, 2),
      cancel = _useState6[0],
      setCancel = _useState6[1];

  (0, _react.useEffect)(function () {
    var handler = setTimeout(function () {
      setDebouncedValue(value);
    }, delay);
    setCancel(handler);
    return function () {
      clearTimeout(handler);
    };
  }, [value, delay]);
  return [debouncedValue, cancel];
}

function useModalState() {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;
  var hookData = (0, _react.useState)(state);
  var isShow = hookData[0];
  var onShow = hookData[1];

  var onOpenForm = function onOpenForm() {
    return onShow(true);
  };

  var onCloseForm = function onCloseForm() {
    return onShow(false);
  };

  return {
    isShow: isShow,
    onOpenForm: onOpenForm,
    onCloseForm: onCloseForm
  };
}

function useTabListener() {
  var focus = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : false;

  var _useState7 = (0, _react.useState)(focus),
      _useState8 = _slicedToArray(_useState7, 2),
      focusedByTab = _useState8[0],
      setFocusedByTab = _useState8[1];

  (0, _react.useEffect)(function () {
    var listener = function listener(event) {
      setFocusedByTab(function () {
        return 9 === event.keyCode || event.keyCode === 48;
      });
    };

    document.body.addEventListener('mousedown', listener);
    document.body.addEventListener('keydown', listener);
    return function () {
      document.body.removeEventListener('mousedown', listener);
      document.body.removeEventListener('keydown', listener);
    };
  });
  return {
    focusedByTab: focusedByTab
  };
}

var WindowSizeObserver = function () {
  var listeners = [];

  var getSize = function getSize() {
    var _document$documentEle, _document$documentEle2;

    return {
      width: ((_document$documentEle = document.documentElement) === null || _document$documentEle === void 0 ? void 0 : _document$documentEle.clientWidth) || window.innerWidth,
      height: ((_document$documentEle2 = document.documentElement) === null || _document$documentEle2 === void 0 ? void 0 : _document$documentEle2.clientHeight) || window.innerHeight
    };
  };

  var resizeListener = function resizeListener() {
    var windowSize = getSize();
    listeners.map(function (listener) {
      return listener(windowSize);
    });
  };

  return {
    addListener: function addListener(listener) {
      if (listeners.length === 0) {
        window.addEventListener('resize', resizeListener);
      }

      listeners.push(listener);
      listener(getSize());
    },
    removeListener: function removeListener(listener) {
      listeners = listeners.filter(function (item) {
        return item !== listener;
      });

      if (listeners.length === 0) {
        window.removeEventListener('resize', resizeListener);
      }
    }
  };
}();

var useWindowSize = function useWindowSize() {
  var _useState9 = (0, _react.useState)({
    width: 0,
    height: 0
  }),
      _useState10 = _slicedToArray(_useState9, 2),
      size = _useState10[0],
      setSize = _useState10[1];

  (0, _react.useLayoutEffect)(function () {
    WindowSizeObserver.addListener(setSize);
    return function () {
      return WindowSizeObserver.removeListener(setSize);
    };
  }, []);
  return _objectSpread({}, size);
};

exports.useWindowSize = useWindowSize;

var useResizeObserver = function useResizeObserver(ref, callback) {
  var _useState11 = (0, _react.useState)(),
      _useState12 = _slicedToArray(_useState11, 2),
      width = _useState12[0],
      setWidth = _useState12[1];

  var _useState13 = (0, _react.useState)(),
      _useState14 = _slicedToArray(_useState13, 2),
      height = _useState14[0],
      setHeight = _useState14[1];

  var handleResize = (0, _react.useCallback)(function (entries) {
    var entry = entries[0];

    if (Array.isArray(entry.borderBoxSize)) {
      setWidth(entry.borderBoxSize[0].inlineSize);
      setHeight(entry.borderBoxSize[0].blockSize);
    } else if (entry.contentRect) {
      setWidth(entry.contentRect.width);
      setHeight(entry.contentRect.height);
    } else {
      var _document$documentEle3, _document$documentEle4;

      setWidth(((_document$documentEle3 = document.documentElement) === null || _document$documentEle3 === void 0 ? void 0 : _document$documentEle3.clientWidth) || window.innerWidth);
      setHeight(((_document$documentEle4 = document.documentElement) === null || _document$documentEle4 === void 0 ? void 0 : _document$documentEle4.clientHeight) || window.innerHeight);
    }

    if (callback) callback(entry.contentRect);
  }, [callback]);
  (0, _react.useLayoutEffect)(function () {
    var resizeObserver = new ResizeObserver(function (entries) {
      return handleResize(entries);
    });
    resizeObserver.observe(ref === null || ref === void 0 ? void 0 : ref.current);
    return function () {
      resizeObserver.disconnect();
    };
  }, [handleResize, ref]);
  return {
    width: width,
    height: height
  };
};

exports.useResizeObserver = useResizeObserver;