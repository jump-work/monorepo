"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TableBody = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _hooks = require("../hooks");

var _paragraph = require("../paragraph");

var _description = require("../description");

var _button = require("../button");

var _icons = require("../icons");

var _newTableRow = require("./new-table-row");

var _newTableCell = require("./new-table-cell");

var _newTableMobileRow = require("./new-table-mobile-row");

var _newTablePopup = require("./new-table-popup");

var _newTableBodyModule = _interopRequireDefault(require("./new-table-body.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

var TableBody = function TableBody(_ref) {
  var data = _ref.data,
      accessor = _ref.accessor,
      headerCell = _ref.headerCell,
      renderCustomCells = _ref.renderCustomCells,
      onDefaultAction = _ref.onDefaultAction;
  var isMobile = (0, _hooks.useIfMediaScreen)();

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      hoverItemId = _useState2[0],
      onHover = _useState2[1];

  return /*#__PURE__*/_react["default"].createElement("div", {
    className: _newTableBodyModule["default"].base
  }, data.map(function (object, index) {
    if (isMobile) {
      return /*#__PURE__*/_react["default"].createElement(_newTableMobileRow.TableMobileRow, {
        key: index
      }, /*#__PURE__*/_react["default"].createElement(_paragraph.Paragraph, {
        className: _newTableBodyModule["default"].paragraph
      }, object[accessor[0]]), /*#__PURE__*/_react["default"].createElement(_description.Description.LH24, {
        className: _newTableBodyModule["default"].description
      }, object[accessor[1]]), /*#__PURE__*/_react["default"].createElement(_newTablePopup.TablePopup, {
        renderProps: function renderProps(open) {
          return /*#__PURE__*/_react["default"].createElement(_button.Button, {
            className: _newTableBodyModule["default"].button,
            styling: "hollow",
            icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconInfo, {
              className: _newTableBodyModule["default"].icon
            }),
            onClick: open
          });
        },
        data: object,
        accessor: accessor,
        headerCell: headerCell,
        renderCustomCells: renderCustomCells
      }));
    }

    return /*#__PURE__*/_react["default"].createElement(_newTableRow.TableRow, {
      key: index,
      style: {
        gridTemplateColumns: "repeat(".concat(accessor.length, ", 1fr)")
      },
      onClick: function onClick() {
        return onDefaultAction(object);
      },
      onMouseEnter: function onMouseEnter() {
        return onHover(object.id);
      },
      onMouseLeave: function onMouseLeave() {
        return onHover(null);
      }
    }, accessor.map(function (item, index) {
      return /*#__PURE__*/_react["default"].createElement(_newTableCell.TableCell, {
        key: index
      }, object[item]);
    }), hoverItemId === object.id && renderCustomCells(object));
  }));
};

exports.TableBody = TableBody;
TableBody.defaultProps = {
  data: [],
  accessor: [],
  renderCustomCells: function renderCustomCells() {}
};
TableBody.propTypes = {
  data: _propTypes["default"].array,
  accessor: _propTypes["default"].array,
  renderCustomCells: _propTypes["default"].func,
  headerCell: _propTypes["default"].array,
  onDefaultAction: _propTypes["default"].func
};