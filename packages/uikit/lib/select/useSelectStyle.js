"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useCustomCompactStyle = exports.useCustomStyle = void 0;

var _react = require("react");

function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); if (enumerableOnly) { symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; }); } keys.push.apply(keys, symbols); } return keys; }

function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i] != null ? arguments[i] : {}; if (i % 2) { ownKeys(Object(source), true).forEach(function (key) { _defineProperty(target, key, source[key]); }); } else if (Object.getOwnPropertyDescriptors) { Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)); } else { ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } } return target; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var useCustomStyle = function useCustomStyle(error, disabled) {
  return (0, _react.useMemo)(function () {
    return {
      indicatorSeparator: function indicatorSeparator() {
        return {
          display: 'none'
        };
      },
      option: function option(provided) {
        return _objectSpread(_objectSpread({}, provided), {}, {
          padding: '0.5rem 0 0.5rem 1rem',
          fontSize: '0.9375rem',
          lineHeight: '1.5rem'
        });
      },
      control: function control(provided, state) {
        return _objectSpread(_objectSpread({}, provided), {}, {
          transition: 'all 0.3s ease',
          boxShadow: state.isFocused ? error ? '0 0 0.0625rem 0.1875rem rgba(236, 57, 61, 0.2)' : '0 0 0.0625rem 0.1875rem rgba(37, 143, 251, 0.3)' : null,
          background: error ? state.isFocused ? '#FFFFFF' : 'rgba(93%, 22%, 24%, 0.2)' : disabled ? '#e7eaee' : '#ffffff',
          border: error ? state.isFocused ? '0.0625rem solid #EC393D' : '0.0625rem solid transparent' : state.isFocused ? '0.0625rem solid #2691FF' : '0.0625rem solid #E7EAEE',
          borderColor: state.isFocused ? 'transparent' : '#E7EAEE',
          '&:hover': {
            borderColor: error ? state.isFocused ? '#EC393D' : '#E7EAEE' : state.isFocused ? '#2691FF' : '#E7EAEE'
          }
        });
      },
      placeholder: function placeholder(provided, state) {
        return _objectSpread(_objectSpread({}, provided), {}, {
          display: state.isFocused ? 'none' : 'block',
          color: error ? '#ABADBA' : disabled ? '#CCCFDB' : '#ABADBA'
        });
      },
      indicatorsContainer: function indicatorsContainer() {
        return {
          padding: '0.4375rem 0.5rem',
          display: 'flex'
        };
      },
      dropdownIndicator: function dropdownIndicator() {
        return {
          color: '#333333',
          width: '1.5rem',
          height: '1.5rem',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        };
      },
      loadingIndicator: function loadingIndicator() {
        return {
          display: 'none'
        };
      }
    };
  }, [error, disabled]);
};

exports.useCustomStyle = useCustomStyle;

var useCustomCompactStyle = function useCustomCompactStyle() {
  return (0, _react.useMemo)(function () {
    return {
      indicatorSeparator: function indicatorSeparator() {
        return {
          display: 'none'
        };
      },
      dropdownIndicator: function dropdownIndicator() {
        return {
          display: 'none'
        };
      },
      menu: function menu() {
        return {
          margin: '0',
          maxWidth: '13.5625rem',
          width: '100%',
          position: 'absolute',
          top: 'calc(100% - 0.1875rem)',
          left: '0.75rem',
          boxShadow: '0 0.25rem 0.75rem rgba(204, 207, 219, 0.5)',
          borderRadius: '0.25rem',
          background: '#FFFFFF'
        };
      }
    };
  }, []);
};

exports.useCustomCompactStyle = useCustomCompactStyle;