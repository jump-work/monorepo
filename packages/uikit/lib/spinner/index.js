"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Ellipsis", {
  enumerable: true,
  get: function get() {
    return _ellipsis.Ellipsis;
  }
});

var _ellipsis = require("./ellipsis");