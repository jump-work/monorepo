"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Ellipsis = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _ellipsisModule = _interopRequireDefault(require("./ellipsis.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Ellipsis = function Ellipsis(_ref) {
  var invert = _ref.invert;
  var clsName = (0, _classnames["default"])(_ellipsisModule["default"].container, _defineProperty({}, _ellipsisModule["default"].invert, invert));
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: clsName
  }, /*#__PURE__*/_react["default"].createElement("div", null), /*#__PURE__*/_react["default"].createElement("div", null), /*#__PURE__*/_react["default"].createElement("div", null), /*#__PURE__*/_react["default"].createElement("div", null));
};

exports.Ellipsis = Ellipsis;
Ellipsis.defaultProps = {
  invert: false
};
Ellipsis.propTypes = {
  invert: _propTypes["default"].bool
};