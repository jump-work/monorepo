"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "ErrorScreen", {
  enumerable: true,
  get: function get() {
    return _errorScreen.ErrorScreen;
  }
});
Object.defineProperty(exports, "ErrorScreen404", {
  enumerable: true,
  get: function get() {
    return _errorScreen2.ErrorScreen404;
  }
});
Object.defineProperty(exports, "ErrorScreen503", {
  enumerable: true,
  get: function get() {
    return _errorScreen3.ErrorScreen503;
  }
});

var _errorScreen = require("./error-screen");

var _errorScreen2 = require("./error-screen-404");

var _errorScreen3 = require("./error-screen-503");