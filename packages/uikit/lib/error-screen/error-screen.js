"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ErrorScreen = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _index = require("../index");

var _errorScreenModule = _interopRequireDefault(require("./error-screen.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var ErrorScreen = function ErrorScreen(_ref) {
  var error = _ref.error,
      footerContent = _ref.footerContent;
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: _errorScreenModule["default"].error
  }, /*#__PURE__*/_react["default"].createElement(_index.Header.H1, null, error === null || error === void 0 ? void 0 : error.title), /*#__PURE__*/_react["default"].createElement(_index.Paragraph, null, error === null || error === void 0 ? void 0 : error.message), footerContent());
};

exports.ErrorScreen = ErrorScreen;
ErrorScreen.defaultProps = {
  footerContent: function footerContent() {
    return null;
  }
};
ErrorScreen.propTypes = {
  error: _propTypes["default"].shape({
    title: _propTypes["default"].string,
    message: _propTypes["default"].string
  }),
  footerContent: _propTypes["default"].func
};