"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.ParagraphV2 = void 0;

var _newParagraph = require("./new-paragraph");

var ParagraphV2 = _newParagraph.Paragraph;
exports.ParagraphV2 = ParagraphV2;