"use strict";

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.MobTable = void 0;

var _react = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _mobTableModule = _interopRequireDefault(require("./mob-table.module.scss"));

var _icons = require("../icons");

var _tablePopup = require("./table-popup");

var _tablePopupContent = require("./table-popup-content");

var _renderHelper = require("./render-helper");

var _tableHeader = require("./table-header");

var _tableHeaderAdd = require("./table-header-add");

var _tableHeaderSort = require("./table-header-sort");

var _excluded = ["columns", "data", "onRowSelection", "sort", "onSort"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || _typeof(obj) !== "object" && typeof obj !== "function") { return { "default": obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj["default"] = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var MobTable = function MobTable(_ref) {
  var columns = _ref.columns,
      data = _ref.data,
      onRowSelection = _ref.onRowSelection,
      sort = _ref.sort,
      onSort = _ref.onSort,
      rest = _objectWithoutProperties(_ref, _excluded);

  var mainColumn = (0, _react.useMemo)(function () {
    return columns.find(function (info) {
      return info.main;
    });
  }, [columns]);
  var descColumn = (0, _react.useMemo)(function () {
    return columns.find(function (info) {
      return info.description;
    });
  }, [columns]);

  var _useState = (0, _react.useState)(null),
      _useState2 = _slicedToArray(_useState, 2),
      currentRow = _useState2[0],
      setCurrentRow = _useState2[1];

  var _useState3 = (0, _react.useState)(false),
      _useState4 = _slicedToArray(_useState3, 2),
      popup = _useState4[0],
      setPopup = _useState4[1];

  var closePopup = function closePopup() {
    setPopup(false);
    setCurrentRow(null);
  };

  var openPopup = function openPopup(row) {
    setCurrentRow(row);
    setPopup(true);
  };

  var _onRowSelection = function _onRowSelection() {
    onRowSelection(currentRow);
    closePopup();
  };

  var renderRow = function renderRow(row, index) {
    var onInfoClicked = function onInfoClicked() {
      return openPopup(row);
    };

    return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement("div", {
      className: _mobTableModule["default"].row,
      key: index
    }, /*#__PURE__*/_react["default"].createElement("div", {
      className: _mobTableModule["default"].description
    }, /*#__PURE__*/_react["default"].createElement("div", {
      className: _mobTableModule["default"].mainText
    }, (0, _renderHelper.renderColumn)(mainColumn, row)), /*#__PURE__*/_react["default"].createElement("div", {
      className: _mobTableModule["default"].descriptionText
    }, (0, _renderHelper.renderColumn)(descColumn, row))), /*#__PURE__*/_react["default"].createElement("div", {
      className: _mobTableModule["default"].info,
      onClick: onInfoClicked
    }, /*#__PURE__*/_react["default"].createElement(_icons.Icons16.IconInfo, null))));
  };

  return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement(_tableHeader.TableHeader, {
    header: rest.header
  }, /*#__PURE__*/_react["default"].createElement(_tableHeaderSort.TableHeaderSort, {
    columns: columns,
    sort: sort,
    onSort: onSort
  }), /*#__PURE__*/_react["default"].createElement(_tableHeaderAdd.TableHeaderAdd, {
    onAdd: rest.onAdd
  })), /*#__PURE__*/_react["default"].createElement("div", {
    className: _mobTableModule["default"].base
  }, data.map(renderRow), " ", /*#__PURE__*/_react["default"].createElement(_tablePopup.TablePopup, {
    onClose: closePopup,
    show: popup
  }, function () {
    return /*#__PURE__*/_react["default"].createElement(_tablePopupContent.TablePopupContent, {
      columns: columns,
      rowData: currentRow,
      onRowSelection: _onRowSelection
    });
  })));
};

exports.MobTable = MobTable;
MobTable.propTypes = {
  columns: _propTypes["default"].array.isRequired,
  data: _propTypes["default"].arrayOf(_propTypes["default"].object).isRequired,
  onRowSelection: _propTypes["default"].func,
  header: _propTypes["default"].string,
  onAdd: _propTypes["default"].func,
  sort: _propTypes["default"].shape({
    by: _propTypes["default"].string,
    desc: _propTypes["default"].bool
  }),
  onSort: _propTypes["default"].func
};