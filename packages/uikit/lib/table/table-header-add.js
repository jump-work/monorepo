"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TableHeaderAdd = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _icons = require("../icons");

var _button = require("../button");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var TableHeaderAdd = function TableHeaderAdd(_ref) {
  var onAdd = _ref.onAdd;
  if (!onAdd) return null;
  return /*#__PURE__*/_react["default"].createElement(_button.Button, {
    element: "button",
    icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconPlus, null),
    onClick: onAdd,
    styling: "hollow",
    style: {
      padding: 0
    }
  });
};

exports.TableHeaderAdd = TableHeaderAdd;
TableHeaderAdd.propTypes = {
  onAdd: _propTypes["default"].func
};