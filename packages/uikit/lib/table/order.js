"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.orders = exports.ORDER_DESC = exports.ORDER_ASC = void 0;

var _icons = require("../icons");

var ORDER_ASC = 'ORDER_ASC';
exports.ORDER_ASC = ORDER_ASC;
var ORDER_DESC = 'ORDER_DESC';
exports.ORDER_DESC = ORDER_DESC;
var orders = [{
  key: ORDER_ASC,
  desc: false,
  icon: _icons.Icons16.IconArrowUp
}, {
  key: ORDER_DESC,
  desc: true,
  icon: _icons.Icons16.IconArrowDown
}];
exports.orders = orders;