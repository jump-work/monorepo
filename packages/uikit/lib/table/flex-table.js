"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.FlexTable = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _reactTable = require("react-table");

var _flexTableModule = _interopRequireDefault(require("./flex-table.module.scss"));

var _icons = require("../icons");

var _tableHeader = require("./table-header");

var _tableHeaderAdd = require("./table-header-add");

var _excluded = ["columns", "data", "onRowSelection", "sort", "onSort"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var FlexTable = function FlexTable(_ref) {
  var columns = _ref.columns,
      data = _ref.data,
      onRowSelection = _ref.onRowSelection,
      sort = _ref.sort,
      onSort = _ref.onSort,
      rest = _objectWithoutProperties(_ref, _excluded);

  var defaultColumn = {
    minWidth: 30,
    width: 150,
    maxWidth: 200
  };

  var headerProps = function headerProps(props, _ref2) {
    var column = _ref2.column;
    return getStyles(props, column.align);
  };

  var cellProps = function cellProps(props, _ref3) {
    var cell = _ref3.cell;
    return getStyles(props, cell.column.align);
  };

  var getStyles = function getStyles(props) {
    var align = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 'left';
    return [props, {
      style: {
        justifyContent: align === 'right' ? 'flex-end' : 'flex-start',
        alignItems: 'flex-start',
        display: 'flex'
      }
    }];
  };

  var getSortProps = function getSortProps(_ref4) {
    var id = _ref4.id,
        sortable = _ref4.sortable;
    return function () {
      var by = sort.by && sort.by !== '' && sort.by === id && sort.desc ? null : id;
      var desc = by === id ? !sort.desc : null;

      if (sortable) {
        onSort({
          by: by,
          desc: desc
        });
      }
    };
  };

  var _useTable = (0, _reactTable.useTable)({
    columns: columns,
    data: data,
    defaultColumn: defaultColumn
  }, _reactTable.useResizeColumns, _reactTable.useFlexLayout, _reactTable.useRowSelect),
      getTableProps = _useTable.getTableProps,
      getTableBodyProps = _useTable.getTableBodyProps,
      headerGroups = _useTable.headerGroups,
      rows = _useTable.rows,
      prepareRow = _useTable.prepareRow;

  return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, /*#__PURE__*/_react["default"].createElement(_tableHeader.TableHeader, {
    header: rest.header
  }, /*#__PURE__*/_react["default"].createElement(_tableHeaderAdd.TableHeaderAdd, {
    onAdd: rest.onAdd
  })), /*#__PURE__*/_react["default"].createElement("div", _extends({}, getTableProps(), {
    className: _flexTableModule["default"].table
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: _flexTableModule["default"].thead
  }, headerGroups.map(function (headerGroup, index) {
    return /*#__PURE__*/_react["default"].createElement("div", _extends({
      key: index
    }, headerGroup.getHeaderGroupProps({
      style: {
        paddingRight: '0px'
      }
    }), {
      className: _flexTableModule["default"].tr
    }), headerGroup.headers.map(function (column, index) {
      return /*#__PURE__*/_react["default"].createElement("div", _extends({
        key: index
      }, column.getHeaderProps(headerProps), {
        className: (0, _classnames["default"])(_flexTableModule["default"].th, _defineProperty({}, _flexTableModule["default"].sortable, column.sortable)),
        onClick: getSortProps(column)
      }), column.render('Header'), column.canResize && /*#__PURE__*/_react["default"].createElement("div", _extends({}, column.getResizerProps(), {
        className: (0, _classnames["default"])([_flexTableModule["default"].resizer], _defineProperty({}, _flexTableModule["default"].isResizing, column.isResizing))
      })), /*#__PURE__*/_react["default"].createElement("span", null, column.sortable && sort.by === column.id ? sort.desc ? /*#__PURE__*/_react["default"].createElement(_icons.Icons16.IconSortDown, null) : /*#__PURE__*/_react["default"].createElement(_icons.Icons16.IconSortUp, null) : ''));
    }));
  })), /*#__PURE__*/_react["default"].createElement("div", _extends({}, getTableBodyProps(), {
    className: _flexTableModule["default"].tbody
  }), rows.map(function (row, index) {
    prepareRow(row);

    var _onRowSelection = function _onRowSelection() {
      return onRowSelection(row.original, row.values);
    };

    var _onEditClick = function _onEditClick(e) {
      e.stopPropagation();
      onRowSelection(row.original, row.values);
    };

    return /*#__PURE__*/_react["default"].createElement("div", _extends({
      key: index
    }, row.getRowProps(), {
      className: _flexTableModule["default"].tr,
      onClick: _onRowSelection
    }), row.cells.map(function (cell, index) {
      return /*#__PURE__*/_react["default"].createElement("div", _extends({
        key: index
      }, cell.getCellProps(cellProps), {
        className: _flexTableModule["default"].td
      }), cell.render('Cell'));
    }), /*#__PURE__*/_react["default"].createElement("div", {
      className: _flexTableModule["default"].editButton,
      onClick: _onEditClick
    }, /*#__PURE__*/_react["default"].createElement("div", {
      className: _flexTableModule["default"].editButtonInner
    }, /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconPencil, null))));
  }))));
};

exports.FlexTable = FlexTable;
FlexTable.propTypes = {
  columns: _propTypes["default"].array.isRequired,
  data: _propTypes["default"].arrayOf(_propTypes["default"].object).isRequired,
  onRowSelection: _propTypes["default"].func,
  header: _propTypes["default"].string,
  onAdd: _propTypes["default"].func,
  sort: _propTypes["default"].shape({
    by: _propTypes["default"].string,
    desc: _propTypes["default"].bool
  }),
  onSort: _propTypes["default"].func
};