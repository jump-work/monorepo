"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.renderColumn = renderColumn;

function renderColumn(columnInfo, rowData) {
  return columnInfo.Cell ? columnInfo.Cell({
    cell: {
      value: rowData[columnInfo.accessor]
    }
  }) : rowData[columnInfo.accessor];
}