"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.TablePopup = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _popup = require("../popup");

var _icons = require("../icons");

var _tablePopupModule = _interopRequireDefault(require("./table-popup.module.scss"));

var _button = require("../button");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var style = {
  top: '100%',
  left: 0,
  width: '100%',
  transform: 'translateY(-100%)',
  maxWidth: 'unset',
  borderRadius: 0
};

var TablePopup = function TablePopup(_ref) {
  var show = _ref.show,
      onClose = _ref.onClose,
      children = _ref.children;
  return /*#__PURE__*/_react["default"].createElement(_react["default"].Fragment, null, show ? /*#__PURE__*/_react["default"].createElement(_popup.Popup, {
    element: "div",
    style: style
  }, /*#__PURE__*/_react["default"].createElement(_button.Button, {
    styling: 'hollow',
    icon: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconClose, null),
    className: _tablePopupModule["default"].close,
    style: {
      padding: 0
    },
    onClick: onClose
  }), /*#__PURE__*/_react["default"].createElement("div", {
    className: _tablePopupModule["default"].content
  }, children())) : null);
};

exports.TablePopup = TablePopup;
TablePopup.defaultProps = {
  show: false
};
TablePopup.propTypes = {
  show: _propTypes["default"].bool,
  onClose: _propTypes["default"].func.isRequired,
  children: _propTypes["default"].func.isRequired
};