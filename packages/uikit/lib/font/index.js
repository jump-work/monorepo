"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Font", {
  enumerable: true,
  get: function get() {
    return _font["default"];
  }
});

var _font = _interopRequireDefault(require("./font"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }