"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Notification = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _itemNotificationModule = _interopRequireDefault(require("./itemNotification.module.scss"));

var _excluded = ["alert", "color"];

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _objectWithoutProperties(source, excluded) { if (source == null) return {}; var target = _objectWithoutPropertiesLoose(source, excluded); var key, i; if (Object.getOwnPropertySymbols) { var sourceSymbolKeys = Object.getOwnPropertySymbols(source); for (i = 0; i < sourceSymbolKeys.length; i++) { key = sourceSymbolKeys[i]; if (excluded.indexOf(key) >= 0) continue; if (!Object.prototype.propertyIsEnumerable.call(source, key)) continue; target[key] = source[key]; } } return target; }

function _objectWithoutPropertiesLoose(source, excluded) { if (source == null) return {}; var target = {}; var sourceKeys = Object.keys(source); var key, i; for (i = 0; i < sourceKeys.length; i++) { key = sourceKeys[i]; if (excluded.indexOf(key) >= 0) continue; target[key] = source[key]; } return target; }

var Notification = function Notification(_ref) {
  var alert = _ref.alert,
      color = _ref.color,
      rest = _objectWithoutProperties(_ref, _excluded);

  var cls = (0, _classnames["default"])(_itemNotificationModule["default"].base, _defineProperty({}, _itemNotificationModule["default"].alert, alert));
  return /*#__PURE__*/_react["default"].createElement("span", _extends({
    className: cls,
    style: {
      background: "".concat(color)
    }
  }, rest));
};

exports.Notification = Notification;
Notification.displayName = 'Item.Notification';
Notification.defaultProps = {
  alert: false
};
Notification.propTypes = {
  alert: _propTypes["default"].bool,
  color: _propTypes["default"].string
};