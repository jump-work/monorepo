"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "OverflowMenu", {
  enumerable: true,
  get: function get() {
    return _overflowMenu.OverflowMenu;
  }
});

var _overflowMenu = require("./overflow-menu");