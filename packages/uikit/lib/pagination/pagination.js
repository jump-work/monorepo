"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Pagination = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _reactPaginate = _interopRequireDefault(require("react-paginate"));

var _classnames2 = _interopRequireDefault(require("classnames"));

var _hooks = require("../hooks");

var _icons = require("../icons");

var _paginationModule = _interopRequireDefault(require("./pagination.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Pagination = function Pagination(_ref) {
  var handlePageClick = _ref.handlePageClick,
      pageCount = _ref.pageCount,
      marginPagesDisplayed = _ref.marginPagesDisplayed,
      pageRangeDisplayed = _ref.pageRangeDisplayed,
      initialPage = _ref.initialPage,
      _ref$fluid = _ref.fluid,
      fluid = _ref$fluid === void 0 ? false : _ref$fluid;
  var isMobile = (0, _hooks.useIfMediaScreen)();
  var cxsPagination = (0, _classnames2["default"])(_paginationModule["default"].pagination, _defineProperty({}, _paginationModule["default"].fluid, fluid));
  var breakLabel = isMobile ? null : '...';
  var marginPages = isMobile ? 0 : marginPagesDisplayed;
  var pageRange = isMobile ? 0 : pageRangeDisplayed;
  return /*#__PURE__*/_react["default"].createElement(_reactPaginate["default"], {
    previousLabel: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconArrowLeft, null),
    nextLabel: /*#__PURE__*/_react["default"].createElement(_icons.Icons24.IconArrowRight, null),
    breakLabel: breakLabel,
    pageCount: pageCount,
    marginPagesDisplayed: marginPages,
    pageRangeDisplayed: pageRange,
    onPageChange: handlePageClick,
    initialPage: initialPage,
    containerClassName: cxsPagination,
    previousClassName: _paginationModule["default"].previous,
    nextClassName: _paginationModule["default"].next,
    pageClassName: _paginationModule["default"].page,
    disabledClassName: _paginationModule["default"].disabled,
    breakClassName: _paginationModule["default"]["break"],
    activeClassName: _paginationModule["default"].active,
    ariaLabelBuilder: function ariaLabelBuilder() {
      return pageCount;
    }
  });
};

exports.Pagination = Pagination;
Pagination.defaultProps = {
  handlePageClick: function handlePageClick() {},
  marginPagesDisplayed: 2,
  pageRangeDisplayed: 5
};
Pagination.propTypes = {
  handlePageClick: _propTypes["default"].func,
  pageCount: _propTypes["default"].number.isRequired,
  marginPagesDisplayed: _propTypes["default"].number,
  pageRangeDisplayed: _propTypes["default"].number,
  initialPage: _propTypes["default"].number,
  fluid: _propTypes["default"].bool
};