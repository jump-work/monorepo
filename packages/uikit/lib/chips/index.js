"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
Object.defineProperty(exports, "Chips", {
  enumerable: true,
  get: function get() {
    return _chips.Chips;
  }
});

var _chips = require("./chips");