"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Navigation = void 0;

var _react = _interopRequireDefault(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _classnames = _interopRequireDefault(require("classnames"));

var _button = require("../button");

var _tabsNavigationModule = _interopRequireDefault(require("./tabsNavigation.module.scss"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var Navigation = function Navigation(_ref) {
  var navLabel = _ref.navLabel,
      value = _ref.value,
      className = _ref.className,
      onChangeActiveTab = _ref.onChangeActiveTab,
      disabled = _ref.disabled,
      notification = _ref.notification,
      isActive = _ref.isActive;
  var classes = (0, _classnames["default"])(_tabsNavigationModule["default"].base, _defineProperty({}, _tabsNavigationModule["default"].active, isActive), className);
  return /*#__PURE__*/_react["default"].createElement("div", {
    className: _tabsNavigationModule["default"].wrapper
  }, /*#__PURE__*/_react["default"].createElement(_button.Button, {
    styling: 'hollow',
    className: classes,
    onClick: function onClick() {
      onChangeActiveTab(navLabel, value);
    },
    disabled: disabled
  }, navLabel, notification), /*#__PURE__*/_react["default"].createElement("div", {
    className: _tabsNavigationModule["default"].focus
  }));
};

exports.Navigation = Navigation;
Navigation.defaultProps = {
  navLabel: 'Tab',
  className: '',
  onChangeActiveTab: function onChangeActiveTab() {}
};
Navigation.propTypes = {
  navLabel: _propTypes["default"].string,
  value: _propTypes["default"].any,
  className: _propTypes["default"].string,
  onChangeActiveTab: _propTypes["default"].func,
  disabled: _propTypes["default"].bool,
  notification: _propTypes["default"].any,
  isActive: _propTypes["default"].bool
};