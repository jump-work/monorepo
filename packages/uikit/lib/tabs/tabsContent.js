"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Content = void 0;

var _propTypes = _interopRequireDefault(require("prop-types"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Content = function Content(_ref) {
  var children = _ref.children;
  return children;
};

exports.Content = Content;
Content.displayName = 'Tab.Content';
Content.defaultProps = {
  activeTab: '',
  disabled: false
};
Content.propTypes = {
  label: _propTypes["default"].string.isRequired,
  value: _propTypes["default"].any,
  children: _propTypes["default"].any.isRequired,
  activeTab: _propTypes["default"].string,
  disabled: _propTypes["default"].bool,
  notification: _propTypes["default"].any
};