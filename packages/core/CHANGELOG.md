# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [1.8.0](https://gitlab.com/justlook/js-core/compare/@jump-work/core@1.7.0...@jump-work/core@1.8.0) (2021-10-17)


### Features

* resolve DEV-2682 testing 2 ([5d9b6b2](https://gitlab.com/justlook/js-core/commit/5d9b6b2a86a78f2cf77c06fb005db96cc6a96dfd))
* resolve DEV-2682 testing 2 ([dd412cc](https://gitlab.com/justlook/js-core/commit/dd412cc9044c87ea2e00fcef08595fd2ce30ed3d))





# 1.7.0 (2021-09-28)


### Features

* resolve DEV-2682 testing ([2c0a049](https://gitlab.com/justlook/js-core/commit/2c0a04973b85e6dacac8e6858c15da032c142a06))
