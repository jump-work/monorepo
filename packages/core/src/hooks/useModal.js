import { useReducer } from 'react';

const reducer = (state, action) => {
    switch (action.type) {
        case 'open':
            return {
                ...state,
                [action.name]: {
                    isOpen: true,
                    payload: action.payload
                }
            }
        case 'close':
            return {
                ...state,
                [action.name]: {
                    isOpen: false,
                    payload: null
                }
            }
        default:
            return state;
    }
}

export const useModal = (initialState = {}) => {
    const [state, dispatch] = useReducer(reducer, initialState);

    const isOpen = (name) => state[name]?.isOpen || false;

    const onOpen = (name, payload = null) => dispatch({ type: 'open', name, payload });

    const onClose = (name) => dispatch({ type: 'close', name });

    const getPayload = (name) => state[name]?.payload || null;

    return { onOpen, onClose, isOpen, getPayload };
};
