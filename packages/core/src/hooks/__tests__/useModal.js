import { renderHook, act } from '@testing-library/react-hooks';
import { useModal } from '../useModal';

const FORM_TEST = 'test';
const FORM_TEST_2 = 'test2';

describe('Hooks > useModal.js', () => {
    it('useModal должен обрабатывать переключение состояния по ключу', () => {
        const { result } = renderHook(() => useModal());

        expect(result.current.isOpen(FORM_TEST)).toBeFalsy();
        expect(result.current.getPayload(FORM_TEST)).toBeNull();

        act(() => {result.current.onOpen(FORM_TEST, { id: 1, name: 'test' })});
        expect(result.current.isOpen(FORM_TEST)).toBeTruthy();
        expect(result.current.getPayload(FORM_TEST)).toEqual({ id: 1, name: 'test' });

        act(() => {result.current.onClose(FORM_TEST)});
        expect(result.current.isOpen(FORM_TEST)).toBeFalsy();
        expect(result.current.getPayload(FORM_TEST)).toBeNull();
    });

    it('Состояния открытия закрытия для нескольких форм', () => {
        const { result } = renderHook(() => useModal());

        // начальное состояние
        expect(result.current.isOpen(FORM_TEST)).toBeFalsy();
        expect(result.current.isOpen(FORM_TEST_2)).toBeFalsy();

        // открытие первой формы
        act(() => {result.current.onOpen(FORM_TEST, { id: 1, name: 'test' })});
        expect(result.current.isOpen(FORM_TEST)).toBeTruthy();
        expect(result.current.getPayload(FORM_TEST)).toEqual({ id: 1, name: 'test' });
        expect(result.current.isOpen(FORM_TEST_2)).toBeFalsy();
        expect(result.current.getPayload(FORM_TEST_2)).toBeNull();

        // открытие второй формы
        act(() => {result.current.onOpen(FORM_TEST_2, { id: 2, name: 'test2' })});
        expect(result.current.isOpen(FORM_TEST)).toBeTruthy();
        expect(result.current.getPayload(FORM_TEST)).toEqual({ id: 1, name: 'test' });
        expect(result.current.isOpen(FORM_TEST_2)).toBeTruthy();
        expect(result.current.getPayload(FORM_TEST_2)).toEqual({ id: 2, name: 'test2' });

        // закрытие первой формы
        act(() => {result.current.onClose(FORM_TEST)});
        expect(result.current.isOpen(FORM_TEST)).toBeFalsy();
        expect(result.current.getPayload(FORM_TEST)).toBeNull();
        expect(result.current.isOpen(FORM_TEST_2)).toBeTruthy();
        expect(result.current.getPayload(FORM_TEST_2)).toEqual({ id: 2, name: 'test2' });
    });
});
