import { renderHook, act } from '@testing-library/react-hooks';
import { useModalState } from '../useModalState';

describe('Hooks > useModalState.js', () => {
    it('useModalState должен обрабатывать переключение состояния', () => {
        const {result} = renderHook(() => useModalState(false));

        expect(result.current.isShow).toBeFalsy();
        expect(result.current.onOpenForm).toBeInstanceOf(Function);
        expect(result.current.onCloseForm).toBeInstanceOf(Function);

        act(() => {result.current.onOpenForm()});
        expect(result.current.isShow).toBeTruthy();

        act(() => {result.current.onCloseForm()});
        expect(result.current.isShow).toBeFalsy();
    });
});