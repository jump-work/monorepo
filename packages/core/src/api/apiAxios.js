import axios from 'axios';

import { Config, ApiError } from '../index';
import { Token, Device } from '../utils';

export function EndpointFactory (config) {
    const apiClientInstance = new ApiClient(config);
    const endpoint = function (EndPoint) {
        return Object.assign({}, apiClientInstance, EndPoint);
    };

    return {
        endpoint: endpoint
    }
}

/**
 *
 * @param string baseURL путь до api включая протокол
 * @param string storage тип хранилища [storage|session]
 * @constructor
 */
export function ApiClient ({ baseURL, storage = 'storage', deviceStorageKey = null }) {
    this.baseURL = baseURL;
    this.appKey = Config.app_key;
    this.storage = storage;
    this.deviceStorageKey = deviceStorageKey;

    this.download  = function (url, token) {
        return this.request({
            method: 'get',
            url,
            responseType: 'blob',
            token,
        });
    };

    this.get = function (url, query = {}, token = null, cancelToken = null) {
        return this.request({
            method: 'get',
            url,
            query,
            token,
            cancelToken
        });
    };

    this.getStatus = function (url, query = {}, token = null, cancelToken = null) {
        return this.request({
            method: 'get',
            url,
            query,
            token,
            cancelToken,
            onlyStatus: true,
        });
    };

    this.put = function (url, data = {}, token) {
        return this.request({
            method: 'put',
            url,
            data,
            token,
        });
    };

    this.patch = function (url, data = {}, token) {
        return this.request({
            method: 'patch',
            url,
            data,
            token,
        });
    };

    this.post = function (url, data = {}, token, cancelToken = null) {
        return this.request({
            method: 'post',
            url,
            data,
            token,
            cancelToken
        });
    };

    this.postMultipart = function (url, formData = {}, token, cancelToken = null) {
        return this.request({
            method: 'post',
            url,
            data: formData,
            token,
            cancelToken,
            headers: {
                'Content-Type': 'multipart/form-data',
            }
        });
    };

    this.deleteRequest = function (url, data, token) {
        return this.request({
            method: 'delete',
            url,
            data,
            token,
        });
    };

    this.getHeaders = function (token) {
        const headers = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
        };

        if (this.appKey) {
            headers['Application-Key'] = this.appKey;
        }

        const deviceKey = Device.getDeviceKey(this.storage, this.deviceStorageKey);
        if (deviceKey) {
            headers['Device-Key'] = deviceKey;
        }

        const tokenLocal = Token.getTokenStorage();
        token = token || tokenLocal?.access_token || null;
        if (token) {
            headers['Authorization'] = 'Bearer ' + token;
        }

        return headers;
    };

    this.request = function ({ url, method, query = {}, data = {}, token, responseType = 'json', cancelToken, onlyStatus = false , headers: additionalHeaders = {}}) {
        const headers = {
            ...this.getHeaders(token),
            ...additionalHeaders
        };

        const config = {
            baseURL: this.baseURL,
            url,
            method,
            responseType,
            headers,
            data,
            params: query,
            validateStatus: function (status) {
                return status < 500;
            },
            cancelToken
        };

        return axios(config)
            .then(res => {
                if(onlyStatus) {
                    return res.status;
                }

                if (res.status >= 400) {
                    throw new ApiError(res.data.error, res.status);
                }

                return res.data;
            }).catch(e => {
                let error;

                try {
                    error = e.name === 'ApiError' ? e : new ApiError(
                        e.response.data.error, e.response.status);
                } catch (e) {
                    error = new ApiError();
                }

                throw error;
            });
    }
}
