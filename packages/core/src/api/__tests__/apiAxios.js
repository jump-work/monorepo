import { TOKEN_STATUS } from '../../utils/token';
import { ApiClient, EndpointFactory } from '../apiAxios';
import { Config } from '../../config';
import { LocalStorage, Token } from '../../utils';

const tokenResp = {
    access_token: 'access_token',
    refresh_token: 'refresh_token',
    status: TOKEN_STATUS.normal,
    is_support_access: false,
    is_admin_access: false,
    expires_in: new Date(),
    created_at: new Date(),
};

describe('Api > apiAxios.js', () => {
    it('создает новые endpoint', () => {
        const { endpoint } = new EndpointFactory({ baseURL: 'http://moc.ru' });


        const ApiTestClient = endpoint({
            fetchAll: jest.fn()
        });

        ApiTestClient.fetchAll();

        expect(ApiTestClient.fetchAll.mock.calls.length).toBe(1);
    });

    it('должен принимать параметры', () => {
        const api = new ApiClient({ baseURL: 'http://moc.ru' });

        expect(api.baseURL).toBe('http://moc.ru');
        expect(api.appKey).toBe(Config.app_key);
    });

    it('должен вызывать request из get, post, put, delete', () => {
        const api = new ApiClient({ baseURL: 'http://moc.ru' });
        api.request = jest.fn();

        const url = 'items';
        const data = { param: 'value' };
        const token = 'test-custom-token';
        const cancelToken = null;

        api.get(url, data, token, cancelToken);
        api.post(url, data, token, cancelToken);
        api.put(url, data, token);
        api.patch(url, data, token);
        api.deleteRequest(url, data, token);
        api.download(url, token);
        api.getStatus(url, data, token, cancelToken);

        expect(api.request.mock.calls.length).toBe(7);

        expect(api.request.mock.calls[0][0])
            .toStrictEqual({ method: 'get', url, query: data, token, cancelToken });

        expect(api.request.mock.calls[1][0])
            .toStrictEqual({ method: 'post', url, data, token, cancelToken });

        expect(api.request.mock.calls[2][0])
            .toStrictEqual({ method: 'put', url, data, token });

        expect(api.request.mock.calls[3][0])
            .toStrictEqual({ method: 'patch', data, url, token });

        expect(api.request.mock.calls[4][0])
            .toStrictEqual({ method: 'delete', url, data, token });

        expect(api.request.mock.calls[5][0])
            .toStrictEqual({ method: 'get', url, token, responseType: 'blob' });

        expect(api.request.mock.calls[6][0])
            .toStrictEqual({ method: 'get', url, query: data, token, cancelToken, onlyStatus: true });
    });

    it('должен содержать токен из стора', () => {
        LocalStorage.set(Token.tokenStorageKey, tokenResp);

        const api = new ApiClient({ baseURL: 'http://moc.ru' });

        const headers = api.getHeaders();

        expect(headers['Authorization'])
            .toBe(`Bearer ${tokenResp.access_token}`);
    });

    it('должен содержать кастомный токен', () => {
        LocalStorage.set(Token.tokenStorageKey, tokenResp);

        const api = new ApiClient({ baseURL: 'http://moc.ru' });

        const headers = api.getHeaders('test-custom-token');

        expect(headers['Authorization']).toBe('Bearer test-custom-token');
    });
});
