export const AuthEndpoint = {
    scenario: async function ({ login, password }) {
        return await this.post('auth/scenario', { login, password });
    },

    login: async function ({ login, password }) {
        return await this.post('auth/login', { login, password });
    },

    refresh (refreshToken) {
        return this.patch('auth/token', { refreshToken });
    },

    logout () {
        return this.deleteRequest('auth/token');
    },

    patchPassword (params) {
        return this.patch('auth/password', params);
    },

    getOtp: async function (params) {
        return await this.post('auth/otp', params);
    },

    confirmOtp: async function (params) {
        return await this.put('auth/otp', params);
    },
};
