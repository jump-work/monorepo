export const AUTH_SCREENS = {
    login: 'login',
    recovery: 'recovery',
    recovery_success: 'recovery-success',
    confirm_otp: 'confirm-otp',
    change_password: 'change-password',
};