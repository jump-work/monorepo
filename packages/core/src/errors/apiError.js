export default function ApiError (data, code = 500) {
    this.responseCode = code;
    this.errorData = data || {
        title: 'Ошибка  на сервере.',
        detail: `Внутренняя ошибка сервера. Code #${code}.`,
        event: null,
        fields: null,
    };

    this.name = 'ApiError';
    this.message = this.errorData.detail;
    this.stack = (new Error()).stack;
}

ApiError.prototype = Object.create(Error.prototype);
ApiError.prototype.constructor = ApiError;