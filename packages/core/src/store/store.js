import { createBrowserHistory } from 'history';
import { createStore, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';

let reduxStoreInstance = null;

const isDevTools =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ &&
    process.env.NODE_ENV !== 'production';

const initialState = {};
const composeEnhancers = isDevTools ? window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({}) : compose;

export const history = createBrowserHistory();

export function configureStore (createRootReducer, reducerPath = './reducers') {
    const enhancer = composeEnhancers(
        applyMiddleware(thunk)
    );

    reduxStoreInstance = createStore(createRootReducer(history), initialState, enhancer);

    if (module.hot) {
        module.hot.accept(reducerPath, () => {
            reduxStoreInstance.replaceReducer(createRootReducer(history));
        });
    }

    return reduxStoreInstance;
}

export function getStore () {
    return reduxStoreInstance;
}