import { REQUEST, RECEIVE, FAILURE } from './actionTypes';

export const INITIAL_STATE_LIST = {
    items: [],
    loading: false,
    error: null
};

export const listReducer = (TYPE) => (state = INITIAL_STATE_LIST, action = {}) => {
    switch (action.type) {
        case TYPE + REQUEST:
            return {...INITIAL_STATE_LIST, loading: true};
        case TYPE + FAILURE:
            return {...INITIAL_STATE_LIST, error: action.error};
        case TYPE + RECEIVE:
            return {...INITIAL_STATE_LIST, items: action.items};
        default:
            return state;
    }
};