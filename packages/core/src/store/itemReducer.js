import { REQUEST, RECEIVE, FAILURE } from './actionTypes';

export const INITIAL_STATE_ITEM = {
    item: null,
    loading: false,
    error: null,
};

export const itemReducer = (TYPE) => (
    state = INITIAL_STATE_ITEM, action = {}) => {
    switch (action.type) {
        case TYPE + REQUEST:
            return { ...INITIAL_STATE_ITEM, loading: true };
        case TYPE + FAILURE:
            return { ...INITIAL_STATE_ITEM, error: action.error };
        case TYPE + RECEIVE:
            return { ...INITIAL_STATE_ITEM, item: action.item };
        default:
            return state;
    }
};