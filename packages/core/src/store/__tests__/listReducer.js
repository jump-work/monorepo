import { REQUEST, RECEIVE, FAILURE } from '../actionTypes';
import { INITIAL_STATE_LIST, listReducer } from '../listReducer';

describe('Store > listReducer.js', () => {
    it('должен вернуть начальное состояние', () => {

        const reducer = listReducer('TEST');
        const state = reducer();

        expect(state).toBe(INITIAL_STATE_LIST);
    });

    it('должен установить loading', () => {

        const reducer = listReducer('TEST');
        let state = reducer(INITIAL_STATE_LIST, { type: 'TEST' + REQUEST });
        expect(state.loading).toBeTruthy();

        state = reducer(state, { type: 'TEST' + RECEIVE });
        expect(state.loading).toBeFalsy();
    });

    it('должен установить items', () => {
        const items = [1, 2, 3];
        const reducer = listReducer('TEST');

        let state = reducer(INITIAL_STATE_LIST, { type: 'TEST' + RECEIVE, items });
        expect(state.items).toBe(items);

        state = reducer(state, { type: 'TEST' + FAILURE });
        expect(state.items).toEqual([]);
    });
});