import { REQUEST, RECEIVE, FAILURE } from './actionTypes';

const INITIAL_STATE_LIST_BY_KEY = {
    itemsByKey: {},
    loading: false,
    error: null,
};

export const listReducerByKey = (
    TYPE, enableCash = false) => (
    state = INITIAL_STATE_LIST_BY_KEY, action = {}) => {

    const itemsByKey = enableCash
        ? state.itemsByKey
        : INITIAL_STATE_LIST_BY_KEY.itemsByKey;

    switch (action.type) {
        case TYPE + REQUEST:
            return {
                ...INITIAL_STATE_LIST_BY_KEY,
                loading: true,
                itemsByKey: {
                    ...itemsByKey,
                },
            };
        case TYPE + FAILURE:
            return { ...INITIAL_STATE_LIST_BY_KEY, error: action.error };
        case TYPE + RECEIVE:
            return {
                ...INITIAL_STATE_LIST_BY_KEY,
                itemsByKey: {
                    ...itemsByKey,
                    [action.key]: action.items,
                },
            };
        default:
            return state;
    }
};