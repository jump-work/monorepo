export const FETCH = '_FETCH';
export const REQUEST = '_REQUEST';
export const RECEIVE = '_RECEIVE';
export const FAILURE = '_FAILURE';

export const SUCCESS = '_SUCCESS';
export const ERROR = '_ERROR';

export const CREATE = '_CREATE';
export const DELETE = '_DELETE';
export const UPDATE = '_UPDATE';

export const CREATE_FETCH = CREATE + FETCH;
export const CREATE_REQUEST = CREATE + REQUEST;
export const CREATE_RECEIVE = CREATE + RECEIVE;
export const CREATE_FAILURE = CREATE + FAILURE;

export const DELETE_FETCH = DELETE + FETCH;
export const DELETE_REQUEST = DELETE + REQUEST;
export const DELETE_RECEIVE = DELETE + RECEIVE;
export const DELETE_FAILURE = DELETE + FAILURE;

export const UPDATE_FETCH = UPDATE + FETCH;
export const UPDATE_REQUEST = UPDATE + REQUEST;
export const UPDATE_RECEIVE = UPDATE + RECEIVE;
export const UPDATE_FAILURE = UPDATE + FAILURE;

export const SHOW = '_SHOW';
export const HIDE = '_HIDE';
export const TOGGLE = '_TOGGLE';

export const APP_RUN = '@@app/RUN';
export const APP_LOADING = '@@app/LOADING';
export const APP_AUTHORIZE = '@@app/AUTHORIZE';
export const APP_READY = '@@app/READY';
export const APP_FATAL = '@@app/FATAL';
export const APP_CONFIG = '@@app/CONFIG';
export const APP_DICT = '@@app/DICT';

export const AUTH = '@@auth/AUTH';
export const AUTH_LOGIN = '@@auth/LOGIN';
export const AUTH_LOGOUT = '@@auth/LOGOUT';
export const AUTH_LOGOUT_ALL = '@@auth/LOGOUT_ALL';
export const AUTH_REFRESH = '@@auth/REFRESH';
export const AUTH_RECOVERY = '@@auth/RECOVERY';
export const AUTH_PASSWORD = '@@auth/PASSWORD';
export const AUTH_GET_OTP = '@@auth/GET_OTP';
export const AUTH_RETRY_OTP = '@@auth/RETRY_OTP';
export const AUTH_CONFIRM_OTP = '@@auth/CONFIRM_OTP';