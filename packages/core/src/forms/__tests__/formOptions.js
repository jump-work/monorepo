import { makeOptions, itemToOption } from '../formOptions';

describe('Utils > form-options.js', () => {
    it('makeOptions парсит массив с параметрами по умолчанию', () => {
        const options = makeOptions(
            [{ id: 1, name: 'test 1' }, { id: 2, name: 'test 2' }]);

        expect(options)
            .toEqual(
                [{ value: 1, label: 'test 1' }, { value: 2, label: 'test 2' }]);
    });

    it('makeOptions парсит массив с пользовательскими параметрами', () => {
        const users = [
            { user_id: 1, first_name: 'test 1' },
            { user_id: 2, first_name: 'test 2' }];
        const options = makeOptions(users,
            { value: 'user_id', label: 'first_name' });

        expect(options)
            .toEqual(
                [{ value: 1, label: 'test 1' }, { value: 2, label: 'test 2' }]);
    });

    it('itemToOptions парсит объект с параметрами по умолчанию', () => {
        const users = { id: 1, name: 'test 1' };
        const options = itemToOption(users);

        expect(options).toEqual({ value: 1, label: 'test 1' });
    });

    it('itemToOptions парсит объект с пользовательскими параметрами', () => {
        const users = { user_id: 1, first_name: 'test 1' };
        const config = { value: 'user_id', label: 'first_name' };
        const options = itemToOption(users, config);

        expect(options).toEqual({ value: 1, label: 'test 1' });
    });

});