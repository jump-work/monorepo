import { Dates } from '../utils';

export function ChatIntercom (props) {
    const intercomSettings = {
        ...props.config,
        custom_launcher_selector: props.customLauncher,
        hide_default_launcher: !!props.customLauncher,
        created_at: Dates.formatUnixTime(props.createdAt)
    };

    process.env.NODE_ENV === 'production' && initIntercome(intercomSettings);

    return null;
}

function initIntercome(intercomSettings){
    const w = window;
    const d = document;
    const ic = w.Intercom;

    w.APP_ID = intercomSettings.app_id;
    w.intercomSettings = intercomSettings;

    if(typeof ic==='function'){
        ic('reattach_activator');
        ic('update', w.intercomSettings);
    }else{
        const i = function(){
            i.c(arguments);
        };
        i.q = [];
        i.c = function(args){
            i.q.push(args);
        };
        w.Intercom = i;
        w.Intercom('boot', intercomSettings);

        const l=function() {
            const s = d.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = 'https://widget.intercom.io/widget/' + w.APP_ID;
            const x = d.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        };
        if(document.readyState==='complete'){l();}
        else if(w.attachEvent){w.attachEvent('onload',l);}
        else{w.addEventListener('load',l,false);}
    }
}
