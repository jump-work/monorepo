import { format, parseISO, isValid, getUnixTime } from 'date-fns';
import ru from 'date-fns/locale/ru';

/**
 * Форматирует дату в строку указанного формата
 *
 * @param date string|Date дата
 * @param formatStr string формат
 * @returns {string}
 */
export function formatDate (date, formatStr = 'dd.MM.yy') {
    const dateInstance = date instanceof Date ? date : parseISO(date);
    return format(dateInstance, formatStr, { locale: ru });
}

export function toISO8601 (date) {
    if (date instanceof Date) {
        return date.toISOString();
    }

    const dt = parseISO(date);

    return isValid(dt) ? dt.toISOString() : null;
}

export function formatUnixTime (date) {
    return getUnixTime(parseISO(date));
}

export function getTime (date) {
    const time = new Date(date);
    return format(time, 'HH:mm');
}

export function toISOJoinedDate (date, time) {
    const d = (new Date(date)).getDate();
    const m = (new Date(date)).getMonth();
    const y = (new Date(date)).getFullYear();
    const t = time.split(':');

    const fullTime = t.map(function (item) {
        return parseInt(item);
    });

    return toISO8601(new Date(y, m, d, fullTime[0], fullTime[1]));
}

export function currentTime (timeFormat = 'HH:mm') {
    return format(new Date(), timeFormat);
}

export function currentDate (dateFormat = 'dd.MM.yyyy') {
    return format(new Date(), dateFormat);
}

/**
 * Возвращает форматированную строку при валидной date иначе пустую строку
 *
 * @param date
 * @param dateFormat
 * @returns {string}
 */
export function formatOrEmpty (date, dateFormat = 'dd.MM.yyyy') {
    return date ? formatDate(date, dateFormat) : '';
}