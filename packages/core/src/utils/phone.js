/**
 * Вырезает все символы кроме цифр и + из номера телефона
 *
 * @param phone string
 * @return string
 */
export function phoneToCalling (phone) {
    return phone.replace(/[^+0-9]/g, '');
}