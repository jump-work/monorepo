export function randomString (length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;

    for (let i = 0; i < length; i++) {
        const position = Math.floor(Math.random() * charactersLength);
        result += characters.charAt(position);
    }

    return result;
}

export const uuid = window?.crypto?.getRandomValues
    ? () => {
        return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c => {
            const r = new Uint8Array(1);
            const v = (c ^ crypto.getRandomValues(r)[0] & 15 >> c / 4);
            return v.toString(16);
        });
    }
    : () => {
        const mask = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx';
        return mask.replace(/[xy]/g, c => {
            const r = Math.random() * 16 | 0;
            const v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        });
    };
