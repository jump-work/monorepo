const _toString = Object.prototype.toString;

export function toRawType (val) {
    return _toString.call(val).slice(8, -1);
}

export function isFunction (val) {
    return toRawType(val) === 'Function';
}

export function isPlainObject (obj) {
    return _toString.call(obj) === '[object Object]';
}

export function isObject (obj) {
    return obj !== null && typeof obj === 'object';
}

export function isDef (val) {
    return val !== undefined && val !== null;
}

export function toString (val) {
    const jsonable = Array.isArray(val) ||
        (isPlainObject(val) && val.toString === _toString);

    return val == null
        ? ''
        : jsonable ? JSON.stringify(val, null, 2) : String(val);
}