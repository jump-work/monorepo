import Cookies from 'js-cookie';
import { uuid } from './utils';
import { get, set } from './localStorage';
import { Config } from '../index';

export const deviceStorageKey = 'deviceKey';
export const deviceSessionKey = 'device_key';

export function newDeviceKey () {
    return uuid().toUpperCase();
}

export function getDeviceKey (store, key = null) {
    const map = {
        session: getDeviceKeySession,
        storage: getDeviceKeyStorage,
    };

    if (map[store]) {
        return map[store](key);
    }

    throw new Error('Store type for "Device-Key" not support');
}

export function getOrNewDeviceKey (store, key = null) {
    const map = {
        session: getOrNewDeviceKeySession,
        storage: getOrNewDeviceKeyStorage,
    };

    if (map[store]) {
        return map[store](key);
    }

    throw new Error('Store type for "Device-Key" not support');
}

export function getDeviceKeySession (key = null) {
    const sessionKey = key || deviceSessionKey;

    return Cookies.get(sessionKey);
}

export function getOrNewDeviceKeySession (key = null) {
    const value = getDeviceKeySession(key) || newDeviceKey();

    return storeDeviceKeySession(value, key);
}

export function storeDeviceKeySession (value, key = null) {
    const sessionKey = key || deviceSessionKey;

    Cookies.set(
        sessionKey, value,
        { expires: 365, path: '/', domain: Config.root_domain },
    );

    return value;
}

export function getDeviceKeyStorage (key = null) {
    return get(key || deviceStorageKey);
}

export function getOrNewDeviceKeyStorage (key = null) {
    const value = getDeviceKeyStorage(key) || newDeviceKey();

    return storeDeviceKeyStorage(value, key);
}

export function storeDeviceKeyStorage (value, key = null) {
    const storeKey = key || deviceStorageKey;

    set(storeKey, value);

    return value;
}