import { parse } from 'query-string';

export function isChangePasswordRequest () {
    return getRecoveryCode() && getEmail();
}

export function getRecoveryCode () {
    const search = parse(window.location.search);

    return search?.recovery_code;
}

export function getEmail () {
    const search = parse(window.location.search);

    return search?.email;
}