export function set (key, value) {
    localStorage.setItem(key, JSON.stringify(value));
}

export function get (key, defaultValue = null) {
    let json = localStorage.getItem(key);
    let value = defaultValue;

    if (json) {
        try {
            value = JSON.parse(json);
        } catch (e) { }
    }

    return value;
}
