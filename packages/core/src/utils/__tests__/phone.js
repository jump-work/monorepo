import { phoneToCalling } from '../phone';

describe('Utils > phone.js', () => {
    it('phoneToCalling удаляет лишние символы', () => {
        const phone = '+7 (903) 933 4488';
        const calling = phoneToCalling(phone);

        expect(calling).toEqual('+79039334488');
    });
});