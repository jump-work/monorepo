import {  formatDate, toISO8601, formatOrEmpty } from '../dates';

describe('Utils > dates.js', ()=>{
    it('formatDate должен вернуть дату из строки', () => {

        const tmp = '2020-03-04T05:39:16.875Z';
        const dt = formatDate(tmp);

        expect(dt).toEqual('04.03.20');
    });

    it('formatDate должен вернуть дату из Date', () => {

        const tmp = new Date('2020-03-04T05:39:16.875Z');
        const dt = formatDate(tmp);

        expect(dt).toEqual('04.03.20');
    });

    it('toISO8601 должен вернуть дату из строки', () => {

        const tmp = '2020-03-04T05:39:16.875Z';
        const dt = toISO8601(tmp);

        expect(dt).toEqual(tmp);
    });

    it('toISO8601 должен вернуть дату из new Date', () => {

        const tmp = '2020-03-04T05:39:16.875Z';
        const dt = new Date('2020-03-04T05:39:16.875Z');

        const iso = toISO8601(dt);

        expect(iso).toEqual(tmp);
    });

    it('formatOrEmpty должен вернуть пустую строку', () => {
        const result = formatOrEmpty('');

        expect(result).toStrictEqual('');
    });

    it('formatOrEmpty должен вернуть форматированную дату', () => {
        const result = formatOrEmpty('2020-03-04T05:39:16.875Z');

        expect(result).toEqual('04.03.2020');
    });
});