import { getDeviceKeyStorage, getOrNewDeviceKeyStorage } from '../deviceKey';

describe('Utils > device-key.js', () => {
    it('deviceKey должен иметь длину 36 символа', () => {
        const key = getOrNewDeviceKeyStorage();
        expect(key.length).toEqual(36);
    });

    it('deviceKey должен сохраняться между вызовами', () => {
        const key = getOrNewDeviceKeyStorage();

        const key2 = getDeviceKeyStorage();
        expect(key).toEqual(key2);
    });
});
