'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var Config = window.__CONFIG__ || {
  protocol: 'https://',
  root_domain: 'nosite.ru',
  app_key: 'nosite-app-key'
};

var AUTH_SCREENS = {
  login: 'login',
  recovery: 'recovery',
  recovery_success: 'recovery-success',
  confirm_otp: 'confirm-otp',
  change_password: 'change-password'
};

function ApiError(data) {
  var code = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 500;
  this.responseCode = code;
  this.errorData = data || {
    title: 'Ошибка  на сервере.',
    detail: "\u0412\u043D\u0443\u0442\u0440\u0435\u043D\u043D\u044F\u044F \u043E\u0448\u0438\u0431\u043A\u0430 \u0441\u0435\u0440\u0432\u0435\u0440\u0430. Code #".concat(code, "."),
    event: null,
    fields: null
  };
  this.name = 'ApiError';
  this.message = this.errorData.detail;
  this.stack = new Error().stack;
}
ApiError.prototype = Object.create(Error.prototype);
ApiError.prototype.constructor = ApiError;

function errorToJs(error) {
  if (error.name === 'ApiError') {
    return {
      title: error.errorData.title,
      detail: error.errorData.detail,
      event: error.errorData.event,
      fields: error.errorData.fields,
      name: error.name,
      code: error.responseCode
    };
  }

  return {
    title: 'Ошибка приложения',
    detail: error.message,
    event: null,
    fields: null,
    name: error.name,
    code: 0
  };
}

exports.AUTH_SCREENS = AUTH_SCREENS;
exports.ApiError = ApiError;
exports.Config = Config;
exports.errorToJs = errorToJs;
